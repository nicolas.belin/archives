import TSP_biblio as tsp

def construit_matrice(liste):
    n = len(liste)
    return tuple([tuple([tsp.distance(liste, i, j) for i in range(n)]) for j in range(n)])

def ville_plus_proche(ville_i, liste_i, d_mat):
    d_min = float('inf')
    ville_min = None
    for i in liste_i:
        if i != ville_i:
            d = d_mat[i][ville_i]
            if d < d_min :
                ville_min = i
                d_min = d
    return ville_min

def tour(start_i, liste_i, d_mat):
    restantes = liste_i.copy()
    presente = start_i
    trajet_i = [start_i]
    while(len(restantes) > 1):
        j = ville_plus_proche(presente, restantes, d_mat)
        trajet_i.append(j)
        if presente in restantes:
            del restantes[restantes.index(presente)]
        presente = j
    trajet_i.append(start_i)
    return trajet_i

def longueur_trajet(liste_i):
    l = len(liste_i)
    S = 0.0
    for i in range(l):
        S += d_mat[liste_i[i]][liste_i[(i+1) % l]]
    return S

villes = tsp.get_tour_fichier('exemple.txt')

d_mat = construit_matrice(villes)

trajet_i = tour(0, list(range(len(villes))), d_mat)

trajet = [villes[i] for i in trajet_i]

print("Longueur du trajet :", longueur_trajet(trajet_i))
tsp.trace(trajet)



