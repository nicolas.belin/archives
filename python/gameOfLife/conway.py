"""   5/1/2019

Touches :
q : quitter
f : plein écran
"""
import pygame
import math
import random

class Hi:
    def __init__(self, w, h, caption):
        self.width = w
        self.height = h
        pygame.display.init()
        self.screen = pygame.display.set_mode((w, h), pygame.RESIZABLE)
        pygame.display.set_caption(caption)
        pygame.mouse.set_visible(False)
        self.looping = True
        self.is_fullscreen = False
        self.keys_dict = dict()
        self.user_events_dict = dict()
        self.drawing_funcs = []
        self.register_key('q', self.stop)
        self.register_key('f', self.toggle_fullscreen)

    def stop(self):
        self.post(pygame.QUIT)
        
    def post(self, type, *args):
        pygame.event.post(pygame.event.Event(type, *args))

    def post_user(self, name, args):
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype = name, args = args))

    def refresh(self):
        for f in self.drawing_funcs:
            f(self.screen)
        pygame.display.flip()
        
    def toggle_fullscreen(self):
        if self.is_fullscreen:
            self.is_fullscreen = False
            self.screen = pygame.display.set_mode((self.w_old,self.h_old), pygame.RESIZABLE)
        else:
            self.is_fullscreen = True
            self.w_old, self.h_old = self.screen.get_size()
            self.screen = pygame.display.set_mode(pygame.display.list_modes(0,pygame.FULLSCREEN)[0], pygame.FULLSCREEN)
        self.refresh()
            
    def register_key(self, unicode, func):
        self.keys_dict[unicode] = func

    def register_user_event(self, name, func):
        self.user_events_dict[name] = func

    def register_drawing(self, func):
        self.drawing_funcs.append(func)

    def set_timer(self, func, time):
        self.timer_func = func
        pygame.time.set_timer(pygame.USEREVENT+1, time)
        
    def loop(self):
        while self.looping:
            ev = pygame.event.wait()
            if ev.type == pygame.QUIT:
                self.looping = False
            elif ev.type == pygame.KEYDOWN:
                if ev.unicode in self.keys_dict:
                    self.keys_dict[ev.unicode]()
            elif ev.type == pygame.VIDEORESIZE:
                self.screen = pygame.display.set_mode(ev.size, pygame.RESIZABLE)
                self.refresh()
            elif ev.type == pygame.USEREVENT:
                if ev.utype in self.user_events_dict:
                    self.user_events_dict[ev.utype](*ev.args)
            elif ev.type == pygame.USEREVENT+1:
                self.timer_func()

class Conway:
    def __init__(self, w:int, h:int):
        self.w = w
        self.h = h
        self.cells = [[random.randint(0, 1) for i in range(w)] for j in range(h)]
        
    def display(self, surface):
        x = lambda i: math.floor(i * ws / self.w)
        y = lambda j: math.floor(j * hs / self.h)
        ws, hs = surface.get_size()
        for j in range(self.h):
            for i in range(self.w):
                color = 0x0000FF if self.cells[j][i] == 1 else 0x000444
                rect = (x(i), y(j), x(i+1)-x(i)-1, y(j+1)-y(j)-1)  
                surface.fill(color, rect)

    def iterate(self):
        modw = lambda i: i % self.w
        modh = lambda j: j % self.h
        new = [[0 for i in range(self.w)] for j in range(self.h)]
        ce = self.cells
        for j in range(self.h):
            for i in range(self.w):
                s = ce[modh(j+1)][modw(i+1)]
                s += ce[j][modw(i+1)]
                s += ce[modh(j-1)][modw(i+1)]
                s += ce[modh(j+1)][i]
                s += ce[modh(j-1)][i]
                s += ce[modh(j+1)][modw(i-1)]
                s += ce[j][modw(i-1)]
                s += ce[modh(j-1)][modw(i-1)]
                if ce[j][i] == 0 and s == 3:
                    new[j][i] = 1
                elif ce[j][i] == 1 and (s < 2 or s > 3):
                    new[j][i] = 0
                else:
                    new[j][i] = ce[j][i]
        self.cells = new
                    
hi = Hi(600, 400, 'Game Of Life')

conw = Conway(60, 40)
hi.register_drawing(conw.display)
hi.refresh()

hi.set_timer(lambda: conw.iterate() or hi.refresh(), 2000)

hi.loop()

pygame.quit()

