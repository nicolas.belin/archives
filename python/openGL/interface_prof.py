import math
import pygame
import ctypes
import OpenGL
OpenGL.FULL_LOGGING = True
OpenGL.ERROR_ON_COPY = True
from OpenGL.GL import *

#verb = lambda s: print(s)
verb = lambda s: 0

class Num:
    """Les differents types de nombres (a definir)"""
    list = dict()
    
    def __init__(self, name, gl, c):
        """
        Un type de nombre :
            name : nom du type (string)
            gl : type dans OpenGL
            c : type en ctypes
        """
        self.gl = gl
        self.c = c
        self.list[name] = self

    def type(self, list):
        """Le type pour ctypes de list"""
        return self.c * len(list)
        
    def conv(self, list):
        """Convertit list en ctypes"""
        return (self.type(list))(*list)

class Uniform:
    """Classe encapsulant une uniform"""
    
    def __init__(self, name, type):
        """
            name : nom de l'uniform
            type : type de l'uniform"""
        self.name = name
        self.transpose = GL_FALSE
        self.type = type
        types = {'float': self.vec1fv, 'vec2': self.vec2fv, 'vec3': self.vec3fv,
                 'vec4': self.vec4fv, 'mat2': self.matrix2fv,
                 'mat3': self.matrix3fv, 'mat4': self.matrix4fv}
        if type in types:
            self.activate = types[type]
        else:
            print("Uniform(): type d'uniform inconnu '" + type + "'")
            return False

    def vec1fv(self):
        glUniform1fv(self.loc, 1, self.value)
        
    def vec2fv(self):
        glUniform2fv(self.loc, 1, self.value)
        
    def vec3fv(self):
        glUniform3fv(self.loc, 1, self.value)
        
    def vec4fv(self):
        glUniform4fv(self.loc, 1, self.value)
        
    def matrix2fv(self):
        glUniformMatrix2fv(self.loc, 1, self.transpose, self.value)
        
    def matrix3fv(self):
        glUniformMatrix3fv(self.loc, 1, self.transpose, self.value)
        
    def matrix4fv(self):
        glUniformMatrix4fv(self.loc, 1, self.transpose, self.value)
        
    def is_transposed(self, b): 
        self.transpose = GL_TRUE if b else GL_FALSE

    def conv2float(self, m):
        return Num.list['float'].conv(m)
        
    def set(self, p):
        self.value = self.conv2float(p)
    
class Ortho(Uniform):
    """Uniform implementant la matrice 4x4 de la projection orthogonale sur le plan (Oxy)"""
    
    def __init__(self, name, xmin, xmax, ymin, ymax, zmin, zmax):
        """Projection du cube [xmin, xmax] x [ymin; ymax] x [zmin; zmax]"""
        
        super().__init__(name, 'mat4')
        m = (1.0, 0.0, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.0,
             0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 1.0)
        self.value = self.conv2float(m)
        self.change(xmin, xmax, ymin, ymax, zmin, zmax)

    def change(self, xmin, xmax, ymin, ymax, zmin, zmax):
        """Modifie une projection precedement definie"""
        
        self.value[0] = 2.0 / (xmax - xmin)
        self.value[5] = 2.0 / (ymax - ymin)
        self.value[10] = 2.0 / (zmax - zmin)
        self.value[12] = -(xmax + xmin) / (xmax - xmin)
        self.value[13] = -(ymax + ymin) / (ymax - ymin)
        self.value[14] = -(zmax + zmin) / (zmax - zmin)

class Transform2D(Uniform):
    """Uniform implementant la matrice 4x4 de transformations dans le plan (Oxy)"""

    def __init__(self, name, a = 0.0, s = 1.0, tx = 0.0, ty = 0.0):
        """  Rotation d'angle a, homothetie de parametre s, translation de vecteur (tx, ty)"""
        
        super().__init__(name, 'mat4')
        self.s = 1.0
        m = (1.0, 0.0, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.0,
             0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 1.0)
        self.value = self.conv2float(m)
        self.rotate(a)
        self.scale(s)
        self.translate(tx, ty)

    def rotate(self, a):
        scos = self.s * math.cos(a)
        sin = math.sin(a)
        self.value[0] = scos
        self.value[1] = sin
        self.value[4] = -sin
        self.value[5] = scos

    def scale(self, s):
        r = s / self.s
        self.value[0] *= r
        self.value[5] *= r
        self.value[10] *= r
        self.s = s
        
    def translate(self, tx, ty):
        self.value[12] = tx
        self.value[13] = ty

class Frustum(Uniform):
    """ Projective projection
        Looking along the Oz axis, toward negative numbers
        a : aperture angle
        p : width/height
        e : eye's z
        n : near clipping plane z
        f : far clipping plane z
    """

    def __init__(self, name, a, p, e, n, f):
        super().__init__(name, 'mat4')
        m = (1.0, 0.0, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.0,
             0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 1.0)
        self.value = self.conv2float(m)
        self.change(a, p, e, n, f)

    def change(self, a, p, e, n, f):
        self.p = p
        t = (e - n) * math.sin(a / 2.0)
        r = 1.0 / (t * p)
        n -= e
        f -= e
        self.value[0] = r 
        self.value[5] = 1.0 / t
        self.value[10] = -(f + n) / (n * (f - n))
        self.value[11] = 1.0 / n
        self.value[14] = (f * (n + e) + n * (f + e)) / (n * (f - n))
        self.value[15] = -e / n

    def resize(self, w, h):
        self.value[0] *= self.p * h / w
        self.p = w / h
        
class Transform3D(Uniform):
    """ R( Oy, phi ) -> R( Ox, theta ) -> T( 0, 0, r ) """

    def __init__(self, name, theta = 0.0, phi = 0.0, r = 0.0):
        super().__init__(name, 'mat4')
        m = (1.0, 0.0, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.0,
             0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 1.0)
        self.value = self.conv2float(m)
        self.change(r, theta, phi)
        
    def change(self, theta, phi, r):
        cos_phi = math.cos(phi)
        cos_theta = math.cos(theta)
        sin_phi = math.sin(phi)
        sin_theta = math.sin(theta)
        self.value[0] = cos_phi
        self.value[1] = cos_theta * sin_phi
        self.value[2] = sin_theta * sin_phi
        self.value[4] = -sin_phi
        self.value[5] = cos_theta * cos_phi
        self.value[6] = sin_theta * cos_phi
        self.value[9] = -sin_theta
        self.value[10] = cos_theta
        self.value[14] = r 
        
Uniform.Ortho = Ortho
Uniform.Transform2D = Transform2D
Uniform.Frustum = Frustum
Uniform.Transform3D = Transform3D

class VBO:
    def __init__(self, type, usage = GL_STATIC_DRAW):
        self.id = glGenBuffers(1)
        self.type = type
        self.usage = usage
        self.data = None
        self.attributes = []
        if type == 'indexes':
            self.target = GL_ELEMENT_ARRAY_BUFFER
        elif type == 'attributes':
            self.target = GL_ARRAY_BUFFER
        else:
            print("VBO(): type de buffer inconnu '" + type +"'")
            
    def add_attribute(self, name, dtype, dim):
        if self.type == 'indexes':
            print("VBO.add_attribute('" + name + "') :" +
                  "ce VBO contient des index, pas des attributs")
            return False
        if name in self.attributes:
            print("VBO.add_attribute('" + name + "') :" +
                  "cet attribut est déjà défini")
            return False
        self.attributes.append({'name':name, 'dim':dim, 'dtype':dtype})
        return True

    def load(self, data, itype = 'ushort'):
        self.nr = len(data)

        if self.type == 'attributes':
            struct_fields = []
            first = 0
            for d in self.attributes:
                name = d['name']
                dim = d['dim']
                dtype = d['dtype'] 
                if not dtype in Num.list:
                    print("VBO['" + self.name + "'].load(): " +
                          " type de données '" + dtype + "' inconnu")
                    return False
                dtype = Num.list[dtype]
                d['dtype'] = dtype.gl
                d['first'] = first
                first += ctypes.sizeof(dtype.c) * dim
                struct_fields.append((name, dtype.c * dim))
            self.stride = first
            class data_type(ctypes.Structure):
                _fields_ = struct_fields

        else: # type == 'indexes':
            dtype = Num.list[itype]
            data_type = dtype.c
            self.indexes_type = dtype.gl

        self.data = (data_type * self.nr)(*data)
        return True
    
    def bind(self):
        if self.data:
            glBindBuffer(self.target, self.id)
            glBufferData(self.target, self.data, self.usage)
        else:
            print("VBO.bind(): les données ne sont pas définie")
            return False
        for d in self.attributes:
            glVertexAttribPointer(d['loc'], d['dim'], d['dtype'], GL_FALSE,
                                  self.stride, ctypes.c_voidp(d['first']))
            glEnableVertexAttribArray(d['loc'])
        return True
        

class VAO:
    has_indexes = False
    primitive_type = None
    vtx_nr = 0
    idx_nr = 0
    
    def __init__(self, name, chain, ptype):
        verb("VAO() : " + ptype + " '" + name + "' -> '" + chain.name + "'")
        self.name = name
        self.chain = chain
        self.uniforms = dict()
        self.id = glGenVertexArrays(1)
        types = {'triangles': GL_TRIANGLES,
                 'triangle_fan': GL_TRIANGLE_FAN,
                 'triangle_strip': GL_TRIANGLE_STRIP,
                 'lines': GL_LINES,
                 'line_strip': GL_LINE_STRIP,
                 'line_loop': GL_LINE_LOOP,
                 'points': GL_POINTS}
        if not ptype in types:
            self.primitive_type = None
            print("VAO() : type de primitive inconnu : '" + ptype + "'"  )
        self.primitive_type = types[ptype]

    def bind(self):
        glBindVertexArray(self.id)
        
    def add_vbo(self, vbo):
        if vbo.type == 'indexes':
            self.has_indexes = True
            self.nr = vbo.nr
            self.indexes_type = vbo.indexes_type
        else: # vbo.type == 'attribute':
            for d in vbo.attributes:
                loc = glGetAttribLocation(self.chain.program, d['name'])
                if loc < 0:
                    print("VAO['" + self.name + "'].add_vbo(): l'attribut '" +
                          name + "' n'est pas actif")
                    return False
                d['loc'] = loc

            if not self.vtx_nr:
                self.vtx_nr = vbo.nr
            elif self.vtx_nr != vbo.nr:
                print("VAO['" + self.name + "'].add_vbo(): deux VBOs" +
                      "ont des nombres de points différents")
            if not self.has_indexes:
                self.nr = self.vtx_nr
            
        self.bind()
        vbo.bind()
        return True

    def add_uniform(self, uniform):
        name = uniform.name
        glUseProgram(self.chain.program)
        uniform.loc = glGetUniformLocation(self.chain.program, name)
        if uniform.loc == -1:
            print("VAO.add_uniform(): pas d'uniform de ce nom '" + name + "'")
            return
        self.uniforms[name] = uniform
            
    def draw(self, first = 0, nr = None):
        nr = nr if nr else self.nr
        self.bind()
        for name in self.uniforms:
            self.uniforms[name].activate()
        if self.has_indexes:
            glDrawElements(self.primitive_type, nr, self.indexes_type,
                           ctypes.c_voidp(0))
        else:
            glDrawArrays(self.primitive_type, first, nr, ctypes.c_void_p(0))

class GLchain:
    
    def __init__(self, wgl, d):
        verb("GLchain(): " + d['name']) 
        self.is_ready = False
        self.name = d['name']
        self.wgl = wgl
        self.vaos = dict()
        vertex_shader = self.create_shader(d['vertex_shader_filename'], 'vertex')
        fragment_shader = self.create_shader(d['fragment_shader_filename'], 'fragment')
        if not vertex_shader or not fragment_shader:
            return
        program = glCreateProgram()
        for shader in vertex_shader, fragment_shader:
            glAttachShader(program, shader)
        glLinkProgram(program)
        l = glGetProgramiv(program, GL_INFO_LOG_LENGTH, None)
        if l > 0:
            print('\nLinking :')
            print(glGetProgramInfoLog(program).decode('UTF-8'))
        if glGetProgramiv(program, GL_LINK_STATUS, None) == GL_FALSE:
            return
        self.program = program
        self.is_ready = True
      
    def create_shader(self, filename:str, stype:str):
        """Construit un shader d'un certain type à partir d'un fichier source"""
        verb("  create_shader(): " + stype + " shader <- " + filename)
        o = open(filename, 'r')
        s = o.read()
        o.close()
        if stype == 'vertex':
            t = GL_VERTEX_SHADER
        elif stype == 'fragment':
            t = GL_FRAGMENT_SHADER
        shader = glCreateShader(t)
        glShaderSource(shader, s)
        glCompileShader(shader)
        l = glGetShaderiv(shader, GL_INFO_LOG_LENGTH, None)
        if l > 0:
            print('\nCompilation de ' + filename + ' :')
            print(glGetShaderInfoLog(shader).decode('UTF-8'))
        if glGetShaderiv(shader, GL_COMPILE_STATUS, None) == GL_FALSE:
            return False
        else:
            return shader

    def add_vao(self, vao):
        self.vaos[vao.name] = vao

    def draw(self):
        glUseProgram(self.program)
        for v in self.vaos:
            self.vaos[v].draw()

class Wgl:
    name = 'wgl'

    float = Num('float', GL_FLOAT, ctypes.c_float)            # coordonnées
    ushort = Num('ushort', GL_UNSIGNED_SHORT, ctypes.c_ushort)# index
    ubyte = Num('ubyte', GL_UNSIGNED_BYTE, ctypes.c_ubyte)    # rgb
    
    def __init__(self, interface):
        self.sdl = interface
        self.chains = dict()
        w, h = self.sdl.screen.get_size()
        glViewport(0, 0, w, h)
        self.sdl.register_resizing(self)
        #glDepthFunc(GL_GEQUAL)
        #glEnable(GL_DEPTH_TEST)
        #glClearDepth(-1.0)
        #self.clear_bits = GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT
        glEnable(GL_BLEND)
        glBlendEquation(GL_FUNC_ADD)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        self.clear_bits = GL_COLOR_BUFFER_BIT

    def resize(self, w, h):
        glViewport(0, 0, w, h)
        
    def info(self):
        print("\n* Version : ", glGetString(GL_VERSION).decode('UTF-8'))
        print("* Vendor : ", glGetString(GL_VENDOR).decode('UTF-8'))
        print("* Renderer : ", glGetString(GL_RENDERER).decode('UTF-8'))
        print("* Shading language version : ",
              glGetString(GL_SHADING_LANGUAGE_VERSION).decode('UTF-8'))

    def set_clear_color(self, r, g, b, a = 1.):
        glClearColor(r, g, b, a)

    def add_chain(self, chain):
        self.chains[chain.name] = chain

    def refresh(self):
        glClear(self.clear_bits)
        for name in self.chains:
            self.chains[name].draw()
        
        
class Sdl:
    """ Simple Direct Layer """
    looping = True
    is_fullscreen = False
    call_on_resize = dict()
    call_on_refresh = dict()
    keys_unicode_dict = dict()
    keys_scancode_dict = dict()
    user_events_dict = dict()

    def __init__(self, caption, width = None, height = None, ratio = 1.5):
        pygame.display.init()
        if width == None or height == None :
            w, h = pygame.display.list_modes(0, pygame.FULLSCREEN)[0]
            if ratio >= w / h:
                w = 3 * w // 4
                h = int(w / ratio)
            else:
                h = 3 * h // 4
                w = int(h * ratio)
        else:
            w, h = width, height
        self.width = w
        self.height = h
        self.set_screen(w, h)
        pygame.display.set_caption(caption)
        #pygame.mouse.set_visible(False)
        self.register_key(lambda t: self.stop(), unicode = 'q')
        self.register_key(lambda t: t and self.toggle_fullscreen(), unicode = 'f')
          
    def set_screen(self, w, h):
        flags = pygame.RESIZABLE|pygame.OPENGL|pygame.DOUBLEBUF
        if self.is_fullscreen:
            flags |= pygame.FULLSCREEN
        self.screen = pygame.display.set_mode((w, h), flags)
        
    def stop(self):
        pygame.event.post(pygame.event.Event(pygame.QUIT))

    """    
    def post(self, type):
        pygame.event.post(pygame.event.Event(type))

    def post_user(self, name, args):
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype = name,
                                             args = args))
    """
    def refresh(self):
        for name in self.call_on_refresh:
            self.call_on_refresh[name].refresh()
        pygame.display.flip()
        
    def toggle_fullscreen(self):
        if self.is_fullscreen:
            self.is_fullscreen = False
            ev = pygame.event.Event(pygame.VIDEORESIZE)
            ev.size = (self.w_old, self.h_old)
            pygame.event.post(ev)
        else:
            self.is_fullscreen = True
            self.w_old, self.h_old = self.screen.get_size()
            w, h = pygame.display.list_modes(0, pygame.FULLSCREEN)[0]
            ev = pygame.event.Event(pygame.VIDEORESIZE)
            ev.size = (w, h)
            pygame.event.post(ev)
            
    def register_key(self, func, unicode = None, scancode = None):
        if unicode != None:
            self.keys_unicode_dict[unicode] = func
        elif scancode != None:
            self.keys_scancode_dict[scancode] = func
        else:
            print("Sdl.register_key(): need a scancode or a unicode")
            return False
        return True
        
    def register_user_event(self, name, func):
        self.user_events_dict[name] = func

    def register_drawing(self, obj):
        self.call_on_refresh[obj.name] = obj

    def set_timer(self, func, time):
        self.timer_func = func
        pygame.time.set_timer(pygame.USEREVENT+1, time)

    def get_time(self):
        return pygame.time.get_ticks()
        
    def register_resizing(self, obj):
        self.call_on_resize[obj.name] = obj

    def on_resize(self, w, h):
        self.set_screen(w,h)
        self.width = w
        self.height = h
        for name in self.call_on_resize:
            self.call_on_resize[name].resize(w, h)
        self.refresh()
            
    def loop(self):
        self.t0 = pygame.time.get_ticks()
        while self.looping:
            ev = pygame.event.wait()
            if ev.type == pygame.QUIT:
                self.looping = False
            elif ev.type == pygame.KEYDOWN:
                #print('\n' + ev.unicode + ' ' + str(ev.scancode) + ' ' + str(ev.key))
                if ev.unicode in self.keys_unicode_dict:
                    func = self.keys_unicode_dict[ev.unicode]
                    self.register_key(func, scancode = ev.scancode)
                    func(True)
                elif ev.scancode in self.keys_scancode_dict:
                    self.keys_scancode_dict[ev.scancode](True)
            elif ev.type == pygame.KEYUP:
                if ev.scancode in self.keys_scancode_dict:
                    self.keys_scancode_dict[ev.scancode](False)
            elif ev.type == pygame.VIDEORESIZE:
                self.on_resize(*ev.size)
            elif ev.type == pygame.USEREVENT:
                if ev.utype in self.user_events_dict:
                    self.user_events_dict[ev.utype](*ev.args)
            elif ev.type == pygame.USEREVENT+1:
                t1 = self.t0
                self.t0 = pygame.time.get_ticks()
                self.timer_func(self.t0, t1)

    def quit(self):
        pygame.quit()
        exit()

def test():
    sdl = Sdl('OpenGL', ratio = 1.0)
    wgl = Wgl(sdl)

    chain_d = {'name':'bare',
               'vertex_shader_filename':'shader.vertex.c',
               'fragment_shader_filename':'shader.fragment.c'}
    chain = GLchain(wgl, chain_d)
    if not chain.is_ready:
        sdl.quit()
    wgl.add_chain(chain)
    sdl.register_drawing(wgl)
    wgl.info()

    wgl.set_clear_color(0.0, 0.0, 0.0)

    hexagon = (# ((x, y), (r, g, b, a))
        ((0.0, 0.0), (0, 0, 0, 255)),
        ((1.0, 0.0), (255, 0, 0, 255)),
        ((0.5, 0.866), (255, 255, 0, 255)),
        ((-0.5, 0.866), (0, 255, 0, 255)),
        ((-1.0, 0.0), (0, 255, 255, 255)),
        ((-0.5, -0.866), (0, 0, 255, 255)),
        ((0.5, -0.866), (255, 0, 255, 255)))

    hex_a = VBO('attributes')
    hex_a.add_attribute('a_position', 'float', 2)
    hex_a.add_attribute('a_color', 'ubyte', 4)
    hex_a.load(hexagon)
    
    hex_i = VBO('indexes')
    hex_i.load((0, 1, 2, 3, 4, 5, 6, 1), 'ushort')
    
    hex_vao = VAO('fan', chain, 'triangle_fan')
    hex_vao.add_vbo(hex_a)
    hex_vao.add_vbo(hex_i)
    
    ortho_u = Uniform.Ortho('u_projection', -1.2, 1.2, -1.2, 1.2, -1.0, 1.0)
    hex_vao.add_uniform(ortho_u)

    transform2D_u = Uniform.Transform2D('u_transform')
    hex_vao.add_uniform(transform2D_u)

    chain.add_vao(hex_vao)

    vertices = (((1.0, 1.0, 1.0), (200, 200, 200, 255)),# 0
        ((-1.0, 1.0, 1.0), (200, 200, 200, 255)),  # 1
        ((-1.0, -1.0, 1.0), (200, 200, 200, 255)), # 2
        ((1.0, -1.0, 1.0), (200, 200, 200, 255)),  # 3
        ((1.0, 1.0, -1.0), (200, 200, 200, 255)),  # 4
        ((-1.0, 1.0, -1.0), (200, 200, 200, 255)), # 5
        ((-1.0, -1.0, -1.0), (200, 200, 200, 255)),# 6
        ((1.0, -1.0, -1.0), (200, 200, 200, 255))) # 7
    vtc_a = VBO('attributes')
    vtc_a.add_attribute('a_position', 'float', 3)
    vtc_a.add_attribute('a_color', 'ubyte', 4)
    vtc_a.load(vertices)

    cube = (0, 1, 0, 3, 1, 2, 2, 3,
            4, 5, 4, 7, 5, 6, 6, 7,
            0, 4, 1, 5, 2, 6, 3, 7)
    vtc_i = VBO('indexes')
    vtc_i.load(cube)
    
    cube_vao = VAO('lines', chain, 'lines')
    cube_vao.add_vbo(vtc_a)
    cube_vao.add_vbo(vtc_i)

    frustum_u = Uniform.Frustum('u_projection', 1.74, 1.0, 3.0, 2.0, -2.0)
    cube_vao.add_uniform(frustum_u)
    sdl.register_resizing(frustum_u)
    
    transform3D_u = Uniform.Transform3D('u_transform', theta = 0.0, phi = 0.0)
    cube_vao.add_uniform(transform3D_u)
    
    chain.add_vao(cube_vao)

    wgl.add_chain(chain)
    sdl.register_drawing(wgl)
    
    angle = 0.0
    def move(t0, t1):
        nonlocal angle
        angle += 0.0001 * (t1 - t0)
        if angle >= 6.2832:
            angle -= 6.2832
        transform2D_u.scale(1.3 + math.sin(angle))
        transform2D_u.rotate(angle)
        transform3D_u.change(angle * 2.0, angle * 3.0, 0.0)
        sdl.refresh()
        
    sdl.set_timer(move, 1000 // 30)
        
    sdl.loop()
    pygame.quit()

#test()
if __name__ == '__main__':
    test()
