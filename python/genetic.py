import random

nucleotides = ('A', 'C', 'G', 'T')

ADN2ARN = {'A':'U', 'T':'A', 'G':'C', 'C':'G'}
ARN2ADN = {'U':'A', 'A':'T', 'C':'G', 'G':'C'}

code = {('UUU', 'UUC'): 'F',
        ('UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG'): 'L',
        ('AUU', 'AUC', 'AUA'): 'I',
        ('AUG'): 'M',
        ('GUU', 'GUC', 'GUA', 'GUG'): 'V',
        ('UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'): 'S',
        ('CCU', 'CCC', 'CCA', 'CCG'): 'P',
        ('ACU', 'ACC', 'ACA', 'ACG'): 'T',
        ('GCU', 'GCC', 'GCA', 'GCG'): 'A',
        ('UAU', 'UAC'): 'Y',
        ('UAA', 'UAG', 'UGA'): '*',
        ('CAU', 'CAC'): 'H',
        ('CAA', 'CAG'): 'Q',
        ('AAU', 'AAC'): 'N',
        ('AAA', 'AAG'): 'K',
        ('GAU', 'GAC'): 'D',
        ('GAA', 'GAG'): 'E',
        ('UGU', 'UGC'): 'C',
        ('UGG'): 'W',
        ('CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'): 'R',
        ('GGU', 'GGC', 'GGA', 'GGG'): 'G'}

def estADN(s):
    """Teste si la chaine de caractere s est un brin d'ADN"""
    if len(s) == 0:
        return True
    for c in s:
        if not c in nucleotides:
            return False
    return True

def genereADN(n):
    s = ""
    for i in range(n):
        s += nucleotides[random.randint(0, len(nucleotides)-1)]
    return s

def baseComplementaire(c, type):
    if type == 'ADN':
        d = ADN2ARN
    else:
        d = ARN2ADN
    return d[c]

def transcrit(s, n, m):
    s = s[n-1:m]
    t = ""
    for c in s:
        t += baseComplementaire(c, 'ADN')
    return t

def codeGenetique(codon):
    liste = code.keys()
    for c in liste:
        if codon in c:
            return code[c]
    return False

def replique(s):
    t = ''
    for c in s:
        t = baseComplementaire(c, 'ADN') + t
    return t



        