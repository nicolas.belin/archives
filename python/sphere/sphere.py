import math
from interface_prof import Sdl, Wgl, GLchain, VAO, VBO, Uniform

class Vertex:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.lk = []
        self.color = (200, 200, 200, 255)
        
class Sphere:
    def __init__(self, layerNr):
        self.layerNr = layerNr
        a = self.make()
        self.vertices = a[0]
        for i in range(1, len(a)):
            self.vertices += a[i]
        
    def mod5i(self, m, i):
        if m < 0:
            m += 5*i
        return m % (5 * i)

    def makeHemi(self):
        layerNr = self.layerNr
        mod5i = self.mod5i
        
        # construit une demi-sphere à layerNr couche
        vtxu = [[] for i in range(layerNr)]
    
        # point au sommet : centre du pentagone
        vtxu[0].append(Vertex(0.0, 0.0, 1.0))
  
        # les autres points : hexagones

        # coordonnées
        for i in range(1, layerNr):
            for j in range(5):
                for k in range(i):
                    m = i*j + k
                    vtxu[i].append(Vertex(
                        math.cos(math.pi / 2.0 * (1 - i / layerNr)) *
                        math.cos(2.0 * math.pi * m / (5 * i)),
                        math.cos(math.pi / 2.0 * (1.0 - i / layerNr)) *
                        math.sin(2.0 * math.pi * m / (5 * i)),
                        math.sin(math.pi / 2.0 * (1.0 - i / layerNr))))
    
        # arrêtes
        for j in range(5):
            vtxu[0][0].lk.append(vtxu[1][j])

            for i in range(1, layerNr):
                for j in range(5):
                    for k in range(i):
                        m = i*j + k;
                        vtxu[i][m].lk.append(vtxu[i][mod5i(m+1, i)])
                        if k == 0:
                            vtxu[i][m].lk.append(vtxu[i-1][(i-1) * j])
                            vtxu[i][m].lk.append(vtxu[i][mod5i(m-1, i)])
                            if i != layerNr-1:
                                vtxu[i][m].lk.append(
                                    vtxu[i+1][mod5i((i+1) * j - 1, i+1)])
                                vtxu[i][m].lk.append(
                                    vtxu[i + 1][mod5i((i+1) * j, i+1)])
                                vtxu[i][m].lk.append(
                                    vtxu[i+1][mod5i((i+1) * j + 1, i+1)])
                        else:
                            vtxu[i][m].lk.append(
                                vtxu[i-1][mod5i((i-1) * j + k, i-1)])
                            vtxu[i][m].lk.append(
                                vtxu[i-1][mod5i((i-1) * j + k - 1, i-1)])
                            vtxu[i][m].lk.append(vtxu[i][mod5i(m-1, i)])
                            if i != layerNr-1: 
                                vtxu[i][m].lk.append(
                                    vtxu[i+1][mod5i((i+1) * j + k, i+1)])
                                vtxu[i][m].lk.append(
                                    vtxu[i+1][mod5i((i+1) * j + k + 1, i+1)])
        return vtxu

    def make(self):
        mod5i = self.mod5i

        # demi-sphère supérieure
        up = self.makeHemi()

        # demi-sphere inférieure
        down = self.makeHemi()

        # symétrie par rapport au plan équatorial
        for a in down:
            for vtx in a:
                vtx.z *= -1 

        # la symétrie précédente à inverser l'orientation des polygônes
        for a in down:
            for vtx in a:
                l = len(vtx.lk)
                for j in range(math.floor(l / 2)):
                    vtx.lk[j], vtx.lk[l-j-1] = vtx.lk[l-j-1], vtx.lk[j]

        # équateur
        equa = []
        l = self.layerNr
        for j in range(5):
            for k in range(l):
                i = l*j + k
                equa.append(
                    Vertex(math.cos(2 * math.pi * i / (5*l)),
                           math.sin(2 * math.pi * i / (5*l)), 0.0))
    
        # collage des deux demi-sphères 
        for j in range(5):
            for k in range(l):
                i = l*j + k
                equa[i].lk.append(equa[mod5i(i+1, l)])
                if  k == 0:
                    equa[i].lk.append(up[l-1][(l-1) * j])
                    equa[i].lk.append(equa[mod5i(i-1, l)])
                    equa[i].lk.append(down[l-1][(l-1) * j])
                else:
                    equa[i].lk.append(up[l-1][mod5i((l-1) * j + k, l-1)])
                    equa[i].lk.append(up[l-1][mod5i((l-1) * j + k - 1, l-1)])
                    equa[i].lk.append(equa[ mod5i(i-1, l)])
                    equa[i].lk.append(down[l-1][mod5i((l-1) * j + k - 1, l-1)])
                    equa[i].lk.append(down[l-1][mod5i((l-1) * j + k, l-1)])
    
        l = self.layerNr - 1
        for j in range(5):
            for k in range(l):
                i = l*j + k
                if k == 0:
                    up[l][i].lk.append(equa[mod5i((l+1) * j - 1, l+1)])
                    down[l][i].lk.append(equa[mod5i((l+1) * j - 1, l+1)])
                    up[l][i].lk.append(equa[mod5i((l+1) * j, l+1)])
                    down[l][i].lk.append(equa[mod5i((l+1)*j, l+1)])
                    up[l][i].lk.append(equa[mod5i((l+1) * j + 1, l+1)])
                    down[l][i].lk.append(equa[mod5i((l+1) * j + 1, l+1)])
                else:
                    up[l][i].lk.append(equa[mod5i((l+1) * j + k, l+1)])
                    down[l][i].lk.append(equa[mod5i((l+1) * j + k, l+1)])
                    up[l][i].lk.append(equa[mod5i((l+1) * j + k + 1, l+1)])
                    down[l][i].lk.append(equa[mod5i((l+1) * j + k + 1, l+1)])

        return up + [equa] + down

    def get_data(self):
        return [((vtx.x, vtx.y, vtx.z), vtx.color) for vtx in self.vertices]

    def get_indexes(self):
        indexes = []
        for i in range(len(self.vertices)):
            self.vertices[i].index = i
        for vtx1 in self.vertices:
            for vtx2 in vtx1.lk:
                if vtx1.index > vtx2.index:
                    indexes.append(vtx1.index)
                    indexes.append(vtx2.index)
        return indexes
    
def glide(vtx, f):
    dl = 0.001;
    x, y, z = vtx.x, vtx.y, vtx.z
    while True:
        fxyz = f.p(x, y, z)
        dfx = f.x(x, y, z)
        dfy = f.y(x, y, z)
        dfz = f.z(x, y, z)
        sq = dfx * dfx + dfy * dfy + dfz * dfz
        l = math.sqrt(sq)
        dx = -fxyz * dfx * dl / sq
        dy = -fxyz * dfy * dl / sq
        dz = -fxyz * dfz * dl / sq
        x += dx
        y += dy
        z += dz
        if abs(fxyz) / l < 0.05:
            break
        
    #v.nx = dfx / l
    #v.ny = dfy / l
    #v.nz = dfz / l
    vtx.x, vtx.y, vtx.z = x + dx, y + dy, z + dz

    
sdl = Sdl('OpenGL', ratio = 1.0)
wgl = Wgl(sdl)

chain_d = {'name':'bare',
           'vertex_shader_filename':'shader.vertex.c',
           'fragment_shader_filename':'shader.fragment.c'}
chain = GLchain(wgl, chain_d)
if not chain.is_ready:
    sdl.quit()
wgl.add_chain(chain)
sdl.register_drawing(wgl)
#wgl.info()

wgl.set_clear_color(0.0, 0.0, 0.0)

sphere = Sphere(10)

a = 1.2
f = lambda: None
f.p = lambda x, y, z: math.pow(math.fabs(x), a) + math.pow(math.fabs(y), a) + math.pow(math.fabs(z), a) - 1.0
f.x = lambda x, y, z: math.copysign(a, x) * math.pow(math.fabs(x), a - 1.0)
f.y = lambda x, y, z: math.copysign(a, y) * math.pow(math.fabs(y), a - 1.0)
f.z = lambda x, y, z: math.copysign(a, z) * math.pow(math.fabs(z), a - 1.0)
for vtx in sphere.vertices:
    glide(vtx, f)


vtc_a = VBO('attributes')
vtc_a.add_attribute('a_position', 'float', 3)
vtc_a.add_attribute('a_color', 'ubyte', 4)
vtc_a.load(sphere.get_data())

vtc_i = VBO('indexes')
vtc_i.load(sphere.get_indexes())

cube_vao = VAO('sphere', chain, 'lines')
cube_vao.add_vbo(vtc_a)
cube_vao.add_vbo(vtc_i)

frustum_u = Uniform.Frustum('u_projection', 1.74, 1.0, 2.0, 1.5, -2.0)
cube_vao.add_uniform(frustum_u)
sdl.register_resizing(frustum_u)
    
transform3D_u = Uniform.Transform3D('u_transform', theta = 0.0, phi = 0.0)
cube_vao.add_uniform(transform3D_u)
    
chain.add_vao(cube_vao)

wgl.add_chain(chain)
sdl.register_drawing(wgl)
    
angle = 0.0
def move(t0, t1):
    global angle
    angle += 0.0001 * (t1 - t0)
    if angle >= 6.2832:
        angle -= 6.2832
    transform3D_u.change(angle * 2.0, angle * 3.0, 0.0)
    sdl.refresh()
        
sdl.set_timer(move, 1000 // 30)
        
sdl.loop()
sdl.quit()
