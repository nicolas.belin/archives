import numpy as np
import pyglet

def render(w, h):
    xmin = -2.1
    ymin = -1.5
    xmax = 0.9
    ymax = 1.5
    nmax = 500
    x = np.linspace(xmin, xmax, w)
    y = np.linspace(ymin, ymax, h).reshape(h,1)
    c = (x+y*1j).reshape(h*w)
    z = np.zeros(c.shape, dtype=complex)
    r = np.zeros(c.shape, dtype=int)
    d = (r == 0)
    for i in range(nmax):
        z[d] = z[d]**2 + c[d]
        nd = ( z*np.conj(z) > 4 ) & d
        r[nd] = i
        d = ~nd & d
    
    rgb = np.array([ 1, 5, 10 ], dtype=int)
    r.resize(w*h, 1)
    rgb = rgb * r
    rgb.resize(w*h*3)
    return rgb % 256
 
w = 600
h = 600

data = bytes(tuple(render(w, h)))

window = pyglet.window.Window(w, h)
fmt = 'RGB'
image = pyglet.image.ImageData(w, h, fmt, data, w*len(fmt))

@window.event
def on_draw():
    window.clear()
    image.blit(0, 0)
    
pyglet.app.run()
