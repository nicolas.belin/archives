import Time
import Competitor

NOM_CVS = "../data/small_inscrits.csv"
PERF_CVS = "../data/small_performances.csv"

def read_competitors(filename):
    cvs = open(filename, 'rt').read()
    liste_str = cvs.split()[2:]
    liste_tuple = [string.split(sep=';') for string in liste_str]

    competitors_d = dict()
    nr = 1
    for comp in liste_tuple:
        competitors_d[str(nr)] = Competitor.create(*comp, nr)
        nr += 1
    return competitors_d

def display(liste):
    s = ''
    for competitor in liste:
        s += Competitor.to_string(competitor) + '\n'
    print(s)
    
def print_results(comp_d, sort_comp):
    liste = sort_comp(comp_d)
    display(liste)
    
def select_competitor_by_bib(d, bib):
    """
    :param d: (dict) un dictionnaire contenant les competiteurs
    :param bib: (int) un numero d'inscription
    :returns: le competiteur dont le numero est 'bib'
    """
    key = str(bib)
    try:
        c = d[key]
    except KeyError:
        print(f"Aucun competiteur ne correspond au numero d'inscription {key} !")
        return None
    else:
        return c

def select_competitor_by_birth_year(d, birth_year):
    """
    :param d: (dict) un dictionnaire contenant les competiteurs
    :param birth_year: (int) une annee de naissance
    :returns: les competiteurs dont l'annee de naissance est 'birth_year'
    """
    birth_year_s = str(birth_year)
    return [c for c in d.values() if c['birth_date'].split('/')[2] == birth_year_s]

def read_performances(filename):
    cvs = open(filename, 'rt').read()
    liste_str = cvs.split()[1:]
    d = dict()
    
    for p in liste_str:
        line = [int(s) for s in p.split(';')]
        d[str(line[0])] = Time.create(*line[1:])
    return d


def tri(l, comp):
    """
    Tri par insertion
    ion
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
            l[k] = tmp

def sort_competitors_by_lastname(competitors_d):
    liste = list(competitors_d.values())
    tri(liste, Competitor.compare_lastname)
    return liste

def compare_perfs(comp1, comp2):
    if comp1['performance'] == None:
        return 1
    elif comp2['performance'] == None:
        return -1
    else:
        return Time.compare(comp1['performance'], comp2['performance'])

def sort_competitors_by_performance(competitors_d):
    liste = list(competitors_d.values())
    tri(liste, compare_perfs)
    return liste

def save_results(comp_d, filename):
    s = 'Num_dossard;Prénom;Nom;Performance'
    for c in comp_d.values():
        s += '\n'
        p = c['performance']
        if p == None:
            ps = 'Aucune'
        else:
            ps = Time.to_string(p)
        line = "{bib_num};{first_name};{last_name}".format(**c)
        line += f";{ps}"
        s += line
    file = open(filename, 'wt')
    print(s, file = file)

if __name__ == '__main__':
    
    competitors_d = read_competitors(NOM_CVS)
    perf_d = read_performances(PERF_CVS)
    Competitor.set_performance(competitors_d, perf_d)
    
    save_results(competitors_d, 'save.csv')
    
    #print_results(competitors_d, sort_competitors_by_performance)
    #print_results(competitors_d, sort_competitors_by_lastname)
    #liste = sort_competitors_by_performance(competitors_d)
    #display(liste)
    #print(sort_competitors_by_performance(perf_d))
    
    