import Competitor
from collections import namedtuple

Time = namedtuple('Time', ('hours', 'minutes', 'seconds'))

def create(hours, minutes, seconds):
    """
    Cree un tupple contenant le temps du coureur.
    :param hours: (int) heure
    :param minutes: (int) minutes
    :param seconds: (float) secondes
    """
    return Time(hours=hours, minutes=minutes, seconds=seconds)

def compare(time1, time2):
    """
    Compare les deux temps.
    :param time1, time2: (Time) les temps de deux coureurs
    :returns: 1 si time1 > time2 et -1 sinon
    """
    lin = lambda time: time.hours * 60 * 60 + time.minutes * 60 + time.seconds
    l1  = lin(time1)
    l2 = lin(time2)
    
    if l1 > l2:
        return 1
    elif l1 < l2:
        return -1
    else:
        return 0
    
def to_string(time):
    """
    Formate time
    :param time: (Time) un temps d'un coureur
    :returns: (str)
    """
    return f"{time.hours}h{time.minutes}m{time.seconds}s"

if __name__ == '__main__':
    time1 = create(3, 5, 12.4)
    time2 = create(3, 5, 10)
    print(to_string(time1))
    print(to_string(time2))
    
    print('Compare les temps : {}'.format(compare(time2, time1)))
