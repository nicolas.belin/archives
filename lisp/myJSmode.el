
(defun my-JS-mode ()
  (progn
    (hs-minor-mode)
    (define-key js-mode-map "\C-ct" 'hs-toggle-hiding)
    )
  )

(add-hook 'js-mode-hook 'my-JS-mode)
