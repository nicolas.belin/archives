(defun my-end-env ()
  "Insère le \end{} correspondant au dernier \begin{}."
  (interactive)
  (condition-case nil
	(let ((n 1))
	  (save-excursion
	    (while (> n 0)
		(re-search-backward "\\\\end\\|\\\\begin")
		(if (char-equal ?b (char-after (+ 1 (point))))
		    (setq n (- n 1))
		  (setq n (+ n 1))))
	    (search-forward "{")
	    (copy-region-as-kill (- (point) 1) (progn (search-forward "}") (point))))
	  (princ "\\end" (current-buffer))
	  (yank))
    (error (message "Pas de \\begin{} à compléter"))))

;-----------------------------------------------------------------

(defun my-start-ghostview ()
  "Launch ghostview"
  (interactive)
  (let (name)
    (setq name (my-delete-suffix (buffer-name) ".tex"))
    (setq name (concat name ".ps"))
    (start-process "gv-process" nil "gv" name)))

;-------------------------------------------------------------------

(defun my-get-dvips-options ()
  "Get dvips options"
  (interactive)
  (let ((options "") lower-bound upper-bound)
    (save-excursion
      (beginning-of-buffer)
      (search-forward "\\documentclass")
      (if (not (char-equal ?\[ (char-after (point))))
          (message "Pas d'options")
        (setq lower-bound (point))
        (search-forward "\]")
        (setq upper-bound (point))
        (goto-char lower-bound)
        (if (search-forward "a4paper" upper-bound t)
            (setq options (concat options "-t a4 ")))
        (goto-char lower-bound)
        (if (search-forward "a5paper" upper-bound t)
            (setq options (concat options "-t a5 ")))
        (goto-char lower-bound)
        (if (search-forward "landscape" upper-bound t)
            (setq options (concat options "-t landscape ")))))
    options))

(defun my-check-errors (output)
  (interactive)
  (let ((l-output (length output)))
    (if (string= "FinishedOK" (substring output (- l-output 11) (- l-output 1)))
        (message "Document has been successfully compiled")
      (split-window-vertically)
      (other-window 1)
      (switch-to-buffer "my-latex-error-buffer")
      (erase-buffer)
      (insert output))))

(defun my-LaTeX-build ()
  "Run LaTeX and then dvips on the current buffer"
  (interactive)
  (save-buffer)
  (let (command output)
    (setq command (concat "latex " (buffer-file-name)))
    (setq command (concat command " && dvips " (my-get-dvips-options)
                          (my-delete-suffix (buffer-file-name) ".tex") ".dvi"))
    ;(setq command (concat command " && dvips " (my-delete-suffix (buffer-file-name) ".tex") ".dvi"))
    (setq command (concat command " && echo \"FinishedOK\""))
    (my-check-errors (shell-command-to-string command))))

;--------------------------------------------------------------

(defun my-math-copy ()
  "Copy the last math expression"
  (interactive)
  (let (left current expr)
    (save-excursion
      (if (eq last-command 'my-math-copy)
          (progn
            (setq current (nth 0 my-math-copy-data))
            (delete-region current (point))
            (goto-char (nth 1 my-math-copy-data)))
        (setq current (point)))
      (re-search-forward "" nil t)
      (re-search-backward "\\(\[^\\$\]\\$\[^\$\]\\$\[^\$\]\\)\\|\\(\[^\\$\]\\$\[^\$\].*\[^\\$\]\\$\[^\$\]\\)\\|\\(\\\\\\[.+\\\\]\\)\\|\\(\\$\\$.+\\$\\$\\)\\|\\(\\\\(.+\\\\)\\)" nil t)
      (setq expr (match-string 0))
      (if (string-equal expr "")
          (setq left current)
        (setq left (point))
        (if (string-match "^[^\\$]" expr)
            (setq expr (substring expr 1 (- (length expr) 1)))
          )
        ))
    (insert expr)
    (setq my-math-copy-data (list current left))
    (setq this-command 'my-math-copy)))

;--------------------------------------------------------

(defun my-add-latex-keys ()
  (define-key latex-mode-map "\C-ce" 'my-end-env)
  (define-key latex-mode-map "\C-cv" 'my-start-ghostview)
  (define-key latex-mode-map "\C-cc" 'my-LaTeX-build)
  (define-key latex-mode-map "\C-cp" 'my-math-copy))

(add-hook 'latex-mode-hook 'my-add-latex-keys)
