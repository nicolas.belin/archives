; simple $ : "\\(\[^\\$\]\\$\[^\\$\].+\[^\\$\]\\$\[^\\$\]\\)"
; double $ : "\\(\\$\\$.+\\$\\$\\)"
; \( \) : "\\(\\\\(.+\\\\)\\)"
; \[ \] : "\\(\\\\\\[.+\\\\]\\)"

(defun my-math-copy ()
  "Copy the last math expression"
  (interactive)
  (let (left current expr)
    (save-excursion
      (if (eq last-command 'my-math-copy)
          (progn
            (setq current (nth 0 my-math-copy-data))
            (delete-region current (point))
            (goto-char (nth 1 my-math-copy-data)))
        (setq current (point)))
      (re-search-forward "" nil t)
      (re-search-backward "\\(\[^\\$\]\\$\[^\$\]\\$\[^\$\]\\)\\|\\(\[^\\$\]\\$\[^\$\].*\[^\\$\]\\$\[^\$\]\\)\\|\\(\\\\\\[.+\\\\]\\)\\|\\(\\$\\$.+\\$\\$\\)\\|\\(\\\\(.+\\\\)\\)" nil t)
      (setq expr (match-string 0))
      (if (string-equal expr "")
          (setq left current)
        (setq left (point))
        (if (string-match "^[^\\$]" expr)
            (setq expr (substring expr 1 (- (length expr) 1)))
          )
        ))
    (insert expr)
    (setq my-math-copy-data (list current left))
    (setq this-command 'my-math-copy)))

(global-set-key (kbd "`") 'my-math-copy)

;-----------------------------------------------------------

(defun my-word-completion ()
  "Complete the current word"
  (interactive)
  (let (the-prefix start-word-left start-word-right found-word-left str)
    (setq start-word-right (point))
    (re-search-backward "\\_<")
    (if (eq last-command 'my-word-completion)
        (setq the-prefix (car my-word-completion-data))
      (setq the-prefix (buffer-substring (point) start-word-right)))
    (setq start-word-left (point))
    (save-excursion
      (if (eq last-command 'my-word-completion)
          (goto-char (nth 1 my-word-completion-data)))
      (if (setq found-word-left (re-search-backward (concat "\\_<" the-prefix) nil t))
          (progn
            (setq str
                  (buffer-substring found-word-left (re-search-forward "\\_>")))
            (goto-char found-word-left))
        (goto-char start-word-left)
        (setq str the-prefix))
      (setq my-word-completion-data (list the-prefix (point))))
    (delete-region start-word-left start-word-right)
    (insert str))
  (setq this-command 'my-word-completion))

(global-set-key [print] 'my-word-completion)
