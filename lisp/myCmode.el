
(defun my-C-build ()
  "Run gcc on the current buffer"
  (interactive)
  (save-buffer)
  (let (command output)
    (setq command "make && ")
    (setq command (concat command (my-delete-suffix (buffer-file-name) ".c")))
    (setq output (shell-command-to-string command))
    (split-window-vertically)
    (other-window 1)
    (switch-to-buffer "my-buffer")
    (erase-buffer)
    (insert output)
))

;--------------------------------------------------------

(defun my-add-c-keys ()
  (define-key c-mode-map "\C-cc" 'my-C-build))

(add-hook 'c-mode-hook 'my-add-c-keys)

