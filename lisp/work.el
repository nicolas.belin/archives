;------------------------------------------------------------------
(defun my-word-completion ()
  "Complete the current word"
  (interactive)
  (let (prefix word prefix-point last-point)
    (setq last-point (point))
    (if (eq last-command 'my-word-completion)
        (setq prefix (car my-word-completion-data))
      (setq prefix (current-word)))
    (search-backward prefix)
    (setq prefix-point (point))
    (save-excursion
      (if (eq last-command 'my-word-completion)
          (goto-char (nth 1 my-word-completion-data)))
      (if (re-search-backward (concat "[^[:word:]]" prefix) nil t)
          (setq word (progn (forward-char) (current-word)))
        (goto-char prefix-point)
        (setq word prefix))
      (setq my-word-completion-data (list prefix (point))))
      (delete-region prefix-point last-point)
      (insert word))
  (setq this-command 'my-word-completion))

(global-set-key [print] 'my-word-completion)


(buffer-name)


(defun my-delete-suffix (s suffix)
  "If current buffer filename has suffix SUFFIX, give this filename without it" 
  (let (l l-suffix)
    (setq l-suffix (length suffix))
    (setq l (length s))
;    (if (not (string= suffix (substring buffer-file-name (- l l-suffix))))
    (if (not (string= suffix (substring s (- l l-suffix))))
        (error "Suffix doesn't match %s" suffix)
      (substring s 0 (- l l-suffix))
      )))


(my-delete-suffix (buffer-file-name) ".el" )


\probabilite improbable

0pro9

pro

(re-search-backward (concat "[^[:word:]]" prefix))

improbable probable



(re-search-forward "pro[^[:word:]]")
====/////////\\\\\\\\\\\\\\\\\
*    ;:/!?****************))))))))))))))$$$$$$$$$###########
hgfr9htg

(setq prefix "pro")


itest

 fvsf \test

-123

+45