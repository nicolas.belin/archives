#!/usr/bin/python3

def opener(filename:str, flags:str):
    """
    Ouvre le fichier nommé 'filename', envoie le fichier en question.
    :param filename: le nom du fichier à ouvrir
    :param flags: les flags à appliquer
    :returns: le fichier ou False si un problème est survenu
    :CU: aucun
    """
    try:
        fichier = open(filename, flags)
    except OSError:
        print(f"Impossible d'ouvrir le fichier {filename}.")
        return False
    else:
        return fichier

def recopie(filename0:str, filename1:str):
    """
    Copie le fichier texte nommé 'filename0' vers 'filename1'
    après avoir remplacé les 'o' par des 'O'.
    :param filename0: le nom du fichier en entrée
    :param filename1: le nom du fichier en sortie
    :returns: aucun
    :CU: aucune
    """

    fichier0 = opener(filename0, 'rt')
    fichier1 = opener(filename1, 'wt')
    if not fichier0 or not fichier1:
        return False
    texte = fichier0.read()
    fichier0.close()

    texte_modifié = texte.replace('o', 'O')
    print(texte_modifié, file=fichier1)
    fichier1.close()

if __name__ == '__main__':
    
    import sys

    recopie(*sys.argv[1:3])
    
