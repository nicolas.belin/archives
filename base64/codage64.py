BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']

def to_base64(triplet):
    '''
    convertit le triplet d'octets en une chaîne de quatre symboles
    
    :param triplet: (tuple ou list) une séquence d'octets
    :return: (str) la chaîne de symboles de la base 64 représentant le triplet d'octets
    :CU: 1 <= len(triplet) <= 3 et les entiers de triplet tous compris entre 0 et 255
    :Exemple:
    
    >>> to_base64((18, 184, 156))
    'Eric'
    >>> to_base64((18, 184))
    'Erg='
    >>> to_base64((18,))
    'Eg=='
    '''
    if len(triplet) == 2:
        triplet = (*triplet, 0)
        term = "="
    elif len(triplet) == 1:
        triplet = (*triplet, 0, 0)
        term = "=="
    else:
        term = ""
        
    a = BASE64_SYMBOLS[triplet[0] >> 2]
    b = BASE64_SYMBOLS[((triplet[0] & 0b11) << 4) | (triplet[1] >> 4)]
    c = BASE64_SYMBOLS[((triplet[1] & 0b1111) << 2) | (triplet[2] >> 6)]
    d = BASE64_SYMBOLS[(triplet[2] & 0b111111)]

    return a + b + c + d + term
    

def from_base64(b64_string):
    '''
    convertit une chaîne de quatre symboles en un tuple (le plus souvent triplet) d'octets
    
    :param b64_string: (str) une chaîne de symboles de la base 64
    :return: (tuple) un tuple d'octets dont b64_string est la représentation en base 64
    :CU: len(b64_string) == 4 et les caractères de b64_string sont dans la table ou le symbole =
    :Exemple:
    
    >>> from_base64('Eric')
    (18, 184, 156)
    >>> from_base64('Erg=')
    (18, 184)
    >>> from_base64('Eg==')
    (18,)
    '''
    
    if b64_string[3] == '=':
        b64_string = b64_string[:-1]
        d = 1
        if b64_string[2] == '=':
            b64_string = b64_string[:-1]
            d = 2
    else:
        d = 0
        
    n = 0
    for c in b64_string:
        n <<= 6
        n |= BASE64_SYMBOLS.index(c)
    
    if d == 0:
        return (n >> 16, (n >> 8) & 0xFF, n & 0xFF)
    elif d == 1:
        return (n >> 10,  (n >> 2) & 0xFF)
    else:
        return (n >> 4,)

