#from tris import *
#from listes import *
from timeit import timeit
import pylab

def mesure_temps_mono(n, fonction_tri):
    chaine = fonction_tri + '(cree_liste_melangee(' + str(n) + '))'
    setup = 'from tris import ' + fonction_tri + '; from listes import cree_liste_melangee'
    t = timeit(setup=setup, stmt=chaine, number=100)
    return t/100

def mesure_temps_multi(n, fonction_tri):
    return [mesure_temps_mono(i, fonction_tri) for i in range(1, n)]

def courbe_temps_exec(fonction_tri):
    l = mesure_temps_multi(NBRE_ESSAIS, fonction_tri)
    pylab.plot(l)
    pylab.title(f'Temps du tri par sélection (pour {NBRE_ESSAIS} essais)')
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()

NBRE_ESSAIS = 50
courbe_temps_exec('tri_fusion')
