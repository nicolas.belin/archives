#from tris import *
#from listes import *
from timeit import timeit


timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
       stmt='tri_select(cree_liste_melangee(10))',
       number=100)
