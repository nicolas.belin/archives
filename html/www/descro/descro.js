"use strict";


function Vertex(args) {
    if ( typeof args[0] === 'number' &&
         typeof args[1] === 'number' &&
         typeof args[2] === 'number' ) {
        this.x = args[0];
        this.y = args[1];
        this.z = args[2];
        this.isVertex = true;
    }
    else {
        log( 'Vertex() : badly formed vertex' );
        return;
    }
}

Vertex.prototype.log = function() {
    var s = 'Point ';
    if ( this.label ) {
        s += this.label.toString() + ' ';
    }
    s += '( ' + this.x + ', ' + this.y + ', ' + this.z + ' )';
    log(s);
};

Vertex.prototype.color = [ 0.0, 0.0, 0.0, 1.0 ];

Vertex.prototype.get3D = function( coords, colors ) {
    if ( ! this.noDisplay ) {
        coords.push( this.x );
        coords.push( this.y );
        coords.push( this.z );
        coords.push( 1.0 );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
    }
};

function Segment(args) {
    if ( args[0].isVertex && args[1].isVertex ) {
        this.vtx1 = args[0];
        this.vtx2 = args[1];
        this.isSegment = true;
        if ( this.vtx1.isTrace && this.vtx2.isTrace ) {
            this.isTrace = true;
        }
    }
    else {
        log( 'Badly formed segment' );
        return;
    }
}

Segment.prototype.log = function() {
    var s = 'Segment ';

    if ( this.label ) {
        s += this.label + ' ';
    }
    if ( this.vtx1.label && this.vtx2.label ) {
        if ( this.label ) {
            s += '= ';
        }
        s += '[' + this.vtx1.label + this.vtx2.label + ']';
    }
    log(s);
};

Segment.prototype.color = [ 0.0, 0.0, 0.0, 1.0 ];

Segment.prototype.get3D = function( coords, colors ) {
    if ( ! this.noDisplay ) {
        coords.push( this.vtx1.x );
        coords.push( this.vtx1.y );
        coords.push( this.vtx1.z );
        coords.push( 1.0 );
        coords.push( this.vtx2.x );
        coords.push( this.vtx2.y );
        coords.push( this.vtx2.z );
        coords.push( 1.0 );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
    }
};

function Line(args) {
    var vtx1, vtx2;
    
    if ( args[0].isVertex && args[1].isVertex ) {
        vtx1 = args.shift();
        vtx2 = args.shift();
        this.x = vtx1.x;
        this.y = vtx1.y;
        this.z = vtx1.z;
        this.dx = vtx1.x - vtx2.x;
        this.dy = vtx1.y - vtx2.y;
        this.dz = vtx1.z - vtx2.z;
    }
    else if ( args[0].isVertex && args[1].isLine ) {
        this.x = args[0].x;
        this.y = args[0].y;
        this.z = args[0].z;
        this.dx = args[1].dx;
        this.dy = args[1].dy;
        this.dz = args[1].dz;
        args.shift();
        args.shift();
    }
    else {
        log( 'Badly formed line' );
        return;
    }
    this.isLine = true;
}

Line.prototype.inter = function(b) {
    var x, y, z, t, d, a = this;
    
    x = a.x - b.x;
    y = a.y - b.y;
    z = a.z - b.z;
    t = vect.triple( x, y, z,
                     a.dx, a.dy, a.dz,
                     b.dx, b.dy, b.dz );
    if ( Math.abs(t) > 0.001 ) {
        log( "Line.inter() : lines don't intersect, not even coplanar" );
        return false;
    }
    if ( Math.abs( d = a.dx * b.dy - a.dy * b.dx ) > 0.001 ) {
        t = ( b.dy * ( b.x - a.x ) - b.dx * ( b.y - a.y ) ) / d;
        x = a.x + t * a.dx;
        y = a.y + t * a.dy;
        z = a.z + t * a.dz;
    }
    else if ( Math.abs( d = a.dx * b.dz - a.dz * b.dx ) > 0.001 ) {
        t = ( b.dz * ( b.x - a.x ) - b.dx * ( b.z - a.z ) ) / d;
            x = a.x + t * a.dx;
        y = a.y + t * a.dy;
            z = a.z + t * a.dz;
    }
    else if ( Math.abs( d = a.dy * b.dz - a.dz * b.dy ) > 0.001 ) {
        t = ( b.dz * ( b.y - a.y ) - b.dy * ( b.z - a.z ) ) / d;
        x = a.x + t * a.dx;
        y = a.y + t * a.dy;
        z = a.z + t * a.dz;
    }
    else {
        log( "Line.inter() : lines don't intersect, they are parallel" );
        return false;
    }
    return new Vertex( [ x, y, z ] );
};

Line.prototype.log = function() {
    var s = 'Droite ';

    if ( this.label ) {
        s += this.label + ' ';
    }
    if ( this.vtx1 && this.vtx2 &&
         this.vtx1.label && this.vtx2.label ) {
        if ( this.label ) {
            s += '= ';
        }
        s += '(' + this.vtx1.label + this.vtx2.label + ')';
    }
    log(s);
};

Line.prototype.color = [ 0.0, 0.0, 0.0, 1.0 ];

Line.prototype.get3D = function( coords, colors ) {
    if ( ! this.noDisplay ) {
        coords.push( this.x );
        coords.push( this.y );
        coords.push( this.z );
        coords.push( 1.0 );
        coords.push( this.dx );
        coords.push( this.dy );
        coords.push( this.dz );
        coords.push( 0.0 );
        coords.push( this.x );
        coords.push( this.y );
        coords.push( this.z );
        coords.push( 1.0 );
        coords.push( -this.dx );
        coords.push( -this.dy );
        coords.push( -this.dz );
        coords.push( 0.0 );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
    }
};

function Plane(args) {
    var vtx1, vtx2, vtx3, d1, d2, n, l;

    if ( args[0].isVertex && args[1].isVertex && args[1].isVertex ) {
        vtx1 = args.shift();
        vtx2 = args.shift();
        vtx3 = args.shift();
    }
    else if ( args[0].isLine && args[1].isLine ) {
        d1 = args.shift();
        d2 = args.shift();
        n = vect.cross( d1.dx, d1.dy, d1.dz, d2.dx, d2.dy, d2.dz );
        l = vect.norm( n[0], n[1], n[2] );
        if ( l < 0.0001 ) { //   (d1)//(d2)
            n = vect.cross( d1.dx, d1.dy, d1.dz,
                            d2.x - d1.x, d2.y - d1.y, d2.z - d1.z );
            l = vect.norm( n[0], n[1], n[2] );
            if ( l < 0.0001 ) {
                log( 'Plane() : d1 and d2 are only one line !' );
                return;
            }
            vtx1 = { x:d1.x, y:d1.y, z:d1.z };
            vtx2 = { x:d2.x, y:d2.y, z:d2.z };
            vtx3 = { x:d1.x+d1.dx, y:d1.y+d1.dy, z:d1.z+d1.dz };
        }
        else { // (d1) and (d2) are secant
            vtx1 = d1.inter(d2);
            vtx2 = { x:vtx1.x+d1.dx, y:vtx1.y+d1.dy, z:vtx1.z+d1.dz };
            vtx3 = { x:vtx1.x+d2.dx, y:vtx1.y+d2.dy, z:vtx1.z+d2.dz };
        }
        args.shift();
        args.shift();
    }
    else {
        log( 'Plane() : Badly formed plane' );
        return;
    }
    this.x = vtx1.x;
    this.y = vtx1.y;
    this.z = vtx1.z;
    this.dx1 = vtx2.x - vtx1.x;
    this.dy1 = vtx2.y - vtx1.y;
    this.dz1 = vtx2.z - vtx1.z;
    l = vect.norm( this.dx1, this.dy1, this.dz1 );
    this.dx1 /= l;
    this.dy1 /= l;
    this.dz1 /= l;
    this.dx2 = vtx3.x - vtx1.x;
    this.dy2 = vtx3.y - vtx1.y;
    this.dz2 = vtx3.z - vtx1.z;
    l = vect.norm( this.dx2, this.dy2, this.dz2 );
    this.dx2 /= l;
    this.dy2 /= l;
    this.dz2 /= l;
    n = vect.cross( this.dx1, this.dy1, this.dz1,
                    this.dx2, this.dy2, this.dz2 );
    this.nx = n[0];
    this.ny = n[1];
    this.nz = n[2];
    this.isPlane = true;
}

Plane.prototype.proj = function(a) {
    var t, x, y, z, l, vtx1, vtx2;

    if ( a.isVertex ) {
        x = a.x-this.x;
        y = a.y-this.y;
        z = a.z-this.z;
        t = vect.dot( this.nx, this.ny, this.nz, x, y, z );
        x -= t*this.nx;
        y -= t*this.ny;
        z -= t*this.nz;
        x += this.x;
        y += this.y;
        z += this.z;
        return new Vertex( [ x, y, z ] );
    }
    else if ( a.isLine ) {
        l = vect.norm( a.dx, a.dy, a.dz );
        vtx1 = new Vertex( [ a.x, a.y, a.z ] );
        vtx2 = new Vertex( [ a.x + a.dx/l, a.y + a.dy/l, a.z + a.dz/l ] );
        vtx1 = this.proj(vtx1);
        vtx2 = this.proj(vtx2);
        l = vect.norm( vtx1.x - vtx2.x, vtx1.y - vtx2.y, vtx1.z - vtx2.z );
        if ( l < 0.001 ) {
            return vtx1;
        }
        return new Line( [ vtx1, vtx2 ] );
    }
    else if ( a.isSegment ) {
        vtx1 = this.proj(a.vtx1);
        vtx2 = this.proj(a.vtx2);
        l = vect.norm( vtx1.x - vtx2.x, vtx1.y - vtx2.y, vtx1.z - vtx2.z );
        if ( l < 0.001 ) {
            return vtx1;
        }
        return new Segment( [ vtx1, vtx2 ] );
    }
    else {
        log( "Plane.proj() : bad argument" );
    }
};

Plane.prototype.log = function() {
    var s = 'Plan ';

    if ( this.label ) {
        s += this.label + ' ';
    }
    if ( this.vtx1 && this.vtx2 &&
         this.vtx1.label && this.vtx2.label ) {
        if ( this.label ) {
            s += '= ';
        }
        s += '(' + this.vtx1.label + this.vtx2.label + ')';
    }
    log(s);
};

Plane.prototype.color = [ 0.0, 0.0, 0.0, 1.0 ];

function Frame(args) {
    var O, D, P;
    var i, j, k, l, k, v, n, S, m;
    var s, nr = construct.frameNr++;

    this.isFrame = true;
    if ( nr === 0 ) {
        s = '';
    }
    else {
        s = nr.toString();
    }
    if ( args.length === 0 ) {
        this.O = new Vertex( [ 0.0, 0.0, 0.0 ] );
        this.i = [ 1, 0, 0 ];
        this.j = [ 0, 1, 0 ];
        this.k = [ 0, 0, 1 ];
        this.vtc = construct.chunks;
        this.prev = null;
        this.color = [ 0.0, 0.0, 0.7, 1.0 ];
        this.type = null;
        s = '';
        this.nr = 0;
    }
    else {
        O = args.shift(); // vertex
        D = args.shift(); // line
        this.prev = args.shift(); // frame
        this.type = args.shift(); // frontal or horizontal
        this.color = args.shift();
        this.vtc = args; // vertices
        if ( this.type === 'frontal' ) {
            P = this.prev.OXY;
            n = this.prev.k;
        }
        else if ( this.type === 'horizontal' ) {
            P = this.prev.OYZ;
            n = this.prev.i;
        }
        else {
            log( 'Frame() : unknown type' );
            return;
        }
        D = P.proj(D);
        if ( D.isVertex ) {
            log( "Frame() : line is perpendicular to previous frame plane" );
            return;
        }
        this.O = O = P.proj(O);
        i = vect.cross( D.dx, D.dy, D.dz, n[0], n[1], n[2] );
        l = vect.norm( i[0], i[1], i[2] );
        i[0] /= l;
        i[1] /= l;
        i[2] /= l;
        S = 0;
        for ( m of this.vtc ) {
            if ( m.isVertex ) {
                S += vect.dot( m.x - O.x, m.y - O.y, m.z - O.z,
                               i[0], i[1], i[2] );
            }
        }
        if ( S < 0 ) {
            i[0] = -i[0];
            i[1] = -i[1];
            i[2] = -i[2];
        }
        if ( this.type === 'frontal' ) {
            this.i = i;
            this.k = n;
        }
        else {
            this.i = n;
            this.k = i;
        }
        this.j = vect.cross( this.k[0], this.k[1], this.k[2],
                             this.i[0], this.i[1], this.i[2] );
    }
    i = this.i;
    j = this.j;
    k = this.k;
    O = this.O;
    this.OX = construct.addLine(
        [ 'OX'+s, O, new Vertex( [ i[0]+O.x, i[1]+O.y, i[2]+O.z ] ) ] );
    this.OY = construct.addLine(
        [ 'OY'+s, O, new Vertex( [ j[0]+O.x, j[1]+O.y, j[2]+O.z ] ) ] );
    this.OZ = construct.addLine(
        [ 'OZ'+s, O, new Vertex( [ k[0]+O.x, k[1]+O.y, k[2]+O.z ] ) ] );
    this.OX.noDisplay = true;
    this.OY.noDisplay = true;
    this.OZ.noDisplay = true;
    this.OXY = construct.addPlane( [ 'OXY'+s, this.OX, this.OY ] );
    this.OYZ = construct.addPlane( [ 'OYZ'+s, this.OY, this.OZ ] );
}

Frame.prototype.log = function() {
};

Frame.prototype.getBoxDim = function() {
    var m, v, O = this.O;

    var M = new Matrix( 3, 3,
                        [ this.i[0], this.j[0], this.k[0],
                          this.i[1], this.j[1], this.k[1],
                          this.i[2], this.j[2], this.k[2] ] );
    
    this.xmax = 0.0;
    this.xmin = 0.0;
    this.ymax = 0.0;
    this.ymin = 0.0;
    this.zmax = 0.0;
    this.zmin = 0.0;
    for ( m of this.vtc ) {
        if (  m.isVertex ) {
            v = M.multVec( [ m.x - O.x, m.y - O.y, m.z - O.z ] );
            this.xmax = Math.max( this.xmax, v[0] );
            this.ymax = Math.max( this.ymax, v[1] );
            this.zmax = Math.max( this.zmax, v[2] );
            this.xmin = Math.min( this.xmin, v[0] );
            this.ymin = Math.min( this.ymin, v[1] );
            this.zmin = Math.min( this.zmin, v[2] );
        }
    }
};

Frame.prototype.get3D = function( coords, colors ) {
    var i = this.i;
    var j = this.j;
    var k = this.k;
    var O = this.O;
    var front = false;
    var horiz = false;
    var A, d, n, l, Pf, Ph, p, pColor;

    if ( this.noDisplay ) {
        return;
    }
    
    this.getBoxDim();
    d = 0.2 * Math.max( this.xmax - this.xmin, this.ymax - this.ymin,
                        this.zmax - this.zmin );

    A = [ this.xmin-d, 0.0, 0.0, // 0   (Ox)
          this.xmax+d, 0.0, 0.0, // 1
          this.xmax+d/2, d/3, 0.0,  // 2
          this.xmax+d, 0.0, 0.0, // 1
          this.xmax+d/2, -d/3, 0.0, // 3
          this.xmax+d, 0.0, 0.0, // 1
          0.0, this.ymin-d, 0.0, // 4   (Oy)
          0.0, this.ymax+d, 0.0, // 5
          0.0, this.ymax+d, 0.0, // 5
          0.0, this.ymax+d/2, d/3, // 6
          0.0, this.ymax+d, 0.0, // 5
          0.0, this.ymax+d/2, -d/3, // 7
          0.0, 0.0, this.zmin-d, // 8   (Oz)
          0.0, 0.0, this.zmax+d, // 9
          0.0, 0.0, this.zmax+d, // 9
          0.0, d/3, this.zmax+d/2, // 10
          0.0, 0.0, this.zmax+d, // 9
          0.0, -d/3, this.zmax+d/2 ]; // 11

    l = A.length;
    for ( n=0; n<l; n+=3 ) {
        coords.push( O.x + A[n]*i[0] + A[n+1]*j[0] + A[n+2]*k[0] );
        coords.push( O.y + A[n]*i[1] + A[n+1]*j[1] + A[n+2]*k[1] );
        coords.push( O.z + A[n]*i[2] + A[n+1]*j[2] + A[n+2]*k[2] );
        coords.push(1.0);
        colors.push( this.color[0] );
        colors.push( this.color[1] );
        colors.push( this.color[2] );
        colors.push( this.color[3] );
    }
    if ( construct.triDi.projecting ) {
        if ( this.type === 'frontal' ) {
            front = true;
            Pf = this.OYZ;
        }
        else if ( this.type === 'horizontal' ) {
            horiz = true;
            Ph = this.OXY;
        }
        else {
            front = true;
            horiz = true;
            Pf = this.OYZ;
            Ph = this.OXY;
        }
        for ( n of this.vtc ) {
            if ( n.noDisplay ) {
                continue;
            }
            pColor = [ 0.8, 0.0, 0.0, 0.7 ];
            if ( front ) {
                if ( n.isVertex ) {
                    coords.push( n.x );
                    coords.push( n.y );
                    coords.push( n.z );
                    coords.push(1.0);
                    p = Pf.proj(n);
                    coords.push( p.x );
                    coords.push( p.y );
                    coords.push( p.z );
                    coords.push(1.0);
                    colors.push( pColor[0] );
                    colors.push( pColor[1] );
                    colors.push( pColor[2] );
                    colors.push( pColor[3] );
                    colors.push( pColor[0] );
                    colors.push( pColor[1] );
                    colors.push( pColor[2] );
                    colors.push( pColor[3] );
                }
                else if ( n.isSegment ) {
                    p = Pf.proj(n);
                    if ( p.isSegment ) {
                        p.color = [ n.color[0], n.color[1], n.color[2], n.color[3]*0.7 ];
                        p.get3D( coords, colors );
                    }
                }
            }
            if ( horiz ) {
                if ( n.isVertex ) {
                    coords.push( n.x );
                    coords.push( n.y );
                    coords.push( n.z );
                    coords.push(1.0);
                    p = Ph.proj(n);
                    coords.push( p.x );
                    coords.push( p.y );
                    coords.push( p.z );
                    coords.push(1.0);
                    colors.push( pColor[0] );
                    colors.push( pColor[1] );
                    colors.push( pColor[2] );
                    colors.push( pColor[3] );
                    colors.push( pColor[0] );
                    colors.push( pColor[1] );
                    colors.push( pColor[2] );
                    colors.push( pColor[3] );
                }
                else if ( n.isSegment ) {
                    p = Ph.proj(n);
                    if ( p.isSegment ) {
                        p.color = [ n.color[0], n.color[1], n.color[2], n.color[3]*0.7 ];
                        p.get3D( coords, colors );
                    }
                }
            }
        }
    }
};

function Sketch( canvas, construct ) {
    this.construct = construct;
    this.element = canvas;
    construct.windowing.addWindow(this);
    this.box = [ 0.0, 0.0, 1.0, 1.0 ]; // [ xc, yc, xScale, yScale ]
    this.scaleBase = [ 1.0, 1.0 ];
    this.distance = 0;
    this.modified = true;
    this.mouse = { x:null, y:null };
    util.trackPointer( this.element, 'mousedown', function(){},
                       document, 'mouseup', function(){},
                       this.scroll.bind(this) );
    canvas.addEventListener( 'wheel', this, false );
}

Sketch.prototype.init = function(first) {

    function appendLine( x0, y0, w0, x1, y1, w1, z ) {
        coords.push(x0); 
        coords.push(y0); 
        coords.push(z); 
        coords.push(w0); 
        coords.push(x1); 
        coords.push(y1); 
        coords.push(z);
        coords.push(w1); 
        colors.push(r);
        colors.push(g);
        colors.push(b);
        colors.push(r);
        colors.push(g);
        colors.push(b);
    }
    
    var c = this.construct;
    var coords = [];
    var colors = [];
    var w, h, r, g, b;
    var i, dx, dy, dz, d, a, b;
    var ymin, ymax, xmax, zmax, m;
    var shader, wgl, gl;
    
    if (first) {
        this.wgl = new WebGL(this.element);
        wgl = this.wgl;
        gl = wgl.gl;

        if ( ! gl ){
            $('no_webgl').hidden = null;
            return false;
        }
        gl.clearColor( 1.0, 1.0, 1.0, 0.0 );
        gl.clear( gl.COLOR_BUFFER_BIT );

        shader = new Shader( 'Basic', wgl );
        if ( ! shader.make( util.loaded['sketch.shader.vertex.c'],
                            util.loaded['shader.fragment.c'] ) ) {
            return false;
        }

        this.chain = new Chain(shader);
        this.chain.setPrimitiveType('lines');
        this.chain.addUniform( 'u_box', this.box );
        this.chain.setLineWidth(2.0);
        this.chain.addAttribute( 'a_position', 4 );
        this.chain.addAttribute( 'a_color', 4 );
        this.distance = 0;
        wgl.resize();
        this.setSize( wgl.w, wgl.h );
    }
//    this.distance = 0;
//    wgl.resize();
//    this.setSize( wgl.w, wgl.h );
    d = 0.1 * Math.max( c.ymax - c.ymin, c.zmax + c.xmax );
    
    // axes
    r = 0.0;
    g = 0.0;
    b = 0.7;
    appendLine( 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0 ); 
    appendLine( 0.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0 ); 
    //appendLine( c.ymax+d/2, d/3, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    //appendLine( c.ymax+d/2, -d/3, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    //appendLine( c.ymin-d, 0.0, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    appendLine( 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0 );
    appendLine( 0.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0 );
    //appendLine( -d/3, c.zmax+d/2, 1.0, 0.0, c.zmax+d, 1.0, -0.1 );
    //appendLine( d/3, c.zmax+d/2, 1.0, 0.0, c.zmax+d, 1.0, -0.1 );
    //appendLine( -d/3, -c.xmax-d/2, 1.0, 0.0, -c.xmax-d, 1.0, -0.1 );
    //appendLine( d/3, -c.xmax-d/2, 1.0, 0.0, -c.xmax-d, 1.0, -0.1 );
/*
    appendLine( c.ymin-d, 0.0, 1.0, c.ymax+d, 0.0, 1.0, -0.2 ); 
    appendLine( c.ymax+d/2, d/3, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    appendLine( c.ymax+d/2, -d/3, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    appendLine( c.ymin-d, 0.0, 1.0, c.ymax+d, 0.0, 1.0, -0.1 ); 
    appendLine( 0.0, -c.xmax-d, 1.0, 0.0, c.zmax+d, 1.0, -0.1 );
    appendLine( -d/3, c.zmax+d/2, 1.0, 0.0, c.zmax+d, 1.0, -0.1 );
    appendLine( d/3, c.zmax+d/2, 1.0, 0.0, c.zmax+d, 1.0, -0.1 );
    appendLine( -d/3, -c.xmax-d/2, 1.0, 0.0, -c.xmax-d, 1.0, -0.1 );
    appendLine( d/3, -c.xmax-d/2, 1.0, 0.0, -c.xmax-d, 1.0, -0.1 );
  */  
    // points et lignes de rappel
    d *= 0.15;
    for ( i=c.vertex.length-1; i>=0; i-- ){
        m = c.vertex[i];
        if ( ! m.sketchNoDisp ) {
            if ( m.isTrace ) {
                r = 0.0, g = 0.8, b = 0.0;
            }
            else {
                r = 0.2, g = 0.2, b = 0.2;
            }
            appendLine( m.y-d, -m.x-d, 1.0, m.y+d, -m.x+d, 1.0, 0.0 );
            appendLine( m.y+d, -m.x-d, 1.0, m.y-d, -m.x+d, 1.0, 0.0 );
            appendLine( m.y-d, m.z-d, 1.0, m.y+d, m.z+d, 1.0, 0.0 );
            appendLine( m.y+d, m.z-d, 1.0, m.y-d, m.z+d, 1.0, 0.0 );
            r = 1.0, g = 0.0, b = 0.0;
            appendLine( m.y, -m.x, 1.0, m.y, m.z, 1.0, -0.1 );
        }
    }
    
    // plan frontal
    r = 0.0, g = 0.0, b = 0.0;
    for ( i=c.segment.length-1; i>=0; i-- ){
        m = c.segment[i];
        if ( ! m.sketchNoDisp ) {
            if ( m.isTrace ) {
                r = 0.0, g = 0.8, b = 0.0;
            }
            else {
                r = 0.0, g = 0.0, b = 0.0;
            }
            appendLine( m.vtx1.y, m.vtx1.z, 1.0,
                        m.vtx2.y, m.vtx2.z, 1.0, 0.0 );
        }
    }

    // plan horizontal
    for ( i=c.segment.length-1; i>=0; i-- ){
        m = c.segment[i];
        if ( ! m.sketchNoDisp ) {
            if ( m.isTrace ) {
                r = 0.0, g = 0.8, b = 0.0;
            }
            else {
                r = 0.0, g = 0.0, b = 0.0;
            }
            appendLine( m.vtx1.y, -m.vtx1.x, 1.0,
                        m.vtx2.y, -m.vtx2.x, 1.0, 0.0 );
        }
    }

    //droites
    for ( i=c.line.length-1; i>=0; i-- ){
        m = c.line[i];
        if ( ! m.sketchNoDisp && ! m.noDisplay ) {
            if ( m.dx !== 0.0 ) {
                dx = Math.abs(m.dx);
                if ( m.color ) {
                    r = m.color[0];
                    g = m.color[1];
                    b = m.color[2];
                }
                else {
                    r = 0.0, g = 0.0, b = 0.0;
                }
                appendLine( m.y - m.x*m.dy/m.dx, 0.0, 1.0,
                            m.dy*m.dx/dx, -dx, 0.0, 0.0 );


                if ( m.color ) {
//                    r = 0.8, g = 1.0, b = 0.8;
                    r = m.color[3];
                    g = m.color[4];
                    b = m.color[5];
                }
                else {
                    r = 0.9, g = 0.9, b = 0.9;
                }

                appendLine( m.y - m.x*m.dy/m.dx, 0.0, 1.0,
                            -m.dy*m.dx/dx, dx, 0.0, 0.0 );
            }
            if ( m.dz !== 0.0 ) {
                dz = Math.abs(m.dz);
                if ( m.color ) {
//                    r = 0.0, g = 0.8, b = 0.0;
                    r = m.color[0];
                    g = m.color[1];
                    b = m.color[2];
                }
                else {
                    r = 0.0, g = 0.0, b = 0.0;
                }
                appendLine( m.y - m.z*m.dy/m.dz, 0.0, 1.0,
                            m.dy*m.dz/dz, dz, 0.0, 0.0 );
                if ( m.color ) {
//                    r = 0.8, g = 1.0, b = 0.8;
                    r = m.color[3];
                    g = m.color[4];
                    b = m.color[5];
                }
                else {
                    r = 0.9, g = 0.9, b = 0.9;
                }
                appendLine( m.y - m.z*m.dy/m.dz, 0.0, 1.0,
                            -m.dy*m.dz/dz, -dz, 0.0, 0.0 );
            }
        }
    }
    
    this.chain.nr = null;
    this.chain.attributes.a_position.store(coords);
    this.chain.attributes.a_color.store(colors);
    this.modified = true;
    return true;
};

Sketch.prototype.setSize = function( w, h ) {
    var w, h, dx, dy, d, a, b, m;
    var xmax, ymax, xmin, ymin, zmax;
    var c = this.construct;

    this.element.width = w;
    this.element.height = h;
    dx = c.ymax - c.ymin;
    dy = c.zmax + c.xmax;
    d = Math.max( dx, dy );
    d *= 0.1;
    ymax = c.ymax + d;
    ymin = c.ymin - d;
    zmax = c.zmax + d;
    xmax = c.xmax + d;
    dx = ymax - ymin;
    dy = zmax + xmax;
    if ( dy / dx > h / w ) {
        b = 2.0 / dy;
        a = b * h / w;
    }
    else {
        a = 2.0 / dx;
        b = a * w / h;
    }
    this.scaleBase[0] = a;
    this.scaleBase[1] = b;
    m = Math.pow( 1.1, this.distance );
    this.box[2] = m * this.scaleBase[0]; 
    this.box[3] = m * this.scaleBase[1]; 
    this.box[0] = a * ( ymin + ymax ) / 2;
    this.box[1] = b * ( zmax - xmax ) / 2;
    if ( this.wgl ) {
        this.wgl.resize();
    }
    this.modified = true;
};

Sketch.prototype.render = function(t) {
    if ( this.modified ) {
        this.wgl.gl.clear( this.wgl.gl.COLOR_BUFFER_BIT );
        this.chain.render();
        this.modified = false;
    }
};

Sketch.prototype.displayLabels = function() {
    
};

Sketch.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'wheel':
        this.wheel(event);
        break;
    }
};

Sketch.prototype.scroll = function( dx, dy, t ) {
    this.box[0] -= dx * 2.0 / this.wgl.w;
    this.box[1] += dy * 2.0 / this.wgl.h;
    this.modified = true;
};

Sketch.prototype.resize = function(event) {
    this.wgl.resize();
};

Sketch.prototype.wheel = function(event) {
    var n = event.deltaY;
    var m;
    
    event.preventDefault();
    n /= Math.abs(n);

    this.distance += n;
    m = Math.pow( 1.1, this.distance );
    this.box[2] = m * this.scaleBase[0]; 
    this.box[3] = m * this.scaleBase[1]; 
    this.modified = true;
};

function TriDi( canvas, construct ) {
    this.construct = construct;
    this.element = canvas;
    construct.windowing.addWindow(this);
    this.rotating = $('rotationBox').checked;
    this.projecting = $('projectionBox').checked;

    util.trackPointer( this.element, 'mousedown', function(){},
                       document, 'mouseup', function(){},
                       this.rotate.bind(this) );
    canvas.addEventListener( 'wheel', this, false );
}

TriDi.prototype.init = function(first) {

    function sq(x) {
        return x*x;
    }
    
    var c = this.construct;
    var colors = [];
    var coords = [];
    var l, i, d, f, ch;
    var shader, wgl, gl;

    if (first) {
        this.wgl = new WebGL(this.element);
        wgl = this.wgl;
        gl = wgl.gl;

        if ( ! gl ){
            $('no_webgl').hidden = null;
            return;
        }
        gl.enable( gl.BLEND );
        gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA );
        this.fustrum = [];
        wgl.resize();
        this.setSize( wgl.w, wgl.h );
        
        gl.clearColor( 1.0, 1.0, 1.0, 1.0 );
        gl.clear( gl.COLOR_BUFFER_BIT );
        shader = new Shader( 'TriDi', wgl );
        if ( ! shader.make( util.loaded['triDi.shader.vertex.c'],
                            util.loaded['shader.fragment.c'] ) ) {
            return false;
        }
        this.mesh = new Chain(shader);
        this.mesh.setPrimitiveType('lines');
        this.mesh.addUniform( 'u_projection', this.fustrum );
        this.translate = [ 0.0, 0.0, 0.0, 1.0 ]; 
        this.mesh.addUniform( 'u_translate', this.translate );
        this.mesh.setLineWidth(3.0);
        this.sincos = [ 1.0, 0.0, 1.0, 0.0 ];
        this.mesh.addUniform( 'u_sincos', this.sincos );
        this.mesh.addAttribute( 'a_position', 4 );
        this.mesh.addAttribute( 'a_color', 4 );
    
        shader = new Shader( 'Points', wgl );
        if ( ! shader.make( util.loaded['triDi.shader.vertex.c'],
                            util.loaded['points.shader.fragment.c'] ) ) {
            return false;
        }
        this.dots = new Chain(shader);
        this.dots.setPrimitiveType('points');
        this.dots.addUniform( 'u_projection', this.fustrum );
        this.dots.addUniform( 'u_translate', this.translate );
        this.dots.addUniform( 'u_sincos', this.sincos );
        this.dots.addAttribute( 'a_position', 4 );
        this.dots.addAttribute( 'a_color', 4 );
    }
    
    this.translate[0] = c.xg;
    this.translate[1] = c.yg;
    this.translate[2] = c.zg;

    d = Math.max(
        sq( c.xmax - c.xg ) + sq( c.ymax - c.yg ) + sq( c.zmax - c.zg ), 
        sq( c.xmax - c.xg ) + sq( c.ymax - c.yg ) + sq( c.zmin - c.zg ), 
        sq( c.xmax - c.xg ) + sq( c.ymin - c.yg ) + sq( c.zmax - c.zg ), 
        sq( c.xmax - c.xg ) + sq( c.ymin - c.yg ) + sq( c.zmin - c.zg ), 
        sq( c.xmin - c.xg ) + sq( c.ymax - c.yg ) + sq( c.zmax - c.zg ), 
        sq( c.xmin - c.xg ) + sq( c.ymax - c.yg ) + sq( c.zmin - c.zg ), 
        sq( c.xmin - c.xg ) + sq( c.ymin - c.yg ) + sq( c.zmax - c.zg ), 
        sq( c.xmin - c.xg ) + sq( c.ymin - c.yg ) + sq( c.zmin - c.zg ) );
    d = Math.sqrt(d);
    this.baseScale = this.translate[3] = 1.0 / d;
    
    this.dmax = Math.max( c.xmax - c.xmin,
                          c.ymax - c.ymin, c.zmax - c.zmin );
    ch = c.chunks;
    l = ch.length;
    for ( i=0; i<l; i++ ){
        if ( ch[i].isVertex ) {
            ch[i].get3D( coords, colors );
        }
    }
    this.dots.nr = null;
    this.dots.attributes.a_position.store(coords);
    this.dots.attributes.a_color.store(colors);
    
    coords.length = 0;
    colors.length = 0;
    for ( i=0; i<l; i++ ) {
        if ( ch[i].isSegment ) {
            ch[i].get3D( coords, colors );
        }
    }
    for ( i=0; i<l; i++ ) {
        if ( ch[i].isLine ) {
            ch[i].get3D( coords, colors );
        }
    }
    for ( i=0; i<l; i++ ) {
        if ( ch[i].isFrame ) {
            ch[i].get3D( coords, colors ); 
        }
    }

    this.mesh.nr = null;
    this.mesh.attributes.a_position.store(coords);
    this.mesh.attributes.a_color.store(colors);

    this.time = -1;
    this.phi = 0.0;
    this.theta = 0.0;
    this.distance = 0;
    this.modified = true;
    return true;
};

TriDi.prototype.render = function(t) {
    if ( this.rotating ) {
        if ( this.time < 0 ) {
            this.time = t;
        }
        this.phi += ( t - this.time ) / 20;
        this.time = t;
        this.modified = true;
    }
    if ( this.modified ) {
        this.wgl.gl.clear( this.wgl.gl.COLOR_BUFFER_BIT );
        this.sincos[0] = Math.cos( ( Math.PI * this.phi ) / 180 );
        this.sincos[1] = Math.sin( ( Math.PI * this.phi ) / 180 );
        this.sincos[2] = Math.cos( ( Math.PI * this.theta ) / 180 );
        this.sincos[3] = Math.sin( ( Math.PI * this.theta ) / 180 );
        this.phi %= 360;
        this.theta %= 360;
        this.dots.render();
        this.mesh.render();
        this.modified = false;
    }
};

TriDi.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'wheel':
        this.wheel(event);
        break;
    }
};

TriDi.prototype.rotate = function( dx, dy, t ) {
    this.phi += dx;
    this.theta -= dy;
    this.modified = true;
};

TriDi.prototype.resize = function(event) {
    this.wgl.resize();
    this.modified = true;
};

TriDi.prototype.setSize = function( w, h ) {
    this.element.width = w;
    this.element.height = h;
    if ( this.wgl ) {
        this.wgl.fustrum( 40.0, w/h, 3.0, 2.5, -10.0, this.fustrum );
        this.wgl.resize();
    }
    this.modified = true;
};

TriDi.prototype.wheel = function(event) {
    var n = event.deltaY;

    event.preventDefault();
    n /= Math.abs(n);
    this.distance += n;
    this.translate[3] = this.baseScale * Math.pow( 1.1, this.distance );
    this.modified = true;
};

function Construct( windowing, canvas3d, canvasSketch ) {
    this.chunks = [];
    this.firstInit = true;
    this.windowing = windowing;
    this.triDi = new TriDi( canvas3d, this );
    //this.sketch = new Sketch( canvasSketch, this );
    this.ready = false;
}

Construct.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'mousedown':
        break;
    }
};

Construct.prototype.clean = function() {
    this.chunks.length = 0;
    this.vertexNr = 0;
    this.frameNr = 0;
    this.byLabel = {};
    this.xmax = -Infinity;
    this.xmin = 0.0;
    this.ymax = -Infinity;
    this.ymin = 0.0;
    this.zmax = -Infinity;
    this.zmin = 0.0;
    this.xg = 0.0;
    this.yg = 0.0;
    this.zg = 0.0;
};

Construct.prototype.init = function(string) {
    this.clean();
    this.setColor( 'vert', [ 0.0, 0.8, 0.0, 1.0 ] );
    this.setColor( 'violet', [ 0.8, 0.0, 0.8, 1.0 ] );
    this.setColor( 'turquoise', [ 0.0, 0.8, 0.8, 1.0 ] );
    this.addFrame( [ 'OXYZ' ] );
    this.parse(string);
    this.xg /= this.vertexNr;
    this.yg /= this.vertexNr;
    this.zg /= this.vertexNr;

    if ( this.firstInit ) {
        if ( ! this.triDi.init(true) /*||
             ! this.sketch.init(true)*/ ) {
            // certainly a shader compilation error
            return false;
        }
        delete util.loaded;
        this.firstInit = false;
    }
    else {
        this.triDi.init();
       // this.sketch.init();
    }
    return true;
};

Construct.prototype.checkArgs = function(args) {
    var i, a, f;

    for ( i=args.length-1; i>=0; i-- ) {
        a = args[i];
        if ( this.byLabel.hasOwnProperty(a) ) {
            args[i] = this.byLabel[a];
        }
        else if ( ! isNaN( f = parseFloat(a) ) ) {
            args[i] = f;
        }
    }
};

Construct.prototype.setLabel = function( label, a ) {
    this.byLabel[label] = a;
    a.label = label;
};

Construct.prototype.setColor = function( id, rgb ) {
    this.byLabel[id] = rgb;
};

Construct.prototype.addVertex = function(args) {
    var vertex, x, y, z, label = false;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    if ( args[0].isVertex ) {
        vertex = args[0];
    }
    else {
        vertex = new Vertex(args);
    }
    if ( ! vertex.isVertex ) {
        return false;
    }
    if ( label ) {
        this.setLabel( label, vertex );
    }
    this.chunks.push(vertex);
    this.vertexNr++;
    
    x = vertex.x;
    y = vertex.y;
    z = vertex.z;
    this.xmax = Math.max( this.xmax, x );
    this.xmin = Math.min( this.xmin, x );
    this.ymax = Math.max( this.ymax, y );
    this.ymin = Math.min( this.ymin, y );
    this.zmax = Math.max( this.zmax, z );
    this.zmin = Math.min( this.zmin, z );
    this.xg += x;
    this.yg += y;
    this.zg += z;
    return vertex;
};

Construct.prototype.addSegment = function(args) {
    var segment, label = false;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    segment = new Segment(args);
    if ( ! segment.isSegment ) {
        return false;
    }
    this.chunks.push(segment);
    if ( label ) {
        this.setLabel( label, segment );
    }
    return segment;
};

Construct.prototype.addLine = function(args) {
    var line, label = false;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    line = new Line(args);
    if ( ! line.isLine ) {
        return false;
    }
    this.chunks.push(line);

    if ( label ) {
        this.setLabel( label, line );
    }
    if ( args.length > 0 ) {
        line.color = args[0];
    }
    return line;
};

Construct.prototype.addPlane = function(args) {
    var p, label = false;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    p = new Plane(args);
    if ( ! p.isPlane ) {
        return false;
    }
    this.chunks.push(p);

    if ( label ) {
        this.setLabel( label, p );
    }
    if ( args.length > 0 ) {
        p.color = args[0];
    }
    return p;
};

Construct.prototype.addFrame = function(args) {
    var f, label;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    f = new Frame(args);
    
    if ( label ) {
        this.setLabel( label, f );
    }
    this.chunks.push(f);
    return f;
};

Construct.prototype.log = function() {
};

Construct.prototype.addTrace = function(args) {
    var m, p, line, type, A, label = null;

    type = args.shift();
    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    line = args[0];
    if ( line.isLine ) {
        m = line;
        switch ( type ) {
        case 'H':
            if ( Math.abs(m.dz) < 0.001 ) {
                log( "No horizontal trace, line is parallel " +
                     "to OXY plane" );
                return false;
            }
            A = [ m.x - m.z*m.dx/m.dz, 
                  m.y - m.z*m.dy/m.dz, 0.0 ];
            break;
        case 'F':
            if ( Math.abs(m.dx) < 0.0001 ) {
                log( "No fontal trace, line is parallel " +
                     "to OYZ plane" );
                return false;
            }
            A = [ 0.0, m.y - m.x*m.dy/m.dx,
                  m.z - m.x*m.dz/m.dx ];
            break;
        default:
            log( "Unknown trace type : " + type );
            return false;
        }
        if ( label ) {
            A.unshift(label);
        }
        p = this.addVertex(A);
        p.isTrace = true;
        return p;
    }
    log( "Only line traces" );
    return false;
};

Construct.prototype.addInter = function(args) {
    var t, d, label = null;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    if ( args[0].isLine && args[1].isLine ) {
        d = [ args[0].inter( args[1] ) ];
        if ( ! d ) {
            return false;
        }
        if ( label ) {
            d.unshift(label);
        }
        t = "Intersection of two lines ";
        if ( args[0].label && args[1].label ) {
            t += args[0].label + ' and ' + args[1].label + ' ';
        }
        t += ':'
        log(t);
        return this.addVertex(d);
    }
    else {
        log( "Unsupported intersection" );
        return false;
    }
};

Construct.prototype.addParallel = function(args) {
    var r, line, s;
    var k = 0;
    
    if ( typeof args[0] === 'string' ) {
        k = 1;
    }
    if ( args[k].isVertex && args[k+1].isLine ) {
    }
    else if ( args[k+1].isVertex && args[k].isLine ) {
        line = args[k];
        args[k] = args[k+1];
        args[k+1] = line;
    }
    else {
        log( "Unsupported parallel" );
        return false;
    }
    s = "Parallel ";
    if ( args[k].label && args[k+1].label ) {
        s += "of " + args[k+1].label + " through " +
            args[k].label + ' ';
    }
    s += ':';
    log(s);
    return this.addLine(args);
};

Construct.prototype.addProjection = function(args) {
    var t, d, label = null;

    if ( typeof args[0] === 'string' ) {
        label = args.shift();
    }
    if ( args[0].isVertex && args[1].isPlane ) {
        d = [ args[1].proj( args[0] ) ];
        if ( label ) {
            d.unshift(label);
        }
        t = "Orthogonal projection";
        if ( args[0].label && args[1].label ) {
            t += ' of ' + args[0].label + ' on ' + args[1].label + ' ';
        }
        t += ':'
        log(t);
        return this.addVertex(d);
    }
    else {
        log( "Unsupported projection" );
        return false;
    }
};

Construct.prototype.render = function(t) {
    if ( this.ready ) {
        this.triDi.render(t);
        //this.sketch.render(t);
    }
};

Construct.prototype.parse = function(string) {
    var i, j, n, A, r, command;
    var lines = [];
    var noSketch = false;
    var noDisp;
    
    while( ( i = string.indexOf('(') ) !== -1 ) {
        j = string.indexOf(')');
        if ( j === -1 || n < i ) {
            log( 'Mismatching parenthesis :\n' + string );
            return false;
        }
        lines.push( string.slice( i+1, j ) );
        string = string.slice(j+1);
    }
    n = lines.length;
    for ( i=0; i<n; i++ ) {
        A = lines[i].split(',');
        for ( j=A.length-1; j>=0; j-- ) {
            A[j] = A[j].trim();
            if ( A[j].length === 0 ) {
                log( 'Missing argument :\n' + lines[i] ); 
            }
        }
        if ( A[0].charAt(0) === '#' ) { // stop parsing
            i = n;
            continue;
        }
        if ( A[0].charAt(0) === '@' ) { // don't show on sketch
            noSketch = ! noSketch;
            A[0] = A[0].substr(1);
        }
        if ( A[0].charAt(0) === '$' ) { // not displayed at all
            noDisp = true;
            A[0] = A[0].substr(1);
        }
        else {
            noDisp = false;
        }
        command = '_' + A.shift();
        if ( ! this[command] ) {
            log( 'Unkmown command : ' + command );
            //return false;
        }
        this.checkArgs(A);
        r = this[command](A);
        if (r) {
            if ( noSketch ) {
                r.sketchNoDisp = true;
            }
            if ( noDisp ) {
                r.noDisplay = true;
            }
            r.log();
        }
        else {
            return false;
        }
    }
    return true;
};

Construct.prototype._point = function(args) {
    return this.addVertex(args);
};

Construct.prototype._segment = function(args) {
    return this.addSegment(args);
};

Construct.prototype._droite = function(args) {
    return this.addLine(args);
};

Construct.prototype._traceH = function(args) {
    args.unshift('H');
    return this.addTrace(args);
};

Construct.prototype._traceF = function(args) {
    args.unshift('F');
    return this.addTrace(args);
};

Construct.prototype._inter = function(args) {
    return this.addInter(args);
};

Construct.prototype._parallele = function(args) {
    return this.addParallel(args);
};

Construct.prototype._referentiel = function(args) {
    return this.addFrame(args);
};

Construct.prototype._projection = function(args) {
    return this.addProjection(args);
};
