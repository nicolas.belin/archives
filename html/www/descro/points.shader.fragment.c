#version 100
precision mediump float;

varying vec4 v_color;

void main() {
  vec2 d = gl_PointCoord - vec2(0.5);
  float r = length(d);
  r = step( 0.5, r );
  gl_FragColor = r*v_color;
}
