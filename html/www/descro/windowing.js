"use strict";

/*    object.element : the element to be embedded in the window
      object.setSize : set the size of the previous element
*/

function Window(object) {
    var ctx;

    var color = '#808080';
    
    this.handleHeight = 20; // pixels
    this.plusSize = 30; // pixels
    this.span = document.createElement('span');
    this.handle = document.createElement('span');
    this.plus = document.createElement('canvas');
    this.object = object;
    this.span.classList.add('window');
    this.span.style.borderColor = color;
    this.handle.classList.add('handle');
    this.handle.style.backgroundColor = color;
    this.plus.classList.add('plus');
    this.handle.style.height = this.handleHeight + 'px';
    this.plus.width = this.plus.height = this.plusSize;
    this.plus.style.right = this.plus.style.bottom = '0px';
    this.plus.style.zIndex = '10000';
    object.element.classList.add('embedded');
    object.element.style.top = (this.handleHeight) + 'px';
    object.element.style.left = '0px';
    this.span.appendChild( this.handle );
    this.span.appendChild( this.plus );
    this.span.appendChild( object.element );
    util.trackPointer( this.handle, 'mousedown', this.grab.bind(this),
                       document, 'mouseup', this.ungrab.bind(this),
                       this.scroll.bind(this) );
    util.trackPointer( this.plus, 'mousedown', this.startResizing.bind(this),
                       document, 'mouseup', this.stopResizing.bind(this),
                       this.resize.bind(this) );
    ctx = this.plus.getContext('2d');
    ctx.fillStyle = color;
    ctx.scale( this.plusSize/6.0, this.plusSize/6.0 );
    ctx.fillRect( 1.5, 1.5, 1, 2.5 );
    ctx.fillRect( 1.5, 1.5, 2.5, 1 );
    ctx.fillRect( 0, 4.5, 4.6, 1 );
    ctx.fillRect( 4.5, 0.0, 1, 5.5 );
}

Window.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'mouseup':
    //    this.mouseUp(event);
        break;
    case 'mousedown':
      //  this.mouseDown(event);
        break;
    }
};

Window.prototype.setSize = function( w, h ) {
    this.span.style.width = w + 'px';
    this.span.style.height = h + 'px';
    this.object.setSize( w, h-this.handleHeight );
    this.width = w;
    this.height = h;
};

Window.prototype.moveTo = function( left, top ) {
    this.span.style.left = left + 'px';
    this.span.style.top = top + 'px';
    this.left = left;
    this.top = top;
};

Window.prototype.grab = function( x, y, t, event ) {
    this.span.style.zIndex = ++this.windowing.zIndex;
};

Window.prototype.ungrab = function( dx, dy, t, event ) {
    this.moveTo( this.left + dx, this.top + dy );
};

Window.prototype.scroll = function( dx, dy, t ) {
    this.moveTo( this.left + dx, this.top + dy );
};

Window.prototype.startResizing = function( x, y, t, event ) {
    this.span.style.zIndex = ++this.windowing.zIndex;
    this.object.element.hidden = 'hidden';
};

Window.prototype.stopResizing = function( dx, dy, t, event ) {
    this.object.element.hidden = null;
    this.setSize( this.width + dx, this.height + dy );
};

Window.prototype.resize = function( dx, dy, t ) {
    this.width += dx;
    this.height += dy;
    this.span.style.width = this.width + 'px';
    this.span.style.height = this.height + 'px';
};

function Windowing(container) {
    this.windows = [];
    this.container = container;
    this.zIndex = 0;
}

Windowing.prototype.addWindow = function(object) {
    var win = new Window(object);

    win.windowing = this;
    win.span.style.zIndex = ++this.zIndex;
    $('theBody').appendChild( win.span );
    this.windows.push(win);
};

Windowing.prototype.tabulate = function() {
    var i, win, uw, uh;
    var sep = 10;
    
    var rect = this.container.getBoundingClientRect();
    var x = rect.left;
    var y = rect.top;
    var w = rect.width;
    var h = rect.height;
    var n = this.windows.length;
    if ( w/h > 1 ) {
        uw = ( w - (n-1)*sep ) / n;
        uh = h;
    }
    else {
        uw = w;
        uh = ( h - (n-1) * sep ) / n;
    }
    
    for ( i=0; i<n; i++ ) {
        win = this.windows[i];
        win.moveTo( x + window.pageXOffset,
                    y + window.pageYOffset );
        if ( w >= h ) {
            x += uw + sep;
        }
        else {
            y += uh + sep;
        }
        win.setSize( uw, uh );
    }
};
