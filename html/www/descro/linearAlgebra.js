"use strict";

var vect = {};

vect.norm = function( x, y, z ) {
    return Math.sqrt( x*x + y*y + z*z );
};

vect.cross = function( x0, y0, z0, x1, y1, z1 ) {
    return [ y0*z1 - y1*z0,
             -x0*z1 + x1*z0,
             x0*y1 - x1*y0 ];
};

vect.dot = function(  x0, y0, z0, x1, y1, z1 ) {
    return x0*x1 + y0*y1 + z0*z1;
};

vect.triple = function( x0, y0, z0, x1, y1, z1, x2, y2, z2 ) {
    return ( y0*z1 - y1*z0 ) * x2 +
        ( -x0*z1 + x1*z0 ) * y2 +
        ( x0*y1 - x1*y0 ) * z2;
};

function Matrix( columnNr, rowNr, elements ) {
    this.n = columnNr;
    this.m = rowNr;
    this.a = elements;
}

Matrix.prototype.add = function(B) { // this + B
    var i, j;

    if ( this.n !== B.n || this.m !== B.m ) {
        console.log( "Matrix.add() : mismatching dimensions" );
        return;
    }
    for( i=0; i<this.n; i++ ) {
        for( j=0; j<this.m; j++ ) {
            this.a[ i*this.m + j ] += B.a[ i*this.m + j ];
        }
    }
};

Matrix.prototype.mult = function(B) { // this * B
    var i, j, k, s, C = [];

    if ( this.n !== B.m ) {
        console.log( "Matrix.mult() : mismatching dimensions" );
        return;
    }
    for ( i=0; i<B.n; i++ ) {
        for( j=0; j<this.m; j++ ) {
            s = 0;
            for ( k=0; k< this.n; k++ ) {
                s += this.a[ k*this.m + j ] * B.a[ i*B.m + k ];
            }
            C.push(s);
        }
    }
    this.a = C;
    this.n = B.n;
};

Matrix.prototype.multVec = function(v) { // this * v
    var i, j, k, s, C = [];

    if ( this.n !== v.length ) {
        console.log( "Matrix.multVec() : mismatching dimensions" );
        return;
    }
    for( j=0; j<this.m; j++ ) {
        s = 0;
        for ( k=0; k< this.n; k++ ) {
            s += this.a[ k*this.m + j ] * v[k];
        }
        C.push(s);
    }
    return C;
};

