#include <stdio.h>
#include <stdlib.h>
#include "std2npi.h"

int main( int argc , char **argv ){
  if( argc > 1 ){
    printf( "De la notation standard vers la notation polonaise inverse (NPI).\n\n" );
    printf( "std2npi re�oit une expression alg�brique par l'entr�e standard et la renvoie en notation post-fix�e vers la sortie standard.\n\n" );
    printf( "Exemple :\n" );
    printf( "2.45*f(x+y,-pi)+(a-1)^2 -> 2.45 x y + 0 pi - f * a 1 - 2 ^ +\n" );
    exit(0);
  }
  char S[SIZE];
  while( scanf( "%s", S ) != EOF ){
    npi_glob(S);
    printf( "%s\n", S );
  }
  return 0;
}
