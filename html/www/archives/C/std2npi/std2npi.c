#include <stdio.h>
#include <stdlib.h>
#include "std2npi.h"

//**************************************************************
//************ De la forme standard � la forme pr�fix�e ********
//**************************************************************
// r�vis� en 4/2009

//#define DEBUG1 1
//#define DEBUG2 1
#define ERROR(i) {     \
  fprintf( stderr, i ); \
  exit(1); \
}

#define SEP '|'

static char ops[] = "^/*-+,";	// par ordre de priorit�
static char pars[] = "()";
static char B[SIZE];          // chaine auxilliaire

static int card( const char* );	   
static void dup( const char*, char* );  
static void del( char*, int, int );
static int test( char, const char* );
static int find( const char*, const char* );
static void insert_char( char*, char, int );
static void insert_str( char*, const char*, int );
static void replace( char*, const char*, const char* );
static void isole( char* );
static void permute( char* );
static void test_syntaxe( char* );
static void npi_op( char*, char );
static void npi_loc( char* );

static int card( const char *A ){
  // taille de la chaine A (le '\0' final inclu)
  int i = -1;
  while( A[++i] );
  return i;
}

static void dup( const char *A, char *B ){
  // duplique A dans B
  int i=0;
  do
    B[i]=A[i];
  while( A[i++] );
}

static void del( char *A, int i, int j ){
  // retranche les caract�res de A[] dans l'intervalle [i;j[
  if( i>=j )
    return;
  while( A[i++] = A[j++] );
  A[i] = '\0';
}

static int test( char c, const char *A ){
  // teste si le caractere c est dans A
  int i = 0;
  char f;
  while( f = A[i++] )
    if( f == c)
      return 1;
  return 0;
}

static int find( const char *A, const char *F ){
  // trouve la premi�re occurence de la chaine F
  // dans la chaine A et renvoie sa position
  // ou -1 si pas trouv�e
  int i = 0;
  int j = 0;
  int p = card(F);
  int t;
  while( A[i+p-1] ){
    t = 0;
    for( j=0; j<p; j++ )
      t += ( A[i+j] != F[j] );
    if( !t )
      return i;
    i++;
  }
  return -1;
}

static void insert_char( char *A, char c, int i ){
  // insert le caractere c a la position i
  int j = i;	
  if( (i<0) || (i>card(A)) )
    return;
  dup( A, B );
  A[j++]=c;
  do
    A[j++]=B[i];
  while( B[i++] );
}

static void insert_str( char *A, const char *F, int i ){
  // insert la chaine F dans la chaine A � la position i
  char c;
  int j = 0;
  dup( A, B);
  while( c = F[j] ){
    A[i+j++] = c;
  }
  while( c = B[i] )
    A[j+i++] = c;
  A[j+i] = '\0';
}

static void replace( char *A, const char *C, const char *D ){
  // remplace dans A toutes les occurences
  // de la chaine C par la chaine D
  int i;
  int k = card(C);
  while( ( i=find( A, C ) ) != -1 ){
    del( A, i, i+k );
    insert_str( A, D, i );
  }
}

static void isole( char *A ){
  // entoure de SEP les blocs d'op�randes de A
  int i = 0;
  int j = 0;
  char c;
  int b = 0;
  dup( A, B );
  do{
    c = B[i];
    if( !test( c, ops ) && !test( c, pars ) && b==0 && c!=0 ){
	A[j++] = SEP;
      b = 1;
    }
    if( ( test( c, ops ) || test( c, pars ) ) && b==1 ){
	A[j++] = SEP;
      b = 0;
    }
    A[j++] = c;
  }
  while( B[i++] );
}

static void permute(char A[]){
  // Dans le cas d'une notation fonctionnelle, il faut
  // permuter la fonction et son argument
  char P[ SIZE ];
  char Ap[] = "";
  int i = 0;
  int k, i0, i1, t;
  char c, d;
  do{
    c = A[i];
    if( c==SEP && A[i+1]=='(' ){
      i1 = i + 2;
	k = t = 0;
      insert_char( A, ')', ++i );	// on rajoute des parentheses
      do{
        d = P[k++] = A[++i];
        t += ( d == '(' );
        t -= ( d == ')' );
      }
      while( t );
      P[k]='\0';
#ifdef DEBUG2
      printf( "    P = %s\n", P );
#endif // DEBUG2
      del( A, i1, i+1 );
#ifdef DEBUG2
      printf( "    A = %s\n", A );
#endif // DEBUG2
      insert_char( A, ',', i0 );
      insert_str( A, P, i0 );
      insert_char( A, '(', i0 );
      i = 0;
    }
    if( c == SEP )
      i0 = i;
  }
  while( A[i++] );	
}

static void test_syntaxe(char A[]){
  // V�rifie que :
  // - toute les caract�res de ops[] sont entour�s par des caract�res SEP
  // - autant de '(' que de ')'
  int i = 0;
  int p = 0;
  char b, c, d;
  do{
    c = A[i];
    p += ( c == '(' );
    p -= ( c == ')' );
    if( test( c, ops ) ){
	b = A[i-1];
      d = A[i+1];
      if( ( b!=SEP && b!=')' ) || ( d!=SEP && d!='(' ) )
        ERROR( "Erreur de syntaxe !\n" )
    }
  }
  while( A[i++] );
  if( p )
    ERROR( "Erreur de parenth�ses !\n" )
}

static void npi_op( char *A, char op ){
 // std->npi, pour la seule operation op
  char B[ SIZE ];
  int i = 0;
  int j = 0;
  char c;
  int w = 0;
  dup( A, B );
  do{
    c = B[i];
    if( c==op && B[i-1]==SEP && B[i+1]==SEP ){
	A[j-1] = ' ';
      i += 2;
      w = 1;
    }
    if( c==SEP && w==1 ){
	if( op != ',' ){		// la virgule n'apparait pas en ecriture npi
        A[j++] = ' ';
        A[j++] = op;
      }
      w=0;
    }
    A[j++] = B[i];
  }
  while( B[i++] );
}

static void npi_loc(char P[]){
  // std->npi, sans parenth�ses
  char par1[] = "(";	// suppression des parentheses
  char par2[] = ")";
  char nil = '\0';
  replace( P, par1, &nil );
  replace( P, par2, &nil );
#ifdef DEBUG2
  printf( "	%s\n", P );
#endif // DEBUG2
  int i = 0;
  while( ops[i] ){ 	// transformations des operations
    npi_op( P, ops[i++] );
#ifdef DEBUG2
    printf( "      %s\n", P );
#endif // DEBUG2
  }
}

void npi_glob( char *P ){
  // std->npi, fonction globale
#ifdef DEBUG1
  printf( "%s\n", P );
#endif //DEBUG1
  char space[] = " ";		// suppression des espaces
  char nil = '\0';
  replace( P, space, &nil );
  insert_char( P, '(', 0 );	// rajout de parenth�ses au d�but et � la fin
  insert_char( P, ')', card(P) );
#ifdef DEBUG1
  printf( "%s\n", P );
#endif //DEBUG1
  char C1[] = "(-";	      // r�solution du probleme du '-' unaire
  char C2[] = "(0-";
  replace( P, C1, C2 );
  char D1[] = ",-";
  char D2[] = ",0-";
  replace( P, D1, D2 );
#ifdef DEBUG1
  printf( "%s\n", P );
#endif //DEBUG1
  isole( P );			// encadrement des operandes par des SEP
#ifdef DEBUG1
  printf( "%s\n", P );
#endif //DEBUG1
  test_syntaxe( P );          // deux tests basiques de syntaxe
  permute( P );			// permutation du nom de la fonction avec son argument
#ifdef DEBUG1
  printf( "%s\n\n", P );
#endif //DEBUG1
  int i = 0;
  int j = 0;
  char R[SIZE];
  char Q[SIZE];
  char c;
  do{
    c = P[i++];
    if( c == '(' )
      j = 0;
    R[j++] = c;
    if( c == ')' ){
	R[j] = '\0';
      dup( R, Q );
      npi_loc( Q );
#ifdef DEBUG1
      printf( "%s -> %s\n", R, Q );
#endif //DEBUG1
      replace( P, R, Q );
      i = j = 0;
#ifdef DEBUG1
      printf( "%s\n\n", P );
#endif //DEBUG1
    }		
  }
  while( P[i] );
  del( P, 0, 1 );		// suppression des dernieres parentheses
  del( P, card(P)-1, card(P) );
}
