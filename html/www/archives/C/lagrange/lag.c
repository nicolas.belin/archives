// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...
#include <cstdlib>
//#include <ctime>	// random

#define screen_w 512	// screen width (largueur)
#define screen_h 512	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
const unsigned int f_rate = 40;		// renouvellement de l'affichage / seconde
const unsigned int pop = 2;		// nombre de particule	

// f f f f f f f f f
// f f f d d d f f f
// f f d d c d d f f
// f d d c b c d d f
// f d c b a b c d f
// f d d c b c d d f
// f f d d c d d f f
// f f f d d d f f f
// f f f f f f f f f 

//         *
//       * * *
//     * * * * *
//   * * * * * * *
// * * * * * * * * *
// f d c b a b c d f

const unsigned int f = 64;	// probabilite du "vide"
const unsigned int d = 1;
const unsigned int c = 2;
const unsigned int b = 4;
const unsigned int a = 8;

int pos[pop];					// positions
int vx[pop];					// abscisses des vitesses
int vy[pop];					// ordonnees des vitesses
unsigned int P[screen_w*screen_h];		// champs de pression
int palette2[2047];				// palette des couleurs

void print();
void vitesses();
void move();
void pix(Uint32* ecran);
void erase();
void border();
void init();
void mixer(unsigned int n);
int v_abs( int x);
void Make_palette(int *palette);                // creation d'une premiere palette de 1786 couleurs
void Scale_palette(int *palette,int *palette2); // Etirement de la palette precedente
                                                // pour avoir 2048=2^11 couleurs.


/*
extern "C" void dif_ASM				// routine diffusion, version ASM
	(	int a[screen_h][screen_w],	// diffusion de a dans b
		int b[screen_h][screen_w]
	);


*/	

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	Uint32 *bitmap;
	bitmap = (Uint32*)screen->pixels;	

	srand(unsigned(time(NULL)));

	// couleurs
        int palette[1785];
        Make_palette(palette);
        Scale_palette(palette,palette2);

	for(int i = 0; i < screen_w * screen_h; i++) P[i] = 0;

//	init();
	pos[0] = 100+256*512;
	pos[1] = 400+255*512;
	vx[0] = 1;
	vy[0] = 0;
	vx[1] = -1;
	vy[1] = 0;


	Uint32 t0,t1,t2,t3;	// temps
	unsigned int frame=0;	// compte les frames
	t0 = SDL_GetTicks();
	t2=SDL_GetTicks();

	while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN))
	{	print();
		border();
		vitesses();
		erase();
		move();

		pix(bitmap);
		if(SDL_GetTicks()-t2 > 1000/f_rate)
		{	//pix(bitmap);
			SDL_Flip(screen);
			t2 = SDL_GetTicks();
		}
		SDL_Delay(10);
		frame++;
	}		
	
{
	t1=SDL_GetTicks();
	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*frame/t << endl;
}	

	//cin.get();
	SDL_Quit();

	return 0;
}


// ****************************** fonctions **********************************

void print()
{	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		P[j] = i+1;
	}
}

void vitesses()
{	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		if(int k = P[j+screen_w-1] != 0) // n(-1;1)
		{	k -= 1;
			int nvx = vx[k] - vx[i];
			int nvy = vy[k] - vy[i];
			int dvx = (nvx-nvy)/2;
			int dvy = (nvy-nvx)/2;
			vx[i] += dvx;
			vy[i] += dvy;
			vx[k] -= dvx;
			vy[k] -= dvy;
		}

		if(int k = P[j+screen_w] != 0) // n(0;1)
		{	k -= 1;
			int nvy = vy[k] - vy[i];
			int dvy = nvy/2;
			vy[i] += dvy;
			vy[k] -= dvy;
		}

		if(int k = P[j+screen_w+1] != 0) // n(1;1)
		{	k -= 1;
			int nvx = vx[k] - vx[i];
			int nvy = vy[k] - vy[i];
			int dvx = (nvy+nvx)/2;
			int dvy = (nvx+nvy)/2;
			vx[i] += dvx;
			vy[i] += dvy;
			vx[k] -= dvx;
			vy[k] -= dvy;
		}

		if(int k = P[j+1] != 0) // n(1;0)
		{	k -= 1;
			int nvx = vx[k] - vx[i];
			int dvx = nvx/2;
			vx[i] -= dvx;
			vx[k] -= dvx;
		}
	}
}


void move()
{	for(int i = 0; i < pop ; i++)
	{	pos[i] += vx[i] + vy[i]*screen_w;
	}
}

void pix(Uint32* ecran)
{	for(int i = 0 ; i < pop ; i++)
		ecran[pos[i]] = 0xffffff;	
}

/*
void pix1(Uint32* ecran)
{	for(int i = 0 ; i < pop ; i++) ecran[pos[i]] = (0xffffff*i)/pop;	
}

void pix2(Uint32* ecran)
{	for(int i = 0 ; i < screen_w*screen_h ; i++) ecran[i] = palette2[((f-P[i]) << 5)-1]; 
}
*/

void init()
{	int L = screen_w;		// positions
	int l = screen_h;
	for(int i = 0 ; i < pop ; i++) pos[i] = rand() % (screen_w*screen_h);


/*	int n_w = int( sqrt( (pop*L) / l )); 
	int n_h = int( sqrt( (pop*l) / L )); 
	for( int x = 1 ; x <= n_w ; x++)
	for( int y = 1 ; y <= n_h ; y++)
	pos[x-1 + (y-1) * n_w] = (x*L) / (n_w+1) + ( (y*l) / (n_h+1) ) * L; 
*/
	for( int i = 0 ; i < pop ; i++)	// vitesses
	{	vx[i] = (rand() % 3) -1;
		vy[i] = (rand() % 3) -1;
	}
}

void border()
{	for(int i = 0 ; i < screen_w ; i++ )
	if( P[i] != 0) vy[P[i]-1] = v_abs(vy[P[i]-1]);

	for(int i = screen_w*(screen_h-1) ; i < screen_w*screen_h ; i++ )
	if( P[i] != 0) vy[P[i]-1] = - v_abs(vy[P[i]-1]);	 

	for(int i = 0 ; i < screen_h ; i++ )
	{	int j = i * screen_w;
		if( P[j] != 0) vx[P[j]-1] = v_abs( vx[P[j]-1] );
		if( P[j+screen_w-1] != 0) vx[P[j+screen_w-1]-1] = - v_abs(vx[P[j+screen_w-1]-1]);
	}
}

void erase()
{	for(int i = 0 ; i < pop ; i++ )
		P[pos[i]] = 0;
}

void mixer(unsigned int n)
{	const int r = 150;
	const int tr = 3000;
	const int xc = screen_w/2;
	const int yc = screen_h/2;
	const int epp = 3;
	float t=((n%tr)*2*PI)/tr;
	for(int i = -r ; i <= r ; i += 3)
	{	int xp = int(xc+i*cos(t));
		int yp = int(yc+i*sin(t));
		for(int k = -epp ; k <= epp ; k++)
		for(int l = -epp ; l <= epp ; l++)
		P[(k+yp)*screen_w+xp+l] = (k+yp)*screen_w+xp+l;
	}
}

void Make_palette(int *palette)
{       int c,i;
        for(i = 0; i<0x100 ; i++) palette[i]=i;
        c=0xff;
        for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
        c=0xffff;
        for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
        c=0xff00;
        for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
        c=0xffff00;
        for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
        c=0xff0000;
        for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
        c=0xff00ff;
        for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2)
{       for(int i = 0 ; i < 0x800 ; i++)
        {       palette2[i]=palette[(0x6f9*i)/0x7ff];
        }
}

int v_abs( int x)
{	if(x < 0) return -x;
}
