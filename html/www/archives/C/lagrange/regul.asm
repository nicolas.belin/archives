section .data

extern	palette2
screen_w equ 640	; largueur de l'ecran
screen_h equ 480	; longueur de l'ecran

section .text 

global dif_ASM,copbuf_ASM,dif_MMX
align 16

dif_ASM:mov esi,[esp+4]		;hot1
	add esi,4
	mov edi,[esp+8]		;hot2
	add edi,4
	mov ebx,screen_h-2
.ord:	add esi,screen_w*4
	add edi,screen_w*4
	mov ecx,(screen_w-2)*4
.absi:	mov eax,[esi+ecx+4]
	add eax,[esi+ecx-4]
	add eax,[esi+ecx+screen_w*4]
	add eax,[esi+ecx-screen_w*4]
	shr eax,2
	mov [edi+ecx],eax
	sub ecx,4
	jnz .absi
	dec ebx
	jnz .ord
	ret

dif_MMX:mov esi,[esp+4]		;hot1
;	add esi,4
	mov edi,[esp+8]		;hot2
;	add edi,4
	emms
	mov ebx,screen_h-2
.ord:	add esi,screen_w*4
	add edi,screen_w*4
	mov ecx,(screen_w-2)*4
.absi:	movq mm0,[esi+ecx+4]
	paddd mm0,[esi+ecx-4]
	paddd mm0,[esi+ecx+screen_w*4]
	paddd mm0,[esi+ecx-screen_w*4]
	pslld mm0,2
	movq [edi+ecx],mm0
	sub ecx,8
	jge .absi
	dec ebx
	jnz .ord
	emms
	ret	
		
copbuf_ASM:
	mov esi,[esp+4]
	add esi,screen_w*4
	mov edi,[esp+8]
	add edi,screen_w*4
	mov ecx,(screen_h-2)*screen_w
.cop:	mov eax,[esi+ecx*4]
	shr eax,18
	mov eax,[palette2+eax*4]
	mov [edi+ecx*4],eax
	loop .cop
	ret
