// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...
#include <cstdlib>
#include <ctime>

#define screen_w 400	// screen width (largueur)
#define screen_h 400	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
const unsigned int f_rate = 40;		// renouvellement de l'affichage / seconde
const unsigned int pop = 5000;		// nombre de particule	

// f f f f f f f f f
// f f f f c f f f f
// f f f c b c f f f
// f f c b a b c f f
// f f f c b c f f f
// f f f f c f f f f
// f f f f f f f f f 

const unsigned int f = 0x1 << 27;	// probabilite du "vide"
const unsigned int d = 1;
const unsigned int c = 3;
const unsigned int b = 3;
const unsigned int a = 4;
const unsigned int r = 3;

int pos[pop];					// liste des positions
unsigned int P[screen_w*screen_h];			// champs des probabilites

void next();
void mov();
void pix(Uint32* ecran);
void fill();
void border();
void mixer(unsigned int n);

/*
extern "C" void dif_ASM				// routine diffusion, version ASM
	(	int a[screen_h][screen_w],	// diffusion de a dans b
		int b[screen_h][screen_w]
	);


*/	

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	Uint32 *bitmap;
	bitmap = (Uint32*)screen->pixels;	

	srand(unsigned(time(NULL)));
	
	
	fill();	
	border();

	for(int i = 0 ; i < pop ; i++) pos[i] = screen_w*screen_h/3+10+screen_w*(i/200)+i%200 ;

	Uint32 t0,t1,t2,t3;	// temps
	unsigned int frame=0;	// compte les frames
	t0 = SDL_GetTicks();
	t2=SDL_GetTicks();

	while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN))
	{	fill();	
		border();
		next();
		pix(bitmap);
		mixer(frame);
		mov();


		if(SDL_GetTicks()-t2 > 1000/f_rate)
		{	SDL_Flip(screen);
			t2 = SDL_GetTicks();
		}
		//SDL_Delay(1);
		frame++;
	}		
	
{
	t1=SDL_GetTicks();
	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*frame/t << endl;
}	

	//cin.get();
	SDL_Quit();

	return 0;
}


// ****************************** fonctions **********************************

void next()
{	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		P[j] = 0;
		P[j+1] >>= b;
		P[j-1] >>= b;
		P[j+screen_w] >>= b;
		P[j-screen_w] >>= b;
		P[j+2] >>=c;
		P[j-2] >>=c;
		P[j+2*screen_w] >>=c;
		P[j-2*screen_w] >>=c;
		P[j+1+screen_w] >>=c;
		P[j+1-screen_w] >>=c;
		P[j-1+screen_w] >>=c;
		P[j-1-screen_w] >>=c;

		P[j+3] >>= d;
		P[j-3] >>= d;

		P[j-3*screen_w] >>=d;
		P[j-3*screen_w-1] >>=d;
		P[j-3*screen_w+1] >>=d;

		P[j-2*screen_w-2] >>=d;
		P[j-2*screen_w-1] >>=d;
		P[j-2*screen_w+1] >>=d;
		P[j-2*screen_w+2] >>=d;

		P[j-screen_w-3] >>=d;
		P[j-screen_w-2] >>=d;
		P[j-screen_w+2] >>=d;
		P[j-screen_w+3] >>=d;
// sym
		P[j+3*screen_w] >>=d;
		P[j+3*screen_w-1] >>=d;
		P[j+3*screen_w+1] >>=d;

		P[j+2*screen_w-2] >>=d;
		P[j+2*screen_w-1] >>=d;
		P[j+2*screen_w+1] >>=d;
		P[j+2*screen_w+2] >>=d;

		P[j+screen_w-3] >>=d;
		P[j+screen_w-2] >>=d;
		P[j+screen_w+2] >>=d;
		P[j+screen_w+3] >>=d;

		 
	}
}

void mov()
{	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		int s = P[j] + P[j+r] + P[j-r] + P[j+r*screen_w] + P[j-r*screen_w];	
		if(s > 0)
		{	int m = rand() % s ;
			int l = P[j+r];
			if( m < l ) pos[i] += 1;
			else {	l += P[j-r];
				if( m < l ) pos[i] -= 1;
				else {	l+= P[j+r*screen_w];
					if( m < l ) pos[i] += screen_w;
					else {	l += P[j-r*screen_w];
						if( m < l ) pos[i] -= screen_w;
					     }
				     }
			     }	
		}
	}
}

void pix(Uint32* ecran)
{	for(int i = 0 ; i < screen_w*screen_h ; i++) ecran[i] = 0; 
	for(int i = 0 ; i < pop ; i++) ecran[pos[i]] = 0xffffff;	
}

void pix1(Uint32* ecran)
{	for(int i = 0 ; i < pop ; i++) ecran[pos[i]] = (0xffffff*i)/pop;	
}

void border()
{	for(int i = screen_w ; i < 2*screen_w ; i++ ) P[i] = 0;
	for(int i = screen_w*(screen_h-2) ; i < screen_w*(screen_h-1) ; i++ ) P[i] = 0;
	for(int i = 0 ; i < screen_h-1 ; i++ )
	{	P[i*screen_w+1] = 0;
		P[i*screen_w+screen_w-2]=0;
	}
}

void fill()
{	for(int i = 0 ; i < screen_w*screen_h ; i++ ) P[i] = f;
}

void mixer(unsigned int n)
{	const int r = 150;
	const int tr = 3000;
	const int xc = screen_w/2;
	const int yc = screen_h/2;
	const int epp = 3;
	float t=((n%tr)*2*PI)/tr;
	for(int i = -r ; i <= r ; i += 3)
	{	int xp = int(xc+i*cos(t));
		int yp = int(yc+i*sin(t));
		for(int k = -epp ; k <= epp ; k++)
		for(int l = -epp ; l <= epp ; l++)
		P[(k+yp)*screen_w+xp+l] = 0;

		

/*
P[yp*screen_w+xp]=0;
		P[yp*screen_w+xp-1]=0;
		P[yp*screen_w+xp+1]=0;

		P[(yp+1)*screen_w+xp]=0;
		P[(yp+1)*screen_w+xp+1]=0;
		P[(yp-1)*screen_w+xp]=0;
		P[(yp-1)*screen_w+xp+1]=0;

		P[(yp-1)*screen_w+xp-1]=0;
		P[(yp+1)*screen_w+xp-1]=0;
*/
	}
}
