// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...
#include <cstdlib>
#include <ctime>

#define screen_w 512	// screen width (largueur)
#define screen_h 512	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
const unsigned int f_rate = 40;		// renouvellement de l'affichage / seconde
const unsigned int pop = 8000;		// nombre de particule	

// f f f f f f f f f
// f f f d d d f f f
// f f d d c d d f f
// f d d c b c d d f
// f d c b a b c d f
// f d d c b c d d f
// f f d d c d d f f
// f f f d d d f f f
// f f f f f f f f f 

//         *
//       * * *
//     * * * * *
//   * * * * * * *
// * * * * * * * * *
// f d c b a b c d f

const unsigned int f = 64;	// probabilite du "vide"
const unsigned int d = 1;
const unsigned int c = 2;
const unsigned int b = 4;
const unsigned int a = 8;

int pos[pop];					// liste des positions
unsigned int P[screen_w*screen_h];		// champs des probabilites
int palette2[2047];

void print();
void mov();
void pix(Uint32* ecran);
void fill();
void border();
void mixer(unsigned int n);
void Make_palette(int *palette);                // creation d'une premiere palette de 1786 couleurs
void Scale_palette(int *palette,int *palette2); // Etirement de la palette precedente
                                                // pour avoir 2048=2^11 couleurs.


/*
extern "C" void dif_ASM				// routine diffusion, version ASM
	(	int a[screen_h][screen_w],	// diffusion de a dans b
		int b[screen_h][screen_w]
	);


*/	

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	Uint32 *bitmap;
	bitmap = (Uint32*)screen->pixels;	

	// couleurs
        int palette[1785];
        Make_palette(palette);
        Scale_palette(palette,palette2);


	srand(unsigned(time(NULL)));
	
	
	fill();	
	border();

	for(int i = 0 ; i < pop ; i++) pos[i] = screen_w*(screen_h/3)+100+screen_w*(i/200)+i%200 ;

	Uint32 t0,t1,t2,t3;	// temps
	unsigned int frame=0;	// compte les frames
	t0 = SDL_GetTicks();
	t2=SDL_GetTicks();

	while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN))
	{	fill();	
		border();
		print();
		mixer(frame);
		mov();


		if(SDL_GetTicks()-t2 > 1000/f_rate)
		{	pix(bitmap);
			SDL_Flip(screen);
			t2 = SDL_GetTicks();
		}
		//SDL_Delay(1);
		frame++;
	}		
	
{
	t1=SDL_GetTicks();
	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*frame/t << endl;
}	

	//cin.get();
	SDL_Quit();

	return 0;
}


// ****************************** fonctions **********************************

void print()
{	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		P[j] -= a;
		P[j+1] -= b;
		P[j-1] -= b;
		P[j+screen_w] -= b;
		P[j-screen_w] -= b;
		P[j+2] -=c;
		P[j-2] -=c;
		P[j+2*screen_w] -=c;
		P[j-2*screen_w] -=c;
		P[j+1+screen_w] -=c;
		P[j+1-screen_w] -=c;
		P[j-1+screen_w] -=c;
		P[j-1-screen_w] -=c;

		P[j+3] -= d;
		P[j-3] -= d;

		P[j-3*screen_w] -=d;
		P[j-3*screen_w-1] -=d;
		P[j-3*screen_w+1] -=d;

		P[j-2*screen_w-2] -=d;
		P[j-2*screen_w-1] -=d;
		P[j-2*screen_w+1] -=d;
		P[j-2*screen_w+2] -=d;

		P[j-screen_w-3] -=d;
		P[j-screen_w-2] -=d;
		P[j-screen_w+2] -=d;
		P[j-screen_w+3] -=d;
// sym
		P[j+3*screen_w] -=d;
		P[j+3*screen_w-1] -=d;
		P[j+3*screen_w+1] -=d;

		P[j+2*screen_w-2] -=d;
		P[j+2*screen_w-1] -=d;
		P[j+2*screen_w+1] -=d;
		P[j+2*screen_w+2] -=d;

		P[j+screen_w-3] -=d;
		P[j+screen_w-2] -=d;
		P[j+screen_w+2] -=d;
		P[j+screen_w+3] -=d;

	}
}

void mov()
{	const int rc = 3;	// rayon de la croix de capteurs
	
	for(int i = 0 ; i < pop ; i++)
	{	int j = pos[i];
		int u = P[j-rc*screen_w];
		int d = P[j+rc*screen_w];
		int l = P[j-rc];
		int r = P[j+rc];

		if( u == d && u == l && u == r)
		{	int m = rand() % 4 ;
			if(m == 0) pos[i] += 1;
			if(m == 1) pos[i] -= 1;
			if(m == 2) pos[i] += screen_w;
			if(m == 3) pos[i] -= screen_w;
			goto nexti;
		}

		if( u >= d && u >= l && u >= r )	// dominance Up
		{	if( u > d && u > l && u > r ) { pos[i] -= screen_w; goto nexti; }
			if( u == d )
			{	if( l > r ) pos[i] -= 1;
				if( l < r ) pos[i] += 1;
				if( l == r)
				{	if( rand() % 2 == 0 ) pos[i]+= 1;
					else pos[i] -= 1;
				}
				goto nexti;
			}
			if( r == l ) { pos[i] -= screen_w; goto nexti; }
			if( u == r ) { pos[i] += 1-screen_w; goto nexti; }
			if( u == l ) { pos[i] -= 1+screen_w; goto nexti; }
		}

		if( d >= u && d >= l && d >= r )	// dominance Down
		{	if( d > u && d > l && d > r ) { pos[i] += screen_w; goto nexti; }
			if( u == d )
			{	if( l > r ) pos[i] -= 1;
				if( l < r ) pos[i] += 1;
				if( l == r)
				{	if( rand() % 2 == 0 ) pos[i]+= 1;
					else pos[i] -= 1;
				}
				goto nexti;
			}
			if( r == l ) { pos[i] += screen_w; goto nexti; }
			if( d == r ) { pos[i] += 1+screen_w; goto nexti; }
			if( d == l ) { pos[i] += screen_w-1; goto nexti; }
		}

		if( r >= u && r >= l && r >= d )	// dominance Right
		{	if( r > u && r > l && r > d ) { pos[i] += 1; goto nexti; }
			if( r == l )
			{	if( u > d ) pos[i] -= screen_w;
				if( u < d ) pos[i] += screen_w;
				if( u == d)
				{	if( rand() % 2 == 0 ) pos[i]+= screen_w;
					else pos[i] -= screen_w;
				}
				goto nexti;
			}
			if( u == d ) { pos[i] += 1; goto nexti; }
			if( r == u ) { pos[i] += 1-screen_w; goto nexti; }
			if( r == d ) { pos[i] += screen_w+1; goto nexti; }
		}

		if( l >= u && l >= r && l >= d )	// dominance Left
		{	if( l > u && l > r && l > d ) { pos[i] -= 1; goto nexti; }
			if( l == r )
			{	if( u > d ) pos[i] -= screen_w;
				if( u < d ) pos[i] += screen_w;
				if( u == d)
				{	if( rand() % 2 == 0 ) pos[i]+= screen_w;
					else pos[i] -= screen_w;
				}
				goto nexti;
			}
			if( u == d ) { pos[i] -= 1; goto nexti; }
			if( l == u ) { pos[i] -= 1+screen_w; goto nexti; }
			if( l == d ) { pos[i] += screen_w-1; goto nexti; }
		}

	nexti:;
	}
}

void pix2(Uint32* ecran)
{	for(int i = 0 ; i < screen_w*screen_h ; i++) ecran[i] = 0; 
	for(int i = 0 ; i < pop ; i++) ecran[pos[i]] = 0xffffff;	
}

void pix1(Uint32* ecran)
{	for(int i = 0 ; i < pop ; i++) ecran[pos[i]] = (0xffffff*i)/pop;	
}

void pix(Uint32* ecran)
{	for(int i = 0 ; i < screen_w*screen_h ; i++) ecran[i] = palette2[((f-P[i]) << 5)-1]; 
}

void border()
{	for(int i = screen_w ; i < 2*screen_w ; i++ ) P[i] = 0;
	for(int i = screen_w*(screen_h-2) ; i < screen_w*(screen_h-1) ; i++ ) P[i] = 0;
	for(int i = 0 ; i < screen_h-1 ; i++ )
	{	P[i*screen_w+1] = 0;
		P[i*screen_w+screen_w-2]=0;
	}
}

void fill()
{	for(int i = 0 ; i < screen_w*screen_h ; i++ ) P[i] = f;
}

void mixer(unsigned int n)
{	const int r = 150;
	const int tr = 3000;
	const int xc = screen_w/2;
	const int yc = screen_h/2;
	const int epp = 3;
	float t=((n%tr)*2*PI)/tr;
	for(int i = -r ; i <= r ; i += 3)
	{	int xp = int(xc+i*cos(t));
		int yp = int(yc+i*sin(t));
		for(int k = -epp ; k <= epp ; k++)
		for(int l = -epp ; l <= epp ; l++)
		P[(k+yp)*screen_w+xp+l] = 0;
	}
}

void Make_palette(int *palette)
{       int c,i;
        for(i = 0; i<0x100 ; i++) palette[i]=i;
        c=0xff;
        for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
        c=0xffff;
        for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
        c=0xff00;
        for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
        c=0xffff00;
        for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
        c=0xff0000;
        for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
        c=0xff00ff;
        for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2)
{       for(int i = 0 ; i < 0x800 ; i++)
        {       palette2[i]=palette[(0x6f9*i)/0x7ff];
        }
}

