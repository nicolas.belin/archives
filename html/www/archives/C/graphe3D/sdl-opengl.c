#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include "sdl-opengl.h"

#define PI 3.141592
#define RAD(X) (0.0174533*(X))

struct window init_SDL_GL( int w, int h, const char *caption ){
  if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0 ){
    fprintf( stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError() );
    exit(EXIT_FAILURE);
  }
  atexit(SDL_Quit);
  //SDL_EnableKeyRepeat( SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL );
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
 
  SDL_DisplayMode mode;
  SDL_GetCurrentDisplayMode( 0, &mode );

  struct window window;
  
  window.w = w > 0 ? w : 3 * mode.w / 4;
  window.h = h > 0 ? h : 3 * mode.h / 4;
  window.id = SDL_CreateWindow( caption,
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED,
                                window.w, window.h,
                                SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE );
  if ( window.id == NULL ){
    fprintf( stderr, "SDL: Could not create a window: %s\n", SDL_GetError() );
    exit(EXIT_FAILURE);
  }
  SDL_GLContext gl_context = SDL_GL_CreateContext( window.id );
  if ( gl_context == NULL ){
    fprintf( stderr, "SDL: Could not create OpenGL context: %s\n", SDL_GetError() );
    exit(EXIT_FAILURE);
  }
  /*
  if ( SDL_GL_MakeCurrent( window.id, gl_context ) != 0 ){
    fprintf( stderr, "SDL: Could not make OpenGL context current: %s\n", SDL_GetError() );
    exit(EXIT_FAILURE);
  }
  */
  return window;
}

void shape_window( const struct window *win ){
  glViewport( 0, 0, win->w, win->h );
}

void toggle_fullscreen( struct window *win ){
  static int is_fullscreen = 0;
  SDL_DisplayMode mode;
  Uint32 s;
  if ( is_fullscreen == 0 ){
    SDL_GetCurrentDisplayMode( 0, &mode );
    SDL_SetWindowDisplayMode( win->id, &mode ); 
    s = SDL_WINDOW_FULLSCREEN;
    is_fullscreen = 1;
  }
  else{
    s = 0;
    is_fullscreen = 0;
  }
  if ( SDL_SetWindowFullscreen( win->id, s ) != 0 ){
    fprintf( stderr, "SDL: Could not toggle fullscreen: %s\n", SDL_GetError() );
    exit(EXIT_FAILURE);
  }
  SDL_GetWindowSize( win->id, &win->w, &win->h );
}

void compile_shaders( struct rendering_context *c ){
  GLint length;
  GLint status;
  GLchar *infoLogP;
  char *vertex_shader_txt_ptr;
  char *fragment_shader_txt_ptr;
  char *geometry_shader_txt_ptr;
  GLuint vertex_shader;
  GLuint fragment_shader;
  GLuint geometry_shader;
  GLuint shaders_program;
  printf( "Compiling %s shader... ", c->name );
  vertex_shader_txt_ptr = load_text_file( c->vert_filename );
  vertex_shader = create_shader( vertex_shader_txt_ptr, GL_VERTEX_SHADER );
  free( vertex_shader_txt_ptr );
  fragment_shader_txt_ptr = load_text_file( c->frag_filename );
  fragment_shader = create_shader( fragment_shader_txt_ptr, GL_FRAGMENT_SHADER );
  free( fragment_shader_txt_ptr );
  int with_geom = c->geom_filename != NULL ? 1 : 0;
  if ( with_geom ){
    geometry_shader_txt_ptr = load_text_file( c->geom_filename );
    geometry_shader = create_shader( geometry_shader_txt_ptr, GL_GEOMETRY_SHADER );
    free( geometry_shader_txt_ptr );
  }
  printf( "linking : " );
  shaders_program = glCreateProgram();
  assert( shaders_program );
  glAttachShader( shaders_program, vertex_shader );
  glAttachShader( shaders_program, fragment_shader );
  if ( with_geom )
    glAttachShader( shaders_program, geometry_shader );
  glLinkProgram( shaders_program );
  glGetProgramiv( shaders_program, GL_INFO_LOG_LENGTH, &length );
  if ( length > 1 ) {
    infoLogP = malloc( length * sizeof( *infoLogP ) );
    assert( infoLogP );
    glGetProgramInfoLog( shaders_program, length, NULL, infoLogP );
    printf( "\n%s\n",infoLogP );
    free( infoLogP );
    glGetProgramiv( shaders_program, GL_LINK_STATUS, &status );
    if ( status == GL_FALSE )
      exit(EXIT_FAILURE);
  }
  else
    printf( "ok\n" );
  assert( shaders_program != 0 );
  c->program =  shaders_program;
}

void refresh_frame( const struct window *win, const struct rendering_context *c ){
  // mise � jour de l'affichage
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  c->draw( (void*)c );
  SDL_GL_SwapWindow( win->id );
}

char *load_text_file( const char *filename ){

  FILE *file_stream = fopen( filename, "rb" );
  if ( file_stream == NULL ){
    perror( "fopen()" );
    exit(EXIT_FAILURE);
  }
  if ( 0 != fseek( file_stream, 0, SEEK_END ) ){
    perror( "fseek()" );
    exit( EXIT_FAILURE );
  }

  long int size;
  size = ftell( file_stream );
  if ( size < 0 ){
    perror( "ftell()" );
    exit( EXIT_FAILURE );
  }

  if ( 0 != fseek( file_stream, 0, SEEK_SET ) ){
    perror( "fseek()" );
    exit( EXIT_FAILURE );
  }

  char *text = malloc( size + sizeof(char) );
  if ( text == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  size_t length_read;
  length_read = fread( text, 1, size, file_stream );
  if( length_read != size ){
    fprintf( stderr, "fread() : reading error\n" );
    exit(EXIT_FAILURE);
  }

  text[ size/sizeof(char) ] = '\0';

  if ( fclose(file_stream) == EOF ){ 
    perror( "fclose()" );
    exit(EXIT_FAILURE);
  }
  return text;
}

GLuint create_shader( const char *source, GLenum shader_type ){
  GLint status;
  GLint length;
  GLchar *infoLog;
  switch ( shader_type ){
  case GL_VERTEX_SHADER :
    printf( "Vertex : " );
    break;
  case GL_FRAGMENT_SHADER :
    printf( "Fragment : " );
    break;
  case GL_GEOMETRY_SHADER :
    printf( "Geometry : " );
    break;
  default :
    fprintf( stderr, "\nErreur : shader de type inconnu\n" );
    exit(1);
  }
  
  GLuint shader = glCreateShader( shader_type );
  assert( shader );
  glShaderSource( shader, 1, &source, NULL );
  glCompileShader( shader );
  glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &length );
  if ( length > 1 ) { 
    infoLog = malloc( length*sizeof(GLchar) );
    if ( infoLog == NULL ){
      perror( "malloc()" );
      exit(EXIT_FAILURE);
    }
    glGetShaderInfoLog( shader, length, NULL , infoLog );
    fprintf( stderr, "\n%s\n",infoLog );
    free( infoLog );
    glGetShaderiv( shader, GL_COMPILE_STATUS, &status );
    if ( status == GL_FALSE )
      exit(EXIT_FAILURE);
  }
  else
    printf( "ok  ");
  return shader;
}

void setup_rendering( struct rendering_context *c ){
  c->setup( (void*)c );
}

void print_gl_specs(){
  printf( "\nOPENGL specs :\n" );
  printf( "* Vendor : %s\n", (char*)glGetString( GL_VENDOR ) );
  printf( "* Renderer : %s\n", (char*)glGetString( GL_RENDERER ) );
  printf( "* Version : %s\n", (char*)glGetString( GL_VERSION ) );
  printf( "* Shading language version : %s\n", (char*)glGetString( GL_SHADING_LANGUAGE_VERSION ) );
  //printf( "* Extensions : %s\n", (char*)glGetString( GL_EXTENSIONS ) );
  printf("\n");
}

void save_color_buffer( int win_w, int win_h, const char *filename ){
  // l'image affich�e est sauvegard�e dans un fichier en rgb (3 floats par pixels)
  const char *default_filename = "color_buffer.raw";
  if ( filename == NULL )
    filename = default_filename;
  FILE *file_stream = fopen( filename, "wb" );
  if ( file_stream == NULL ){
    perror( "fopen()" );
    exit(EXIT_FAILURE);
  }
  void *dump_zone = malloc( 3 * win_w * win_h * sizeof(float) );
  if ( dump_zone == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }
  glReadBuffer( GL_FRONT_LEFT );
  glReadPixels( 0, 0, win_w, win_h, GL_RGB, GL_FLOAT, dump_zone );
  fwrite( dump_zone, sizeof(float), 3*win_w*win_h, file_stream );

  printf( "\n********* SAVE_COLOR_BUFFER **************************\n" );
  printf( "Color buffer has been saved in %s\n", filename );
  printf( "Picture geometry is %i x %i\n", win_w, win_h );
  printf( "Pixel format is RGB, 1 float per pixel\n" );
  printf( "********************************************************\n\n");

  if ( fclose(file_stream) == EOF ){ 
    perror( "fclose()" );
    exit(EXIT_FAILURE);
  }
  free( dump_zone );
}

void myOrtho( GLfloat *M,
              GLfloat l, GLfloat r,   // left, right
              GLfloat b, GLfloat t,   // bottom, top
              GLfloat n, GLfloat f ){ // near, far
  // T( -l, -b, -n ) -> Scale( 2.0/(r-l), 2.0/(t-b), 2.0/(f-n) )
  // -> T( -1, -1, 1 )
  assert( M != NULL && l != r && b != t && n != f );
  M[0] = 2.0 / ( r - l );
  M[1] = M[2] = M[3] = M[4] = 0.0;
  M[5] = 2.0 / ( t - b );
  M[6] = M[7] = M[8] = M[9] = 0.0;
  M[10] = 2.0 / ( n - f );
  M[11] = 0.0;
  M[12] = ( r + l ) / ( l - r );
  M[13] = ( t + b ) / ( b - t );
  M[14] = ( n + f ) / ( f - n );
  M[15] = 1.0;
}

void myFustrum( GLfloat *M,
                GLfloat a,   // angle d'ouverture vertical
                GLfloat p,   // proportion largeur/hauteur
                GLfloat e,   // c�te de l'oeil
                GLfloat n,   // near clipping plane (c�te)
                GLfloat f    // far clipping plane (c�te)
                ){
  // on regarde le long de l'axe Oz, vers les n�gatifs
  assert( M != NULL && e > n && n > f && p > 0.0 && n > f ); 
  GLfloat t = ( e-n ) * sin( RAD(a/2.0) );
  GLfloat r = t * p;
  n -= e;
  f -= e;
  M[0] = 1.0 / r;
  M[1] = M[2] = M[3] = M[4] = 0.0;
  M[5] = 1.0 / t;
  M[6] = M[7] = M[8] = M[9] = 0.0;
  M[10] = -( f+n ) / ( n*(f-n) );
  M[11] = 1.0 / n;
  M[12] = M[13] = 0;
  M[14] = ( f*(n+e) + n*(f+e) ) / ( n*(f-n ) );
  M[15] = -e/n;
}

void make_camera_matrix( GLfloat *M, float r, float phi, float theta ){
  // R( Oy, phi) -> R( Ox, theta) -> T( 0, 0, r )
  assert( M != NULL );
  float cos_phi = cos( phi );
  float cos_theta = cos( theta );
  float sin_phi = sin( phi );
  float sin_theta = sin( theta );
  // by columns :
  M[0] = cos_phi;
  M[1] = cos_theta * sin_phi;
  M[2] = sin_theta * sin_phi;
  M[3] = 0.0;
  M[4] = -sin_phi;
  M[5] = cos_theta * cos_phi;
  M[6] = sin_theta * cos_phi;
  M[7] = M[8] = 0.0;
  M[9] = -sin_theta;
  M[10] = cos_theta;
  M[11] = M[12] = M[13] = 0.0;
  M[14] = r;
  M[15] = 1.0;
}

void myLookAt( GLfloat *M, 
               GLfloat eyex, GLfloat eyey, GLfloat eyez,
               GLfloat centerx, GLfloat centery, GLfloat centerz,
               GLfloat upx, GLfloat upy, GLfloat upz){
  GLfloat wx = -centerx+eyex;	// a l'envers car la camera regarde vers
  GLfloat wy = -centery+eyey;	// les z negatifs
  GLfloat wz = -centerz+eyez;
  GLfloat N = sqrt( wx*wx + wy*wy + wz*wz ); //norme de w
  wx /= N;	// on normalise w
  wy /= N;
  wz /= N;
  GLfloat s = wx*upx + wy*upy + wz*upz; // produit scalaire de w avec up
  GLfloat vx = upx - s*wx; // on enleve dans up sa composante sur w;
  GLfloat vy = upy - s*wy; // ainsi, ils sont orthogonaux.
  GLfloat vz = upz - s*wz;
  N = sqrt( vx*vx + vy*vy + vz*vz ); // norme de v
  vx /= N; // on normalise v
  vy /= N;
  vz /= N;
  GLfloat ux = wz*vy - wy*vz; // produit vectoriel de v par w.
  GLfloat uy = -wz*vx + wx*vz ; // ainsi, (u,v,w) est une base
  GLfloat uz = wy*vx - wx*vy; //  orthonormee directe de l'espace
  
  // la matrice de la transformation (en colonne)
  // 1e colonne : 
  M[0] = ux;	// matrice inverse :
  M[1] = -(uy*wz - uz*wy);// transposee de la matrice des cofacteurs.
  M[2] = uy*vz - uz*vy;	// determinant=1 car c'est une isometrie
  M[3] = 0.0;
  // 2e colonne :
  M[4] = uy;
  M[5] = ux*wz - uz*wx;
  M[6] = -(ux*vz - uz*vx);
  M[7] = 0;
  // 3e colonne :
  M[8] = uz;
  M[9] = -(ux*wy - uy*wx);
  M[10] = ux*vy - uy*vx;
  M[11] = 0.0;
  // 4e colonne :
  M[12] = -eyex;
  M[13] = -eyey;
  M[14] = -eyez;
  M[15] = 1.0;
}
