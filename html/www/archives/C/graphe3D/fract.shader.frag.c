
#version 330

//smooth in vec3 v_normal;
smooth in vec3 frag_pos;

out vec4 frag_color;

vec4 q_product( vec4 q, vec4 p ){
  // produit de 2 quaternions
  float a = q.x * p.x - dot( q.yzw, p.yzw );
  vec3 b = q.x * p.yzw + p.x * q.yzw + cross( q.yzw, q.yzw );
  return vec4( a, b );
}

vec4 q_square( vec4 q ){
  // carré d'un quaternion
  float a = q.x * q.x - q.y * q.y - q.z * q.z - q.w * q.w;
  vec3 b = 2.0 * q.x * q.yzw; 
  return vec4( a, b );
}

void main() {

  /*
  const vec4 c = vec4( -0.2, 0.8, 0.0, 0.0 );
  vec3 l = v_pos * 1.2;
  vec4 q = q_square( vec4( l, 0.1 ) ) + c; 
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  q = q_square( q ) + c;
  
  gl_FragColor.rgb = q.xyz;
  gl_FragColor.a = 1.0;
  */

  
  vec3 p = frag_pos * 3.0;

  
  float s = sin( p.x * 3.0 );
  p.x *= p.y;
  p -= round( p );
  p *= 2.0;
  p.z = p.z * s;
  p *= p;
  p.y /= ( 1.0 + p.z );
  

  /*
  vec3 q = v_pos * 3.0;
  q -= round( q );
  q *= 2.0;
  q *= q;
  p.x = ( q.x + q.y + q.z );
  p.y = ( q.x * q.y + q.y * q.z + q.x * q.z );
  p.z = q.x * q.y * q.z;
  */

  frag_color.rgb = p.xyz;
  frag_color.a = 1.0;
  
}
