#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <assert.h>
#include <SDL2/SDL.h>	// API video, clavier, timer, souris...
#include <GL/gl.h>	// API OpenGL
#include "graphe.h"
#include "transform.h"
#include "sphere.h"
#include "sdl-opengl.h" // mes fonctions gl

#define PI 3.141592
#define RAD(X) (0.0174533*(X))
#define MAX(X,Y) ( (X) >= (Y) ? (X) : (Y) ) 
#define MIN(X,Y) ( (X) <= (Y) ? (X) : (Y) ) 
#define ABS(X) ( (X) >= 0 ? (X) : -(X) )
#define SQ(X) ((X)*(X))
#define MOD(X,N) ( (X) >= 0 ? (X)%(N) : (X)%(N)+N )

#define LAYER_NR 12 // 2+5*N^2 est le nombre de points de la sph�re initiale
#define REFRESH_PER_SECOND 25
#define ROT_V 25  // vitesse de rotation en secondes par tours
#define MOVING 1
#define STATIONARY 0
#define REFRESH_EVENT 1
#define DL1 0.00001 // pour calculer les d�riv�es
#define DL2 (DL1*1000) // pour calculer les d�riv�es secondes
#define NEWTON(R,XM,YM,ZM) SQ(R)/( SQ(x-(XM)) + SQ(y-(YM)) + SQ(z-(ZM)) )
#define P(X) pow(X, 0.45)

// angle d'ouverture de la cam�ra
static const float alpha = 45.0;

typedef struct vector{
  double x;
  double y;
  double z;
} vector_t;

enum boolean loop = YES;
enum boolean modified = YES;   // la vue a �t� modifi�e
enum boolean modifying = NO;   // les points sont en cours de modification
static uint vertices_nr;       // nombre de sommets
static uint edges_nr;          // nombre d'arr�tes
static vertex_t *vtx;          // les sommets
static GLfloat *coords;        // tableau des coordonn�es (au format float )
static GLfloat *normals;       // tableau des vecteurs normaux
static GLuint indices_buffer_name;
static GLuint vao_id;        // vertex array object

static struct polar camera;    // position de la cam�ra
static GLfloat rotation[4] = { 1.0, 0.0, 0.0, 1.0 };
static GLfloat projection[16];
static GLfloat move_camera[16];

static void sigterm_handler( int );
static double function( myfloat, myfloat, myfloat );
static void check_timer( SDL_TimerID );
static void start_video();
static Uint32 refresh_callback( Uint32 , void* );
static Uint32 global_rot( Uint32, void* );
static vector_t cross( vector_t, vector_t );
static myfloat dot( vector_t*, vector_t* );
static vector_t vect( vertex_t*, vertex_t* );
static myfloat dist( vertex_t*, vertex_t* );
static myfloat sq_dist( vertex_t*, vertex_t* );
static void get_normal_vector2( vertex_t* );
static void glide( double, vertex_t* );
static void readfile( void );
static void print();
static void set_view( const struct window* );
static void draw_outline( const void* );
static void draw_triangles( const void* );
static void setup_outline( void* );
static void setup_solid( void* );
static void setup_plastic( void* );

int main( int argc , char **argv ){
  signal( SIGTERM, &sigterm_handler );
  struct window win = init_SDL_GL( 0, 0, " f( x, y, z) = 0" );
  set_view( &win );
  print_gl_specs();
  struct rendering_context outfits[] = {
    { "Outline",
      &draw_outline,
      &setup_outline,
      "bare.shader.vert.c",
      NULL,
      "bare.shader.frag.c",
      -1, NULL, NULL },
    { "Solid",
      &draw_triangles,
      &setup_solid,
      "bare.shader.vert.c",
      NULL,
      "solid.shader.frag.c",
      -1, NULL, NULL },
    { "Plastic",
      &draw_triangles,
      &setup_plastic,
      "plastic.shader.vert.c",
      NULL,
      "plastic.shader.frag.c",
      -1, NULL, NULL },
    { "Fractal",
      &draw_triangles,
      &setup_plastic,
      "fract.shader.vert.c",
      NULL,
      "fract.shader.frag.c",
      -1, NULL, NULL },
    { "Dual",
      &draw_triangles,
      &setup_solid,
      "do_nothing.shader.vert.c",
      "dual.shader.geom.c",
      "dual.shader.frag.c",
      -1, NULL, NULL }};

  const int rendering_mode_nr = sizeof(outfits) / sizeof(outfits[0]);
  int i = 2;
  for ( i = 0; i < rendering_mode_nr; i++ )
    compile_shaders( outfits + i );
  printf("\n");
  int r_mode = 0;

  srand(time(NULL));

  struct polyhedron sphere_data = init_vertices( LAYER_NR, VERTICES_NR_MAX, DEG_MAX );
  vertices_nr = sphere_data.vertices_nr;
  vtx = sphere_data.vertices_p;

  transform( &vertices_nr, vtx, &glide );

  edges_nr = 0;
  for ( i=0; i<vertices_nr; i++ )
    edges_nr += vtx[i].deg;
  edges_nr /= 2;
  
  coords = malloc( 3 * vertices_nr * sizeof(GLfloat) );
  normals = malloc( 3 * vertices_nr * sizeof(GLfloat) );
  if ( coords == NULL || normals == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }
  int l;
  for ( i=0, l=0; i<vertices_nr; i++ ){
    normals[l] = vtx[i].nx;
    coords[l++] = vtx[i].x;
    normals[l] = vtx[i].ny;
    coords[l++] = vtx[i].y;
    normals[l] = vtx[i].nz;
    coords[l++] = vtx[i].z;
  }

  GLuint buffer_id[2];
  glGenBuffers( 2, buffer_id );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[0] );
  glBufferData( GL_ARRAY_BUFFER, 3*vertices_nr*sizeof(GLfloat), coords, GL_STATIC_DRAW );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[1] );
  glBufferData( GL_ARRAY_BUFFER, 3*vertices_nr*sizeof(GLfloat), normals, GL_STATIC_DRAW );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glGenBuffers( 1, &indices_buffer_name );
  
  // cr�ation du VAO
  glGenVertexArrays( 1, &vao_id );
  glBindVertexArray( vao_id );

  glEnableVertexAttribArray( 0 );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[0] );
  glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  
  glEnableVertexAttribArray( 1 );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[1] );
  glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indices_buffer_name );

  glBindVertexArray(0);   // fin

  camera.r = 0.0;
  camera.theta = 0.0;
  camera.phi = 0.0;  

  setup_rendering( outfits + r_mode );
  start_video();

  SDL_TimerID phi_timer = SDL_AddTimer( 1000/ROT_V, global_rot, NULL ); // rotation
  check_timer( phi_timer );

  SDL_Thread *modifying_thread = NULL;

  /*  
  modifying_thread = SDL_CreateThread( transform, "modifying_thread", (void*)(&data) );
  if( modifying_thread == NULL ){
    fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
  */

  enum boolean left_button_down = NO;
  SDL_Event event;
  while( loop == YES ){
    SDL_WaitEvent( &event );
    switch(event.type){
    case SDL_WINDOWEVENT:
      switch (event.window.event){
        case SDL_WINDOWEVENT_RESIZED :
        win.w = event.window.data1;
        win.h = event.window.data2;
        set_view( &win );
        modified = YES;
        break;
      case SDL_WINDOWEVENT_EXPOSED :
        modified = YES;
        break;
      }
      break;
    case SDL_USEREVENT :
      if( event.user.code == REFRESH_EVENT ){
        if( modified == YES || modifying == YES ){
          refresh_frame( &win, outfits + r_mode );
          modified = NO;
        }
      }
      break;
    case SDL_QUIT :
      loop = NO;
      break;
    case SDL_MOUSEBUTTONDOWN :
      switch( event.button.button ){
      case SDL_BUTTON_LEFT :
        left_button_down = YES;
        break;
      }
      break;
    case SDL_MOUSEBUTTONUP :
      switch( event.button.button ){
      case SDL_BUTTON_LEFT :
        left_button_down = NO;
        break;
      }
      break;
    case SDL_MOUSEMOTION :
      if ( left_button_down == YES ){
        camera.theta += 0.1 * (myfloat)event.motion.yrel;
        camera.phi += 0.1 * (myfloat)event.motion.xrel;
        modified = YES;
      }
      break;
    case SDL_MOUSEWHEEL :
      camera.r += event.wheel.y * 0.3;
      modified = YES;
      break;
    case SDL_KEYDOWN:
      switch( event.key.keysym.sym ){
      case SDLK_SPACE :
        r_mode = ( r_mode + 1 ) % rendering_mode_nr;
        setup_rendering( outfits + r_mode );
        modified = YES;
        break;
      case SDLK_ESCAPE :
      case SDLK_q :
        loop = NO;
        break;
      case SDLK_s :
        save_color_buffer( win.w, win.h, NULL );
        break;
      case SDLK_r :
        if( phi_timer == 0 ){
          phi_timer = SDL_AddTimer( 1000/REFRESH_PER_SECOND, global_rot, NULL );
          check_timer(phi_timer);
        }
        else{
          SDL_RemoveTimer( phi_timer );
          phi_timer = 0;
        }
        break;
      case SDLK_f :
        toggle_fullscreen( &win );
        set_view( &win );
        modified = YES;
      }
      break;
    }
  }
 
  SDL_WaitThread( modifying_thread, NULL );
  //  print();
  SDL_Quit();
  return 0;
}

static void sigterm_handler( int s ){
  loop = NO;
}

static double func_sphere_init(myfloat x, myfloat y, myfloat z ){
  return SQ(x) + SQ(y) + SQ(z) - 1;
}

static double function(myfloat x, myfloat y, myfloat z ){
  return sqrt(abs(x)) + sqrt(abs(y)) + sqrt(abs(z)) - 1;
}

static double function2(myfloat x, myfloat y, myfloat z ){
  // surface de niveau d'un champs newtonien
  
  myfloat xA = 0.6928;
  myfloat yA = 0.4;
  myfloat zA = 0;
  myfloat rA = 0.237;
  myfloat xB = -0.6928;
  myfloat yB = 0.4;
  myfloat zB = 0;
  myfloat rB = 0.237;
  myfloat xC = 0;
  myfloat yC = -0.8;
  myfloat zC = 0;
  myfloat rC = 0.237;
  
  /*
  myfloat xA = 0;
  myfloat yA = 0;
  myfloat zA = 0;
  myfloat rA = 3;
  myfloat xB = 1.1;
  myfloat yB = 0;
  myfloat zB = 0;
  myfloat rB = 0.7;  
  */
  return 1 - P(NEWTON( rA, xA, yA, zA )) - P(NEWTON( rB, xB, yB, zB ))- P(NEWTON( rC, xC, yC, zC ));
}

static double ft( double t, double x, double y, double z ){
  return (1-t)*func_sphere_init(x,y,z) + t*function2(x,y,z);
} 


static void check_timer( SDL_TimerID id ){
  if( id == 0 ){
    fprintf(stderr, "Impossible d'initialiser un timer: %s\n", SDL_GetError());
    exit(1);
  }
}

static void start_video(){
  check_timer( SDL_AddTimer( 1000/REFRESH_PER_SECOND, refresh_callback, NULL ) );
}

static Uint32 refresh_callback( Uint32 intervalle, void *not_used ){
  // fonction appel�e r�guli�rement pour donner l'ordre de rafraichir l'affichage
  SDL_Event event;
  event.user.type = SDL_USEREVENT;
  event.user.code = REFRESH_EVENT;
  SDL_PushEvent( &event );
  return intervalle;
}

static Uint32 global_rot( Uint32 intervalle, void *junk_p ){
  camera.phi += 360.0 / (ROT_V*REFRESH_PER_SECOND);
  modified = YES;
  return intervalle;
}

static vector_t cross( vector_t u, vector_t v ){
  // produit vectoriel de u par v
  vector_t w;
  w.x = u.y * v.z - v.y * u.z;
  w.y = -u.x * v.z + v.x * u.z;
  w.z = u.x * v.y - v.x * u.y;
  return w;
}

static myfloat dot( vector_t *u, vector_t *v ){
  // produit scalaire de *u et *v
  return u->x * v->x + u->y * v->y + u->z * v->z; 
}

static vector_t vect( vertex_t *A, vertex_t *B ){
  // retourne le vecteur de A � B
  vector_t u;
  u.x = B->x - A->x;
  u.y = B->y - A->y;
  u.z = B->z - A->z;
  return u;
}

static vector_t add_vect( vector_t *A, vector_t *B ){
  vector_t s;
  s.x = A->x + B->x;
  s.y = A->y + B->y;
  s.z = A->z + B->z;
  return s;
}

static myfloat dist( vertex_t *A, vertex_t *B ){
  // distance de A � B
  return sqrt( SQ( A->x - B->x ) + SQ( A->y - B->y ) + SQ( A->z - B->z ) );
}

static myfloat sq_dist( vertex_t *A, vertex_t *B ){
  // carr� de la distance de A � B
  return SQ( A->x - B->x ) + SQ( A->y - B->y ) + SQ( A->z - B->z );
}

static myfloat length( vector_t *u ){
  return sqrt( SQ(u->x) + SQ(u->y) + SQ(u->z) );
}

static void normalize( struct vector *u ){
  // normalise le vecteur u
  double l = sqrt( SQ(u->x) + SQ(u->y) + SQ(u->z) );
  u->x = u->x / l;
  u->y = u->y / l;
  u->z = u->z / l;
}

static struct vector gradf( struct vector m ){
  // gradient (normalis�) au point de coordonn�es m
  double fxyz = ft( 1.0, m.x, m.y, m.z);
  double dfx = ft( 1.0, m.x+DL1, m.y, m.z ) - fxyz;
  double dfy = ft( 1.0, m.x, m.y+DL1, m.z ) - fxyz;
  double dfz = ft( 1.0, m.x, m.y, m.z+DL1 ) - fxyz;
  double l = sqrt( SQ(dfx) + SQ(dfy) + SQ(dfz) );
  m.x = dfx/l;
  m.y = dfy/l;
  m.z = dfz/l;
  return m;
}

static struct vector vtx_nv( vertex_t *m ){
  // le verteur normal au sommet m
  struct vector n;
  n.x = m->nx;
  n.y = m->ny;
  n.z = m->nz;
  return n;
}

static struct vector vtx_pos( vertex_t *m ){
  // le verteur position au sommet m
  struct vector p;
  p.x = m->x;
  p.y = m->y;
  p.z = m->z;
  return p;
}

/*
static struct vector get_normal_vector( double x, double y, double z ){
  // calcul des coordonn�es du vecteur normal
  struct vector n;
  double fxyz = ft(x, y, z);
  double dfx = ft( x+DL1, y, z ) - fxyz;
  double dfy = ft( x, y+DL1, z ) - fxyz;
  double dfz = ft( x, y, z+DL1 ) - fxyz;
  double sq_l = SQ(dfx) + SQ(dfy) + SQ(dfz);
  double l = sqrt( sq_l );
  n.x = dfx/l;
  n.y = dfy/l;
  n.z = dfz/l;
  return n;
}
*/

static void get_normal_vector2( vertex_t *Q ){
  // calcul des coordonn�es du vecteur normal
  int n, j, i=0;
  int e = Q->deg;
  myfloat l;
  vertex_t *N, *P;
  vector_t u, v, w, sw;
  sw.x = sw.y = sw.z = 0;
  for( i=0; i<e; i++){
    P = Q->lk[i];
    if( i==0)
      N = Q->lk[e-1];
    else
      N = Q->lk[i-1];
    u = vect(Q,N);
    v = vect(Q,P);
    w = cross( u, v );
    sw = add_vect(&sw,&w);
  }
  l = sqrt( SQ( sw.x ) + SQ( sw.y ) + SQ( sw.z ) );
  Q->nx = sw.x / l;
  Q->ny = sw.y / l;
  Q->nz = sw.z / l;
}

static double triangle_area( vertex_t* A, vertex_t* B, vertex_t* C ){
  // calcule l'aire d'un triangle
  double ux = B->x - A->x;
  double uy = B->y - A->y;
  double uz = B->z - A->z;
  double vx = C->x - A->x;
  double vy = C->y - A->y;
  double vz = C->z - A->z;
  double wx = uy * vz - vy * uz;
  double wy = -ux * vz + vx * uz;
  double wz = ux * vy - vx * uy;
  return sqrt( SQ(wx) + SQ(wy) + SQ(wz) ) / 2.0;
}

static struct vector dGamma ( struct vector *m, struct vector *t ){
  // d�riv�e de l'application de Gauss en m selon le vecteur tangent t
  struct vector m1, m1_n, m_n, dG;
  m1.x = m->x + DL2 * t->x;
  m1.y = m->y + DL2 * t->y;
  m1.z = m->z + DL2 * t->z;
  m1_n = gradf(m1);
  m_n = gradf(*m);
  dG.x = (m1_n.x - m_n.x) / DL2;
  dG.y = (m1_n.y - m_n.y) / DL2;
  dG.z = (m1_n.z - m_n.z) / DL2;
  return dG;
}

static int modify_shape( void *data ){
  /*
  struct transform_data data;
  data.vertices_nr_p = &vertices_nr;
  data.vtx = vtx;
  data.glide = &glide;
  */
}

static void set_view( const struct window *win ){
  shape_window( win );
  myFustrum( projection, 40.0, (GLfloat)win->w / win->h, 3.0, 2.5, -10.0 ); 
}

static void draw_outline( const void* context ){
  int i, j, k;
  const struct rendering_context *c = context;

  make_camera_matrix( move_camera, camera.r, RAD(camera.phi), RAD(camera.theta) );

  glUniformMatrix4fv( c->uniforms_loc[0], 1, GL_FALSE, (GLfloat*)c->uniforms[0]  );
  glUniformMatrix4fv( c->uniforms_loc[1], 1, GL_FALSE, (GLfloat*)c->uniforms[1]  );

  glBindVertexArray( vao_id );
  glDrawElements( GL_LINES, 2*edges_nr, GL_UNSIGNED_INT, 0 );
  glBindVertexArray( 0 );
}

static void draw_triangles( const void* context ){
  const struct rendering_context *c = context;

  make_camera_matrix( move_camera, camera.r, RAD(camera.phi), RAD(camera.theta) );

  glUniformMatrix4fv( c->uniforms_loc[0], 1, GL_FALSE, (GLfloat*)c->uniforms[0]  );
  glUniformMatrix4fv( c->uniforms_loc[1], 1, GL_FALSE, (GLfloat*)c->uniforms[1]  );

  glBindVertexArray( vao_id );
  glDrawElements( GL_TRIANGLES, 3 * (2*edges_nr/3), GL_UNSIGNED_INT, 0 );
  glBindVertexArray( 0 );
}

static void setup_outline( void *context ){

  struct rendering_context *c = context;
  const int uniform_nr = 2;
  glClearColor( 0.0, 0.0, 0.0, 0.0 );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST );
  glLineWidth( 1.5 );
  glEnable( GL_BLEND );
  glEnable( GL_LINE_SMOOTH );
  glDisable( GL_DEPTH_TEST );
  glDisable( GL_CULL_FACE );
  assert( c->program > 0 );
  glUseProgram( c->program );
  c->uniforms = malloc( uniform_nr * sizeof( *(c->uniforms) ) );
  c->uniforms_loc = malloc( uniform_nr * sizeof( *(c->uniforms_loc) ) );
  if ( c->uniforms == NULL || c->uniforms_loc == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  c->uniforms[0] = (void*)projection;
  c->uniforms_loc[0] = glGetUniformLocation( c->program, "u_projection" ); 

  c->uniforms[1] = (void*)move_camera;
  c->uniforms_loc[1] = glGetUniformLocation( c->program, "u_camera" ); 

  GLuint *indices = malloc( 2 * edges_nr * sizeof(GLuint) );
  if ( indices == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  int i, k, l;
  vertex_t *vtxA, *vtxB;
  for ( i=0, l=0; i<vertices_nr; i++ ){
    vtxA = vtx + i;
    for ( k=0; k<vtxA->deg; k++ ){ 
      vtxB = vtxA->lk[k];
      if ( vtxB > vtxA ){
        indices[l++] = i;
        indices[l++] = vtxB - vtx;
      }
    }
  }
  glBindVertexArray( vao_id );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, 2 * edges_nr * sizeof(GLuint), indices, GL_STATIC_DRAW );
  //glDisableVertexAttribArray( 1 );
  glBindVertexArray(0);
  free( indices );
}

static void setup_solid( void *context ){

  struct rendering_context *c = context;
  const int uniform_nr = 2;
  glClearColor( 0.0, 0.0, 0.0, 0.0 );
  glClearDepth( -1.0 );
  glFrontFace( GL_CCW );
  glDepthFunc( GL_GEQUAL );
  glEnable( GL_DEPTH_TEST );
  glDisable( GL_CULL_FACE );
  assert( c->program > 0 );
  glUseProgram( c->program );
  c->uniforms = malloc( uniform_nr * sizeof( *(c->uniforms) ) );
  c->uniforms_loc = malloc( uniform_nr * sizeof( *(c->uniforms_loc) ) );
  if ( c->uniforms == NULL || c->uniforms_loc == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  c->uniforms[0] = (void*)projection;
  c->uniforms_loc[0] = glGetUniformLocation( c->program, "u_projection" ); 

  c->uniforms[1] = (void*)move_camera;
  c->uniforms_loc[1] = glGetUniformLocation( c->program, "u_camera" ); 

  GLuint *indices = malloc( 3 * (2*edges_nr/3) * sizeof(GLuint) );
  if ( indices == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  int i, l, k;
  vertex_t *vtxA, *vtxB, *vtxC;
  for ( i=0, l=0; i<vertices_nr; i++ ){
    vtxA = vtx+i;
    for ( k=0; k<vtxA->deg; k++ ){
      vtxB = vtxA->lk[k];
      vtxC = vtxA->lk[ (k+1) % vtxA->deg ];
      if ( vtxB>vtxA && vtxC>vtxA ){
        indices[l++] = i;
        indices[l++] = vtxB - vtx;
        indices[l++] = vtxC - vtx;
      }
    }
  }
  glBindVertexArray( vao_id );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, 3 * (2*edges_nr/3) * sizeof(GLuint), indices, GL_STATIC_DRAW );
  // glDisableVertexAttribArray( 1 );
  glBindVertexArray(0);
  free( indices );
}

static void setup_plastic( void *context ){

  struct rendering_context *c = context;
  const int uniform_nr = 2;
  glClearColor( 0.0, 0.0, 0.0, 0.0 );
  glClearDepth( -1.0 );
  glFrontFace( GL_CCW );
  glDepthFunc( GL_GREATER );
  glFrontFace( GL_CCW );
  glCullFace( GL_BACK );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_CULL_FACE );

  assert( c->program > 0 );
  glUseProgram( c->program );
  c->uniforms = malloc( uniform_nr * sizeof( *(c->uniforms) ) );
  c->uniforms_loc = malloc( uniform_nr * sizeof( *(c->uniforms_loc) ) );
  if ( c->uniforms == NULL || c->uniforms_loc == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  c->uniforms[0] = (void*)projection;
  c->uniforms_loc[0] = glGetUniformLocation( c->program, "u_projection" ); 

  c->uniforms[1] = (void*)move_camera;
  c->uniforms_loc[1] = glGetUniformLocation( c->program, "u_camera" ); 

  GLuint *indices = malloc( 3 * (2*edges_nr/3) * sizeof(GLuint) );
  if ( indices == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  int i, l, k;
  vertex_t *vtxA, *vtxB, *vtxC;
  for ( i=0, l=0; i<vertices_nr; i++ ){
    vtxA = vtx+i;
    for ( k=0; k<vtxA->deg; k++ ){
      vtxB = vtxA->lk[k];
      vtxC = vtxA->lk[ (k+1) % vtxA->deg ];
      if ( vtxB>vtxA && vtxC>vtxA ){
        indices[l++] = i;
        indices[l++] = vtxB - vtx;
        indices[l++] = vtxC - vtx;
      }
    }
  }

  glBindVertexArray( vao_id );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, 3 * (2*edges_nr/3) * sizeof(GLuint), indices, GL_STATIC_DRAW );
  glEnableVertexAttribArray( 1 );
  glBindVertexArray( 0 );

  free( indices );
}

static void glide( double t, vertex_t *A ){
  // Le point *A glisse jusqu'� la surface
  double x, y, z, fxyz, dfx, dfy, dfz, sq_l, l, dx, dy, dz;
  x = A->x;
  y = A->y;
  z = A->z;
  do {
    fxyz = ft(t, x, y, z );
    dfx = ft( t, x+DL1, y, z ) - fxyz;
    dfy = ft( t, x, y+DL1, z ) - fxyz;
    dfz = ft( t, x, y, z+DL1 ) - fxyz;
    sq_l = SQ(dfx) + SQ(dfy) + SQ(dfz);
    l = sqrt( sq_l );
    dx = -fxyz * dfx * DL1 / (sq_l );
    dy = -fxyz * dfy * DL1 / (sq_l );
    dz = -fxyz * dfz * DL1 / (sq_l );
    x += dx;
    y += dy;
    z += dz;
  }
  while ( ABS(fxyz) / l > 0.1 );
  A->nx = dfx/l;
  A->ny = dfy/l;
  A->nz = dfz/l;
  A->x = x+dx;
  A->y = y+dy;
  A->z = z+dz;
}

static void readfile( void ){	
  /*
#define SIZE_LINE_FILE 400
#define NEXT_ENTRY                                           \
  while( fgets( line_of_file, SIZE_LINE_FILE, thefile ) ){   \
    if( line_of_file[0] != '\n' && line_of_file[0] != '#' && line_of_file[0] != ' '  )  \
      break; } 

  FILE *thefile;
  char line_of_file[SIZE_LINE_FILE];
  float x = 0.0;
  if( !( thefile = fopen( filename, "r" ) ) ){
    fprintf( stderr, "Erreur : le fichier %s ne peut pas �tre ouvert\n", filename );
    exit(1);
  }
  NEXT_ENTRY;
  if( sscanf( line_of_file, "%f", &x ) == EOF )
    fprintf( stderr, "Erreur : probl�me avec la valeur de xmin\n" );
  else
  xmin = x;
  NEXT_ENTRY;
  if( sscanf( line_of_file, "%f", &x ) == EOF )
    fprintf( stderr, "Erreur : probl�me avec la valeur de xmax\n" );
  else
  xmax = x;
  NEXT_ENTRY;
  if( sscanf( line_of_file, "%f", &x ) == EOF )
    fprintf( stderr, "Erreur : probl�me avec la valeur de ymin\n" );
  else
  ymin = x;
  NEXT_ENTRY;
  if( sscanf( line_of_file, "%f", &x ) == EOF )
    fprintf( stderr, "Erreur : probl�me avec la valeur de ymax\n" );
  else
  ymax = x;
  NEXT_ENTRY;
  if( line_of_file[0] != '\n' && line_of_file[0] != '#' ){
    int i = 0;
    while( ( f_xy[i] = line_of_file[i] ) != '\n' )
      i++;
    f_xy[i] = '\0';
  }
  fclose( thefile );
  */
}

static void print(){
  int n, i, deg, count = 0;
  double d, dmax = 0;
  double dmin = HUGE_VAL;
  vertex_t *P;
  double m = 0;
  double Vl = 0;
  int deg_max = INT_MIN;
  int deg_min = INT_MAX;
  for( n=0; n<vertices_nr; n++ ){
    deg = vtx[n].deg;
    deg_max = MAX( deg_max, deg );
    deg_min = MIN( deg_min, deg );
    for( i=0; i<deg; i++ ){
      count++;
      P = vtx[n].lk[i];
      d = sq_dist( P, vtx+n );
      m += sqrt(d);
      Vl += d;
      if( d > dmax )
        dmax = d;
      if( d < dmin ){
        dmin = d;
      }
    }
  }
  m /= count;
  Vl /= count;
  Vl -= SQ(m);

  deg = deg_max - deg_min + 1;
  int *degs_p = malloc( deg*sizeof(int) );
  if ( degs_p == NULL ){
    fprintf( stderr, "Impossible d'allouer de la m�moire\n" );
    exit(1);
  }
  for( i=0; i<deg; i++ )
    degs_p[i] = 0;
  for( n=0; n<vertices_nr; n++ ){
    degs_p[ vtx[n].deg - deg_min ]++;
  }
  printf( "\nDEGRES DES SOMMETS :\n" );
  for( i=0; i<deg; i++ )
    printf( "Sommets de degr� %i : %i\n", deg_min + i, degs_p[i] );

  double A = 0.0;
  int edges_nr = 0;
  for ( i=0; i<vertices_nr; i++ )
    edges_nr += vtx[i].deg;
  edges_nr /= 2;

  for( n=0; n<vertices_nr; n++ )
    for( i=0; i<vtx[n].deg; i++ )
      A += triangle_area( vtx+n, vtx[n].lk[i], vtx[n].lk[ (i+1) % vtx[n].deg ] );
  A /= 3.0;
  printf( "\nAire : %f\n", A );
  printf( "Nombre de triangles : %i\n", 2*(edges_nr/3));

  printf( "\nSOMMETS :\n");
  printf( "Nombre initial (sph�re) : %i\n", 2+5*LAYER_NR*LAYER_NR);
  printf( "Nombre final : %i\n", vertices_nr);

  printf( "\n\nARRETES :\n");
  printf( "Nombre: %i\n", edges_nr );
  printf( "Longueur moyenne : %f\n", m );
  printf( "Ecart type : %f\n", sqrt(Vl) );
  printf( "Maximale : %f\n", sqrt(dmax) );
  printf( "Minimale : %f\n", sqrt(dmin) );
  printf( "Etendue : %f\n", sqrt(dmax)-sqrt(dmin) );
}

    /*
    // courbure de la surface
    vertex_t *vi;
    struct vector mi, mi_n, ei, mi_t1, mi_t2, dG1, dG2;
    double dG_11, dG_12, dG_21, dG_22;
    for( i=0; i<vertices_nr; i++){
      vi = vtx_list[i];
      mi = vtx_pos(vi);
      mi_n = gradf(mi);
      ei = vect( vi, vi->lk[0] );
      mi_t1 = cross( mi_n, ei );    // ( mi_t1, mi_t2 ) forme une base orthonorm�e
      normalize( &mi_t1 );          // du plan tangent au point mi
      mi_t2 = cross( mi_t1, mi_n ); 
      dG1 = dGamma( &mi, &mi_t1 );  // diff�rentielle de l'application de Gauss
      dG2 = dGamma( &mi, &mi_t2 );  // = endomorphisme de Weingarten
      dG_11 = dot( &dG1, &mi_t1 );   // coefficients de l'endomorphisme de Weingarten
      dG_12 = dot( &dG1, &mi_t2 );   // dans la base ( mi_t1, mi_t2 )
      dG_21 = dot( &dG2, &mi_t1 );
      dG_22 = dot( &dG2, &mi_t2 );
      
      vi->nx = ABS( dG_11 * dG_22 - dG_12 * dG_21 )/100;
      vi->ny = dG2.y;
      vi->nz = dG2.z;
    }    
    */
