#version 330

// inputs :
layout( triangles ) in;
in vec3 vertex_normal[3];
uniform mat4 u_projection;
uniform mat4 u_camera;

// outputs :
layout( triangle_strip, max_vertices = 9 ) out;
smooth out vec4 color_out;

const vec3 light = vec3( 0.0, 0.0, 2.0 ); 

void emit( vec4 m ){
  gl_Position = m;
  EmitVertex();
}

void draw_triangle( vec4 A, vec4 B, vec4 C ){
  vec3 n = cross( C.xyz - B.xyz, A.xyz - B.xyz );
  if ( n.z > 0 ) {
    n = normalize(n);
    color_out = vec4( n.z, n.z, n.z, 1.0 );
    emit( u_projection * A );
    emit( u_projection * B );
    emit( u_projection * C );
    EndPrimitive();
  
  }
}

void main(){
  vec4 A, B, C, D;
  vec3 n;
  const float t = 0.05;
  A = gl_in[0].gl_Position / gl_in[0].gl_Position.w;
  B = gl_in[1].gl_Position / gl_in[1].gl_Position.w;
  C = gl_in[2].gl_Position / gl_in[2].gl_Position.w;
  n = vertex_normal[0] + vertex_normal[1] + vertex_normal[2];
  D = A + B + C + t*vec4( n, 0 );
  D = D / D.w;

  mat4 P = u_camera;
  A = P * A;
  B = P * B;
  C = P * C;
  D = P * D;

  /*
  for ( int i=0; i < gl_in.length(); i++ ){
    m[i] = u_projection * u_camera * gl_in[i].gl_Position;
    m[i] /= m[i].w;
  }
  // culling :
  //  if ( cross( m[1].xyz - m[0].xyz, m[2].xyz - m[0].xyz ).z > -0.0003 ){
  */

  
  //  color_out = vec4( 1.0, 1.0, 1.0, 1.0 );
  draw_triangle( A, B, D );
       
  /// color_out = vec4( 1.0, 0.0, 0.0, 1.0 );
  draw_triangle( D, B, C );

  //color_out = vec4( 0.0, 1.0, 0.0, 1.0 );
  draw_triangle( A, D, C );
}
