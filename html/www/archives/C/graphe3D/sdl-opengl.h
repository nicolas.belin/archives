#ifndef MY_GL_FUNC_H
#define MY_GL_FUNC_H

typedef void (*draw_func_t)( const void* );
typedef void (*setup_GL_t)( void* );

struct window{
  int w;
  int h;
  SDL_Window *id;
};

struct rendering_context{
  const char* name;
  draw_func_t draw;
  setup_GL_t setup;
  const char *vert_filename;
  const char *geom_filename;
  const char *frag_filename;
  GLuint program;
  void **uniforms;
  GLint *uniforms_loc;
};

struct polar{
  // coordonnées polaires
  float r;     // distance à O(0,0)
  float theta; // angle par rapport au plan Oxy
  float phi;   // angle dans le plan Oxy, par rapport à Ox, orienté par Oz
};

extern struct window init_SDL_GL( int, int, const char* );
extern void shape_window( const struct window* );
extern void toggle_fullscreen( struct window* );
extern void compile_shaders( struct rendering_context* );
extern void refresh_frame( const struct window*, const struct rendering_context* );
extern char *load_text_file( const char* );
extern GLuint create_shader( const char*, GLenum );
extern void setup_rendering( struct rendering_context* );
extern void print_gl_specs();
extern void save_color_buffer( int, int, const char* );
extern void myOrtho( GLfloat*, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat );
extern void myFustrum( GLfloat*, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat );
extern void myLookAt( GLfloat*, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat,
                      GLfloat, GLfloat, GLfloat, GLfloat);
extern void make_camera_matrix( GLfloat*, float, float, float );

extern void myEyeBall(float eyex,float eyey,float eyez,float phi,float theta);

#endif // MY_GL_FUNC_H
