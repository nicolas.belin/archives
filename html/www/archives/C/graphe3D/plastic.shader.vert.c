#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 u_projection;
uniform mat4 u_camera;

smooth out vec3 v_normal;
smooth out vec3 v_pos;

void main() {
  
  vec4 m = vec4( position, 1.0 );
  vec4 n = vec4( normal, 0.0 );

  m = u_camera * m;
  n = u_camera * n;

  v_pos = m.xyz;
  v_normal = n.xyz;

  gl_Position = u_projection * m;
}
