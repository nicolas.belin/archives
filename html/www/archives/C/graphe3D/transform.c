#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include "graphe.h"
#include "sphere.h"
#include "transform.h"

#define __ASSERT_USE_STDERR 1
#include <assert.h>

#define MAX(X,Y) ( (X) >= (Y) ? (X) : (Y) ) 
#define MIN(X,Y) ( (X) <= (Y) ? (X) : (Y) ) 
#define ABS(X) ( (X) >= 0 ? (X) : -(X) )
#define SQ(X) ((X)*(X))
#define MOD(X,N) ( (X) >= 0 ? (X)%(N) : (X)%(N)+N )

#define STEP_NR 100           // nombre de surfaces intermédiaires
#define CURV_METRIC_COEFF 0.2 // coefficient de la courbure dans la métrique de la surface

static int vertices_nr;
static vertex_t *vtx;
static int *vtcs_list;  // un nombre par sommet
static double t = 0.0;  // coefficient barycentrique : t = 0 surface initiale, t = 1 surface finale

static void (*glide)( double, vertex_t* );

static void del_lk( vertex_t *A, int lki ){
  // supprimer le lien lki de la liste des sommets liés à *A
  int i;
  for( i=lki+1; i<A->deg; i++){
    A->lk[i-1] = A->lk[i];
  }
  if ( A->deg == 3 ){
    printf("degre < 3\n");
    exit(1);
  }
  A->deg--;
}

static int find_lk( vertex_t *A, vertex_t *lki ){
  // donne l'indice dans le tableau A->lk[] du lien vers *lki
  int i;
  for ( i=0; i < A->deg; i++ )
    if ( A->lk[i] == lki )
      return i;
  return -1;
  //  fprintf( stderr, "Erreur : un lien n'a pas été trouvé" );
  //  exit(1);
}

static int insert_lk_after( vertex_t *A, vertex_t *B, vertex_t *C ){
  // insert un lien vers *C dans A->lk[] apres le lien vers *B
  // retourne l'indice de C dans A->lk[]
  if ( A->deg == DEG_MAX ){
    fprintf( stderr, "\nTrop de liens pour le sommet %i\n", A-vtx );
    return -1;
    //exit(1);
  }
  int i = A->deg - 1;
  while ( A->lk[i] != B ){
    A->lk[i+1] = A->lk[i];
    i--;
    if ( i == -1 ){
      fprintf( stderr, "\nErreur dans la fonction insert_lk_after au sommet %i\n", A-vtx );
      return -1;
    }
  }
  i++;
  A->lk[i] = C;
  A->deg++;
  return i;
}

static int insert_lk_before( vertex_t *A, vertex_t *B, vertex_t *C ){
  // insert un lien vers *C dans A->lk[] avant le lien vers *B
  // retourne l'indice de C dans A->lk[] ou -1 si B n'est pas dans A->lk[]
  if ( A->deg == DEG_MAX ){
    fprintf( stderr, "\nTrop de liens pour le sommet %i\n", A-vtx );
    return -1;
  }
  int i = A->deg - 1;
  do{
    if ( i == -1 ){
      fprintf( stderr, "\nErreur dans la fonction insert_lk_before au sommet %i\n", A-vtx );
      return -1;
      // exit(1);
    }
    A->lk[i+1] = A->lk[i];
  }
  while ( A->lk[i--] != B );
  i++;
  A->lk[i] = C;
  A->deg++;
  return i;
}

static void del_edge( vertex_t *vtxA, int j ){
  // efface l'arrête de *A à *(A->lk[j])
  vertex_t *vtxB = vtxA->lk[j];
  int jB = find_lk( vtxB, vtxA ); 
  //  if ( vtxB->lk[jB] != vtxA ){
  //    return;
  //  }
  del_lk( vtxA, j );
  del_lk( vtxB, jB );
}

static int check_triangles( vertex_t *A ){
  int i, c = 0;
  vertex_t *B, *C;
  for ( i=0; i < A->deg; i++ ){
    B = A->lk[i];
    C = A->lk[ MOD(i+1,A->deg) ];
    if ( find_lk( C, B )==-1 || find_lk( B,C)==-1 )
      c++;
  }
  return c;
}

static void print_lks( vertex_t *A ){
  int i;
  //printf("\nSommet %i :\n", A-vtx );
  for ( i=0; i<A->deg; i++ ){
    // printf( "     %i -> %i\n", i, A->lk[i] - vtx );
  }
}

static double surface_metric( vertex_t *A, vertex_t *B ){
  // distance de A à B, en tenant compte de la courbure
  double l = sqrt( SQ( A->x - B->x ) + SQ( A->y - B->y ) + SQ( A->z - B->z ) );
  double ln = sqrt( SQ( A->nx - B->nx ) + SQ( A->ny - B->ny ) + SQ( A->nz - B->nz ) );
  return (1-CURV_METRIC_COEFF) * l + CURV_METRIC_COEFF * ln;
}

static int around_a_vertex( vertex_t *A, double lim_sup ){
  // modifications autour d'un sommet
  vertex_t *B, *C, *D, *new;
  int k, c = 0;
  double lAB, lDC, lAC, lAD, lBC, lBD;
  for ( k=0; k<A->deg; k++ ){
    C = A->lk[k];
    if ( C>A ){
      D = A->lk[ MOD(k+1,A->deg) ];
      B = A->lk[ MOD(k-1,A->deg) ];
      lAC = surface_metric( A, C );
      lBD = surface_metric( D, B );
      if (lAC>lBD){
        lAD = surface_metric( A, D );
        lAB = surface_metric( B, A );
        lDC = surface_metric( D, C );
        lBC = surface_metric( C, B );
        if ( lAB<lAC && lAD<lAC && lBC<lAC && lDC<lAC )
          if ( lAB+lBC>1.1*lBD && lAD+lDC>1.1*lBD && A->deg>3 ){
            // échange des diagonales d'un quadrilatère
            if ( D->deg < DEG_MAX && B->deg < DEG_MAX ){
              c++;
              del_edge( A, k );
              k--;
              insert_lk_after( D, A, B );
              insert_lk_before( B, A, D );
              *( vtcs_list + (A-vtx) ) = YES;
              *( vtcs_list + (B-vtx) ) = YES;
              *( vtcs_list + (C-vtx) ) = YES;
              *( vtcs_list + (D-vtx) ) = YES;
            }
          }   
      }
      else if ( lAC > lim_sup && vertices_nr < VERTICES_NR_MAX ){
        /*
        // création d'un point au milieu de la diagonale
        B = A->lk[ MOD(k-1,A->deg) ];
        D = A->lk[ (k+1) % A->deg ];
        assert( B != D );
        if ( D->deg < DEG_MAX && B->deg < DEG_MAX ){
          vertices_nr++;
          c++;
          new = vtx + vertices_nr - 1;
          new->deg = 4;
          new->x = (A->x + C->x) / 2.0;
          new->y = (A->y + C->y) / 2.0;
          new->z = (A->z + C->z) / 2.0;
          new->lk[0] = A;
          new->lk[1] = B;
          new->lk[2] = C;
          new->lk[3] = D;
          A->lk[k] = new;
          insert_lk_after( B, C, new );
          C->lk[ find_lk(C, A) ] = new;
          insert_lk_after( D, A, new );
          //(*glide)( t, new );
          vtcs_list[ A-vtx ] = YES;
          vtcs_list[ B-vtx ] = YES;
          vtcs_list[ C-vtx ] = YES;
          vtcs_list[ D-vtx ] = YES;
          vtcs_list[ new-vtx ] = YES;
        }
        */
      }
    }
  }
  return c;
}

static int rewire( double avg_sup ){
  printf("rewiring\n");
  // modifie le polyèdre
  // divise les arrêtes de longueurs supérieures à moyenne*avg_sup
  vertex_t *A, *B;
  int i, k, c, count = 0;
  double average = 0.0; // moyenne des longueurs des arrêtes
  c = 0; // moyenne des longueurs des arrêtes

  for ( i=0; i<vertices_nr; i++ ){
    A = vtx + i;
    for ( k=0; k<A->deg; k++ ){
      B = A->lk[k];
      if ( B>A ){
        average += surface_metric( A, B );
        c++;
      }
    }
  }
  average /= c;

  for ( i=0; i<vertices_nr; i++ )
    vtcs_list[i] = YES; // tous les sommets sont a vérifier

  printf( "average : %f\n", average );

  do{
    printf( "count = %i\n", count );

    c = 0;
    count++;
    for ( i=0; i<vertices_nr; i++ ){
      printf( "vtc %i - ", i );
      if ( vtcs_list[i] == YES ){
        if ( around_a_vertex( vtx+i, average*avg_sup ) == 0 ){
          vtcs_list[i] = NO;
          c++;
        }
      }
    }
  }
  while ( c > 0 && count<1000 && vertices_nr <= VERTICES_NR_MAX );
  if ( vertices_nr == VERTICES_NR_MAX )
    printf( "Le nombre maximum de sommets a ete atteint\n" );
  return count;
}


int transform( int *vtces_nr_ptr, vertex_t *vtces, void (*glide)( double, vertex_t* ) ){
  modifying = YES;
  vertices_nr = *(vtces_nr_ptr);
  vtx = vtces;

  int i, step;
  vtcs_list = malloc( VERTICES_NR_MAX*sizeof(int) );
  if ( vtcs_list == NULL ){
    fprintf( stderr, "Impossible d'allouer de la mémoire\n" );
    exit(1);
  }

  for( step=1; step<=STEP_NR; step++ ){
    t = ((double)step) / STEP_NR;
    for ( i=0; i<vertices_nr; i++ )
      (*glide)( t, vtx+i );
    printf(" t=%i\n", step );
        rewire(1.5);
  }
  
  vertex_t *vtxA, *vtxB, *vtxC, *vtxD;
  int k, j, c = 0;

  printf( "pre-vertices_nr : %i\n", vertices_nr );

  //rewire(1.5);

  
  for ( i=0; i<vertices_nr; i++ ){
    vtxA = vtx + i;
    if ( vtxA->deg == 3 )
      printf( "\n deg=3\n" );
    for ( k=0; k<vtxA->deg; k++ ){
      vtxB = vtxA->lk[k];
      if ( vtxB == vtxA )
        printf( "\nA=B !\n" );
    }
  }
  
  printf( "post-vertices_nr : %i\n", vertices_nr );

  j = 0;
  for ( i=0; i<vertices_nr; i++ ){
    vtxA = vtx + i;
    for ( k=0; k<vtxA->deg; k++ ){
      vtxB = vtxA->lk[k];
      vtxC = vtxA->lk[ MOD(k+1,vtxA->deg) ];
      if ( find_lk( vtxB, vtxC )==-1 || find_lk( vtxC, vtxB )==-1 ){
        j++;
        printf( "\nProbleme autour du sommet : %i\n", vtxA-vtx );
      }
      
    }
  }
  printf( "\nNon triangles : %i\n", j );
  
  *(vtces_nr_ptr) = vertices_nr;
  free( vtcs_list );

  modifying = NO;
  modified = YES;
  return 0;
}
