#version 330


smooth in vec4 color_out;

out vec4 frag_color;

void main() {

  frag_color = color_out;

}
