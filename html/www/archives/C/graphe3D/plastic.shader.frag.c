#version 330

#define W 2
#define H 2
#define A 1.0
#define B 0.7

const float pattern[W*H] =
  float[]( A, B,
          B, A );

smooth in vec3 v_normal;
smooth in vec3 v_pos;

out vec4 color;

void main() {

  const vec3 ambient_light = vec3( 0.0, 0.0, 1.0 );
  const vec3 light1_pos = vec3( 0.0, 0.0, 2.5 );
  const vec3 light1_color = vec3( 1.0, 1.0, 1.0 );

  vec3 m = normalize( light1_pos - v_pos );
  vec3 n = normalize( v_normal );
  float d = dot( m, n );
  float c = smoothstep( 0.99, 1.005, d );

  
  int i, j, k;
  float p = 1.0;
  vec3 r = reflect( -m, n );
  r *= 5.0;
  i = int( floor( r.x ) );
  j = int( floor( r.y ) );
  k = int( floor( r.z ) );
  i += j+k;
  p = mod( float(i), 2.0 );
  p = mix( A, B, p );

  /*
  //  r.z = ( r.z + 1.0 ) / 2.0;
  if ( r.z > 0.01 ){
    r /= r.z;
    i = uint( mod( r.x, W ) );
    j = uint( mod( r.y, H ) );
    p = pattern[ i + j*uint(W) ];
  }
  //else
  //  p = B; 
  */

  d *= p;
  
  color.rgb = mix( vec3( 0.0, 0.0, d ), ambient_light, 0.3 ) + c * light1_color;
  color.a = 1.0;
}
