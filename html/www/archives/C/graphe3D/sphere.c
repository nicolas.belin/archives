#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "graphe.h"
#include "sphere.h"

#define RAD(X) (0.0174533*(X)) // conversion des degrés en radians

static int vertices_nr;
static vertex_t *vtx;

static vertex_t** get_links_mem( int k ){
  //réserve de la mémoire pour k pointeurs vers des points
  vertex_t **P = malloc( k * sizeof(vertex_t*) );
  if ( P==NULL ){
    fprintf( stderr, "Impossible d'allouer de la mémoire\n" );
    exit(1);
  }
  return P;
}

static vertex_t* get_vertices_mem( int k ){
  //réserve de la mémoire pour k points
  vertex_t *P = malloc( k * sizeof(vertex_t) );
  if ( P==NULL ){
    fprintf( stderr, "Impossible d'allouer de la mémoire\n" );
    exit(1);
  }
  return P;
}

static int mod5i( int m, int i ){
  if (m<0)
    m += 5*i;
  return m%(5*i);
}

static vertex_t** make_hemisphere( int layer_nr, vertex_t *vxt_p ){
  // construit une demi-sphere à layer_nr couche, en plaçant les point en vxt_p
  int i, j, k, m;
  vertex_t **vtxu = get_links_mem( layer_nr+1 );
   
  // point au sommet : centre du pentagone
  vtxu[0] = vxt_p++; // up
  vtxu[0][0].x = 0.0;
  vtxu[0][0].y = 0.0;
  vtxu[0][0].z = 1.0;
  vtxu[0][0].deg = 5;
  //vtxu[0][0].lk = get_links_mem( vtxu[0][0].deg );
  
  // les autres points : hexagones

  // coordonnées, allocation mémoire
  for ( i=1; i<layer_nr; i++ ){
    vtxu[i] = vxt_p;
    vxt_p += 5*i;
    for ( j=0; j<5; j++ )
      for ( k=0; k<i; k++ ){
        m = i*j + k;
        vtxu[i][m].x = cos(RAD(90.0 - i*90.0/layer_nr))*cos(RAD(m*360.0/(5*i)));
        vtxu[i][m].y = cos(RAD(90.0 - i*90.0/layer_nr))*sin(RAD(m*360.0/(5*i)));
        vtxu[i][m].z = sin(RAD(90.0 - i*90.0/layer_nr));
        vtxu[i][m].deg = 6;
        //vtxu[i][m].lk = get_links_mem( vtxu[i][m].deg );
      }
  }

  // arrêtes
  for( j=0; j<5; j++ ){
    vtxu[0][0].lk[j] = vtxu[1] + j;
  }

  for ( i=1; i<layer_nr; i++ ){
    for ( j=0; j<5; j++ ){
      for ( k=0; k<i; k++ ){
        m = i*j + k;
        vtxu[i][m].lk[0] = vtxu[i] + mod5i( m+1, i );
        if ( k==0 ){
          vtxu[i][m].lk[1] = vtxu[i-1] + (i-1)*j;
          vtxu[i][m].lk[2] = vtxu[i] + mod5i( m-1, i );
          if ( i != layer_nr-1 ){ 
            vtxu[i][m].lk[3] = vtxu[i+1] + mod5i( (i+1)*j - 1, i+1 );
            vtxu[i][m].lk[4] = vtxu[i+1] + mod5i( (i+1)*j, i+1 );
            vtxu[i][m].lk[5] = vtxu[i+1] + mod5i( (i+1)*j + 1, i+1 );
          }
        }
        else{
          vtxu[i][m].lk[1] = vtxu[i-1] + mod5i( (i-1)*j + k, i-1 );
          vtxu[i][m].lk[2] = vtxu[i-1] + mod5i( (i-1)*j + k-1, i-1 );
          vtxu[i][m].lk[3] = vtxu[i] + mod5i( m-1, i );
          if ( i != layer_nr-1 ){ 
            vtxu[i][m].lk[4] = vtxu[i+1] + mod5i( (i+1)*j + k, i+1 );
            vtxu[i][m].lk[5] = vtxu[i+1] + mod5i( (i+1)*j + k+1, i+1 );
          }
        }
      }
    }
  }
  return vtxu;
}

static void links_transfer( vertex_t *old, vertex_t *new ){
  // parmi les points autour de new, transfert vers new tous les liens vers old
  int k,l;
  for( k=0; k<new->deg; k++ )
    for( l=0; l<(new->lk[k])->deg; l++ )
      if( (new->lk[k])->lk[l] == old )
        (new->lk[k])->lk[l] = new;
}

static void swap_2_links( vertex_t **lk1, vertex_t **lk2 ){
  vertex_t *lk_tmp;
  lk_tmp = *lk1;
  *lk1 = *lk2;
  *lk2 = lk_tmp;
}

struct polyhedron init_vertices( int layer_nr, int vertices_nr_max, int deg_max ){
  // Initialisation des points de la sphere
  // layer_nr : nombre de couches d'une demi-sphere (sans le sommet)
  // le nombre de sommets de la sphère sera 2 + 5 * layer_nr ^ 2
  // init_vertices alloue de la mémoire pour vertices_nr_max sommets,
  // avec pour chaque sommet deg_max pointeurs vers d'autres sommets
  int i, j, k, m;
  int hemisphere_vertices_nr = 1 + 5 * (layer_nr*(layer_nr-1)) / 2;
  int equator_vertices_nr = 5 * layer_nr;
  vertices_nr = 2*hemisphere_vertices_nr + equator_vertices_nr;
  if( vertices_nr > vertices_nr_max ){
    fprintf( stderr, "\nErreur : la sphère initiale a plus (%i) de vertices_nr_max (%i) sommets\n",
             vertices_nr, vertices_nr_max);
    exit(1);
  }
  vtx = get_vertices_mem( vertices_nr_max );
  for ( i=0; i<vertices_nr_max; i++ ){
    vtx[i].lk = get_links_mem( deg_max );
    vtx[i].deg = 0;
  }

  vertex_t **vtxu, **vtxd;
  vtxu = make_hemisphere( layer_nr, vtx );
  vtxd = make_hemisphere( layer_nr, vtx+hemisphere_vertices_nr );
  
  // symétrie par rapport au plan Oxy pour la demi-sphere inférieure
  for( i=hemisphere_vertices_nr; i<2*hemisphere_vertices_nr; i++ ){
    vtx[i].z = -vtx[i].z;
  }
  
  // équateur
  vertex_t *equa; 
  equa = vtx + 2*hemisphere_vertices_nr;
  i = layer_nr;
  for ( j=0; j<5; j++ )
    for ( k=0; k<i; k++ ){
      m = i*j + k;
      equa[m].x = cos(RAD(m*360.0/(5*i)));
      equa[m].y = sin(RAD(m*360.0/(5*i)));
      equa[m].z = 0;
      if ( k==0 )
        equa[m].deg = 4;
      else
        equa[m].deg = 6;
      //equa[m].lk = get_links_mem( equa[m].deg );
    }

  // collage des deux demi-sphères 
    for ( j=0; j<5; j++ )
      for ( k=0; k<i; k++ ){
        m = i*j + k;
        equa[m].lk[0] = equa + mod5i( m+1, i );
        if ( k==0 ){
          equa[m].lk[1] = vtxu[i-1] + (i-1)*j;
          equa[m].lk[2] = equa + mod5i( m-1, i );
          equa[m].lk[3] = vtxd[i-1] + (i-1)*j;
        }
        else{
          equa[m].lk[1] = vtxu[i-1] + mod5i( (i-1)*j + k, i-1 );
          equa[m].lk[2] = vtxu[i-1] + mod5i( (i-1)*j + k-1, i-1 );
          equa[m].lk[3] = equa + mod5i( m-1, i );
          equa[m].lk[4] = vtxd[i-1] + mod5i( (i-1)*j + k-1, i-1 );
          equa[m].lk[5] = vtxd[i-1] + mod5i( (i-1)*j + k, i-1 );
        }
      }
    
    i = layer_nr-1;
    for ( j=0; j<5; j++ )
      for ( k=0; k<i; k++ ){
        m = i*j + k;
        if ( k==0 ){
          vtxu[i][m].lk[3] = equa + mod5i( (i+1)*j - 1, i+1 );
          vtxd[i][m].lk[3] = equa + mod5i( (i+1)*j - 1, i+1 );
          vtxu[i][m].lk[4] = equa + mod5i( (i+1)*j, i+1 );
          vtxd[i][m].lk[4] = equa + mod5i( (i+1)*j, i+1 );
          vtxu[i][m].lk[5] = equa + mod5i( (i+1)*j + 1, i+1 );
          vtxd[i][m].lk[5] = equa + mod5i( (i+1)*j + 1, i+1 );
        }
        else{
          vtxu[i][m].lk[4] = equa + mod5i( (i+1)*j + k, i+1 );
          vtxd[i][m].lk[4] = equa + mod5i( (i+1)*j + k, i+1 );
          vtxu[i][m].lk[5] = equa + mod5i( (i+1)*j + k+1, i+1 );
          vtxd[i][m].lk[5] = equa + mod5i( (i+1)*j + k+1, i+1 );
        }
      }
    
  // la symétrie précédente à inverser l'orientation des polygônes (sens direct)
  for( i=hemisphere_vertices_nr; i<2*hemisphere_vertices_nr; i++ ){
    k = vtx[i].deg;
    for( j=0; j<k/2; j++ )
      swap_2_links( vtx[i].lk + j, vtx[i].lk + k-j-1 );
  }

  /*
  // pour ne plus avoir deux longueurs égales :
  myfloat d = HUGE_VAL;
  for( n=0; n<L*L; n++ )
    for( i=0; i<M[n].deg; i++ ){
      P = M[n].lk[i];
      d = MIN( sq_dist( P, M+n ), d);
    }
  d = sqrt(d); // plus petite distance entres deux points liés
  myfloat dx_rand, dy_rand, dz_rand;
  for( n=0; n<L*L; n++ ){
    dx_rand = (2*(double)(rand()-RAND_MAX/2))/RAND_MAX * d/sqrt(3) / 10;
    dy_rand = (2*(double)(rand()-RAND_MAX/2))/RAND_MAX * d/sqrt(3) / 10;
    dz_rand = (2*(double)(rand()-RAND_MAX/2))/RAND_MAX * d/sqrt(3) / 10;
    if( M[n].x+dx_rand < xmin || M[n].x+dx_rand > xmax )
      dx_rand = -dx_rand;
    if( M[n].y+dy_rand < ymin || M[n].y+dy_rand > ymax )
       dy_rand = -dy_rand;
    if( M[n].z+dz_rand < zmin || M[n].z+dz_rand > zmax )
      dz_rand = -dz_rand;
    M[n].x += dx_rand;
    M[n].y += dy_rand;
    M[n].z += dz_rand;
  }
  */
    struct polyhedron sphere;
    sphere.vertices_nr = vertices_nr;
    sphere.vertices_p = vtx;
    free( vtxu );
    free( vtxd );
    return sphere;
}
