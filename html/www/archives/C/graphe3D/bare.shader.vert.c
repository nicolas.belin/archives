#version 330

layout(location = 0) in vec3 position;
//layout(location = 1) in vec3 color_in;

uniform mat4 u_projection;
uniform mat4 u_camera;

smooth out vec4 color_out;

void main() {

  vec4 m = vec4( position, 1.0 );
  gl_Position = u_projection * u_camera * m;

  color_out = vec4 ( 1.0, 1.0, 1.0, 1.0 );
}
