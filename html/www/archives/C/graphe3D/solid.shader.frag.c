#version 330

smooth in vec4 color_out;

out vec4 frag_color;

void main() {


  frag_color.r = ( ( gl_PrimitiveID * 13577 ) % 256 ) / 256.0;
  frag_color.g = ( ( gl_PrimitiveID * 1577 ) % 256 ) / 256.0;
  frag_color.b = ( ( gl_PrimitiveID * 577 ) % 256 ) / 256.0;
  frag_color.a = 1.0;

}
