
#version 330

layout(location = 0) in vec3 position;

uniform mat4 u_projection;
uniform mat4 u_camera;

smooth out vec3 frag_pos;
//smooth out vec3 v_normal;

void main() {

  vec4 m = vec4( position, 1.0 );

  frag_pos = m.xyz;

  gl_Position = u_projection * u_camera * m;
}
