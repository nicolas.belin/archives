#ifndef GRAPHE_H
#define GRAPHE_H

#define VERTICES_NR_MAX 2000 // le nombre maximum de points
#define DEG_MAX 10           // degré maximum d'un sommet

enum boolean { NO, YES };

typedef unsigned int uint;
typedef double myfloat;

typedef struct vertex{  
  // point du polyhedre
  myfloat x;            // coordonnées
  myfloat y;
  myfloat z;
  myfloat nx;           // vecteur normal
  myfloat ny;
  myfloat nz;
  int deg;              // nombre d'arrêtes issues du point (degré)
  struct vertex **lk;   // pointeur vers les points reliés à M
} vertex_t;

struct polyhedron{
  int vertices_nr;
  vertex_t *vertices_p;

};

struct transform_data{
  int *vertices_nr_p;
  vertex_t *vtx;
  void (*glide)( double, vertex_t* );
};

extern enum boolean loop;
extern enum boolean modified;    // la vue ont été modifiés
extern enum boolean modifying;   // les points sont en cours de modification

#endif // GRAPHE_H
