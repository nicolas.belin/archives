/* Auteur: Nicolas JANEY          */
/* nicolas.janey@univ-fcomte.fr   */
/* Avril 2001                     */
/* Illustration de l'utilisation  */
/* d'une tache de fond avec GLUt  */

#include <time.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

float anglex = 0.0F ;
float angley = 0.0F ;
float anglez = 0.0F ;
clock_t t0;

void idle(void) {
  anglex += 1.1F ;
  angley += 2.11F ;
  anglez += 3.138F ; 	
  glutPostRedisplay() ;
}

void myInit() {
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  GLfloat l_pos[] = { 0.0,-0.5,-1.0,0.0 };
  glLightfv(GL_LIGHT0,GL_POSITION,l_pos);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
}

void display(void) {
  glClearColor(0.0F,0.0F,0.0F,0.0F) ;
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) ;
  glPushMatrix() ;
  glRotatef(anglex,1.0F,0.0F,0.0F) ;
  glRotatef(angley,0.0F,1.0F,0.0F) ;
  glRotatef(anglez,0.0F,0.0F,1.0F) ;
  glTranslatef(0.5F,0.0F,0.0F) ;
  GLfloat rouge[] = { 1.0,0.0,0.0,0.0 };
  glMaterialfv(GL_FRONT,GL_DIFFUSE,rouge);
  glutSolidCube(0.6) ;
  glTranslatef(-1.0F,0.0F,0.0F) ;
  GLfloat vert[] = { 0.0,1.0,0.0,0.0 };
  glMaterialfv(GL_FRONT,GL_DIFFUSE,vert);
  glutSolidSphere(0.4,50,50) ;
  glPopMatrix() ;
  glFlush() ;

while( 50*(clock() - t0) < CLOCKS_PER_SEC ) {}
t0 = clock();
  glutSwapBuffers() ;
}

int main(int argc,char **argv) {
t0 = clock();
  glutInit(&argc,argv);
  glutInitWindowSize(200,200);
  glutInitWindowPosition(100,100);
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
  glutCreateWindow("Animation automatique") ;
  myInit() ;
  glutIdleFunc(idle) ;
  glutDisplayFunc(display) ;
  glutMainLoop() ;
  return(0);
}

