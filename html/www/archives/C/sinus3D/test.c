#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>

using namespace std;

const int sw = 640;
const int sh = 480;
const int surface = 1;

int WindowName;
clock_t t0;
float theta = 0;

void myReshape( int w , int h );
void myDisplay();
void myIdle();
void InitGL();
void ListMaking();
double f(double x,double z);	// fonction a tracer
void normal(double x,double z);	// fonction vecteur normal
float n[3];					// resultat de la fonction precedente

int main( int argc, char *argv[ ], char *envp[ ] )
{ 	t0 = clock();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize( sw , sh );
	WindowName = glutCreateWindow("OpenGL !");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
	glutIdleFunc(myIdle);
	InitGL();
	ListMaking();
	glutMainLoop();
return 0;
}

void ListMaking()
{			
	glNewList(surface,GL_COMPILE);
		float u=0.125;
/*		glBegin(GL_LINES);	// wires
			for(float i=-2 ; i < 2 ; i+=u)
			{
				for(float j=-2 ; j < 2 ; j+=u)
				{	glVertex3f( i , f(i,j) , j );
					glVertex3f( i+u , f(i+u,j) , j );
					glVertex3f( i , f(i,j) , j );
					glVertex3f( i , f(i,j+u) , j+u );
				}
				glVertex3f( i , f(i,2) , 2);
				glVertex3f( i+u , f(i+u,2) , 2);
			}
			for(float j=-2 ; j < 2 ; j+=u)
			{	glVertex3f( 2 , f(2,j) , j);
				glVertex3f( 2 , f(2,j+u) , j+u );
			}
		glEnd();

	glColor3f(0.7,0.0,0.0);
	glBegin(GL_LINES);			// vecteurs normaux
		for(float i=-2 ; i < 2 ; i+=u)
		for(float j=-2 ; j < 2 ; j+=u)
		{		normal(i,j);
				glVertex3f( i , f(i,j) , j);
				glVertex3f( i+n[0] , f(i,j)+n[1] , j+n[2] );
		}
	glEnd();
	glColor3f(1.0,1.0,1.0);*/

	glColor3f(0.0 , 0.0 , 1 );
	for(float i=-2 ; i < 2 ; i+=u)
	{	glBegin(GL_TRIANGLE_STRIP);
			for(float j=-2 ; j < 2 ; j+=u)
			{	normal(i,j);
				glNormal3fv(n);
				glVertex3f(i , f(i,j) , j);
				normal(i+u,j);
				glNormal3fv(n);
				glVertex3f(i+u , f(i+u,j) , j);
			}
		glEnd();
	}
	glEndList();
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Efface les buffer Z et frame
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 	//Réinitialise la matrice
	glTranslatef(0.0,0,-6.0);
	glRotatef(20 , 1 , 0 , 0);
	glRotatef(theta , 0 , 1 , 0);
	glCallList(surface);
	glFlush();
	while( 50*(clock() - t0) < CLOCKS_PER_SEC ) {};
	t0 = clock();
	glutSwapBuffers();
}

void myIdle()
{	theta++;
	if(theta > 360) theta -= 360;
	glutPostRedisplay();	//Demande de recalculer la scène
}

void InitGL()
{	GLfloat mat_specular[] = { 0.5, 0.5, 1, 1 };
	GLfloat mat_shininess[] = { 50.0 };
	GLfloat mat_ambient[] = { 0 , 0 , 0.2, 1 };
	GLfloat mat_diffuse[] = { 0 , 0 , 0.8 , 1 };
	GLfloat light_position[] = { 0, -13, 9, 0.0 };
	GLfloat light_ambient[] = {1 , 1 , 1 , 1.0 };

	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS, mat_shininess);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE, mat_diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT , light_ambient);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_DST_ALPHA);
}

void myReshape( int w , int h )
{   	glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluPerspective(45,float(w)/float(h),0.1,100);
}

double f(double x,double z)
{	const double lx = 0.5;
	const double lz = 0.5;
	const double pi = 3.14159265359;
	return 0.8*cos(pi*x)*cos(pi*z/2)*cos(pi*z/2)/(1+lx*x*x)/(1+lz*z*z);
}

void normal(double x,double z)	// calcul du vecteur normal
{	double eps = 0.00001;
	double dfx = ( f(x+eps,z)-f(x,z) ) / eps;
	double dfz = ( f(x,z+eps)-f(x,z) ) / eps;
	double norme = -sqrt( dfx*dfx + dfz*dfz + 1.0 );
	n[0] = float( dfx / norme );
	n[1] = float( -1.0 / norme );
	n[2] = float( dfz / norme );
}
