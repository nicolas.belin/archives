// compilation : g++ `sdl-config --cflags` -c %1.c
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <iomanip>
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>        // fonction sinux, cosinus...
#include <cstdlib>

#define screen_w 1300	// screen width (largueur)
#define screen_h 850	// screen height (hauteur)
#define PI 3.14159265359

using namespace std;

Uint32 *bitmap;

// ** Trace des droites et des courbes : fonctions et variables **********
float xmin = -10;        // viewport
float xmax = 7;
float ymin = -5;
float ymax = 8;
int color = 0xffffff;
int abs(int x);
void line_x( int x0, int y0, int dx, int dy);
void line_y( int x0, int y0, int dx, int dy);
void line( int xa, int ya, int xb, int yb);          // coordonnees en pixels
void linev( float x1, float y1, float x2, float y2); // coordonnees dans viewport
void curvef( float (*f)(float) , float a, float b );
void print(float (*f)(float));

// ** fonction d'interpolation ********************************************
const int np = 7;             // nombre de points
float pts[np][3] = { -10, 0, 0,
                      -8, 1, 2,
                      -5, 3, 0,
                      -2, 1, -3,
                      0,-1,0,
                      2,0,2.0/3,
                      5,0,-1};
float L(float x);
float interpolatrice(float x);
float integrate( float (*f)(float), float a, float b, int n );
float affine(float t, float xa, float ya, float xb, float yb);
// ****************** main() ***************************************

int main(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
	
  SDL_Surface *screen;
  SDL_Event event;
	
  screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE);
  if ( screen == NULL ){
    cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
    exit(1);
  }
  bitmap = (Uint32*)screen->pixels;	
  
  linev(xmin,0,xmax,0);
  linev(0,ymin,0,ymax);

  color = 0x0000ff;
  for(int i=0; i<np; i++) linev( pts[i][0], ymin, pts[i][0], ymax);
  color = 0xff0000;
  curvef(L,xmin,xmax);
  color = 0xffffff;
  curvef(interpolatrice,xmin,xmax);

  SDL_Flip(screen);
  bool loop = true;
  while(loop){
    if( SDL_PollEvent(&event)==1 ){
	switch(event.type){
	case SDL_MOUSEBUTTONDOWN :
        loop = false;	
        break;
      case SDL_KEYDOWN:
        if(event.key.keysym.sym==SDLK_ESCAPE){
          loop=false;
        }
        break;						
      }
    }		
  }
  SDL_Quit();
  print(interpolatrice);

  return 0;
}
// ****************************** fonctions **********************************

int abs(int x){
  // fonction valeur absolue
  if( x<0 ) return -x;
  else return x;
}

float interpolatrice(float x){
  // fonction interpolatrice
  // 1/(2s) * int_{-s}^{s}*L(x-u*s)du
  // ou s=s(x) est une fonction affine telle que :
  // s(xl) = xi-xl et s(xr) = xr-xi
  int i = 0;
  if( x<=pts[0][0] ) return pts[0][2] * (x-pts[0][0]) + pts[0][1]; 
  if( x>=pts[np-1][0] ) return pts[np-1][2] * (x-pts[np-1][0]) + pts[np-1][1]; 
  while( x>pts[++i][0] );
  float xl = pts[i-1][0];
  float yl = pts[i-1][1];
  float al = pts[i-1][2];
  float xr = pts[i][0];
  float yr = pts[i][1];
  float ar = pts[i][2];
  float xi = ( xr*ar-yr - (xl*al-yl) ) / (ar-al);
  float s= xi - xl + (xr+xl-2*xi) * (x-xl) / (xr-xl);
  float u0 = (x-xi) / s;
  float f = s*(al-ar)*(u0*u0-1)/2 + (yl+al*(x-xl))*(1-u0) + (yr+ar*(x-xr))*(1+u0);
  return f/2;
}

float L(float x){
  // la fonction affine par morceaux
  int i = 0;
  if( x<=pts[0][0] ) return pts[0][2] * (x-pts[0][0]) + pts[0][1]; 
  if( x>=pts[np-1][0] ) return pts[np-1][2] * (x-pts[np-1][0]) + pts[np-1][1]; 
  while( x>pts[++i][0] );
  float xl = pts[i-1][0];
  float yl = pts[i-1][1];
  float al = pts[i-1][2];
  float xr = pts[i][0];
  float yr = pts[i][1];
  float ar = pts[i][2];
  float xi = ( xr*ar-yr - (xl*al-yl) ) / (ar-al);
  if( x<=xi ) return yl + al*(x-xl);
  return yr + ar*(x-xr);
}

float integrate( float (*f)(float), float a, float b, int n ){
  // integre la fonction f sur l'intervalle [a;b].
  // n est le nombre de subdivisions
  // l'interpolation est affine
  // la distance L1 entre f et la fonction interpolatrice est majoree par :
  // 1/6*max|f''|*(b-a)^3
  double d = double( b-a ) / n;
  double S = 0;

  for(int k=1 ; k<n ; k++ ) S += f( a + k*d );
  S += (f(a)+f(b))/2;
  S *= d;
  return float(S);
}

void print(float (*f)(float)){
  // envoie vers stdout un programme pstricks pour tracer la courbe
  int npoints=100;
  float t;

  cout << setprecision(3);
  cout << endl;
  cout << "\\savedata{\\mydata}[" << endl;
  for(int i=0; i<=npoints; i++){
    t = xmin + i*(xmax-xmin)/npoints;
    cout << "(" << t << " " << f(t) << ")" << " ";
  }
  cout << "]" << endl;
  cout << "\\dataplot[plotstyle=curve]{\\mydata}" << endl << endl;
  }

}

float affine(float t, float xa, float ya, float xb, float yb){
  // Calcule l'image de t par la fonction affine dont la droite
  // passe par (xa,ya) et (xb,yb)

  return (yb-ya)/(xb-xa)*(t-xa) + ya;
}

void line( int xa, int ya, int xb, int yb){
  // trace le segment de (xa,ya) a (xb,yb), coordonnees en pixels
  int dx = xb-xa;
  int dy = yb-ya;
  int a;
  int x,y;

  if( abs(dx) > abs(dy)){
    if( xa <= xb ) line_x( xa, ya, dx, dy );
    else line_x( xb, yb, -dx, -dy );
  }
  else {
    if( ya <= yb ) line_y( xa, ya, dx, dy );
    else line_y( xb, yb, -dx, -dy );
  }
  return;
}

void line_x( int x0, int y0, int dx, int dy){    // |dx|>|dy| et dx>0
  int y;
  for(int i=0; i<=dx; i++){
    y = dy*i;
    y /= dx;
    y += y0;
    bitmap[ x0 + i + y*screen_w ] = color;
  }
  return;
}

void line_y( int x0, int y0, int dx, int dy){    // |dy|>|dx| et dy>0
  int x;
  for(int i=0; i<=dy; i++){
    x = dx*i;
    x /= dy;
    x += x0;
    bitmap[ x + ( y0+i ) * screen_w ] = color;
  }
  return;
}

void linev( float x1, float y1, float x2, float y2){
  // trace le segment de (x1,y1) a (x2,y2), coordonnes du viewport
  int xa = int( ((screen_w-1) * (x1-xmin)) / (xmax-xmin) );
  int xb = int( ((screen_w-1) * (x2-xmin)) / (xmax-xmin) );
  int ya = int( (screen_h-1) * (1 - (y1-ymin)/(ymax-ymin)) );
  int yb = int( (screen_h-1) * (1 - (y2-ymin)/(ymax-ymin)) );
  if( (xa < screen_w) && (xa >= 0) && (xb < screen_w) && (xb >= 0) &&
      (ya < screen_h) && (ya >= 0) && (yb < screen_h) && (yb >= 0) )
    line( xa, ya, xb, yb);
  return;
}

void curvef( float (*f)(float) , float a, float b){
  // Trace la courbe de f sur l'intervalle [a;b]
  int n=screen_w/2;
  float x0,y0,x1,y1;
  x0 = a;
  y0 = f(a);
  
  for(int i=0; i<n; i++){
    x1 = a + (i+1)*(b-a)/n;
    y1 = f(x1);
    linev(x0,y0,x1,y1);
    x0 = x1;
    y0 = y1;
  }
}

