//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>
#include <fstream>
#include <iomanip>

using namespace std;

const float PI = 3.14159265359;

char **arguments;		// arguments en ligne de commande
int effArg;				// nombre d'arguments en ligne de commande

int gener;				// nombre de generations
int N;					// nombre de personnes dans l'arbre

int* sosa = 0;			// numero Sosa
int* generation = 0;	// generation de la personne i
char* noms = 0;			// noms de famille
int* effGen = 0;		// effectifs de chaque generation
int* indexGen = 0;		// index des generations dans la liste precedente
int* nombreChar = 0;	// nombre de caracteres
int* largueurs = 0;		// largueur des noms
float* xNoms = 0;		// abscisses des noms (x,y,s)
double* yGen = 0;		// ordonnee des generations
double* sGen = 0;		// coefficients de reduction des generations
float sInit = 1.0;		// coefficient de reduction de la 1iere gen.

const double unit = 1000.0;// unite de longueurs abscisses et ordonnees
const float dx = 0.05;	// ecart horizontal entre les cases
const float dy = 0.8;	// coeff. de l'ecart vertical entre les cases
double pos_x = 0;		// parametres de position de la camera
double pos_y = -1;
double pos_z = 0.5;
float theta = 0;
int t0 = 0;

int WindowName;			// numero de la fenetre ouverte par Glut
const int sw = 640;		// dimensions de la fenetre au demarrage
const int sh = 480;
int win_w = sw;			// dimensions de la fenetre actuelle
int win_h = sh;
int	nomsLists;			// base des display lists
int fakeGenLists;

void extractionDonnees();// extraction et rangement des donnees
void texte();			// sortie en mode texte
void swap(int a,int b);	//commute 2 lignes
int trouveEnfant(int sosaP);// trouve l'eventuel enfant de sosaP
float f(float x);		// largueur de la nieme generation
void mkArb();			// construction de l'arbre
void traceBranche(int j);	// trace l'eventuelle branche du nom j
void writeNom(int j);	// ecrire le nom j
void fakeGen(int k);	// ecrire la (k+1)-ieme gen. simplifiee
void ListsMaking();		// mise en place des display lists
void sortie();			// nettoyage avant de quitter le programme 

void myReshape( int w , int h );
void myDisplay();
void myIdle();
void clavier(unsigned char key,int x,int y);
void souris( int etat , int bouton , int x , int y );
void write(char text[] , int nbchar , int wwidth);
void InitGL();

int main( int argc, char *argv[] )
{ 	arguments = argv;	// sauvegarde des arguments
	effArg = argc;

	extractionDonnees();
	texte();
	mkArb();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize( sw , sh );
	WindowName = glutCreateWindow("Un arbre");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
	glutIdleFunc(myIdle);
	glutKeyboardFunc(clavier);
	glutMouseFunc(souris);
	InitGL();
	ListsMaking();
	glutMainLoop();
	sortie();
	return 0;
}

float f(float x)
{	return x;
}

// mise en place des display lists
void ListsMaking()
{	nomsLists = glGenLists(N);
	for(int j=0 ; j<N ; j++)
	{	glNewList( nomsLists + j , GL_COMPILE );
			writeNom(j);
		glEndList();
	}

	fakeGenLists = glGenLists(gener);
	for(int k=0 ; k<gener ; k++)
	{	glNewList( fakeGenLists + k , GL_COMPILE );
			fakeGen(k);
		glEndList();
	}
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
	glScaled( unit , unit , 1.0 );
	glRotatef( theta , 0 , 0 , 1 );
	glColor3f(1.0 , 1.0 , 1.0 );
	const double inf = (5.0/(unit*0.180));
	const double sup = (400.0/(unit*0.180));
	double sg;
	double xr;
	for(int k=0 ; k<gener ; k++)
	{	sg = pos_z*sGen[k];
		if( sg > sup) continue; // la generation k n'apparait pas
		glPushMatrix();
		glScaled( sg , sg , 1.0);
		if( sg < inf)			// la gen. k est tres petite
		{	glTranslated( pos_x/sGen[k] , (pos_y+yGen[k])/sGen[k] , 0.0 );
			glCallList( fakeGenLists + k );
			glPopMatrix();
			continue;
		}
		for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++ )
		{		glPushMatrix();
				glTranslated( xNoms[j]+pos_x/sGen[k]
								,(pos_y+yGen[k])/sGen[k] , 0.0 );
				glCallList( nomsLists + j );
				glPopMatrix();
		}
		glPopMatrix();
	}
	glFlush();
	glutSwapBuffers();
}

//ecrire le nom j
void writeNom(int j)
{	const float bordmot = 0.025;
	glPushMatrix();
	glBegin(GL_LINE_LOOP);	// encadrement
	glVertex2f(-0.5 , -0.035 );
	glVertex2f(-0.5 , 0.145 );
	glVertex2f( 0.5 , 0.145 );
	glVertex2f( 0.5 , -0.035 );
	glEnd();
	traceBranche(j);
	glTranslatef( -0.5 + bordmot , 0 , 0 );
	glScalef( (1-2*bordmot)/largueurs[j] , 0.001 , 1.0 );
	for( int i=0 ; i<nombreChar[j] ; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN,noms[j*60+i]);
	glPopMatrix();
}

// ecrire la (j+1)-ieme gen. simplifiee
void fakeGen(int k)
{	const float bordmot = 0.025;
	for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
	{ 	glPushMatrix();
		glTranslatef( xNoms[j] , 0.0 , 0.0 );
		glRectf( -0.5 , -0.035 , 0.5 , 0.145 );
		traceBranche(j);
		glPopMatrix();
	}
}

// trace l'eventuelle branche du nom j
void traceBranche( int j )
{		int indE = trouveEnfant(sosa[j]);// branche sous la case
		if( ( indE!=-1 ) && ( j!=0 ) )
		{	int k = generation[j];
			glBegin(GL_LINES);
			glVertex2f( 0.0 , -0.035 );
			glVertex2f( xNoms[indE]*sGen[k-1]/sGen[k]-xNoms[j]
						, ( yGen[k-1]-yGen[k]+ 0.145*sGen[k-1] )/sGen[k] );
			glEnd();
		}		
}

// Extraction des donnees du fichier, allocation memoire dynamique,
// rangement des donnees dans les tableaux :
void extractionDonnees()
{	// gestion des arguments
	if( effArg == 1 )
	{	cout << "Argument : un fichier au format csv (Comma-separated values)" << endl << endl;
	sortie();
	}

	// ouverture du fichier :
	ifstream Table;				// flux entrant d'un fichier
	Table.open( arguments[1] );
	if( ! Table )
	{	cout << "Probleme pour ouvrir le fichier" << endl;
		sortie();
	}

	// construction d'une poubelle
	char *nil = new char[200];

	// format csv ? test tres sommaire
	char testc;
	Table.getline( nil , 200 );	// on passe la 1iere ligne
	testc = Table.get();		// extraction du premier caractere
	if( testc != '1' )
	{	cout << "Ce fichier n'est pas au bon format !" << endl << endl;
		sortie();
	}

	// comptage du nombre de ligne :
	int l = 0;
	while( Table )
	{	Table.getline( nil , 200 ); // on suppose aucune ligne > 200 char.
		l++;
	}
	N = l - 1;	// nombre de personne dans l'arbre
	if( N <= 0 )
	{	cout << "Il n'y a personne dans ce tableau !" << endl;
		sortie();
	}
	Table.close();

	// extraction des numero sosa, noms et prenoms.
	noms = new char[N*60];		// allocation de la memoire
	sosa = new int[N];
	nombreChar = new int[N];

	Table.open( arguments[1] );	// re-ouverture du fichier
		Table.getline( nil , 200 );// premiere ligne ignoree
	for(int i=0 ; i<N ; i++)	// extraction des noms et prenoms
	{ 	Table >> sosa[i];
		Table.getline( nil ,200,'"');
		int j = 0;
		char c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		noms[i*60+j] = ' ';
		j++;
		Table.getline(nil,200,'"');
		c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		nombreChar[i] = j;
		Table.getline(nil,200);
	}
	Table.close();

	// destruction de la poubelle
	delete[] nil;

	// Classement et indexage des num. sosa :
	int sosasup;
	int sosamax = 0;
	for(int i=0 ; i<N ; i++)	// calcul du num. sosa maximum
		if( sosa[i] > sosamax ) sosamax = sosa[i];
	int sosainf=0;
	sosasup = sosamax+1;
	int js = -1;
	int doublons = 0;
	for(int i=0 ; i<N ; i++)
	{	for(int j=0 ; j<N ; j++)
			if(sosainf < sosa[j] && sosa[j] <= sosasup)
			{	sosasup = sosa[j];
				if( sosa[j] == sosa[js] && j != js )
				{	cout << endl << "Doublon lignes "
						<< js+2 << " et " << j+2 << " !" << endl; 
					doublons++;
				}
				js = j;
			}
		swap( i , js );
		sosainf = sosasup;
		sosasup = sosamax+1;
	}
	N -= doublons;

	// calcul de la generation de chaque personne :
	generation = new int[N];
	for(int i=0 ; i<N ; i++)
		generation[i] = int(floor(log(sosa[i])/log(2.0)));	
	
	// Calcul du nombre de generations :
	gener = 0;
	for(int i=0 ; i<N ; i++)
		if( generation[i] > gener ) gener = generation[i];		
	gener++;

	// Calcul de l'effectif de chaque generation :
	effGen = new int[gener];
	for(int i=0 ; i<gener ; i++)
		effGen[i] = 0;
	for(int i=0 ; i<N ; i++)
		effGen[generation[i]]++;

	// Calcul de l'index de chaque generation :
	indexGen = new int[gener];
	indexGen[0]=0;
	for(int i=1 ; i<gener ; i++)
		indexGen[i] = indexGen[i-1] + effGen[i-1];
}

// formatage des donnees pour l'arbre
void mkArb()
{	// Calcul de la largueurs des noms et prenoms :
	largueurs = new int[N];
	int l;
	for( int i=0 ; i<N ; i++)
	{	l=0;
		for( int j=0 ; j<nombreChar[i] ; j++)
			l+=glutStrokeWidth(GLUT_STROKE_ROMAN,noms[60*i+j]);
		largueurs[i]=l;
	} 
	// calcul des ordonnees et coeff. de reduction de chaque generation
	yGen = new double[gener];
	sGen = new double[gener];
	float h=1;
	float s=f(h);
	yGen[0] = h;
	sGen[0] = s;
	for(int k=1 ; k<gener ; k++)
	{	h += s*dy;
		s = f(h) / ( (1<<k) + dx*( 3*(1<<(k-1)) - 2 ) );
		yGen[k] = h;
		sGen[k] = s;
	}

	// abscisses des cases, tenant compte de la reduction
	int u;					// sosa = 2^n + 2a + u 	
	int a;
	xNoms = new float[N];
	xNoms[0] = 0.0;
	for(int k=1 ; k<gener ; k++)
	for(int i=indexGen[k] ; i<indexGen[k]+effGen[k] ; i++)
	{	u = sosa[i] % 2;
		a = (sosa[i] - (1<<k) - u)/2;
		xNoms[i] = a*(2+3*dx) + 0.5 + u*(1+dx)
			- 0.5*((1<<(k)) + (1<<(k-1))*3*dx - 2*dx);
	}
}

int trouveEnfant(int sosaP)		// trouve l'eventuel enfant de sosaP
{	int sosaE = int( floor( sosaP/2.0 ) );
	for(int i = 0 ; i<N ; i++)
		if(sosa[i] == sosaE) return i;
	return -1;
}

// commute deux lignes du tableau noms[] avec leur num. sosa 
void swap( int a , int b)
{	char tempc;
	int sosat;
	for(int i = 0 ; i<60 ; i++)
	{	tempc = noms[60*a + i];
		noms[60*a + i] = noms[60*b + i];
		noms[60*b + i] = tempc;
	}
	sosat = sosa[a];
	sosa[a] = sosa[b];
	sosa[b] = sosat;
}

void myIdle()
{	if ( glutGet( GLUT_ELAPSED_TIME ) - t0 > 50 )
	{	glEnable(GL_LINE_SMOOTH);	
		glLineWidth( 1.4 );
		glutPostRedisplay();
	}
}

void InitGL()
{	//glEnable(GL_LINE_SMOOTH);	// antialiasing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	
	glLineWidth(1);
}

void myReshape( int w , int h )
{   glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D(-w/2,w/2,-h/2,h/2);
	win_w = w;
	win_h = h;
}

// gestion de la souris
void souris( int etat , int bouton , int x , int y )
{	if( etat == GLUT_DOWN && bouton == GLUT_LEFT_BUTTON )
	{	float rx = -(x-win_w/2)*cos(PI*theta/180)+(y-win_h/2)*sin(PI*theta/180);
		float ry = (x-win_w/2)*sin(PI*theta/180)+(y-win_h/2)*cos(PI*theta/180);
		pos_x += rx/(pos_z*unit);
		pos_y += ry/(pos_z*unit);
	}
}

// gestion du clavier
void clavier(unsigned char key,int x,int y)
{	const double dxy_pix = 5.0;
	const double zz = 15.0;
	switch( key )
	{	case '8' :
			pos_x += -dxy_pix*sin(PI*theta/180.0)/(pos_z*unit);
			pos_y += -dxy_pix*cos(PI*theta/180.0)/(pos_z*unit);
			break;
		case '5' : 
			pos_x += dxy_pix*sin(PI*theta/180.0)/(pos_z*unit);
			pos_y += dxy_pix*cos(PI*theta/180.0)/(pos_z*unit);
			break;
		case '6' :
			pos_x -= dxy_pix*cos(PI*theta/180.0)/(pos_z*unit);
			pos_y += dxy_pix*sin(PI*theta/180.0)/(pos_z*unit);
			break;
		case '4' :
			pos_x += dxy_pix*cos(PI*theta/180.0)/(pos_z*unit);
			pos_y -= dxy_pix*sin(PI*theta/180.0)/(pos_z*unit);
			break;
		case '-' : pos_z-= pos_z/zz; break;
		case '+' : pos_z+= pos_z/zz; break;
		case '7' : theta++; break;
		case '9' : theta--; break;
		case 'f' : glutFullScreen(); break;
		case 27  : sortie();
	}
	t0 = glutGet( GLUT_ELAPSED_TIME );
	glDisable(GL_LINE_SMOOTH);
	glLineWidth( 1.0 );
	glutPostRedisplay();
}

void texte()	// sortie en mode texte
{	cout << endl;
	cout << "Nombre d'ascendants (hors doublons) : " << N << endl;
	cout << endl;
	cout << "Nombre de generations : " << gener << endl;
	cout << endl;
	cout << "Generations |" << " Effectifs"<< endl;
	for(int k = 0 ; k<gener ; k++)
		cout << setw(11) << k+1 << " | " << setw(9) << effGen[k] << endl;
}

// Quitter le programme
void sortie()
{	delete[] xNoms;
	delete[] sGen;
	delete[] yGen;
	delete[] indexGen;
	delete[] effGen;
	delete[] generation;
	delete[] nombreChar;
	delete[] sosa;
	delete[] noms;
	exit(0);
}
