#include <iostream>
#include <cstdlib>
#include <fstream>
//#include <ctime>
#include <iomanip>
#include <cmath>

using namespace std;

int gener;				// nombre de generations
int N;					// nombre de personnes dans l'arbre

char **arguments;		// arguments en ligne de commande
int effArg;				// nombre d'arguments en ligne de commande
int* sosa = 0;			// numero Sosa
int* generation = 0;	// generation de la personne i
char* noms = 0;			// noms de famille
int* effGen = 0;		// effectifs de chaque generation
int* indexGen = 0;		// index des generations dans la liste precedente
int* nombreChar = 0;	// nombre de caracteres

void extractionDonnees();// extraction et rangement des donnees
void swap( int a , int b);//commute 2 lignes
void sortie();			// nettoyage avant de quitter le programme 

int main( int argc , char **argv )
{	arguments = argv;
	effArg = argc;

	extractionDonnees();

	cout << endl;
	cout << "Nombre d'ascendants (hors doublons) : " << N << endl;
	cout << endl;
	cout << "Nombre de generations : " << gener << endl;
	cout << endl;
	cout << "Generations |" << " Effectifs"<< endl;
	for(int k = 0 ; k<gener ; k++)
		cout << setw(11) << k+1 << " | " << setw(9) << effGen[k] << endl;

	sortie();
	return 0;
}	

// Extraction des donnees du fichier, allocation memoire dynamique,
// rangement des donnees dans les tableaux :
void extractionDonnees()
{	// gestion des arguments
	if( effArg == 1 )
	{	cout << "Argument : un fichier au format csv (Comma-separated values)" << endl << endl;
	sortie();
	}

	// ouverture du fichier :
	ifstream Table;				// flux entrant d'un fichier
	Table.open( arguments[1] );
	if( ! Table )
	{	cout << "Probleme pour ouvrir le fichier" << endl;
		sortie();
	}

	// construction d'une poubelle
	char *nil = new char[200];

	// format csv ? test tres sommaire
	char testc;
	Table.getline( nil , 200 );	// on passe la 1iere ligne
	testc = Table.get();		// extraction du premier caractere
	if( testc != '1' )
	{	cout << "Ce fichier n'est pas au bon format !" << endl << endl;
		sortie();
	}

	// comptage du nombre de ligne :
	int l = 0;
	while( Table )
	{	Table.getline( nil , 200 ); // on suppose aucune ligne > 200 char.
		l++;
	}
	N = l - 1;	// nombre de personne dans l'arbre
	if( N <= 0 )
	{	cout << "Il n'y a personne dans ce tableau !" << endl;
		sortie();
	}
	Table.close();

	// extraction des numero sosa, noms et prenoms.
	noms = new char[N*60];		// allocation de la memoire
	sosa = new int[N];
	nombreChar = new int[N];

	Table.open( arguments[1] );	// re-ouverture du fichier
		Table.getline( nil , 200 );// premiere ligne ignoree
	for(int i=0 ; i<N ; i++)	// extraction des noms et prenoms
	{ 	Table >> sosa[i];
		Table.getline( nil ,200,'"');
		int j = 0;
		char c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		noms[i*60+j] = ' ';
		j++;
		Table.getline(nil,200,'"');
		c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		nombreChar[i] = j;
		Table.getline(nil,200);
	}
	Table.close();

	// destruction de la poubelle
	delete[] nil;

	// Classement et indexage des num. sosa :
	int sosasup;
	int sosamax = 0;
	for(int i=0 ; i<N ; i++)	// calcul du num. sosa maximum
		if( sosa[i] > sosamax ) sosamax = sosa[i];
	int sosainf=0;
	sosasup = sosamax+1;
	int js = -1;
	int doublons = 0;
	for(int i=0 ; i<N ; i++)
	{	for(int j=0 ; j<N ; j++)
			if(sosainf < sosa[j] && sosa[j] <= sosasup)
			{	sosasup = sosa[j];
				if( sosa[j] == sosa[js] && j != js )
				{	cout << endl << "Doublon lignes "
						<< js+2 << " et " << j+2 << " !" << endl; 
					doublons++;
				}
				js = j;
			}
		swap( i , js );
		sosainf = sosasup;
		sosasup = sosamax+1;
	}
	N -= doublons;

	// calcul de la generation de chaque personne :
	generation = new int[N];
	for(int i=0 ; i<N ; i++)
		generation[i] = int(floor(log(sosa[i])/log(2.0)));	
	
	// Calcul du nombre de generations :
	gener = 0;
	for(int i=0 ; i<N ; i++)
		if( generation[i] > gener ) gener = generation[i];		
	gener++;

	// Calcul de l'effectif de chaque generation :
	effGen = new int[gener];
	for(int i=0 ; i<gener ; i++)
		effGen[i] = 0;
	for(int i=0 ; i<N ; i++)
		effGen[generation[i]]++;

	// Calcul de l'index de chaque generation :
	indexGen = new int[gener];
	indexGen[0]=0;
	for(int i=1 ; i<gener ; i++)
		indexGen[i] = indexGen[i-1] + effGen[i-1];
}

// commute deux lignes du tableau noms[] avec leur num. sosa 
void swap( int a , int b)
{	char tempc;
	int sosat;
	for(int i = 0 ; i<60 ; i++)
	{	tempc = noms[60*a + i];
		noms[60*a + i] = noms[60*b + i];
		noms[60*b + i] = tempc;
	}
	sosat = sosa[a];
	sosa[a] = sosa[b];
	sosa[b] = sosat;
}

// Fin du programme
void sortie()
{	delete[] indexGen;
	delete[] effGen;
	delete[] generation;
	delete[] nombreChar;
	delete[] sosa;
	delete[] noms;
	exit(0);
}

