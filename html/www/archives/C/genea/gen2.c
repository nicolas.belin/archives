//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>
#include <fstream>

using namespace std;

const float PI = 3.14159265359;

const int gener = 21;	// nombre de generations
const int N = 1146;		// nombre de personnes dans l'arbre

ifstream Table;			// flux entrant d'un fichier
int sosa[N];			// numero Sosa
int generation[N];		// generation de la personne i
char noms[N][30];		// noms de famille
int effGen[gener];		// effectifs de chaque generation
int indexSosa[N];		// index des numero sosa
int indexGen[gener];	// index des generations dans la liste precedente
int NombreChar[N];		// nombre de caracteres
int largueurs[N];		// largueur des noms
float xNoms[N];			// abscisses des noms (x,y,s)
float yGen[gener];		// ordonnee des generations
float sGen[gener];		// coefficients de reduction des generations
char nil[200];			// puit sans fond	

const float unit = 1000.0;// unite de longueurs abscisses et ordonnees
const float dx = 0.05;	// ecart horizontal entre les cases
const float dy = 0.8;	// coeff. de l'ecart vertical entre les cases
float pos_x = 0;		// parametres de position de la camera
float pos_y = -1;
float pos_z = 0.5;
float theta = 0;

const int sw = 640;		// dimensions de la fenetre au demarrage
const int sh = 480;
int	baseDLists;		// 1ier index des display lists des generations
const int branches = 1;

int WindowName;

int trouveEnfant(int sosaP);// trouve l'eventuel enfant de sosaP
void MkArb();			// construction de l'arbre
void MkGen(int n);		// composition de la (n+1)ieme ligne
void writeNom(int j);	// ecrire le nom d'index j
void writeGen(int k);	// ecrire la (j+1)-ieme generation 
void fakeGen(int k);	// ecrire la (j+1)-ieme gen. simplifiee
float f(float x);		// largueur de la nieme generation

void myReshape( int w , int h );
void myDisplay();
void myIdle();
void clavier(unsigned char key,int x,int y);
void write(char text[] , int nbchar , int wwidth);
void InitGL();
void ListsMaking();


int main( int argc, char *argv[ ], char *envp[ ] )
{ 	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize( sw , sh );
	WindowName = glutCreateWindow("Un arbre");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
//	glutIdleFunc(myIdle);
	glutKeyboardFunc(clavier);
	InitGL();
	MkArb();
	ListsMaking();
	glutMainLoop();
	return 0;
}

int trouveEnfant(int sosaP)		// trouve l'eventuel enfant de sosaP
{	int sosaE = int( floor( sosaP/2.0 ) );
	for(int i = 0 ; i<N ; i++)
		if(sosa[i] == sosaE) return i;
	return -1;
}

void MkArb()					// formatage des donnees pour l'arbre
{	Table.open("caroline.csv");	// ouverture du fichier
	if( ! Table )
	{	cout << "Probleme pour ouvrir le fichier" << endl;
		exit(1);
	}
	Table.getline(nil,200);		// premiere ligne ignoree
	for(int i=0 ; i<N ; i++)	// rangement des noms et prenoms
	{ 	Table >> sosa[i];
		Table.getline(nil,200,'"');
		int j = 0;
		char c = Table.get();
		while( c != '"' )
		{	noms[i][j] = c;
			j++;
			c = Table.get();
		}
		noms[i][j] = ' ';
		j++;
		Table.getline(nil,200,'"');
		c = Table.get();
		while( c != '"' )
		{	noms[i][j] = c;
			j++;
			c = Table.get();
		}
		NombreChar[i] = j;
		Table.getline(nil,200);
	}
	int l;
	for( int i=0 ; i<N ; i++)	// largueurs des noms
	{	l=0;
		for( int j=0 ; j<NombreChar[i] ; j++)
			l+=glutStrokeWidth(GLUT_STROKE_ROMAN,noms[i][j]);
		largueurs[i]=l;
	} 

	for(int i=0 ; i<N ; i++)	// calcul de la generation de chaque nom
		generation[i] = int(floor(log(sosa[i])/log(2.0)));	
	
	for(int i=0 ; i<gener ; i++)	// calcul des effectif de chaque generation
		effGen[i] = 0;
	for(int i=0 ; i<N ; i++)
		effGen[generation[i]]++;

	indexGen[0]=0;
	for(int i=1 ; i<gener ; i++)// calcul de l'index de chaque generation
		indexGen[i] = indexGen[i-1] + effGen[i-1];

	int sosasup;				// classement et indexage des num. sosa
	int sosamax = 0;
	for(int i=0 ; i<N ; i++)	// calcul du num. sosa maximum
		if( sosa[i] > sosamax ) sosamax = sosa[i];
	int sosainf=0;
	sosasup = sosamax+1;
	int js;
	for(int i=0 ; i<N ; i++)
	{	for(int j=0 ; j<N ; j++)
			if(sosainf < sosa[j] && sosa[j] < sosasup)
			{	sosasup = sosa[j];
				js = j;
			}
		indexSosa[i] = js;
		sosainf = sosasup;
		sosasup = sosamax+1;
	}

	float h=1;			// calcul des ordonnees et coeff. de reduction	
	float s=f(h);		// de chaque generation
	yGen[0] = h;
	sGen[0] = s;
	for(int k=1 ; k<gener ; k++)
	{	h += s*dy;
		s = f(h) / ( (1<<k) + dx*( 3*(1<<(k-1)) - 2 ) );
		yGen[k] = h;
		sGen[k] = s;
	}



	int u;					// position de chaque nom
	int a;					// sosa = 2^n + 2a + u 
	int ind;				// index du i-ieme noms
	xNoms[indexSosa[0]] = 0.0;
	for(int k=1 ; k<gener ; k++)
	for(int i=indexGen[k] ; i<indexGen[k]+effGen[k] ; i++)
	{	ind = indexSosa[i];
		u = sosa[ind] % 2;
		a = (sosa[ind] - (1<<k) - u)/2;
		xNoms[ind] = a*(2+3*dx) + 0.5 + u*(1+dx)
			- 0.5*((1<<(k)) + (1<<(k-1))*3*dx - 2*dx);
		xNoms[ind] *= sGen[k];
	}
}

float f(float x)
{	return x;
}

void ListsMaking()
{	glNewList(branches,GL_COMPILE);
	glPushMatrix();
	glColor3f(1.0 , 1.0 , 1.0 );
	glBegin(GL_LINES);
		int indP,indE;
		for(int k=1 ; k<gener ; k++)
		for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
		{	indP=indexSosa[j];
			indE = trouveEnfant(sosa[indP]);// branche sous la case
			if( indE != -1)
			{	glVertex2f(xNoms[indP],yGen[k]-0.035*sGen[k]);
				glVertex2f(xNoms[indE],yGen[k-1]+0.145*sGen[k-1]);
			}
		}
	glEnd();
	glPopMatrix();
	glEndList();

	baseDLists=glGenLists(2*gener);	// genere les numero de gener listes
	for(int k=0 ; k<gener ; k++)
	{	glNewList( baseDLists + 2*k , GL_COMPILE );
			writeGen(k);
		glEndList();
		glNewList( baseDLists + 2*k + 1 , GL_COMPILE );
			fakeGen(k);
		glEndList();

	}
	


}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);	//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
	glScalef( unit*pos_z , unit*pos_z , 1.0 );
	glRotatef( theta , 0 , 0 , 1 );
	glTranslatef( pos_x , pos_y , 0 );
	glColor3f(1.0 , 1.0 , 1.0 );

	glCallList(branches);
	glPushMatrix();
	float inf = (5.0/(unit*0.180))/pos_z;
	float sup = (400.0/(unit*0.180))/pos_z;
	float sg;
	glTranslatef( 0.0 , 1.0 , 0.0 );
	if( sGen[0] > inf && sGen[0] < sup)
		glCallList( baseDLists );
	else if( sGen[0] < inf)
		glCallList( baseDLists + 1 );
	for(int k=1 ; k<gener ; k++)
	{	glTranslatef( 0 , yGen[k]-yGen[k-1] , 0 );
		sg = sGen[k];
		if( sg > sup) continue;
		if( sg < inf)
		{	glCallList( baseDLists + 2*k + 1 );
			continue;
		}
		glCallList( baseDLists + 2*k );
	}
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void myIdle()
{	glutPostRedisplay();	//Demande de recalculer la scène
}

void InitGL()
{	/*glEnable(GL_LINE_SMOOTH);	// antialiasing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	*/
	glLineWidth(1.0);
}

void myReshape( int w , int h )
{   glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D(-w/2,w/2,-h/2,h/2);
}

/*void write(char texte[] , int nbchar , int wwidth ) // inutilisee
{	glPushMatrix();
	glBegin(GL_LINE_LOOP);	// encadrement
	glVertex2f(-wwidth/2.0 - 25.0 , -35.0 );
	glVertex2f(-wwidth/2.0 - 25.0 , 145.0 );
	glVertex2f(wwidth/2.0 + 25.0 , 145.0 );
	glVertex2f(wwidth/2.0 + 25.0 , -35.0 );
	glEnd();
	glTranslatef( -wwidth/2 , 0 , 0 );
	for( int j=0 ; j<nbchar ; j++)	
		glutStrokeCharacter(GLUT_STROKE_ROMAN,texte[j]);
	glPopMatrix();
}*/

/*void writeNom(int j)		// ecrire le nom d'index j
{	const float bordmot = 0.025;
	glPushMatrix();
	glTranslatef( coordNoms[j][0] , coordNoms[j][1] , 0.0 );
	glScalef(coordNoms[j][2],coordNoms[j][2],1.0);
	glBegin(GL_LINE_LOOP);	// encadrement
	glVertex2f(-0.5 , -0.035 );
	glVertex2f(-0.5 , 0.145 );
	glVertex2f( 0.5 , 0.145 );
	glVertex2f( 0.5 , -0.035 );
	glEnd();
	glTranslatef( -0.5 + bordmot , 0 , 0 );
	glScalef( (1-2*bordmot)/largueurs[j] , 0.001 , 1.0 );
	for( int i=0 ; i<NombreChar[j] ; i++)	
		glutStrokeCharacter(GLUT_STROKE_ROMAN,noms[j][i]);
	glPopMatrix();
}*/

void writeGen(int k)	// ecrire la (j+1)-ieme generation 
{	const float bordmot = 0.025;
	glPushMatrix();
	for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
	{ 	glPushMatrix();
		int ind = indexSosa[j];
		glTranslatef( xNoms[ind] , 0.0 , 0.0 );
		glScalef( sGen[k] , sGen[k] , 1.0 );
		glBegin(GL_LINE_LOOP);	// encadrement
		glVertex2f(-0.5 , -0.035 );
		glVertex2f(-0.5 , 0.145 );
		glVertex2f( 0.5 , 0.145 );
		glVertex2f( 0.5 , -0.035 );
		glEnd();
		glTranslatef( -0.5 + bordmot , 0 , 0 );
		glScalef( (1-2*bordmot)/largueurs[ind] , 0.001 , 1.0 );
		for( int i=0 ; i<NombreChar[ind] ; i++)	
			glutStrokeCharacter(GLUT_STROKE_ROMAN,noms[ind][i]);
		glPopMatrix();
	}
	glPopMatrix();
}

void fakeGen(int k)		// ecrire la (j+1)-ieme gen. simplifiee
{	const float bordmot = 0.025;
	glPushMatrix();
	for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
	{ 	glPushMatrix();
		int ind = indexSosa[j];
		glTranslatef( xNoms[ind] , 0.0 , 0.0 );
		glScalef( sGen[k] , sGen[k] , 1.0 );
/*		glBegin(GL_LINE_LOOP);	// encadrement
		glVertex2f(-0.5 , -0.035 );
		glVertex2f(-0.5 , 0.145 );
		glVertex2f( 0.5 , 0.145 );
		glVertex2f( 0.5 , -0.035 );
		glEnd();*/
		glRectf( -0.5 , -0.035 , 0.5 , 0.145 );
		glPopMatrix();
	}
	glPopMatrix();
}

void clavier(unsigned char key,int x,int y)
{	const float dxy_pix = 5.0;
	const float zz = 15.0;
	switch( key )
	{	case '8' :
			pos_x += -dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			pos_y += -dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			break;
		case '5' : 
			pos_x += dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			pos_y += dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			break;
		case '6' :
			pos_x -= dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			pos_y += dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			break;
		case '4' :
			pos_x += dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			pos_y -= dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			break;
		case '-' : pos_z-= pos_z/zz; break;
		case '+' : pos_z+= pos_z/zz; break;
		case '7' : theta++; break;
		case '9' : theta--;
	}
	glutPostRedisplay();
}
