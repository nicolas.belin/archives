//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>
#include <fstream>

using namespace std;

const float PI = 3.14159265359;

const int gener = 21;	// nombre de generations
const int N = 1146;		// nombre de personnes dans l'arbre

ifstream Table;			// flux entrant d'un fichier
int sosa[N];			// numero Sosa
int generation[N];		// generation de la personne i
char noms[N][30];		// noms de famille
int effGen[gener];		// effectifs de chaque generation
int indexSosa[N];		// index des numero sosa
int indexGen[gener];	// index des generations dans la liste precedente
int NombreChar[N];		// nombre de caracteres
int largueurs[N];		// largueur des noms
float coordNoms[N][3];	// positions et echelles des noms (x,y,s)
char nil[200];			// puit sans fond	

const float unit = 1000.0;// unite de longueurs abscisses et ordonnees
const float dx = 0.05;	// ecart horizontal entre les cases
const float dy = 0.4;	// ecart vertical entre les cases
float pos_x = 0;		// parametres de position de la camera
float pos_y = 0;
float pos_z = 500;
float theta = 0;

const int sw = 640;		// dimensions de la fenetre au demarrage
const int sh = 480;
const int surface = 1;
int WindowName;

void MkArb();			// construction de l'arbre
void MkGen(int n);		// composition de la (n+1)ieme ligne
void writeNom(int j);	// ecrire le nom d'index j
float f(int n);			// largueur de la nieme generation

void myReshape( int w , int h );
void myDisplay();
void myIdle();
void clavier(unsigned char key,int x,int y);
void write(char text[] , int nbchar , int wwidth);
void InitGL();
void ListMaking();


int main( int argc, char *argv[ ], char *envp[ ] )
{ 	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize( sw , sh );
	WindowName = glutCreateWindow("Un arbre");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
//	glutIdleFunc(myIdle);
	glutKeyboardFunc(clavier);
	InitGL();
	MkArb();
	ListMaking();
	glutMainLoop();
	return 0;
}

void MkGen(int n)		// composition de la (n+1)ieme ligne
{	float x;
	glTranslatef( 0.0 , n*unit*dy , 0.0 );
	for(int i = indexGen[n] ; i<indexGen[n]+effGen[n] ; i++)	
	{	x = (sosa[i] - (1 << n));

	}
}

void MkArb()					// formatage des donnees pour l'arbre
{	Table.open("caroline.csv");	// ouverture du fichier
	if( ! Table )
	{	cout << "Probleme pour ouvrir le fichier" << endl;
		exit(1);
	}
	Table.getline(nil,200);		// premiere ligne ignoree
	for(int i=0 ; i<N ; i++)	// rangement des noms et prenoms
	{ 	Table >> sosa[i];
		Table.getline(nil,200,'"');
		int j = 0;
		char c = Table.get();
		while( c != '"' )
		{	noms[i][j] = c;
			j++;
			c = Table.get();
		}
		noms[i][j] = ' ';
		j++;
		Table.getline(nil,200,'"');
		c = Table.get();
		while( c != '"' )
		{	noms[i][j] = c;
			j++;
			c = Table.get();
		}
		NombreChar[i] = j;
		Table.getline(nil,200);
	}
	int l;
	for( int i=0 ; i<N ; i++)	// largueurs des noms
	{	l=0;
		for( int j=0 ; j<NombreChar[i] ; j++)
			l+=glutStrokeWidth(GLUT_STROKE_ROMAN,noms[i][j]);
		largueurs[i]=l;
	} 

	for(int i=0 ; i<N ; i++)	// calcul de la generation de chaque nom
		generation[i] = int(floor(log(sosa[i])/log(2.0)));	
	
	for(int i=0 ; i<gener ; i++)	// calcul des effectif de chaque generation
		effGen[i] = 0;
	for(int i=0 ; i<N ; i++)
		effGen[generation[i]]++;

	indexGen[0]=0;
	for(int i=1 ; i<gener ; i++)	// calcul de l'index de chaque generation
		indexGen[i] = indexGen[i-1] + effGen[i-1];

	int sosasup;					// classement et indexage des num. sosa
	int sosamax = 0;
	for(int i=0 ; i<N ; i++)			// calcul du num. sosa maximum
		if( sosa[i] > sosamax ) sosamax = sosa[i];
	int sosainf=0;
	sosasup = sosamax+1;
	int js;
	for(int i=0 ; i<N ; i++)
	{	for(int j=0 ; j<N ; j++)
			if(sosainf < sosa[j] && sosa[j] < sosasup)
			{	sosasup = sosa[j];
				js = j;
			}
		indexSosa[i] = js;
		sosainf = sosasup;
		sosasup = sosamax+1;
	}

	int u;					// position de chaque nom
	int a;					// sosa = 2^n + 2a + u 
	int n;
	coordNoms[0][0] = 0.0;
	coordNoms[0][1] = 0.0;
	coordNoms[0][2] = 1.0;
	for(int i=1 ; i<N ; i++)
	{	u = sosa[i] % 2;
		n = generation[i];
		a = (sosa[i] - (1<<n) - u)/2;
		coordNoms[i][0] = a*(2+3*dx) + 0.5 + u*(1+dx)
						- 0.5*((1<<(n)) + (1<<(n-1))*3*dx - 2*dx);
		coordNoms[i][0] *= unit;
		coordNoms[i][1] = n*dy*unit;
		coordNoms[i][2] = 1.0;
	}	
}

float f(int n)
{	return float(n);
}

void ListMaking()
{	glNewList(surface,GL_COMPILE);
	glColor3f(1.0 , 1.0 , 1.0 );
	for(int i = 0 ; i< N; i++) writeNom(indexSosa[i]);
	glEndList();
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);	//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
	glScalef(pos_z/500.0,pos_z/500.0,pos_z/500.0);
	glRotatef(theta,0,0,1);
	glTranslatef(pos_x,pos_y,0);
	glCallList(surface);
	glFlush();
	glutSwapBuffers();
}

void myIdle()
{	glutPostRedisplay();	//Demande de recalculer la scène
}

void InitGL() {}

void myReshape( int w , int h )
{   glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D(-500*w/h,500*w/h,-500,500);
}

void write(char texte[] , int nbchar , int wwidth )
{	glPushMatrix();
	glBegin(GL_LINE_LOOP);	// encadrement
	glVertex2f(-wwidth/2.0 - 25.0 , -35.0 );
	glVertex2f(-wwidth/2.0 - 25.0 , 145.0 );
	glVertex2f(wwidth/2.0 + 25.0 , 145.0 );
	glVertex2f(wwidth/2.0 + 25.0 , -35.0 );
	glEnd();
	glTranslatef( -wwidth/2 , 0 , 0 );
	for( int j=0 ; j<nbchar ; j++)	
		glutStrokeCharacter(GLUT_STROKE_ROMAN,texte[j]);
	glPopMatrix();
}

void writeNom(int j)		// ecrire le nom d'index j
{	glPushMatrix();
	glTranslatef( coordNoms[j][0] , coordNoms[j][1] , 0.0 );
	glScalef( unit/(largueurs[j]+50) , 1.0 , 1.0 );
	write(noms[j],NombreChar[j],largueurs[j]);
	glPopMatrix();
}

void clavier(unsigned char key,int x,int y)
{	const float zxy = 2000.0;
	const float zz = 200.0;
	switch( key )
	{	case '8' :
			pos_x += -zxy/pos_z*sin(PI*theta/180);
			pos_y += -zxy/pos_z*cos(PI*theta/180);
			break;
		case '5' : 
			pos_x += zxy/pos_z*sin(PI*theta/180);
			pos_y += zxy/pos_z*cos(PI*theta/180);
			break;
		case '6' :
			pos_x -= zxy/pos_z*cos(PI*theta/180);
			pos_y += zxy/pos_z*sin(PI*theta/180);
			break;
		case '4' :
			pos_x += zxy/pos_z*cos(PI*theta/180);
			pos_y -= zxy/pos_z*sin(PI*theta/180);
			break;
		case '-' : pos_z-= pos_z/zz; break;
		case '+' : pos_z+= pos_z/zz; break;
		case '7' : theta++; break;
		case '9' : theta--;
	}
	glutPostRedisplay();
}
