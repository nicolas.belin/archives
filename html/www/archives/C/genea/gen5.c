//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>
#include <fstream>
#include <iomanip>

using namespace std;

const float PI = 3.14159265359;

char **arguments;		// arguments en ligne de commande
int effArg;				// nombre d'arguments en ligne de commande

int gener;				// nombre de generations
int N;					// nombre de personnes dans l'arbre

int* sosa = 0;			// numero Sosa
int* generation = 0;	// generation de la personne i
char* noms = 0;			// noms de famille
int* effGen = 0;		// effectifs de chaque generation
int* indexGen = 0;		// index des generations dans la liste precedente
int* nombreChar = 0;	// nombre de caracteres
int* largueurs = 0;		// largueur des noms
float* xNoms = 0;		// abscisses des noms (x,y,s)
float* yGen = 0;		// ordonnee des generations
float* sGen = 0;		// coefficients de reduction des generations

const float unit = 1000.0;// unite de longueurs abscisses et ordonnees
const float dx = 0.05;	// ecart horizontal entre les cases
const float dy = 0.8;	// coeff. de l'ecart vertical entre les cases
float pos_x = 0;		// parametres de position de la camera
float pos_y = -1;
float pos_z = 0.5;
float theta = 0;

int WindowName;			// numero de la fenetre ouverte par Glut
const int sw = 640;		// dimensions de la fenetre au demarrage
const int sh = 480;
int	baseDLists;			// 1ier index des display lists des generations
const int branches = 1;

void extractionDonnees();// extraction et rangement des donnees
void texte();			// sortie en mode texte
void swap(int a,int b);	//commute 2 lignes
int trouveEnfant(int sosaP);// trouve l'eventuel enfant de sosaP
float f(float x);		// largueur de la nieme generation
void mkArb();			// construction de l'arbre
void traceBranches();	// tracer des branches
void MkGen(int n);		// composition de la (n+1)ieme ligne
void writeNom(int j);	// ecrire le nom d'index j
void writeGen(int k);	// ecrire la (j+1)-ieme generation 
void fakeGen(int k);	// ecrire la (j+1)-ieme gen. simplifiee
void ListsMaking();		// mise en place des display lists
void sortie();			// nettoyage avant de quitter le programme 

void myReshape( int w , int h );
void myDisplay();
void myIdle();
void clavier(unsigned char key,int x,int y);
void write(char text[] , int nbchar , int wwidth);
void InitGL();

int main( int argc, char *argv[] )
{ 	arguments = argv;	// sauvegarde des arguments
	effArg = argc;

	extractionDonnees();
	texte();
	mkArb();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize( sw , sh );
	WindowName = glutCreateWindow("Un arbre");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
//	glutIdleFunc(myIdle);
	glutKeyboardFunc(clavier);
	InitGL();
	ListsMaking();
	glutMainLoop();
	sortie();
	return 0;
}

float f(float x)
{	return x;
}

// mise en place des display lists
void ListsMaking()
{	glNewList(branches,GL_COMPILE);
	traceBranches();
	glEndList();

	baseDLists=glGenLists(2*gener);	// genere les numero de gener listes
	for(int k=0 ; k<gener ; k++)
	{	glNewList( baseDLists + 2*k , GL_COMPILE );
			writeGen(k);
		glEndList();
		glNewList( baseDLists + 2*k + 1 , GL_COMPILE );
			fakeGen(k);
		glEndList();
	}
}

int trouveEnfant(int sosaP)		// trouve l'eventuel enfant de sosaP
{	int sosaE = int( floor( sosaP/2.0 ) );
	for(int i = 0 ; i<N ; i++)
		if(sosa[i] == sosaE) return i;
	return -1;
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
	glScalef( unit*pos_z , unit*pos_z , 1.0 );
	glRotatef( theta , 0 , 0 , 1 );
	glTranslatef( pos_x , pos_y , 0 );
	glColor3f(1.0 , 1.0 , 1.0 );

	glCallList(branches);
	glPushMatrix();
	float inf = (5.0/(unit*0.180))/pos_z;
	float sup = (400.0/(unit*0.180))/pos_z;
	float sg;
	glTranslatef( 0.0 , 1.0 , 0.0 );
	if( sGen[0] > inf && sGen[0] < sup)
		glCallList( baseDLists );
	else if( sGen[0] < inf)
		glCallList( baseDLists + 1 );
	for(int k=1 ; k<gener ; k++)
	{	glTranslatef( 0 , yGen[k]-yGen[k-1] , 0 );
		sg = sGen[k];
		if( sg > sup) continue;
		if( sg < inf)
		{	glCallList( baseDLists + 2*k + 1 );
			continue;
		}
		glCallList( baseDLists + 2*k );
	}
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void myIdle()
{	glutPostRedisplay();	//Demande de recalculer la scène
}

void InitGL()
{	/*glEnable(GL_LINE_SMOOTH);	// antialiasing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	*/
	glLineWidth(1.0);
}

void myReshape( int w , int h )
{   glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D(-w/2,w/2,-h/2,h/2);
}

// ecrire la (j+1)-ieme generation : 
void writeGen(int k)
{	const float bordmot = 0.025;
	glPushMatrix();
	for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
	{ 	glPushMatrix();
		glTranslatef( xNoms[j] , 0.0 , 0.0 );
		glScalef( sGen[k] , sGen[k] , 1.0 );
		glBegin(GL_LINE_LOOP);	// encadrement
		glVertex2f(-0.5 , -0.035 );
		glVertex2f(-0.5 , 0.145 );
		glVertex2f( 0.5 , 0.145 );
		glVertex2f( 0.5 , -0.035 );
		glEnd();
		glTranslatef( -0.5 + bordmot , 0 , 0 );
		glScalef( (1-2*bordmot)/largueurs[j] , 0.001 , 1.0 );
		for( int i=0 ; i<nombreChar[j] ; i++)	
			glutStrokeCharacter(GLUT_STROKE_ROMAN,noms[j*60+i]);
		glPopMatrix();
	}
	glPopMatrix();
}

// ecrire la (j+1)-ieme gen. simplifiee
void fakeGen(int k)
{	const float bordmot = 0.025;
	glPushMatrix();
	for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
	{ 	glPushMatrix();
		glTranslatef( xNoms[j] , 0.0 , 0.0 );
		glScalef( sGen[k] , sGen[k] , 1.0 );
/*		glBegin(GL_LINE_LOOP);	// encadrement
		glVertex2f(-0.5 , -0.035 );
		glVertex2f(-0.5 , 0.145 );
		glVertex2f( 0.5 , 0.145 );
		glVertex2f( 0.5 , -0.035 );
		glEnd();*/
		glRectf( -0.5 , -0.035 , 0.5 , 0.145 );
		glPopMatrix();
	}
	glPopMatrix();
}

// trace ses branches
void traceBranches()
{	glPushMatrix();
	glColor3f(1.0 , 1.0 , 1.0 );
	glBegin(GL_LINES);
		int indE;
		for(int k=1 ; k<gener ; k++)
		for(int j=indexGen[k] ; j<indexGen[k]+effGen[k] ; j++)
		{	indE = trouveEnfant(sosa[j]);// branche sous la case
			if( indE != -1)
			{	glVertex2f(xNoms[j],yGen[k]-0.035*sGen[k]);
				glVertex2f(xNoms[indE],yGen[k-1]+0.145*sGen[k-1]);
			}
		}
	glEnd();
	glPopMatrix();
}

// gestion du clavier
void clavier(unsigned char key,int x,int y)
{	const float dxy_pix = 5.0;
	const float zz = 15.0;
	switch( key )
	{	case '8' :
			pos_x += -dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			pos_y += -dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			break;
		case '5' : 
			pos_x += dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			pos_y += dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			break;
		case '6' :
			pos_x -= dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			pos_y += dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			break;
		case '4' :
			pos_x += dxy_pix*cos(PI*theta/180)/(pos_z*unit);
			pos_y -= dxy_pix*sin(PI*theta/180)/(pos_z*unit);
			break;
		case '-' : pos_z-= pos_z/zz; break;
		case '+' : pos_z+= pos_z/zz; break;
		case '7' : theta++; break;
		case '9' : theta--;
	}
	glutPostRedisplay();
}

// Extraction des donnees du fichier, allocation memoire dynamique,
// rangement des donnees dans les tableaux :
void extractionDonnees()
{	// gestion des arguments
	if( effArg == 1 )
	{	cout << "Argument : un fichier au format csv (Comma-separated values)" << endl << endl;
	sortie();
	}

	// ouverture du fichier :
	ifstream Table;				// flux entrant d'un fichier
	Table.open( arguments[1] );
	if( ! Table )
	{	cout << "Probleme pour ouvrir le fichier" << endl;
		sortie();
	}

	// construction d'une poubelle
	char *nil = new char[200];

	// format csv ? test tres sommaire
	char testc;
	Table.getline( nil , 200 );	// on passe la 1iere ligne
	testc = Table.get();		// extraction du premier caractere
	if( testc != '1' )
	{	cout << "Ce fichier n'est pas au bon format !" << endl << endl;
		sortie();
	}

	// comptage du nombre de ligne :
	int l = 0;
	while( Table )
	{	Table.getline( nil , 200 ); // on suppose aucune ligne > 200 char.
		l++;
	}
	N = l - 1;	// nombre de personne dans l'arbre
	if( N <= 0 )
	{	cout << "Il n'y a personne dans ce tableau !" << endl;
		sortie();
	}
	Table.close();

	// extraction des numero sosa, noms et prenoms.
	noms = new char[N*60];		// allocation de la memoire
	sosa = new int[N];
	nombreChar = new int[N];

	Table.open( arguments[1] );	// re-ouverture du fichier
		Table.getline( nil , 200 );// premiere ligne ignoree
	for(int i=0 ; i<N ; i++)	// extraction des noms et prenoms
	{ 	Table >> sosa[i];
		Table.getline( nil ,200,'"');
		int j = 0;
		char c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		noms[i*60+j] = ' ';
		j++;
		Table.getline(nil,200,'"');
		c = Table.get();
		while( c != '"' )
		{	noms[i*60+j] = c;
			j++;
			c = Table.get();
		}
		nombreChar[i] = j;
		Table.getline(nil,200);
	}
	Table.close();

	// destruction de la poubelle
	delete[] nil;

	// Classement et indexage des num. sosa :
	int sosasup;
	int sosamax = 0;
	for(int i=0 ; i<N ; i++)	// calcul du num. sosa maximum
		if( sosa[i] > sosamax ) sosamax = sosa[i];
	int sosainf=0;
	sosasup = sosamax+1;
	int js = -1;
	int doublons = 0;
	for(int i=0 ; i<N ; i++)
	{	for(int j=0 ; j<N ; j++)
			if(sosainf < sosa[j] && sosa[j] <= sosasup)
			{	sosasup = sosa[j];
				if( sosa[j] == sosa[js] && j != js )
				{	cout << endl << "Doublon lignes "
						<< js+2 << " et " << j+2 << " !" << endl; 
					doublons++;
				}
				js = j;
			}
		swap( i , js );
		sosainf = sosasup;
		sosasup = sosamax+1;
	}
	N -= doublons;

	// calcul de la generation de chaque personne :
	generation = new int[N];
	for(int i=0 ; i<N ; i++)
		generation[i] = int(floor(log(sosa[i])/log(2.0)));	
	
	// Calcul du nombre de generations :
	gener = 0;
	for(int i=0 ; i<N ; i++)
		if( generation[i] > gener ) gener = generation[i];		
	gener++;

	// Calcul de l'effectif de chaque generation :
	effGen = new int[gener];
	for(int i=0 ; i<gener ; i++)
		effGen[i] = 0;
	for(int i=0 ; i<N ; i++)
		effGen[generation[i]]++;

	// Calcul de l'index de chaque generation :
	indexGen = new int[gener];
	indexGen[0]=0;
	for(int i=1 ; i<gener ; i++)
		indexGen[i] = indexGen[i-1] + effGen[i-1];
}

void texte()	// sortie en mode texte
{	cout << endl;
	cout << "Nombre d'ascendants (hors doublons) : " << N << endl;
	cout << endl;
	cout << "Nombre de generations : " << gener << endl;
	cout << endl;
	cout << "Generations |" << " Effectifs"<< endl;
	for(int k = 0 ; k<gener ; k++)
		cout << setw(11) << k+1 << " | " << setw(9) << effGen[k] << endl;
}

void mkArb()					// formatage des donnees pour l'arbre
{	// Calcul de la largueurs des noms et prenoms :
	largueurs = new int[N];
	int l;
	for( int i=0 ; i<N ; i++)
	{	l=0;
		for( int j=0 ; j<nombreChar[i] ; j++)
			l+=glutStrokeWidth(GLUT_STROKE_ROMAN,noms[60*i+j]);
		largueurs[i]=l;
	} 
	// calcul des ordonnees et coeff. de reduction de chaque generation
	yGen = new float[gener];
	sGen = new float[gener];
	float h=1;
	float s=f(h);
	yGen[0] = h;
	sGen[0] = s;
	for(int k=1 ; k<gener ; k++)
	{	h += s*dy;
		s = f(h) / ( (1<<k) + dx*( 3*(1<<(k-1)) - 2 ) );
		yGen[k] = h;
		sGen[k] = s;
	}

	// abscisses des cases, tenant compte de la reduction
	int u;					// sosa = 2^n + 2a + u 	
	int a;
	xNoms = new float[N];
	xNoms[0] = 0.0;
	for(int k=1 ; k<gener ; k++)
	for(int i=indexGen[k] ; i<indexGen[k]+effGen[k] ; i++)
	{	u = sosa[i] % 2;
		a = (sosa[i] - (1<<k) - u)/2;
		xNoms[i] = a*(2+3*dx) + 0.5 + u*(1+dx)
			- 0.5*((1<<(k)) + (1<<(k-1))*3*dx - 2*dx);
		xNoms[i] *= sGen[k];
	}
}

// commute deux lignes du tableau noms[] avec leur num. sosa 
void swap( int a , int b)
{	char tempc;
	int sosat;
	for(int i = 0 ; i<60 ; i++)
	{	tempc = noms[60*a + i];
		noms[60*a + i] = noms[60*b + i];
		noms[60*b + i] = tempc;
	}
	sosat = sosa[a];
	sosa[a] = sosa[b];
	sosa[b] = sosat;
}

// Quitter du programme
void sortie()
{	delete[] xNoms;
	delete[] sGen;
	delete[] yGen;
	delete[] indexGen;
	delete[] effGen;
	delete[] generation;
	delete[] nombreChar;
	delete[] sosa;
	delete[] noms;
	exit(0);
}
