// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...
#include <cstdlib>

#define screen_w 1190	// screen width (largueur)
#define screen_h 850	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
float xmin = -0.7;        // viewport
float xmax = 0.7;
float ymin = -0.5;
float ymax = 0.5;
float x;                // t -> L(t)*cs(x-t)
float s=0.125;           // support de la fonction c : [-s,s]

Uint32 *bitmap;
int abs(int x);
float L(float t);
float c(float t);
float cs(float t);
float sx(float t);
float Lxcs( float x);
float conv( float p );
float integrate( float (*f)(float), float a, float b, int n );
void line_x( int x0, int y0, int dx, int dy);
void line_y( int x0, int y0, int dx, int dy);
void line( int xa, int ya, int xb, int yb); // coordonnees en pixels
void linev( float x1, float y1, float x2, float y2); // coordonnees dans viewport
void curvef( float (*f)(float) , float a, float b );
// ****************** main() ***************************************

int main(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
	
  SDL_Surface *screen;
  SDL_Event event;
	
  screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE);
  if ( screen == NULL ){
    cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
    exit(1);
  }
  bitmap = (Uint32*)screen->pixels;	
  
  linev(xmin,0,xmax,0);
  linev(0,ymin,0,ymax);
  linev(-.25,ymin,-0.25,ymax);
  linev(0.125,ymin,0.125,ymax);
  linev(0.375,ymin,0.375,ymax);

  curvef(L,xmin,xmax);
  curvef(conv,xmin,xmax);
  curvef(sx,xmin,xmax);

  SDL_Flip(screen);
  while(!(SDL_PollEvent(&event) && event.type == SDL_MOUSEBUTTONDOWN)){}		

  SDL_Quit();

  cout<< integrate(c,-1,1,10000);

  return 0;
}
// ****************************** fonctions **********************************

int abs(int x){
  // fonction valeur absolue
  if( x<0 ) return -x;
  else return x;
}

float L(float t){
  if( t<-0.5 ) return 0;
  if( t<0 ) return t+0.5;
  if( t<0.25 ) return -4*t+0.5;
  if( t<0.5 ) return 2*t-1;
  return 0;
}

float c(float t){
  // fonction regularisante, C3, de support [-1;1] et d'integrale 1
  if( t<=-1 ) return 0;
  if( t<1 ){
    return 0.5;//exp( t*t/(t*t-1))/1.2069;
  }
  return 0;
}

float ck(float t){
  // fonction regularisante, C3, de support [-1;1] et d'integrale 1
  if( t<-1 ) return 0;
  if( t<1 ){
    float p = cos( (PI/2)*t );
    return 4*p*p*p*p/3;
  }
  return 0;
}

float cs(float t){
  // fonction t -> c(x/s)/s, de support [-s;s] et d'integrale 1
  return c(t/s)/s;
}

float sx(float t){
  if( t<-0.25 ) return 1.0/3*(t+0.7)+0.1;
  if( t<0.125 ) return -1.0/3*(t+0.25)+0.25;
  return 0.125;
}

float Lxcs(float t){
  // fonction t -> L(x-t)*cs(t)
  // cette fonction a pour support [-s;s]
  return L(x-t)*cs(t);
}

float conv( float p ){
  // Calcule l'integrale de f(x-t)*g(t) sur [-s;s]
  x=p;
  s=sx(x);
  return integrate(Lxcs,-s,s,1000);
}

float integrate( float (*f)(float), float a, float b, int n ){
  // integre la fontion f sur l'intervalle [a;b].
  // n est le nombre de subdivisions
  // l'interpolation est affine
  // la distance L1 entre f et la fonction interpolatrice est majoree par :
  // 1/6*max|f''|*(b-a)^3
  double d = double( b-a ) / n;
  double S = 0;

  for(int k=1 ; k<n ; k++ ) S += f( a + k*d );
  S += (f(a)+f(b))/2;
  S *= d;
  return float(S);
}

void line( int xa, int ya, int xb, int yb){
  // trace le segment de (xa,ya) a (xb,yb), coordonnees en pixels
  int dx = xb-xa;
  int dy = yb-ya;
  int a;
  int x,y;

  if( abs(dx) > abs(dy)){
    if( xa <= xb ) line_x( xa, ya, dx, dy );
    else line_x( xb, yb, -dx, -dy );
  }
  else {
    if( ya <= yb ) line_y( xa, ya, dx, dy );
    else line_y( xb, yb, -dx, -dy );
  }
  return;
}

void line_x( int x0, int y0, int dx, int dy){    // |dx|>|dy| et dx>0
  int y;
  for(int i=0; i<=dx; i++){
    y = dy*i;
    y /= dx;
    y += y0;
    bitmap[ x0 + i + y*screen_w ] = 0xffffff;
  }
  return;
}

void line_y( int x0, int y0, int dx, int dy){    // |dy|>|dx| et dy>0
  int x;
  for(int i=0; i<=dy; i++){
    x = dx*i;
    x /= dy;
    x += x0;
    bitmap[ x + ( y0+i ) * screen_w ] = 0xffffff;
  }
  return;
}

void linev( float x1, float y1, float x2, float y2){
  // trace le segment de (x1,y1) a (x2,y2), coordonnes du viewport
  int xa = int( ((screen_w-1) * (x1-xmin)) / (xmax-xmin) );
  int xb = int( ((screen_w-1) * (x2-xmin)) / (xmax-xmin) );
  int ya = int( (screen_h-1) * (1 - (y1-ymin)/(ymax-ymin)) );
  int yb = int( (screen_h-1) * (1 - (y2-ymin)/(ymax-ymin)) );
  if( (xa < screen_w) && (xa >= 0) && (xb < screen_w) && (xb >= 0) &&
      (ya < screen_h) && (ya >= 0) && (yb < screen_h) && (yb >= 0) )
    line( xa, ya, xb, yb);
  return;
}

void curvef( float (*f)(float) , float a, float b ){
  // Trace la courbe de f sur l'intervalle [a;b]
  int n=screen_w/2;
  float x0,y0,x1,y1;
  x0 = a;
  y0 = f(a);
  
  for(int i=0; i<n; i++){
    x1 = a + (i+1)*(b-a)/n;
    y1 = f(x1);
    linev(x0,y0,x1,y1);
    x0 = x1;
    y0 = y1;
  }
}
