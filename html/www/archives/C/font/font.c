#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <assert.h>
#include <SDL2/SDL.h>	// API video, clavier, timer, souris...
#include <GL/gl.h>	// API OpenGL
#include <ft2build.h>   // freetype
#include FT_FREETYPE_H
#include "font.h"
#include "sdl-opengl.h" // mes fonctions gl

#define REFRESH_EVENT 1
#define REFRESH_PER_SECOND 25
#define PI 3.141592
#define RAD(X) (0.0174533*(X))
#define MAX(X,Y) ( (X) >= (Y) ? (X) : (Y) ) 
#define MIN(X,Y) ( (X) <= (Y) ? (X) : (Y) ) 
#define ABS(X) ( (X) >= 0 ? (X) : -(X) )
#define SQ(X) ((X)*(X))
#define MOD(X,N) ( (X) >= 0 ? (X)%(N) : (X)%(N)+N )

enum boolean loop = YES;
enum boolean modified = YES;   // la vue a �t� modifi�e
enum boolean modifying = NO;   // les points sont en cours de modification
static int vertices_nr = 3;
static GLfloat *coords;        // tableau des coordonn�es (au format float )
static GLuint indices_buffer_name;
static GLuint vao_id;        // vertex array object

static FT_Library library;
static FT_Face face;
static GLfloat* interpolate_contour_points( FT_Outline*, int, int, uint );

static struct {    // position de la cam�ra
  float x;
  float y;
  float w;
} camera;
static GLfloat projection[16];
static GLfloat move_camera[16];
static const int texture_nr = 1;
static GLuint texture_id[1];
static GLuint texture_unit_index = 0; 

static void sigterm_handler( int );
static void check_timer( SDL_TimerID );
static void print_font_info( FT_Face );
static void start_video();
static Uint32 refresh_callback( Uint32 , void* );
static void set_view( const struct window* );
static void draw_outline( const void* );
static void draw_triangles( const void* );
static void setup_outline( void* );
static void setup_solid( void* );

int main( int argc , char **argv ){
  signal( SIGTERM, &sigterm_handler );

  // init FreeType
  if (  FT_Init_FreeType( &library ) ){
    fprintf( stderr, "Couldn't init FreeType...\n" );
    exit(1);
  }
  const char *fontfile = "DejaVuSerif.ttf";
  if ( FT_New_Face( library, fontfile, 0, &face ) ){
    fprintf( stderr, "Couldn't open font %s...\n", fontfile );
    exit(1);
  }
  print_font_info( face );

  if ( FT_Set_Pixel_Sizes( face, 100, 100 ) )
    printf( "Couldn't set pixel size...\n" );

  uint index;
  if ( !( index = FT_Get_Char_Index( face, 'e' ) ) )
    printf( "Couldn't get char index...\n" );
  printf( " index = %d\n", index );

  if ( FT_Load_Glyph( face, index, FT_LOAD_DEFAULT ) )
    printf( "Couldn't load glyph %i...\n", index );

  FT_GlyphSlot slot = face->glyph;
  FT_Outline ol = slot->outline;

  printf( "n_points = %i\n", slot->outline.n_points );
  printf( "n_contours = %i\n", slot->outline.n_contours );
  printf( " %i,  %i\n", slot->outline.contours[0], slot->outline.contours[1] );
  int j, k, beg, end;
  beg = 0;
  end = ol.contours[0];
  for ( k=0; k<ol.n_contours; k++ ){
    printf( "Contours no %i :\n", k );
    for ( j=beg; j<=end; j++ ){
      printf( "  Point %i (%s) : %i x %i\n",
              j,
              (ol.tags[j] & 0x1) ? "On" : ((ol.tags[j] & 0x2) ? "Off3" : "Off2"),
              ol.points[j].x,
              ol.points[j].y ); 
    }
    beg = ol.contours[k] + 1;
    if ( ol.contours[k+1]==0 )
      end = ol.n_points;
    else
      end = ol.contours[k+1]; 
  }
  interpolate_contour_points( &ol, ol.contours[0]+1, ol.n_points-1, 3 ); 


  vertices_nr = slot->outline.n_points;
  coords = malloc( 2 * vertices_nr * sizeof(*coords) );
  if ( coords == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }
  for ( j=0; j<vertices_nr; j++ ){
    coords[ 2*j ] = ( (GLfloat)slot->outline.points[j].x ) / 6000.0;
    coords[ 2*j + 1 ] = ( (GLfloat)slot->outline.points[j].y ) / 6000.0; 
  }
  

  /*
  if ( FT_Render_Glyph( slot, FT_RENDER_MODE_NORMAL ) )
    printf( "Couldn't render glyph %i...\n", index );

  printf( "rows x width = %i x %i  pitch = %i\n",
          slot->bitmap.rows, slot->bitmap.width, slot->bitmap.pitch );
  printf( "bitmap_left = %i   bitmap_top = %i\n",
          slot->bitmap_left, slot->bitmap_top );

  int k, l, m = 0;
  unsigned char *b = slot->bitmap.buffer;
  for ( k=0; k<slot->bitmap.rows; k++ ){
    printf( "\n" );
    for ( l=0; l<slot->bitmap.width; l++ )
      printf( "%i", b[m+l]/32 );
    m += slot->bitmap.pitch;
  }
  */
  
  struct window win = init_SDL_GL( 0, 0, "NEW !!!" );
  set_view( &win );
  print_gl_specs();

  struct rendering_context outfits[] = {
    { "Outline",
      &draw_outline,
      &setup_outline,
      "bare.shader.vert.c",
      NULL,
      "bare.shader.frag.c",
      -1, NULL, NULL },
    { "Solid",
      &draw_triangles,
      &setup_solid,
      "solid.shader.vert.c",
      NULL,
      "solid.shader.frag.c",
      -1, NULL, NULL }};

  const int rendering_mode_nr = sizeof(outfits) / sizeof(outfits[0]);
  int i;
  for ( i = 0; i < rendering_mode_nr; i++ )
    compile_shaders( outfits + i );
  printf("\n");
  int r_mode = 0;

  srand(time(NULL));

  // buffers :
  const int buffer_nr = 1;
  GLuint buffer_id[buffer_nr];
  glGenBuffers( buffer_nr, buffer_id );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[0] );
  glBufferData( GL_ARRAY_BUFFER,
                2*vertices_nr*sizeof(*coords),
                coords, GL_STATIC_DRAW );
  
  // cr�ation du VAO
  glGenVertexArrays( 1, &vao_id );
  glBindVertexArray( vao_id );

  glEnableVertexAttribArray( 0 );
  glBindBuffer( GL_ARRAY_BUFFER, buffer_id[0] );
  glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );

  glBindVertexArray(0);   // fin

  camera.x = 0.0;
  camera.y = 0.0;
  camera.w = 1.0;  

  setup_rendering( outfits + r_mode );
  start_video();

  SDL_Thread *modifying_thread = NULL;

  /*  
  modifying_thread = SDL_CreateThread( transform, "modifying_thread", (void*)(&data) );
  if( modifying_thread == NULL ){
    fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }
  */

  
  enum boolean left_button_down = NO;
  SDL_Event event;
  while( loop == YES ){
    SDL_WaitEvent( &event );
    switch(event.type){
    case SDL_WINDOWEVENT:
      switch (event.window.event){
        case SDL_WINDOWEVENT_RESIZED :
        win.w = event.window.data1;
        win.h = event.window.data2;
        set_view( &win );
        modified = YES;
        break;
      case SDL_WINDOWEVENT_EXPOSED :
        modified = YES;
        break;
      }
      break;
    case SDL_USEREVENT :
      if( event.user.code == REFRESH_EVENT ){
        if( modified == YES || modifying == YES ){
          refresh_frame( &win, outfits + r_mode );
          modified = NO;
        }
      }
      break;
    case SDL_QUIT :
      loop = NO;
      break;
    case SDL_MOUSEBUTTONDOWN :
      switch( event.button.button ){
      case SDL_BUTTON_LEFT :
        left_button_down = YES;
        break;
      }
      break;
    case SDL_MOUSEBUTTONUP :
      switch( event.button.button ){
      case SDL_BUTTON_LEFT :
        left_button_down = NO;
        break;
      }
      break;
    case SDL_MOUSEMOTION :
      if ( left_button_down == YES ){
        camera.y -= 2.0 * (myfloat)event.motion.yrel / win.h;
        camera.x += 2.0 * (myfloat)event.motion.xrel / win.h;
        modified = YES;
      }
      break;
    case SDL_MOUSEWHEEL :
      camera.w += event.wheel.y * 0.05;
      modified = YES;
      break;
    case SDL_KEYDOWN:
      switch( event.key.keysym.sym ){
      case SDLK_SPACE :
        r_mode = ( r_mode + 1 ) % rendering_mode_nr;
        setup_rendering( outfits + r_mode );
        modified = YES;
        break;
      case SDLK_ESCAPE :
      case SDLK_q :
        loop = NO;
        break;
      case SDLK_s :
        save_color_buffer( win.w, win.h, NULL );
        break;
      case SDLK_f :
        toggle_fullscreen( &win );
        set_view( &win );
        modified = YES;
      }
      break;
    }
  }
 
  SDL_WaitThread( modifying_thread, NULL );
  //  print();
  SDL_Quit();
  return 0;
}

static void sigterm_handler( int s ){
  loop = NO;
}

static void print_font_info( FT_Face face ){
  printf( "\nNumber of faces : %i\n", face->num_faces );
  printf( "Family name : %s\n", face->family_name );
  printf( "Style name  : %s\n", face->style_name );
  printf( "Scalable : %s\n",
          FT_IS_SCALABLE(face) ? "yes" : "no" );
  printf( "Glyphs names : %s\n",
          FT_HAS_GLYPH_NAMES(face) ? "yes" : "no" );
  printf( "Number of glyphs : %i\n", face->num_glyphs );
  if ( face->style_flags )
    printf( "Style : %s%s\n",
            ( face->style_flags & FT_STYLE_FLAG_ITALIC ) ? "Italic" :"",
            ( face->style_flags & FT_STYLE_FLAG_BOLD ) ? "Bold" : "" );
  printf( "Number of charmaps : %i\n", face->num_charmaps );
  printf( "Font units per EM : %i\n", face->units_per_EM );
  /*
  int i;
  char buffer[30];
  if ( FT_HAS_GLYPH_NAMES(face) ){
    for ( i=0; i<face->num_glyphs; i++ )
      if( ! FT_Get_Glyph_Name( face, i, buffer, 30 ) )
        printf( "%s ", buffer );
  }
  */
}

static GLfloat* interpolate_contour_points( FT_Outline *outline, int first, int last, uint subdiv ){
#define IS_ON(A) (outline->tags[A] & 0x1) 
  int i, N;
  int prev_is_off = 0;
  // recherche du nombre de points final :
  N = 0;
  for ( i=first; i<=last; i++ ){
    if ( IS_ON(i) ){
      N++;
      prev_is_off = 0; 
    }
    else {
      N += subdiv - 1;
      if ( prev_is_off )
        N++;
      prev_is_off = 1;
    }
  }
  printf( "%i\n", N );
  /*
  GLfloat buffer = malloc( 2 * N * sizeof(*buffer) );
  if ( buffer == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }
  prev_is_off = 0;
  uint l = 0;
  for ( i=first; i<=last; i++ ){
    if ( IS_ON(i) ){
      buffer[l++] = ( (GLfloat)outline.points[i].x ) / 6000.0;
      buffer[l++] = ( (GLfloat)outline.points[i].y ) / 6000.0;
      prev_is_off = 0; 
    }
    else {
      
    }
  }
  */  
  return NULL;
}

static void check_timer( SDL_TimerID id ){
  if( id == 0 ){
    fprintf(stderr, "Impossible d'initialiser un timer: %s\n", SDL_GetError());
    exit(1);
  }
}

static void start_video(){
  check_timer( SDL_AddTimer( 1000/REFRESH_PER_SECOND, refresh_callback, NULL ) );
}

static Uint32 refresh_callback( Uint32 intervalle, void *not_used ){
  // fonction appel�e r�guli�rement pour donner l'ordre de rafraichir l'affichage
  SDL_Event event;
  event.user.type = SDL_USEREVENT;
  event.user.code = REFRESH_EVENT;
  SDL_PushEvent( &event );
  return intervalle;
}

static void set_view( const struct window *win ){
  shape_window( win );
  myOrtho( projection, -(1.0*win->w)/win->h, (1.0*win->w)/win->h, -1.0, 1.0, 1.0, -1.0 );  
}

static void draw_outline( const void* context ){
  int i, j, k;
  const struct rendering_context *c = context;

  make_camera_matrix( move_camera, camera.x, camera.y, camera.w );

  glUniformMatrix4fv( c->uniforms_loc[0], 1, GL_FALSE, (GLfloat*)c->uniforms[0]  );
  glUniformMatrix4fv( c->uniforms_loc[1], 1, GL_FALSE, (GLfloat*)c->uniforms[1]  );

  glBindVertexArray( vao_id );
  glDrawArrays( GL_LINE_LOOP, 0, vertices_nr );
  glBindVertexArray( 0 );
}

static void draw_triangles( const void* context ){
  const struct rendering_context *c = context;

  //make_camera_matrix( move_camera, camera.r, RAD(camera.phi), RAD(camera.theta) );

  glUniformMatrix4fv( c->uniforms_loc[0], 1, GL_FALSE, (GLfloat*)c->uniforms[0]  );
  glUniformMatrix4fv( c->uniforms_loc[1], 1, GL_FALSE, (GLfloat*)c->uniforms[1]  );
  glUniform1iv( c->uniforms_loc[2], 1, (GLint*)c->uniforms[2] );

  glBindVertexArray( vao_id );

  //glActiveTexture( GL_TEXTURE0 );
  //glBindTexture( GL_TEXTURE_2D, texture_id[0] );

  glDrawElements( GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0 );
  glBindVertexArray( 0 );
}

static void setup_outline( void *context ){

  struct rendering_context *c = context;
  const int uniform_nr = 2;
  glClearColor( 0.0, 0.0, 0.0, 0.0 );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST );
  glLineWidth( 1.5 );
  glEnable( GL_BLEND );
  glEnable( GL_LINE_SMOOTH );
  glDisable( GL_DEPTH_TEST );
  glDisable( GL_CULL_FACE );
  glDisable(GL_TEXTURE_2D);
  assert( c->program > 0 );
  glUseProgram( c->program );
  c->uniforms = malloc( uniform_nr * sizeof( *(c->uniforms) ) );
  c->uniforms_loc = malloc( uniform_nr * sizeof( *(c->uniforms_loc) ) );
  if ( c->uniforms == NULL || c->uniforms_loc == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  c->uniforms[0] = (void*)projection;
  c->uniforms_loc[0] = glGetUniformLocation( c->program, "u_projection" ); 

  c->uniforms[1] = (void*)move_camera;
  c->uniforms_loc[1] = glGetUniformLocation( c->program, "u_camera" ); 

  /*
  glBindVertexArray( vao_id );
  glBufferData( GL_ARRAY_BUFFER, vertices_nr4 * sizeof(*coords), indices, GL_STATIC_DRAW );
  //glDisableVertexAttribArray( 1 );
  glBindVertexArray(0);
  */
}

static void setup_solid( void *context ){

  // textures :

  float pixels[] = {
    0.0f, 0.5f, 0.0f,   1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,   0.0f, 0.7f, 0.0f
  };


  glActiveTexture( GL_TEXTURE0 + texture_unit_index );
  glGenTextures( texture_nr, texture_id );
  glBindTexture( GL_TEXTURE_2D, texture_id[0] );
  /*
  glTexImage2D( GL_TEXTURE_2D,
                0,
                GL_LUMINANCE,
                slot->bitmap.width,
                slot->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                slot->bitmap.buffer );  
*/
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels );


  struct rendering_context *c = context;
  const int uniform_nr = 3;
  glClearColor( 0.0, 0.0, 0.0, 0.0 );
  glClearDepth( -1.0 );
  glFrontFace( GL_CCW );
  glDepthFunc( GL_GEQUAL );
  glEnable( GL_DEPTH_TEST );
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_LIGHTING);
  glDisable( GL_CULL_FACE );
  assert( c->program > 0 );
  glUseProgram( c->program );
  c->uniforms = malloc( uniform_nr * sizeof( *(c->uniforms) ) );
  c->uniforms_loc = malloc( uniform_nr * sizeof( *(c->uniforms_loc) ) );
  if ( c->uniforms == NULL || c->uniforms_loc == NULL ){
    perror( "malloc()" );
    exit(EXIT_FAILURE);
  }

  c->uniforms[0] = (void*)projection;
  c->uniforms_loc[0] = glGetUniformLocation( c->program, "u_projection" ); 

  c->uniforms[1] = (void*)move_camera;
  c->uniforms_loc[1] = glGetUniformLocation( c->program, "u_camera" ); 

  c->uniforms[2] = (void*)&texture_unit_index;
  c->uniforms_loc[2] = glGetUniformLocation( c->program, "u_ColorMap" ); 

  GLuint indices[3] = { 0, 1, 2 };

  glBindVertexArray( vao_id );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(*indices), indices, GL_STATIC_DRAW );
  // glDisableVertexAttribArray( 1 );
  glBindVertexArray(0);
}
