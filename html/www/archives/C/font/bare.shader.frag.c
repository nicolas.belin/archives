#version 110

varying vec4 color_out;

void main() {

  gl_FragColor = color_out;

}
