#version 110

uniform sampler2D u_ColorMap;

//varying vec4 color_out;
varying vec2 v_TexCoord;

void main() {

  gl_FragColor = vec4( texture2D( u_ColorMap, v_TexCoord).rgb, 1.0 );

}
