#version 110

// input :
attribute vec3 position;

uniform mat4 u_projection;
uniform mat4 u_camera;

// output :
varying vec4 color_out;

void main() {

  vec4 m = vec4( position, 1.0 );
  gl_Position = u_projection * u_camera * m;

  color_out = vec4 ( 1.0, 1.0, 1.0, 1.0 );
}
