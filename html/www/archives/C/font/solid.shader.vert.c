#version 110

// input :
attribute vec3 a_Position;
//attribute vec2 a_TexCoord;

uniform mat4 u_projection;
uniform mat4 u_camera;

// output :
//varying vec4 color_out;
varying vec2 v_TexCoord;

void main() {

  //  gl_TexCoord[0] = gl_MultiTexCoord0;
  v_TexCoord = a_Position.xy;

  vec4 m = vec4( a_Position, 1.0 );
  gl_Position = u_projection * u_camera * m;

  //  color_out = vec4 ( 1.0, 1.0, 1.0, 1.0 );
}
