// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f -elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...
#include <ctime>

#define screen_w 1024	// screen width (largueur)
#define screen_h 1024	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
const unsigned int max_int = 0x33333332;	// nombre maximum pour eviter les debordements
const unsigned int col = 0x7fffffff >> 11;
const unsigned int f_rate = 40;		// renouvellement de l'affichage/seconde

int E1[screen_h][screen_w];
int E2[screen_h][screen_w];
int palette2[4095];	

void Make_palette(int *palette);		// creation d'une premiere palette de 1786 couleurs
			
void Scale_palette(int *palette,int *palette2);	// Etirement de la palette precedente
						// pour avoir 2048=2^11 couleurs.
void copbuf_C(int E[screen_h][screen_w],Uint32 *bitmap);

void it_C(int E1[screen_h][screen_w],int E2[screen_h][screen_w]);

extern "C" void it_ASM(int E1[screen_h][screen_w],int E2[screen_h][screen_w]);

extern "C" void amort(int E1[screen_h][screen_w],int E2[screen_h][screen_w]);

extern "C" void itam(int E1[screen_h][screen_w],int E2[screen_h][screen_w]);

extern "C" void copbuf_ASM
	(	int a[screen_h][screen_w],	
		Uint32 *c
	);

void source(int E[screen_h][screen_w],unsigned int n);

int abs(int x);

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_HWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	Uint32 *bitmap;
	bitmap = (Uint32*)screen->pixels;	

	Uint32 t0,t1,t2,t3;	// temps
	unsigned int frame=0;	// compte les frames
	
	// couleurs
	int palette[1785];	
	Make_palette(palette);
	Scale_palette(palette,palette2);	
		
	for(int yf=0;yf<screen_h;yf++)
	for(int xf=0;xf<screen_w;xf++)	// etat initial
	{	E1[yf][xf]=0;
		E2[yf][xf]=0;
	}
	
	t0 = SDL_GetTicks();
	t2=SDL_GetTicks();

	srand(unsigned(time(NULL)));

	while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN))
	{	source(E1,frame);
		it_ASM(E1,E2);
		frame++;
		source(E2,frame);
		it_ASM(E2,E1);
		frame++;

//		amort(E1,E2);
		if(frame % 10 == 0) amort(E1,E2);

		if(SDL_GetTicks()-t2 > 1000/f_rate)
		{	copbuf_ASM(E1,bitmap);
			SDL_Flip(screen);
			t2 = SDL_GetTicks();
		}
	}		
	
	t1=SDL_GetTicks();
	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s<<" s : "<<ms<< " ms" << endl;
	cout << "nombre d'iterations par seconde : " << 1000.0*frame/t << endl;
	//cout << "maximum atteint : " << mint << endl;
	//cin.get();
	SDL_Quit();
	return 0;
}


// ****************************** fonctions **********************************

void it_C(int nE1[screen_h][screen_w],int nE2[screen_h][screen_w])
{	for(int y = 1 ; y < screen_h-1 ; y++)
	for(int x = 1 ; x < screen_w-1 ; x++)
	{	int E0 = nE1[y][x];
		int E = nE1[y][x+1];
		E -= E0;
		E += nE1[y][x-1];
		E -= E0;
		E += nE1[y-1][x];
		E -= E0;
		E += nE1[y+1][x];
		E -= E0;
		E /= 2;
		E += E0;
		E -= nE2[y][x];
		E += E0;
		nE2[y][x] = E;


/*		int E = nE1[y][x+1];
		E += nE1[y][x-1];
		E += nE1[y-1][x];
		E += nE1[y+1][x];
		E -= nE1[y][x]*4;
		E /= 2;
		E -= nE2[y][x];
		E += nE1[y][x]*2;
		nE2[y][x] = E;
*/	}
}

void source2(int E[screen_h][screen_w],unsigned int n) // va et vient horizontal
{	const int amp = max_int/10;
	float t=((n%1500)*2*PI)/1500;
	int xp = int(screen_w/2+(screen_w/3)*sin(t));
	int yp = screen_h/2;
	E[yp][xp] += amp;
	E[yp+1][xp] += amp;
	E[yp-1][xp] += amp;
	E[yp][xp+1] += amp;
	E[yp][xp-1] += amp;
	E[yp+1][xp+1] += amp;
	E[yp-1][xp+1] += amp;
	E[yp+1][xp-1] += amp;
	E[yp-1][xp-1] += amp;
}

void source3(int E[screen_h][screen_w],unsigned int n)	// cercles dont le centre se deplace
{	const int r = 100;
	const int w = 10;
	const int amp = max_int/5;
	float t=((n%2000)*2*PI)/2000;
	int xc = int(screen_w/2+(screen_w/2-2*r-4)*sin(t));
	int yc = screen_h/2;
	int xp = int(xc+r*cos(w*t));
	int yp = int(yc+r*sin(w*t));
	E[yp][xp] += amp;
	E[yp+1][xp] += amp;
	E[yp-1][xp] += amp;
	E[yp][xp+1] += amp;
	E[yp][xp-1] += amp;
	E[yp+1][xp+1] += amp;
	E[yp-1][xp+1] += amp;
	E[yp+1][xp-1] += amp;
	E[yp-1][xp-1] += amp;
}

void source1(int E[screen_h][screen_w],unsigned int n)	// barre clignotante
{	int nb = 0;
	if(n <= 60)
	{	nb = n % 60;
		float t = max_int*sin((2*nb*PI)/60);
		for(int x = 3 ; x < screen_w-3 ; x++) E[10][x]=int(t);
	}	
}

void source(int E[screen_h][screen_w],unsigned int n)	// barre clignotante
{	 int amp = /*rand() % */(2*max_int/3);
	int xp = (rand() % (screen_w-10)) + 5;
	int yp = (rand() % (screen_h-10)) + 5;
	E[yp][xp] += amp;
	E[yp+1][xp] += amp;
	E[yp-1][xp] += amp;
	E[yp][xp+1] += amp;
	E[yp][xp-1] += amp;
	E[yp+1][xp+1] += amp;
	E[yp-1][xp+1] += amp;
	E[yp+1][xp-1] += amp;
	E[yp-1][xp-1] += amp;	
}

void copbuf_C(int E[screen_h][screen_w],Uint32 *bitmap)
{	int *hot;
	hot = &E[0][0];
	int temp;
	for(int m = screen_w ; m <= screen_w*(screen_h-2) ; m++)
	{	temp = hot[m]/0x100000;
		temp += 0x7ff;
		bitmap[m] =palette2[temp];
	}
}

void Make_palette(int *palette)
{	int c,i;
	for(i = 0; i<0x100 ; i++) palette[i]=i;
	c=0xff;
	for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
	c=0xffff;
	for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
	c=0xff00;
	for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
	c=0xffff00;
	for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
	c=0xff0000;
	for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
	c=0xff00ff;
	for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2)
{	for(int i = 0 ; i < 0x800 ; i++)
	{	palette2[2047+i]=palette[(0x6f9*i)/0x7ff];
		palette2[i]= ~(palette[(0x6f9*i)/0x7ff]);
	}
}

