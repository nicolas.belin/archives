#=======================================================================
#@V@:Note: File automatically generated by VIDE - 2.00/10Apr03 (g++).
# Generated 12:09:30 PM 21 Feb 2006
# This file regenerated each time you run VIDE, so save under a
#    new name if you hand edit, or it will be overwritten.
#=======================================================================

# Standard defines:
CC  	=	g++
LD  	=	g++
WRES	=	windres
CCOMP	=	gcc
HOMEV	=	$(HOME)/v
VPATH	=	$(HOMEV)/include
oDir	=	.
Bin	=	.
libDirs	=	-L/usr/X11R6/lib

incDirs	=	-I/usr/X11R6/include

LD_FLAGS =	`sdl-config --libs`
LIBS	=	
C_FLAGS	=	`sdl-config --cflags`

SRCS	=\
	g.c

EXOBJS	=\
	$(oDir)/g.o

ALLOBJS	=	$(EXOBJS)
ALLBIN	=	$(Bin)/a.out
ALLTGT	=	$(Bin)/a.out

# User defines:

#@# Targets follow ---------------------------------

all:	$(ALLTGT)

objs:	$(ALLOBJS)

cleanobjs:
	rm -f $(ALLOBJS)

cleanbin:
	rm -f $(ALLBIN)

clean:	cleanobjs cleanbin

cleanall:	cleanobjs cleanbin

#@# User Targets follow ---------------------------------


#@# Dependency rules follow -----------------------------

$(Bin)/a.out: $(EXOBJS)
	$(LD) -o $(Bin)/a.out $(EXOBJS) $(incDirs) $(libDirs) $(LD_FLAGS) $(LIBS)
