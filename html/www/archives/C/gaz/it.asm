section .data

n: dw 0

extern	palette2
screen_w equ 1024	; largueur de l'ecran
screen_h equ 1024	; longueur de l'ecran

section .text 

global it_ASM,copbuf_ASM,dif_ASM,amort,itam
align 16

;****************************************
;************ propagation ***************
;****************************************
it_ASM2:mov esi,[esp+4]		;hot1
	add esi,4
	mov edi,[esp+8]		;hot2
	add edi,4
	mov word [n],screen_h-2
.ord:	add esi,screen_w*4
	add edi,screen_w*4
	mov ecx,(screen_w-2)*4
.absi:	mov eax,[esi+ecx+4]
	mov edx,[esi+ecx]
	add eax,[esi+ecx-4]
	sal edx,2
	add eax,[esi+ecx+screen_w*4]
	add eax,[esi+ecx-screen_w*4]
	sub eax,edx
	sar eax,1
	sub eax,[edi+ecx]
	mov ebx,[esi+ecx]
	lea eax,[eax+ebx*2]
	mov [edi+ecx],eax
	sub ecx,4
	jnz .absi
	dec word [n]
	jnz .ord
	ret

;****************************************
;************ propagation SSE2 **********
;****************************************
it_ASM:	mov esi,[esp+4]		; E1
	mov edi,[esp+8]		; E2
	mov ecx,screen_w*4
.absi:	xor ebx,ebx
.ord:	movdqa xmm0,[esi+ecx]		; xmm0 = E(x,y,t)
	movdqu xmm1,[esi+ecx+4]		; xmm1 = E(x+1,y,t)
	psubd xmm1,xmm0		
	movdqu xmm2,[esi+ecx-4]		; xmm2 = E(x-1,y,t)
	psubd xmm2,xmm0
	paddd xmm1,xmm2
	paddd xmm1,[esi+ecx-screen_w*4]	; xmm1 += E(x,y-1,t)
	psubd xmm1,xmm0
	paddd xmm1,[esi+ecx+screen_w*4]	; xmm1 += E(x,y+1,t)
	psubd xmm1,xmm0
	psrad xmm1,1
	movdqa xmm2,[edi+ecx]		; xmm2 = E(x,y,t-1)
	paddd xmm1,xmm0
	psubd xmm1,xmm2
	paddd xmm1,xmm0
	movdqa [edi+ecx],xmm1		; E(x,y,t+1) = xmm1
	add ecx,16
	add ebx,4
	cmp ebx,screen_w-8
	jnz .ord
	add ecx,32
	cmp ecx,(screen_h-1)*screen_w*4
	jnz .absi
	ret


;*********************************************
;************  amortissement  ****************
;*********************************************
amort:	mov esi,[esp+4]		; source 1
	mov edi,[esp+8]		; source 2
	mov ecx,screen_w*4
.fgh:	movdqa xmm0,[esi+ecx]
	movdqa xmm2,[edi+ecx]
	movdqa xmm1,xmm0
	movdqa xmm3,xmm2
	psrad xmm1,6
	psrad xmm3,6
	psubd xmm0,xmm1
	psubd xmm2,xmm3
	movdqa [esi+ecx],xmm0
	movdqa [edi+ecx],xmm2
	add ecx,16
	cmp ecx,screen_w*(screen_h-1)*4 	
	jne .fgh
	ret

;****************************************
;****** propagation + amortissement *****
;****************************************
itam:	mov esi,[esp+4]		; E1
	mov edi,[esp+8]		; E2
	mov ecx,screen_w*4
.absi:	xor ebx,ebx
.ord:	movdqa xmm0,[esi+ecx]		; xmm0 = E(x,y,t)
	movdqu xmm1,[esi+ecx+4]		; xmm1 = E(x+1,y,t)
	psubd xmm1,xmm0		
	movdqu xmm2,[esi+ecx-4]		; xmm2 = E(x-1,y,t)
	psubd xmm2,xmm0
	paddd xmm1,xmm2
	paddd xmm1,[esi+ecx-screen_w*4]	; xmm1 += E(x,y-1,t)
	psubd xmm1,xmm0
	paddd xmm1,[esi+ecx+screen_w*4]	; xmm1 += E(x,y+1,t)
	psubd xmm1,xmm0
	psrad xmm1,1
	movdqa xmm2,[edi+ecx]		; xmm2 = E(x,y,t-1)
	paddd xmm1,xmm0
	psubd xmm1,xmm2
	paddd xmm1,xmm0
	movdqa xmm3,xmm0		; amortissement xmm0
	psrad xmm3,6
	psubd xmm0,xmm3
	movdqa [esi+ecx],xmm0
	movdqa xmm4,xmm1		; amortissement xmm1
	psrad xmm4,6
	psubd xmm1,xmm4
	movdqa [edi+ecx],xmm1		; E(x,y,t+1) = xmm1
	add ecx,16
	add ebx,4
	cmp ebx,screen_w-8
	jnz .ord
	add ecx,32
	cmp ecx,(screen_h-1)*screen_w*4
	jnz .absi
	ret

;******************************************
;*************** affichage*****************
;******************************************
copbuf_ASM:
	mov esi,[esp+4]
	mov edi,[esp+8]
	mov ecx,4*screen_w
.cop:	mov eax,[esi+ecx]
	sar eax,20
	add eax,0x7ff
	mov edx,[palette2+eax*4]
	add ecx,4
	mov [edi+ecx],edx
	cmp ecx,screen_w*(screen_h-1)*4
	jne .cop
	ret

