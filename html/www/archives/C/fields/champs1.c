//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>

using namespace std;

const float PI = 3.14159265359;

const double xmin = -5;		// fenetre
const double xmax = 5;
const double ymin = -5;
const double ymax = 5;

const int N = 50;			// nombre de points : N*N

int ix = 0;
int iy = 0;

int WindowName;			// numero de la fenetre ouverte par Glut
int win_w = 1024;		// dimensions de la fenetre
int win_h = 1024;

double X(double x , double y);	// composante horizontale du champs
double Y(double x , double y);	// composante verticale du champs
void flot(double x , double y , int s);// trace le flot

void drawArrow( double dx , double dy );
void souris( int etat , int bouton , int xp , int yp );
void myReshape( int w , int h );
void myDisplay();
void myIdle();
void InitGL();

int main( int argc, char *argv[] )
{ 	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutInitWindowSize( win_w , win_h );
	WindowName = glutCreateWindow("Champs vectoriel");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
	glutIdleFunc(myIdle);
	glutMouseFunc(souris);
	InitGL();
	glutMainLoop();
	return 0;
}

void myIdle()
{	if( (ix <= N) && (iy <= N) )
	{	double x = xmin + ix*((xmax-xmin)/N);
		double y = ymin + iy*((ymax-ymin)/N);

		glColor3f( 1.0 , 1.0 , 1.0 );
		//flot(x , y , 1);
		//flot(x , y , -1);


		glPushMatrix();
		glColor3f( 0.7 , 0.0 , 0.0 );
		glTranslated( x , y , 0.0 );
		double dx = X(x,y);
		double dy = Y(x,y);
		if( (abs(dx) < 1000.0) && (abs(dy) < 1000.0) )
			drawArrow( dx/10.0 , dy/10.0 );
		glPopMatrix();

		ix++;
		if(ix > N){ iy++; ix = 0; }
	}
}


double X(double x , double y)	// composante horizontale du champs
{	return y;
}

double Y(double x , double y)	// composante verticale du champs
{	return -0.8*sin(x);
}

void flot(double x , double y , int s)
{		double xp = x;
		double yp = y;
		glBegin(GL_LINE_STRIP);
		glVertex2d(x,y);
		for(int i = 0 ;
			(i<10000) && (x>=xmin) && (x<=xmax)
				&& (y>=ymin) && (y<=ymax) ; i++)
		{	double T = sqrt( X(x,y)*X(x,y) + Y(x,y)*Y(x,y) );
			if(T < 1.0) T = 1.0;
			x += s*0.001*X(x,y)/T;
			y += s*0.001*Y(x,y)/T;
			if( (x-xp)*(x-xp) + (y-yp)*(y-yp) > 0.01*0.01 )
			{	glVertex2d(x,y);
				xp = x; yp = y;
			}
		}
		glVertex2d(x,y);
		glEnd();
		glFinish();
} 

void drawArrow(double dx , double dy )
{	glBegin(GL_LINES);
		glVertex2f( dx , dy );
		glVertex2f( 0.0 , 0.0 );
		glVertex2f( dx , dy );
		glVertex2f( 0.85*dx - 0.05*dy , 0.85*dy + 0.05*dx );
		glVertex2f( dx , dy );
		glVertex2f( 0.85*dx + 0.05*dy , 0.85*dy - 0.05*dx );
	glEnd();
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);	//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
}

void InitGL()
{	glEnable(GL_LINE_SMOOTH);	// antialiasing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glLineWidth(1.0);
	glColor3f(1.0,1.0,1.0);
}

void myReshape( int w , int h )
{   win_w=w;
	win_h=h;
	glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D( xmin , xmax , ymin , ymax );
	ix = 0;
	iy = 0;
}

// gestion de la souris
void souris( int etat , int bouton , int xp , int yp )
{	if( etat == GLUT_DOWN && bouton == GLUT_LEFT_BUTTON )
	{	double x = xmin + xp*(xmax-xmin)/win_w;
		double y = ymin + (win_h-yp)*(ymax-ymin)/win_h;
		glColor3f( 1.0 , 1.0 , 1.0 );
		flot(x , y , 1);
		flot(x , y , -1);
	}
}
