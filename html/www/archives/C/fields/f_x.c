#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

typedef float decimal;		// precision des calculs

const int taille=100;
int matrice[taille];		// version digeree de l'ecriture npi
decimal constantes[taille];	// les constantes dans l'ordre d'arrivee

void prepare_f( string npi, int matrice[], decimal constantes[] );
decimal image_f( decimal x, decimal y , int matrice[], decimal constantes[] );

int main( int argc , char **argv )
{	string npi;
	getline(cin,npi);	// on recupere la ligne
	decimal x=3;
	decimal y=5;
	prepare_f( npi, matrice, constantes );
	cout<<"f("<<x<<","<<y<<")=" << image_f(x,y,matrice,constantes) << endl;
	return 0;
}

void prepare_f( string npi, int matrice[], decimal constantes[] )
{	int inum=0;		// index des constantes
	int imat=0;		// index de la matrice
	char c;
	string op;
	stringstream npis;

	if( npi=="" )
	{	cerr << "Erreur : La fonction n'est pas definie." << endl;
		exit(1);
	}
	npis << npi;		// on en fait un flux de char
	while( npis )
	{	c=npis.get();
		npis.unget();
		if( (c<='9') && (c>='0') )
		{	npis >> constantes[inum++];
			npis.get();
			matrice[imat++]=1;
		}
		else
		{	npis >> op;
			npis.get();
			if ( op=="x" ) matrice[imat++]=2;
			else if ( op=="y" ) matrice[imat++]=3;
			else if( op=="+" ) matrice[imat++]=10;
			else if ( op=="-" ) matrice[imat++]=11;
			else if ( op=="/" ) matrice[imat++]=12;
			else if ( op=="*" ) matrice[imat++]=13;
			else if ( op=="sqrt" ) matrice[imat++]=20;
			else if ( op=="cos" ) matrice[imat++]=21;
			else if ( op=="sin" ) matrice[imat++]=22;
			else if ( op=="tan" ) matrice[imat++]=23;
			else if ( op=="exp" ) matrice[imat++]=24;
			else if ( (op=="log") || (op=="ln") ) matrice[imat++]=25;
			else if ( (op=="^") || (op=="pow") ) matrice[imat++]=26;
			else if ( (op=="abs") || (op=="fabs") ) matrice[imat++]=27;
			else if ( op=="acos" ) matrice[imat++]=28;
			else if ( op=="asin" ) matrice[imat++]=29;
			else if ( op=="atan" ) matrice[imat++]=30;
			else if ( op=="cosh" ) matrice[imat++]=31;
			else if ( op=="sinh" ) matrice[imat++]=32;
			else if ( op=="tanh" ) matrice[imat++]=33;
			else if ( op=="e" ) matrice[imat++]=100;
			else if ( (op=="pi") || (op=="PI") ) matrice[imat++]=101;
			else
			{	cerr << "Operateur '" << op << "' non reconnu." << endl;
				exit(1);
			}
		}
	}
	matrice[imat]=0;
}

decimal image_f( decimal x ,decimal y ,int matrice[] ,decimal constantes[] )
{	decimal pile[taille];
	int ip=-1;	// index dans la pile	
	int inum=0;
	for( int i=0 ; matrice[i]!=0 ; i++ )
	{	switch (matrice[i])
		{	case 1:	pile[++ip]=constantes[inum++];		break;
			case 2:	pile[++ip]=x;						break;
			case 3: pile[++ip]=y;						break;
			case 10:pile[ip-1]+=pile[ip--];				break;
			case 11:pile[ip-1]-=pile[ip--];				break;
			case 12:pile[ip-1]/=pile[ip--];				break;
			case 13:pile[ip-1]*=pile[ip--];				break;
			case 20:pile[ip]=sqrt(pile[ip]);			break;
			case 21:pile[ip]=cos(pile[ip]);				break;
			case 22:pile[ip]=sin(pile[ip]);				break;
			case 23:pile[ip]=tan(pile[ip]);				break;
			case 24:pile[ip]=exp(pile[ip]);				break;
			case 25:pile[ip]=log(pile[ip]);				break;
			case 26:pile[--ip]=pow(pile[ip],pile[ip+1]);break;
			case 27:pile[ip]=fabs(pile[ip]);			break;
			case 28:pile[ip]=acos(pile[ip]);			break;
			case 29:pile[ip]=asin(pile[ip]);			break;
			case 30:pile[ip]=atan(pile[ip]);			break;
			case 31:pile[ip]=cosh(pile[ip]);			break;
			case 32:pile[ip]=sinh(pile[ip]);			break;
			case 33:pile[ip]=tanh(pile[ip]);			break;
			case 100:pile[++ip]=exp(1);					break;
			case 101:pile[++ip]=acos(-1);				break;
		}
	}
	if( ip!=0 ) cerr << "Les comptes ne sont bons !" << endl;
	return pile[0];
}
