//compilation:
//g++ ?.c -o ? -I/usr/include/ -L/usr/lib -lX11 -lglut -lGL -lGLU

#include <iostream>     // entrees/sorties avec la console
#include <cstdlib>
#include <GL/glut.h>
#include <cmath>
#include <string>
#include <sstream>

using namespace std;

const float PI = 3.14159265359;

const double xmin = -2;		// fenetre
const double xmax = 2;
const double ymin = -2;
const double ymax = 2;

const int N = 50;			// nombre de points : N*N

int ix = 0;
int iy = 0;

int WindowName;			// numero de la fenetre ouverte par Glut
int win_w = 1024;		// dimensions de la fenetre
int win_h = 1024;
double pix = ( xmax-xmin ) / win_w; 

double X(double x , double y);	// composante horizontale du champs
double Y(double x , double y);	// composante verticale du champs
void flot(double x , double y , int ud);// calcule le flot

// gestion des fonctions :
const int taille=100;
int matrice_X[taille];
double constantes_X[taille];
int matrice_Y[taille];
double constantes_Y[taille];
void prepare_f( string npi, int matrice[], double constantes[] );
double image_f(double x,double y,int matrice[],double constantes[]);

// opengl et glut :
void drawArrow( double dx , double dy );
void souris( int etat , int bouton , int xp , int yp );
void myReshape( int w , int h );
void myDisplay();
void myIdle();
void InitGL();

int main( int argc, char *argv[] )
{ 	string npi_X,npi_Y;	// recuperation des expressions NPI
	getline(cin,npi_X);	// des fonctions X et Y.
	getline(cin,npi_Y);
	prepare_f( npi_X,matrice_X,constantes_X );
	prepare_f( npi_Y,matrice_Y,constantes_Y );

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutInitWindowSize( win_w , win_h );
	WindowName = glutCreateWindow("Champs vectoriel");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay); 
	glutIdleFunc(myIdle);
	glutMouseFunc(souris);
	InitGL();
	glutMainLoop();
	return 0;
}

void myIdle()
{	if( (ix <= N) && (iy <= N) )
	{	double x = xmin + ix*((xmax-xmin)/N);
		double y = ymin + iy*((ymax-ymin)/N);

		glColor3f( 1.0 , 1.0 , 1.0 );
		//flot(x , y , 1);
		//flot(x , y , -1);


		glPushMatrix();
		glColor3f( 0.7 , 0.0 , 0.0 );
		glTranslated( x , y , 0.0 );
		double dx = X(x,y);
		double dy = Y(x,y);
		double v = sqrt( dx*dx + dy*dy );
		dx /= v;
		dy /= v;
		if( (abs(dx) < 2.0) && (abs(dy) < 2.0) )
			drawArrow( dx/N , dy/N );
		glPopMatrix();

		ix++;
		if(ix > N){ iy++; ix = 0; }
	}
}


double X(double x , double y)	// composante horizontale du champs
{	return image_f(x,y,matrice_X,constantes_X);
}

double Y(double x , double y)	// composante verticale du champs
{	return image_f(x,y,matrice_Y,constantes_Y);
}

void flot(double x , double y , int ud)
{		double xp = x;
		double yp = y;
		double s = 0;
		double dx , dy , T;
		glBegin(GL_LINE_STRIP);
		glVertex2d(x,y);
		for(int i = 0 ;
			(i<10000) && (x>=xmin) && (x<=xmax)
				&& (y>=ymin) && (y<=ymax) ; i++)
		{	dx = X(x,y);
			dy = Y(x,y);
			T = sqrt( dx*dx + dy*dy  );
			s += 0.001*0.001;
			if(T < 0.000000001) break;
			x += ud * 0.001 * dx / T;
			y += ud * 0.001 * dy / T;
			if( (x-xp)*(x-xp) + (y-yp)*(y-yp) - s*s > 4.0*9.0*pix*pix )
			{	glVertex2d(x,y);
				xp = x; yp = y; s = 0;
			}
		}
		glVertex2d(x,y);
		glEnd();
		glFinish();
} 

void drawArrow(double dx , double dy )
{	glBegin(GL_LINES);
		glVertex2f( dx , dy );
		glVertex2f( 0.0 , 0.0 );
		glVertex2f( dx , dy );
		glVertex2f( 0.85*dx - 0.05*dy , 0.85*dy + 0.05*dx );
		glVertex2f( dx , dy );
		glVertex2f( 0.85*dx + 0.05*dy , 0.85*dy - 0.05*dx );
	glEnd();
}

void myDisplay()
{ 	glClear(GL_COLOR_BUFFER_BIT);	//Efface le framebuffer 
	glMatrixMode(GL_MODELVIEW); 	//Choisit la matrice MODELVIEW
	glLoadIdentity(); 				//Réinitialise la matrice
}

void InitGL()
{	glEnable(GL_LINE_SMOOTH);	// antialiasing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glLineWidth(1.0);
	glColor3f(1.0,1.0,1.0);
}

void myReshape( int w , int h )
{   win_w=w;
	win_h=h;
	pix = ( xmax-xmin ) / win_w; 
	glViewport( 0 , 0 , w , h );
 	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
  	gluOrtho2D( xmin , xmax , ymin , ymax );
	ix = 0;
	iy = 0;
}

// gestion de la souris
void souris( int etat , int bouton , int xp , int yp )
{	if( etat == GLUT_DOWN && bouton == GLUT_LEFT_BUTTON )
	{	double x = xmin + xp*(xmax-xmin)/win_w;
		double y = ymin + (win_h-yp)*(ymax-ymin)/win_h;
		glColor3f( 1.0 , 1.0 , 1.0 );
		flot(x , y , 1);
		flot(x , y , -1);
	}
}

void prepare_f( string npi, int matrice[], double constantes[] )
{	int inum=0;		// index des constantes
	int imat=0;		// index de la matrice
	char c;
	string op;
	stringstream npis;

	if( npi=="" )
	{	cerr << "Erreur : La fonction n'est pas definie." << endl;
		exit(1);
	}
	npis << npi;		// on en fait un flux de char
	while( npis )
	{	c=npis.get();
		npis.unget();
		if( (c<='9') && (c>='0') )
		{	npis >> constantes[inum++];
			npis.get();
			matrice[imat++]=1;
		}
		else
		{	npis >> op;
			npis.get();
			if ( op=="x" ) matrice[imat++]=2;
			else if ( op=="y" ) matrice[imat++]=3;
			else if( op=="+" ) matrice[imat++]=10;
			else if ( op=="-" ) matrice[imat++]=11;
			else if ( op=="/" ) matrice[imat++]=12;
			else if ( op=="*" ) matrice[imat++]=13;
			else if ( op=="sqrt" ) matrice[imat++]=20;
			else if ( op=="cos" ) matrice[imat++]=21;
			else if ( op=="sin" ) matrice[imat++]=22;
			else if ( op=="tan" ) matrice[imat++]=23;
			else if ( op=="exp" ) matrice[imat++]=24;
			else if ( (op=="log") || (op=="ln") ) matrice[imat++]=25;
			else if ( (op=="^") || (op=="pow") ) matrice[imat++]=26;
			else if ( (op=="abs") || (op=="fabs") ) matrice[imat++]=27;
			else if ( op=="acos" ) matrice[imat++]=28;
			else if ( op=="asin" ) matrice[imat++]=29;
			else if ( op=="atan" ) matrice[imat++]=30;
			else if ( op=="cosh" ) matrice[imat++]=31;
			else if ( op=="sinh" ) matrice[imat++]=32;
			else if ( op=="tanh" ) matrice[imat++]=33;
			else if ( op=="e" ) matrice[imat++]=100;
			else if ( (op=="pi") || (op=="PI") ) matrice[imat++]=101;
			else
			{	cerr << "Operateur '" << op << "' non reconnu." << endl;
				exit(1);
			}
		}
	}
	matrice[imat]=0;
}

double image_f(double x,double y,int matrice[],double constantes[])
{	double pile[taille];
	int ip=-1;	// index dans la pile	
	int inum=0;
	for( int i=0 ; matrice[i]!=0 ; i++ )
	{	switch (matrice[i])
		{	case 1:	pile[++ip]=constantes[inum++];		break;
			case 2:	pile[++ip]=x;						break;
			case 3: pile[++ip]=y;						break;
			case 10:pile[ip-1]+=pile[ip--];				break;
			case 11:pile[ip-1]-=pile[ip--];				break;
			case 12:pile[ip-1]/=pile[ip--];				break;
			case 13:pile[ip-1]*=pile[ip--];				break;
			case 20:pile[ip]=sqrt(pile[ip]);			break;
			case 21:pile[ip]=cos(pile[ip]);				break;
			case 22:pile[ip]=sin(pile[ip]);				break;
			case 23:pile[ip]=tan(pile[ip]);				break;
			case 24:pile[ip]=exp(pile[ip]);				break;
			case 25:pile[ip]=log(pile[ip]);				break;
			case 26:pile[--ip]=pow(pile[ip],pile[ip+1]);break;
			case 27:pile[ip]=fabs(pile[ip]);			break;
			case 28:pile[ip]=acos(pile[ip]);			break;
			case 29:pile[ip]=asin(pile[ip]);			break;
			case 30:pile[ip]=atan(pile[ip]);			break;
			case 31:pile[ip]=cosh(pile[ip]);			break;
			case 32:pile[ip]=sinh(pile[ip]);			break;
			case 33:pile[ip]=tanh(pile[ip]);			break;
			case 100:pile[++ip]=exp(1);					break;
			case 101:pile[++ip]=acos(-1);				break;
		}
	}
	if( ip!=0 ) cerr << "Les comptes ne sont bons !" << endl;
	return pile[0];
}
