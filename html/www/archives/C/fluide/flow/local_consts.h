// *********** PARAMETRES DE COMPILATION **************
//#define CHOCS
//#define V_FIELD
#define STATS
#define STATS_EXEC
#define STATS_STACKS

// ************ CONSTANTES SYMBOLIQUES ****************
#define N 80000              // nombre de particules
#define INV_M 2              // inverse de la masse d'une particule
#define T_MIN 40000          // force repulsive (statique) entre particules a densit<E9> nulle
#define T_MAX 100000         // force repulsive (statique) entre particules a densit<E9> maximale
#define DT 2                 // largeur et longueur des taches de densit� = 3*2^DT
#define CVMAX_NUM 4          // vmax est �gale � CVMAX_NUM * v9 / CVMAX_DEN
#define CVMAX_DEN 3
#define DENS_MAX (3*(1<<DT)*3*(1<<DT)) // densit� maximale : ( 3*2^DT )^2
#define ENT 50               // coefficient d'entrainement du flux des vitesses 
#define SUBD_EXP 5           // SUBD = 2 ^ SUBD_EXP
#define SUBD (1<<SUBD_EXP)   // nombre de subdivisions d'un pixel
#define W 800	           // largueur (width)
#define H 300                // hauteur (height)
