#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <string.h>     // pour memset
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <inttypes.h>   // def. du type uint64_t
#include "defines.h"
#include "local_consts.h"
#include "main.h"
#include "exec_flow.h"
#include "fluid.h"
#include "video.h"

SDL_TimerID video_timer_id;
Uint32 video_mode;
static Uint32 *video;    // pointeur sur la memoire video
static SDL_Surface *screen;
static int *palette1;    // palette de couleurs
static int *palette2;    // palette circulaire
static int *distrib_vit; // voir makevid7()

#ifdef STATS
static unsigned long frame_nr = 0;
#endif // STATS

static Uint32 display( Uint32, void* );
static void Make_palette();
static void clear_screen();
static void makevid0();
static void makevid1();
static void makevid2();
static void makevid3();
static void makevid4();
static void makevid5();
static void makevid6();
static void makevid7();
static void makevid8();
static void makevid9();
static void MemError();

void init_video(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit); // commande � executer quand le programme se termine
 
 screen = SDL_SetVideoMode(W, H,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( screen == NULL ){
   fprintf(stderr,"Impossible d'obtenir la r�solution souhait�e :%s\n",SDL_GetError());
   exit(1);
 }
 SDL_WM_SetCaption( "fluid", "fluid" );
 video = (Uint32*)screen->pixels;
 
 palette1 = malloc( 0x800*sizeof(*palette1) );
 palette2 = malloc( 0x800*sizeof(*palette2) );
 distrib_vit = malloc( W*sizeof(*distrib_vit) );
 if( !(palette1 && palette2 && distrib_vit) )
   MemError();
 Make_palette();  // fabrication des palettes de couleurs
 video_mode = 0;
}

void start_video(){
  video_timer_id = SDL_AddTimer( 1000/IM_SEC, display, &video_mode );
  if( video_timer_id == NULL ){
   fprintf(stderr, "Impossible d'initialiser un timer: %s\n", SDL_GetError());
   exit(1);
 }
}

void end_video(){
 if( SDL_RemoveTimer( video_timer_id ) == SDL_FALSE ){
   fprintf(stderr, "Impossible d'enlever un timer: %s\n", SDL_GetError());
   exit(1);
 }
}

Uint32 display( Uint32 intervalle, void *vid_modep ){
  const int vid_mode_nr = 9; // nombre de mode d'affichage
  static int mod_2, mod_9;
  int vid_mode = *((int*)vid_modep);

  vid_mode %= vid_mode_nr + 1;
  if ( vid_mode != 2 ) mod_2 = 0;
  if ( vid_mode != 9 ) mod_9 = 0;
  switch( vid_mode ){
  case 0 :
    makevid0();
    break;
  case 1 :
    makevid1();
    break;
  case 2 : 
    if( mod_2 == 0 )
      clear_screen();
    mod_2 = 1;
    makevid2();
    break;
  case 3 :
    makevid3();
    break;
  case 4 :
    makevid4();
    break;
  case 5 :
    makevid5();
    break;
  case 6 :
    makevid6();
    break;
  case 7 :
    makevid7();
    break;
  case 8 :
    makevid8();
    break;
  case 9 :
    if( mod_9 == 0 )
      clear_screen();
    mod_9 = 1;
    makevid9();
    break;
  }
  SDL_Flip(screen);
#ifdef STATS
  frame_nr++;
#endif // STATS  
  return intervalle;
}

static void clear_screen(){
  memset( video, 0, W * H * sizeof(*video) );
}

static void makevid0(){
  clear_screen();
  int i;
  for ( i=0; i<N; i++ )
    video[ LINEAR_POS( part + i ) ] = 0xffffff;
}

static void makevid1(){ // la couleur depend de la direction du vecteur vitesse.
  clear_screen();
  int i,x,y;
  double theta;
  for ( i=0; i<N; i++ ){
    x = part[i].vx;
    y = part[i].vy;
    if( x==0 && y==0 )
      continue; // pas affich�e si immobile
    theta = atan2( x, y ) + M_PI;
    video[ LINEAR_POS( part + i ) ]
      = palette2[ (int)(theta*2047/(2*M_PI)) ];
  }
}

static void makevid2(){ // parcours d'une seule particule : rapide
  video[ LINEAR_POS( part + N/2 ) ] = 0x00ffff;
  video[ LINEAR_POS( part + 2*(N/3) ) ] = 0xff00ff;
  video[ LINEAR_POS( part + N/3 ) ] = 0xffff00;
}

static void makevid3(){ // couleurs fonctions de la vitesse
  clear_screen();
  unsigned int i,ci;
  int vxt,vyt,vmaxl = vmax;
  for ( i=0; i<N; i++ ){
    vxt = part[i].vx;
    vyt = part[i].vy;
    ci = sqrt( SQ(vxt) + SQ(vyt) );
    ci = ( ci * 0xff ) / MAX( ci, vmaxl );
    ci *= 0x10101;
    video[ LINEAR_POS( part + i ) ] = ci;
  }
}

static void makevid4(){ // densit�
  int c, x, y, l = 0;
  for ( y=0; y < H>>DT; y++ ){
    for ( x=0; x < W>>DT; x++ ){
      c = palette1[ ( MIN( densite[ x + y * (W>>DT) ], DENS_MAX) * 2047 ) / DENS_MAX ];
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;
    }
    l += 3*W;
  }
}

static void makevid5(){ // champs des vitesses (intensit�s)
#ifdef V_FIELD
  int vx, vy, d, x, y, s, s4, l = 0;
  uint c;
  for ( y=0; y < H>>DT; y++ ){
    for ( x=0; x < W>>DT; x++ ){
      s = x + y * (W>>DT);
      if ( d = densite[s] ){
        vx = v_field[s].x / d;
        vy = v_field[s].y / d;
        c = SQ(vx) + SQ(vy); 
        c = ( 2047 * MIN(c,vmax) ) / vmax;
        if ( c > 2047 ){
          printf("\n vx : %i\n", vx);
          printf(" vy : %i\n", vy);
          exit(1);
          c = 0;
        }
        if(c<0){
          printf("\n vx : %i\n", vx);
          printf(" vy : %i\n", vy);
          exit(1);
          c = 2047;
        }
        c = palette1[c];
      }
      else
        c = 0x000000;
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;;
    }
    l += 3*W;
  }
#endif // V_FIELD
}

static void makevid6(){ // lignes de s�paration des threads
  int t, i, x, k;

  clear_screen();
  for ( t=0; t<THREAD_NR; t++ )
    for ( i=k_sep[t]; i<k_sep[t+1]; i++ )
      video[ LINEAR_POS( part + i ) ] = palette2[ ( t * 2047 ) / THREAD_NR ];
}


static void makevid7(){ // distribution des vitesses
  int i, av, y, ymax, maxo = 0;

  for ( i=0; i<W; i++ )
    distrib_vit[i] = 0;
  for ( i=0; i<N; i++){
    av = sqrt( SQ(part[i].vx) + SQ(part[i].vy) );
    av = ((W-1)*MIN(vmax,av)) / vmax;
    av = ++distrib_vit[av];
    if( av > maxo )
      maxo = av;
  }
  for ( i=0; i<W; i++ ){
    ymax = (distrib_vit[i]*H) / maxo;
    for ( y=H-1; y>H-1-ymax ; y-- )
      video[ i + y*W ] = 0xffffff;
    for ( y=H-1-ymax; y>=0; y--)
      video[ i + y*W ] = 0;
  }
  if ( 1000 <= vmax){
    i = ((W-1)*1000) / vmax;
    for ( y=0; y<H; y++ )
      video[ y*W + i ] = 0xff0000;
  }
}

static void makevid8(){
  int k, l, y, ci;

  clear_screen();
  for ( k=0; k<N; k++ ){
    y = part[k].y>>SUBD_EXP;
    ci = (k * H) / N;
    ci = ABS( ci - y ) + 1;
    ci = (5 * 0xff) / ci;
    ci = MIN( ci, 0xff );
    ci *= 0x10101;
    video[ LINEAR_POS( part + k ) ] = ci;
  }
}

static void makevid9(){
  static int n = 0;
  int i;

  for ( i=0; i<H; i++ )
    video[ n + i*W ] = 0;
  for ( i=0; i<THREAD_NR; i++ ){
    video[ n + W * ( (H*k_sep[i]) / N ) ] = 0xffffff;
  }
  n = (++n) % W;
}

static void MemError(){
  fprintf(stderr,"Probleme de m�moire !\n");
  exit(1);
}

static void Make_palette(){
  int *palette, i, c = 0x0;

  if( ( palette = malloc( 0x6fa*sizeof(int))) == NULL )
    MemError();
  for ( i=0; i<0x100 ; i++ )
    palette[i]=i;
  c=0xff;
  for ( i=0x100 ; i<0x1ff ; i++ ){
    c += 0x100;
    palette[i] = c;
  }
  c = 0xffff;
  for ( i=0x1ff ; i<0x2fe ; i++ ){
    c -= 0x1;
    palette[i] = c;
  }
  c = 0xff00;
  for ( i=0x2fe ; i<0x3fd ; i++ ){
    c += 0x10000;
    palette[i] = c;
  }
  c = 0xffff00;
  for ( i=0x3fd ; i<0x4fc ; i++ ){
    c -= 0x100;
    palette[i] = c;
  }
  c = 0xff0000;
  for ( i=0x4fc ; i<0x5fb ; i++ ){
    c += 0x1;
    palette[i] = c;
  }
  c = 0xff00ff;
  for ( i=0x5fb ; i<0x6fa ; i++ ){
    c += 0x100;
    palette[i] = c;
  }
  c=0xffffff;
  for ( i=0; i<0x800; i++ )
    palette1[i] = palette[(0x6f9*i)/0x7ff];

  // palette circulaire :
  c = 0xffffff;
  for(i = 0; i<0x100 ; i++){
    palette[i]=c;
    c -= 0x10100;
  }
  for(i = 0 ; i < 0x800 ; i++)
    palette2[i] = palette[(0x6f9*i)/0x7ff];
    
  free(palette);
}

#ifdef STATS
void print_stats_video( Uint32 t_ms ){
  float t_s = t_ms / 1000.0;
  printf("\nvideo.c =========================\n");
  printf("\nNombre d'images affich�es : %u\n", frame_nr);
  printf("Images par secondes : %f\n", frame_nr/t_s );
}
#endif // STATS
