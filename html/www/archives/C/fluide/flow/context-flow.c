#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "defines.h"
#include "local_consts.h"
#include "fluid.h"
#include "context.h"

#define PI 3.14159265359

#ifdef STATS
static unsigned int trans_part_nr = 0;
static unsigned int collision_nr = 0;
#endif // STATS

void init_particules(){ // initialisation des positions des positions
  int i,j,x,y,l;            // et vitesses initiales des particules

  // bordures de l'�cran
  #define LB 4 // largeur de la bordure de l'�cran
  for( j=0; j<LB; j++ )
    for( i=0; i<W; i++ ){ //barriere horizontale 
      map[ i + j*W ] = FRAME_H;
      map[ W*(H-j-1) + i ] = FRAME_H;
    }
  
  for( j=0; j<LB; j++ )
    for( i=0; i<H; i++ ){ // barriere verticale
      map[ W*i + j ] = TRANSLATE_PART;
      map[ W*(i+1) - j - 1  ] = FRAME_V;
    }

  
  const int xcd = 3*W/4*SUBD + SUBD/2; // abscisse du centre du demi-disque
  const int ycd = H/3*SUBD + SUBD/2; // ordonnee du centre du demi-disque
  const int rc = H*SUBD/10; // rayon du demi-disque 
  int xp,yp;
  for( x=0; x<W; x++)  // demi-disque
    for( y=0; y<H; y++){
      xp = x*SUBD + SUBD/2;
      yp = y*SUBD + SUBD/2;
      if( ( xp >= xcd) && ( SQ(xp-xcd) + SQ(yp-ycd) <= SQ(rc) ) )
        map[ x + y * W ] = FORBIDDEN;
    }
  

  srand( time(NULL));
  for(i=0; i<N; i++){ // positions initiales des particules
    do{
      x = ( rand() % W ) * SUBD + SUBD/2; // positions al�atoires
      y = ( rand() % H ) * SUBD + SUBD/2;
      l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
    }
    while( map[ l ] != LAST_PART );
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
    part[i].next = LAST_PART;
    map[l] = i;
  }

}

void champs_force( int x, int y, int *fx, int *fy){
  //if( x >= 7*W*SUBD/8)
  *fx -= 4000;
  /*#define L_CHAMPS 200 // largueur du champs
#define H_CHAMPS 100 // hauteur du champs
#define I_CHAMPS -15000 // intensit� du champs
  if( ( x >= (W/5)*SUBD ) && ( x <= (W/5+L_CHAMPS)*SUBD ) && (y >= H/4*SUBD) && (y <= (3*H/4+H_CHAMPS)*SUBD ) )
    *fy += I_CHAMPS;
    */
}

void translation( struct particule *pk ){ // ventilateur
  pk->y = ( pk->y * 29 + 101 ) % ( (H-2*LB) * SUBD ) + LB*SUBD; // hashing
  pk->x += (W-2*LB-1) << SUBD_EXP;
  pk->vy = 0;
  pk->vx = -2000;
#ifdef STATS
  trans_part_nr++;
#endif // STATS
 }

void wall1(struct particule *pk, struct particule *pki){
  int l = ( (pk->x - pki->x) >> SUBD_EXP ) + ( (pk->y - pki->y) >> SUBD_EXP ) * W;
  l = ABS( l );
  if( l == 1 )
    pk->vx = -pk->vx;
  else if( l == W )
    pk->vy = -pk->vy;
  else{
    pk->vx = -pk->vx;
    pk->vy = -pk->vy;
  }
  pk->x = pki->x;
  pk->y = pki->y;
#ifdef STATS
  collision_nr++;
#endif // STATS
}

#ifdef STATS

void print_stats_context(){
  /*
  printf("\nSTATISTIQUES context-flow.c ================\n\n");
  printf("Nombre moyen de particules translat�es par it�ration : %f (%f \%)\n",
         (float)trans_part_nr/iterations, (float)trans_part_nr/iterations/N*100.0 );
  printf("Nombre moyen de collision par it�ration : %f (%f \%)\n",
         (float)collision_nr/iterations, (float)collision_nr/iterations/N*100.0 );
  */
}
#endif // STATS
