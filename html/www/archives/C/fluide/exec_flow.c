#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
//#define NDEBUG  // pas de controle avec assert() si defini
#include <assert.h>
#include <SDL/SDL.h>
#include "local_consts.h"
#include "main.h"
#include "defines.h"
#include "exec_flow.h"
#include "fluid.h"
#include "context.h"

SDL_Thread *thread_struct_ptr[THREAD_NR];
int vmax;                      // vitesse maximum (pour la norme max)
uint k_sep[THREAD_NR+1];
uint64_t total_count = 0;      // nombre total de particules trait�es
static uint thread_id[THREAD_NR];
static uint v9_thread[THREAD_NR]; // le 97e centile des vitesses
static const uint alea_nr = 10*N;
static uint *alea;             // liste de nombres aleatoires
static const int prime_nr = 100;// nombre de nombres premier dans prime[]
static uint *prime;            // table de nombres premiers plus grands que sqrt(N)
static volatile int sync_sem = THREAD_NR;
static volatile int wait_int[THREAD_NR];

#ifdef STATS
static uint vmaxmax = 0;   // le max des vmax
static uint64_t count_per_thread[THREAD_NR];
#endif

/*
static void check_malloc(void *malloc_p){
  if( malloc_p == NULL ){
    fprintf(stderr, "\nProbl�me de m�moire !\n");
    exit(1);
  }
}
*/

void init_exec_flow(){
  int i,j;
  for( i=0; i<THREAD_NR; i++ )
    thread_id[i] = i;

  // table de nombres al�atoires : alea
  alea = malloc( alea_nr * sizeof(alea[0]) );
  check_malloc(alea);
  srand( time(NULL));               // liste de nombres al�atoires
  for(i=0; i<alea_nr; i++)
    alea[i] = rand();

  // table de nombres premiers plus grand que sqrt(N)
  prime = malloc( prime_nr * sizeof(prime[0]) );
  check_malloc(prime);
  uint p = sqrt(N) + 1;
  int v = 1;
  for ( i=0; i<prime_nr; i++ ){
    do{
      v = 1, p++;
      for ( j=2; j<=sqrt(p); j++ )
        v *= ( j*(p/j) != p );
    }
    while ( v == 0 );
    prime[i] = p;
  }

  for( i=0; i<THREAD_NR; i++ )
    k_sep[i] = ( i * N ) / THREAD_NR;
  k_sep[THREAD_NR] = N;

  for ( i=0; i<THREAD_NR; i++ )
    v9_thread[i] = V9_INIT;

  vmax = ( ( CVMAX_NUM * V9_INIT ) / CVMAX_DEN ) >> 16;
  if( vmax > VMAX_MAX )
    vmax = VMAX_MAX;
  if( vmax < VMAX_MIN )
    vmax = VMAX_MIN;

  for(i=0; i<W*H; i++)
    map[i] = UNSET_LOCK_BIT( map[i] ); // aucun LOCK
}

void start_fluid(){ // mise en place des threads g�rant les mouvements des particules
  int i;
  for( i=0; i<THREAD_NR; i++ ){
    thread_struct_ptr[i] = SDL_CreateThread( move_fluid, thread_id + i );
    if( thread_struct_ptr[i] == NULL ){
      fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
      exit(1);
    }
  }
}

void end_fluid(){ // On attend que toutes les threads soient �teintes
  int i;
  for( i=0; i<THREAD_NR; i++ )
    SDL_WaitThread( thread_struct_ptr[i], NULL );
}

static inline void update_vmax(){
  uint64_t v9_mean = 0;
  int j;
  for( j=0; j<THREAD_NR; j++ )
    v9_mean += (uint64_t)(v9_thread[j])
      * ( k_sep[j+1] - k_sep[j] );
  v9_mean /= N;
  uint vmax_temp = ( (CVMAX_NUM*v9_mean)/CVMAX_DEN ) >> 16;
  vmax_temp = MAX( vmax_temp, VMAX_MIN );
  vmax_temp = MIN( vmax_temp, VMAX_MAX );
  vmax = vmax_temp;
#ifdef STATS
  vmaxmax = MAX( vmax, vmaxmax );
#endif // STATS
}

/* 
 * Cette fonction (re-entrante) est lanc�e
 * dans chaque thread
 */
int move_fluid( void *thread_ptr ){ 
  uint64_t count = 0;
  uint k, l, li, thread = *(int*)thread_ptr;    // numero de la thread actuelle
  uint v9 = V9_INIT;
  uint seed = rand();
  uint it_count = 0;
  uint k_beg = k_sep[thread];
  uint part_nr = k_sep[thread+1] - k_sep[thread];
  uint n =  part_nr;
  uint ip = rand_r(&seed);
  ulong p = prime[ rand_r(&seed) % prime_nr ];
  ulong q = rand_r(&seed) % part_nr;
  ulong wait_count = 0;
  wait_int[thread] = 0;

  ulong wait_count_mean = 0;

  struct particule pk;
  struct particule pki; // sauvegarde de l'�tat initial
  while( loop == 1 ){
    if( count % (N/THREAD_NR) == 0 ){ 
      v9_thread[thread] = v9;
      if ( thread == 0 ){
        update_vmax();
      }
    }

    k = k_beg + ( p*n + q ) % part_nr; // n -> ( p*n + q ) % part_nr est bijective 

    pki = part[k];

    li = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
    if ( GET_LOCK( map + li ) )         // lock map[li]
      continue;                         // rare
    int mapli = RMV_LOCK_BIT( map[li] );
    int mapl = mapli;
    pki = part[k];

    if ( li != ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W ){
      mapli = UNSET_LOCK_BIT(mapli);    // mapli peut �tre n�gative...
      map[li] = mapli;                  // unlock map[li]
      continue;
    }

    pk = dM( pki );                     // nouvelle position

    l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
    if ( l != li ){ // la particule change de pixel, et donc de p�le
      // on lock la nouvelle p�le
      if ( GET_LOCK( map + l ) ){  // lock map[l]
        mapli = UNSET_LOCK_BIT(mapli);
        map[li] = mapli;               // unlock map[li]
        continue;                      // rare
      }
      
      // p�le que l'on quitte :
      int j = mapli;
      if( j == k )       // si part[k] est t�te de liste
        mapli = pki.next;
      else {             
        int prevp;
        do               // on descend la p�le
          prevp = j;
        while ( (j = part[prevp].next) != k );
        part[prevp].next = pki.next;
      }

      //p�le o� l'on arrive
      pk.next = RMV_LOCK_BIT( map[l] ); 
      mapl = k;

      // interversion de 2 particules
      int nextp = RMV_LOCK_BIT(map[l]);
      int qt, q = 0;
      int nextp_min;
      while ( nextp != LAST_PART ){
        if ( (qt = ( nextp - k ) * ( part[nextp].vy - pk.vy )) < q ){
          nextp_min = nextp;
          q = qt;
        }
        nextp = part[nextp].next;
      }
      if ( q < 0 ){ // on intervertit part[k] et part[nextp_min]
        part[k] = part[nextp_min];
        part[k].next = pk.next;
        pk.next = part[nextp_min].next;
        k = nextp_min;
      }
    }

    part[k] = pk;                    // sauvegarde finale
    count++;

    mapli = UNSET_LOCK_BIT(mapli);       // mapli peut �tre n�gative
    map[li] = mapli;                // unlock map[li]
    if( l != li ){
      mapl = UNSET_LOCK_BIT(mapl);
      map[l] = mapl;                // unlock map[l]
    }

    const int step = 10;
    if ( count % step == 0 ){
      // op�rations qu'il est inutile de r�p�ter pour chaque particules

      // la suite des v9 converge vers le 97e centile des vitesses
      if( (ABS(pk.vx)<<16) >= v9 || (ABS(pk.vy)<<16) >= v9 )
        v9 += 31*(1<<11);
      else
        v9 -= 1*(1<<11);
    }

    if ( n-- == 0 ){ // toute les part. de cette thread ont �t� d�plac�
      it_count++;
      ip += thread + 1;
      uint a = alea[ ip % alea_nr ];
      p = prime[ a % prime_nr ];

            
      // synchronisation des threads
      int i, sem_val = DEC_SEM( &sync_sem );
      //thread_order[thread] = sem_val;
      wait_count = 0;
      //printf( " thread %i  semval %i sync_sem %i wait_count %u  wait_int %i\n",
      //        thread, sem_val, sync_sem, wait_count, wait_int[thread] );

                  
      if ( sem_val > 1 ){ // les premieres threads arriv�es
        do
          if ( wait_count < ULONG_MAX )
            wait_count++;
        while ( wait_int[thread] == 0 && loop == 1 );
        wait_count_mean += wait_count;
      }
      else{               // et la derni�re
        // modification des bornes des threads :
        int temp_sep;
        i = thread;
        if ( i > 0 ){
          temp_sep = k_sep[i] + 10;
          if ( temp_sep > k_sep[i-1] + 1000 
               && temp_sep < k_sep[i+1] - 1000 )
            k_sep[i] = temp_sep;
        }
        if ( i < THREAD_NR - 1 ){
          temp_sep = k_sep[i+1] - 10;
          if ( temp_sep > k_sep[i] + 1000 
               && temp_sep < k_sep[i+2] - 1000 )
            k_sep[i+1] = temp_sep;
        }

        sync_sem = THREAD_NR;
        for ( i=0; i<THREAD_NR; i++ )
          if ( i != thread )
            wait_int[i] = 1;
      }
      assert( sem_val == 1 || wait_count > 0 );
      wait_int[thread] = 0;

      k_beg = k_sep[thread];
      part_nr = k_sep[thread+1] - k_beg;
      q = a % part_nr;
      n = part_nr;
    }
  }
  // avant de finir cette thread :
  ATOMIC_FETCH_AND_ADD( &total_count, count );
#ifdef STATS
  count_per_thread[thread] = count;
#endif // STATS
  printf("wait_count_mean: %f  count : %u  itcount: %u  wait_count: %u\n", (double)wait_count_mean/it_count, count, it_count, wait_count );
  return 0;
}

void print_stats_min( int tf ){ // donn�es statistiques minimales
  int i;
  printf( "Nombre de particules trait�es : %"PRIu64"\n", total_count );
  printf( "Dur�e r�elle : %i ms\n", tf );
  printf( "Particules par ms : %f\n", (double)total_count / tf );
}

#ifdef STATS
void print_stats_exec( int tf ){
  int ms=tf%1000;
  int h=tf/(60*60*1000);
  int mn=(tf/(60*1000))%60;
  int s=(tf/1000)%60;
  int i;
  printf("\nexec_flow.c ======================\n");
  printf("\nStatistiques : TEMPS\n");
  printf("Nombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", (float)N/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf( "Nombre de particules trait�es : %"PRIu64"\n", total_count );
  printf("Dur�e r�elle : %i h %i mn %i s %i ms\n",h,mn,s,ms);
  printf("Particules par ms : %f\n", (double)total_count / tf );
  printf("Maximum des vmax : %i\n", vmaxmax);
  printf("Etat final :\n");
  uint dens_max = 0;
  uint dens_count = 0;
  for(i=0; i<(W>>DT)*(H>>DT); i++){
    dens_max = MAX( dens_max, densite[i] );
    dens_count += densite[i];
  }
  printf("  Densit� max : %i\n", dens_max );
  if ( dens_count != 9*N )
    printf( "\nDENSITE INCONSISTANTE : total = %u  9*N = %u\n\n", dens_count, 9*N );
  int vxmean = 0;
  int vymean = 0;
  for( i=0; i<N; i++ ) vxmean += ABS( part[i].vx );
  for( i=0; i<N; i++ ) vymean += ABS( part[i].vy );
  printf("  Vitesse x moyenne : %i\n", vxmean/N);
  printf("  Vitesse y moyenne : %i\n", vymean/N);
  printf("  vmax = %i\n", vmax );
  uint64_t v9_mean = 0;
  for( i=0; i<THREAD_NR; i++ )
    v9_mean += (uint64_t)v9_thread[i] * ( k_sep[i+1] - k_sep[i] );
  v9_mean /= N;
  printf("  v9 moyen : %f\n", (double)v9_mean / (1<<16) );
  uint count = 0;
  for( i=0; i<N; i++ )
    if( (MAX(ABS( part[i].vx ), ABS( part[i].vy ))<<16) <= v9_mean )
      count++;
  printf("  Proportion de particules de vitesse < v9 moyen : %f \%\n", (double)count / N * 100.0 );

  #ifdef STATS_EXEC
  printf("\nStatistiques : EXECUTION FLOW\n");
  printf("Nombre de threads : %i\n", THREAD_NR );
  printf("Nombre de particules trait�es par thread :\n");
  for( i=0; i<THREAD_NR; i++ ){
    printf( "* Thread no. %d : %"PRIu64"\n", i, count_per_thread[i] );
  }
  printf("Etat final :\n");
  for ( i=0; i<THREAD_NR; i++ ){
    printf( "  * Thread %i :  k_sep: %i   #part. : %i  v97 = %f\n", i,
            k_sep[i],
            k_sep[i+1] - k_sep[i],
            (double)v9_thread[i] / (1<<16) );
  }
#endif // STATS_EXEC

#ifdef STATS_STACKS
  uint l, p, k = 1;
  uint max_stack = 0;
  int  nextp;
  uint greater_s = 0;
  uint wrong_stack_nr = 0;
  uint loop_nr = 0;
  uint s_nr = 0;
  for ( i=0; i<W*H; i++ ){
    if ( ( nextp = RMV_LOCK_BIT(map[i]) ) >= 0 ){
      s_nr++;
      k = 1;
      l = (part[nextp].y>>SUBD_EXP)*W + (part[nextp].x>>SUBD_EXP);
      while( part[nextp].next != LAST_PART ){
        if( k == 1 )
          greater_s++;
        nextp = part[nextp].next;
        k++;
        if( k > 150 ){
          loop_nr++;
          printf( "\n x : %i        y: %i \n", part[nextp].y>>SUBD_EXP, part[nextp].x>>SUBD_EXP );
          break;
        }
        if ( (part[nextp].y>>SUBD_EXP)*W + (part[nextp].x>>SUBD_EXP) != l ){
          wrong_stack_nr++;
          break;
        }
      }
    }
    max_stack = MAX( max_stack, k );
  }
  printf( "\nStatistiques : EMPILEMENTS\n" );
  printf( "Etat final :\n" );
  printf( "  Emp�lement maximum : %u\n", max_stack );
  printf( "  stacks > 1 : %i\n", greater_s );
  printf( "  Proportion de p�les > 1 : %f \%\n", 100.0 * (double)greater_s / s_nr );
  if( wrong_stack_nr > 0 || loop_nr > 0 ){
    printf( "\n  WRONG STACKS NR : %i !!\n", wrong_stack_nr );
    printf( "  LOOP NR : %i !!\n\n", loop_nr );
  }
#endif // STATS_STACKS
}
#endif // STATS
