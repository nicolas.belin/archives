#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "fluide.h"
#include "fluide-local.h" // param�tres de compilation locaux
#include "video.h"

#define PI 3.14159265359

#define IM_SEC 40       // nombre max d'images par seconde 

#define max(A,B) ((A) > (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))
#define sq(A)  ((A)*(A))

// ********* main() ******************************
int main(){
  init_video();
  unsigned int video_mode = 0;
  init_fluide(); // initialisations diverses; utilise la fonction init_particules
#ifdef STATS
  Uint32 t0,t1,t3;        // temps
  t0 = SDL_GetTicks();
#endif
 SDL_Event event;
 Uint32 t2 = SDL_GetTicks();
 int loop = 1;
 //********** boucle principale
 while( loop /* && (SDL_GetTicks()-t0<60000)*/ ){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE) loop = 0;
       if(event.key.keysym.sym==SDLK_f) force_on = 1 - force_on; 
       if(event.key.keysym.sym==SDLK_SPACE){
         video_mode++;
       }
       break;
     }
   }
   if( SDL_GetTicks()-t2 > 1000/IM_SEC ){
     display( video_mode );
     t2 = SDL_GetTicks();
     }
   move();
 }
 print_stats( SDL_GetTicks() - t0 );
 return 0;
}

// ****************************** fonctions **********************************

void init_particules(){ // initialisation des positions des positions
  int i,j,x,y,l;            // et vitesses initiales des particules

  // bordures de l'�cran
  #define LB 4 // largeur de la bordure de l'�cran
  for( j=0; j<LB; j++ )
    for( i=0; i<W; i++ ){ //barriere horizontale 
      map[ i + j*W ] = -2;
      map[ W*(H-j-1) + i ] =-2;
    }
  
  for( j=0; j<LB; j++ )
    for( i=0; i<H; i++ ){ // barriere verticale
      map[ W*i + j ] = -5;
      map[ W*(i+1) - j - 1  ] = -3;
    }

/*
  const int xcd = 3*W/4*SUBD + SUBD/2; // abscisse du centre du demi-disque
  const int ycd = H/3*SUBD + SUBD/2; // ordonnee du centre du demi-disque
  const int rc = H*SUBD/10; // rayon du demi-disque 
  int xp,yp;
  for( x=0; x<W; x++)  // demi-disque
    for( y=0; y<H; y++){
      xp = x*SUBD + SUBD/2;
      yp = y*SUBD + SUBD/2;
      if( ( xp >= xcd) && ( sq(xp-xcd) + sq(yp-ycd) <= sq(rc) ) )
        map[ x + y * W ] = -6;
    }
*/

  srand( time(NULL));
  for(i=0; i<N; i++){ // positions initiales des particules
    do{
      x = ( rand() % W ) * SUBD + SUBD/2; // positions al�atoires
      y = ( rand() % H ) * SUBD + SUBD/2;
      l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
    }
    while( map[ l ] != -1 );
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
  }

}

void champs_force( int x, int y, int *fx, int *fy){
  //if( x >= 7*W*SUBD/8)
  *fx -= 4000;
  /*#define L_CHAMPS 200 // largueur du champs
#define H_CHAMPS 100 // hauteur du champs
#define I_CHAMPS -15000 // intensit� du champs
  if( ( x >= (W/5)*SUBD ) && ( x <= (W/5+L_CHAMPS)*SUBD ) && (y >= H/4*SUBD) && (y <= (3*H/4+H_CHAMPS)*SUBD ) )
    *fy += I_CHAMPS;
    */
}

void mur5(struct particule *pk, int k){ // ventilateur
  int y = ( (*pk).y * 29 + 101 ) % ( (H-2*LB) * SUBD ) + LB*SUBD; // repartition homogene
  int l = ( (*pk).x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
  int lp = l + W - 2*LB - 1;
  if( map[ lp ] == -1 ){
    (*pk).x += (W-2*LB-1) << SUBD_EXP;
    (*pk).y = y;
    (*pk).vy = 0;
    //    (*pk).vx = 1000;
    map[ lp ] = k;
    map[ ( part[k].x>>SUBD_EXP ) + ( part[k].y>>SUBD_EXP ) * W ] = -1;
  }
  else{
    (*pk).x = part[k].x;
    (*pk).y = part[k].y;
  }
}

void mur6(struct particule *pk, int k){
  int x = part[k].x;
  int y = part[k].y;
  int l = ( ((*pk).x - x) >> SUBD_EXP ) + ( ((*pk).y-y) >> SUBD_EXP ) * W;
  l = abs( l );
  if( l == 1 )
    (*pk).vx = -(*pk).vx;
  else if( l == W )
    (*pk).vy = -(*pk).vy;
  else{
    (*pk).vx = -(*pk).vx;
    (*pk).vy = -(*pk).vy;
  }
  (*pk).x = x;
  (*pk).y = y;
}
