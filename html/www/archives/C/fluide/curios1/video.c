#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include "fluide-local.h"
#include "fluide.h"

#define PI 3.14159265359

#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) < (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))
#define sq(A) ((A)*(A))

static Uint32 *video;          // pointeur sur la memoire video
static SDL_Surface *screen;

static int *palette1; // palette de couleurs
static int *palette2; // palette circulaire

static int *distrib_vit; // voir makevid7()

static void Make_palette();

static void clear_screen();
static void makevid0();
static void makevid1();
static void makevid2();
static void makevid3();
static void makevid4();
static void makevid5();
static void makevid6();
static void makevid7();
static void makevid8();
static void MemError();

void init_video(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit); // commande � executer quand le programme se termine
 
 screen = SDL_SetVideoMode(W, H,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( !screen ){
   printf("Impossible d'obtenir la r�solution souhait�e :%s\n",SDL_GetError());
   exit(1);
 }
 video = (Uint32*)screen->pixels;
 
 palette1 = malloc( 0x800*sizeof(int) );
 palette2 = malloc( 0x800*sizeof(int) );
 distrib_vit = malloc( W*sizeof(int) );
 if( !(palette1 && palette2 && distrib_vit) ) MemError();

 Make_palette();  // fabrication des palettes de couleurs
}

void display( unsigned int vid_mode ){
#define N_VID_MODE 8 // nombre de mode d'affichage
  vid_mode %= N_VID_MODE + 1;
  static int mod_2,mod_8;
  if ( vid_mode != 2 ) mod_2 = 0;
  if ( vid_mode != 8 ) mod_8 = 0;
  switch( vid_mode ){
  case 0 :
    makevid0();
    break;
  case 1 :
    makevid1();
    break;
  case 2 : 
    if( mod_2 == 0 )
      clear_screen();
    mod_2 = 1;
    makevid2();
    break;
  case 3 :
    makevid3();
    break;
  case 4 :
    makevid4();
    break;
  case 5 :
    makevid5();
    break;
  case 6 :
    makevid6();
    break;
  case 7 :
    makevid7();
    break;
  case 8 :
    if( mod_8 == 0 )
      clear_screen();
    mod_8 = 1;
    makevid8();
    break;
  }
  SDL_Flip(screen);
}

static void clear_screen(){
  int i;
  for(i=0; i<W*H; i++)
    video[i]=0;
}

static void makevid0(){
  clear_screen();
  int i;
  for(i=0; i<N; i++)
    video[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = 0xffffff;
}

static void makevid1(){ // la couleur depend de la direction du vecteur vitesse.
  clear_screen();
  int i,x,y;
  double theta;
  for(i=0; i<N; i++){
    x = part[i].vx;
    y = part[i].vy;
    if( (x==0) && (y==0) ) continue; // pas affich�e si immobile
    theta = atan2( x, y ) + PI;
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ]
      = palette2[ (int)(theta*2047/(2*PI)) ];
  }
}

static void makevid2(){ // parcours d'une seule particule : rapide
  video[ part[N/2].x/SUBD + (part[N/2].y/SUBD) * W ] = 0x00ffff;
  video[ part[2*(N/3)].x/SUBD + (part[2*(N/3)].y/SUBD) * W ] = 0xff00ff;
  video[ part[N/3].x/SUBD + (part[N/3].y/SUBD) * W ] = 0xffff00;
}

static void makevid3(){ // couleurs fonctions de la vitesse
  clear_screen();
  unsigned int i,ci;
  int vxt,vyt,vmaxl = vmax;
  for(i=0; i<N; i++){
    vxt = part[i].vx;
    vyt = part[i].vy;
    ci = sqrt( sq(vxt) + sq(vyt) );
    ci = (ci * 0xff ) / max( ci, vmaxl );
    ci *= 0x10101;
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ] = ci;
  }
}

static void makevid4(){ // densit�
  int c,x,y,l=0;
  for( y=0; y < H>>DT; y++ ){
    for( x=0; x < W>>DT; x++ ){
      c = palette1[ ( densite[ x + y * (W>>DT) ] * 2047 ) / DENS_MAX ];
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;
    }
    l += 3*W;
  }
}

static void makevid5(){ // champs des vitesses (intensit�s)
  int vx,vy,c,d,x,y,s,l=0;

  for( y=0; y < H>>DT; y++ ){
    for( x=0; x < W>>DT; x++ ){
      s = x + y * (W>>DT);
      if( d = densite[s] ){
        vx = vit[s].x / d;
        vy = vit[s].y / d;
        c = sqrt( sq(vx) + sq(vy) );
        c = (2047*c) / max( c, v9>>16 );
        c = palette1[c];
      }
      else
        c = 0x000000;
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;
    }
    l += 3*W;
  }
}

static void makevid6(){ // champs des vitesses (direction)
  int c,x,y,vx,vy,s,l=0;
  float theta;
  for( y=0; y < H>>DT; y++ ){
    for( x=0; x < W>>DT; x++ ){
      s = x + y * (W>>DT);
      vx = vit[s].x;
      vy = vit[s].y;
      if( (vx != 0) || (vy != 0) ){ 
        theta = atan2( vx, vy ) + PI;
        c = palette2[ (int)((theta*2047)/(2*PI)) ];
      }
      else
        c = 0x000000;
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;
    }
    l += 3*W;
  }
}

static void makevid7(){ // distribution des vitesses
  int i,av,y,ymax,maxo=0;

  for( i=0; i<W; i++)
    distrib_vit[i] = 0;
  for( i=0; i<N; i++){
    av = sqrt( sq(part[i].vx) + sq(part[i].vy) );
    av = ((W-1)*min(vmax,av)) / vmax;
    av = ++distrib_vit[av];
    if( av > maxo )
      maxo = av;
  }
  for( i=0; i<W; i++ ){
    ymax = (distrib_vit[i]*H) / maxo;
    for( y=H-1; y>H-1-ymax ; y-- )
      video[ i + y*W ] = 0xffffff;
    for( y=H-1-ymax; y>=0; y--)
      video[ i + y*W ] = 0;
  }
  if( 1000 <= vmax){
    i = ((W-1)*1000) / vmax;
    for( y=0; y<H; y++ )
      video[ y*W + i ] = 0xff0000;
  }
}

static void makevid8(){ // rotationnel du champs des quantit�s de mouvement
  int rot,c,x,y,s,l=4*W + 4;
  int vu = 20*(v9>>16);

  for( y=1; y < (H>>DT) - 1; y++ ){
    for( x=1; x < (W>>DT) - 1; x++ ){
      s = x + y * (W>>DT);
      rot = vit[ s + 1 + (W>>DT) ].x - vit[ s - 1 - (W>>DT) ].x;
      rot += -vit[ s + 1 + (W>>DT) ].y + vit[ s - 1 - (W>>DT) ].y;
      rot += vit[ s - 1 + (W>>DT) ].x - vit[ s + 1 - (W>>DT) ].x;
      rot += vit[ s - 1 + (W>>DT) ].y - vit[ s + 1 - (W>>DT) ].y;
      rot *= 3; // approximation de 1/sqrt(2) : 3/4
      rot >>= 2;
      rot += vit[s + (W>>DT) ].x - vit[s - (W>>DT) ].x;
      rot += vit[s - 1].y - vit[s + 1].y;
      c = abs( rot );
      c = (2047*c) / max( c, vu );
      c = palette1[c];
      video[ l ] = c;
      video[ l+1 ] = c;
      video[ l+2 ] = c;
      video[ l+3 ] = c;
      video[ l+W ] = c;
      video[ l+1+W ] = c;
      video[ l+2+W ] = c;
      video[ l+3+W ] = c;
      video[ l+2*W ] = c;
      video[ l+1+2*W ] = c;
      video[ l+2+2*W ] = c;
      video[ l+3+2*W ] = c;
      video[ l+3*W ] = c;
      video[ l+1+3*W ] = c;
      video[ l+2+3*W ] = c;
      video[ l+3+3*W ] = c;
      l += 4;
    }
    l += 3*W + 8;
  }
}

static void MemError(){
  printf("Probleme de zero !\n");
  exit(1);
}

static void Make_palette(){
  int *palette;
  if( !( palette = malloc( 0x6fa*sizeof(int)))) MemError();
  int i;
  int c = 0x0;
  for(i = 0; i<0x100 ; i++)
    palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c = 0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){
    c -= 0x1;
    palette[i] = c;
  }
  c = 0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){
    c += 0x10000;
    palette[i] = c;
  }
  c = 0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){
    c -= 0x100;
    palette[i] = c;
  }
  c = 0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){
    c += 0x1;
    palette[i] = c;
  }
  c = 0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c=0xffffff;
  for( i=0; i < 0x800; i++)
    palette1[i] = palette[(0x6f9*i)/0x7ff];

    // palette circulaire :
  c = 0xffffff;
  for(i = 0; i<0x100 ; i++){
    palette[i]=c;
    c-=0x10100;
  }
  for(i = 0 ; i < 0x800 ; i++)
    palette2[i] = palette[(0x6f9*i)/0x7ff];
    
  free(palette);
}
