#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "fluide.h"
#include "fluide-local.h"

#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) < (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))

// fonctions d�finies dans le programme appelant :
extern void champs_force(int,int,int*,int*);
extern void init_particules(); // initialisations
extern void mur5(struct particule *pk, int k); // types de particules fixes
extern void mur6(struct particule *pk, int k);

struct particule *part;
int *map;  // carte des pixels
int vmax;  // vitesse maximum (pour la norme max)
int nvmax; // numero de la particule qui a la vitesse vmax
int force_on = 0; // champs de force activ� ou non
unsigned int iterations = 0; // compte les iterations
static int *alea; // liste de nombres aleatoires
static int vmax2; // deuxieme vitesse la plus elevee
static int nvmax2;// numero de la precedente
#ifdef VISC
int visc_on = 0; // la viscosit� est activ�e ou non
int *flux_x; // champs des vitesses (abscisses) 
int *flux_y; // champs des vitesses (ordonn�es)
#endif
#ifdef DENS
int expan_on = 0; // expansion bas�e sue la densit� activ�e ou non
int *p3;    // p3 conserve le champs des densit�s courant
static int *p1;     // champs scalaire des densit�s (ie pression statique)
static int *p2;     // p1 et p2 alternent quand ils sont regularis�s
#endif
#ifdef STATS
static float vxmean = 0; // vitesse moyenne
static float vymean = 0;
static unsigned int count = 0; // nombre de particules trait�es
static float vmaxmean = 0; // vitesse maximale moyenne
static int vmaxmax = 0; // le max des vmax
static unsigned int nchocs = 0; // nombre de chocs
#endif

static void MemErrorF(){
  printf("Probl�me de m�moire !");
  exit(1);
}

void init_fluide(){
  int i;
  map = malloc( W*H*sizeof(int) );
  alea = malloc( N*10*sizeof(int) );
  part = malloc( N*sizeof(struct particule) );
  if( !(alea && map && part ) ) MemErrorF();
  for(i=0; i<W*H; i++) map[i] = -1; // effacer la carte ( -1 -> vide )
  srand( time(NULL));               // liste de nombres al�atoires
  for(i=0; i<10*N; i++) alea[i] = rand()%400;

  #ifdef VISC
  flux_x = malloc( W*H*sizeof(int) );
  flux_y = malloc( W*H*sizeof(int) );
  if( !(flux_x && flux_y) ) MemErrorF();
  for(i=0; i<W*H; i++) flux_x[i] = 0; 
  for(i=0; i<W*H; i++) flux_y[i] = 0; 
  #endif

  #ifdef DENS
  p1 = malloc( W*H*sizeof(int) );
  p2 = malloc( W*H*sizeof(int) );
  p3 = malloc( W*H*sizeof(int) );
  if( !(p1 && p2 && p3) ) MemErrorF();
  for(i=0; i<W*H; i++) p1[i] = 0; 
  for(i=0; i<W*H; i++) p2[i] = 0; 
  for(i=0; i<W*H; i++) p3[i] = 0; 
  #endif

  init_particules();

  vmax = 500;
  vmax2 = 1;
  nvmax = -1;
  nvmax2 = -1;
  int av;
  for(i=0; i<N; i++){   // determine les deux plus grandes vitesses initiales
    av = max( abs(part[i].vx), abs(part[i].vy) );
    if( av > vmax2 ){
      if( av > vmax ){
        vmax = av;
        nvmax = i;
      }
      else {
        vmax2 = av;
        nvmax2 = i;
      }
    }
  }
}

static void choc( int a, int  b,int *vx1, int *vy1, int *vx2, int *vy2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int abvx1 = ab*(*vx1); // evite de faire ces produits deux fois.
  int abvy1 = ab*(*vy1);
  int abvx2 = ab*(*vx2);
  int abvy2 = ab*(*vy2);
  int vx1_p = b2*(*vx1) - abvy1     + a2*(*vx2) + abvy2;
  int vy1_p = -abvx1    + a2*(*vy1) + abvx2     + b2*(*vy2);
  int vx2_p = a2*(*vx1) + abvy1     + b2*(*vx2) - abvy2;
  int vy2_p = abvx1     + b2*(*vy1) - abvx2     + a2*(*vy2);
  *vx1 = vx1_p / S;
  *vy1 = vy1_p / S;
  *vx2 = vx2_p / S;
  *vy2 = vy2_p / S;
}

static void checkv(int i, int *vx, int *vy){
  int av = max( abs(*vx) , abs(*vy) );
  if( av >= 0x8000 ){ // majoration de la vitesse a 0x8000
    *vx = min( *vx, 0x7fff );
    *vy = min( *vy, 0x7fff );
    *vx = max( *vx, -0x7fff );
    *vy = max( *vy, -0x7fff );
    av = 0x7fff;
  }
  if( i == nvmax ){   // recherche de la particule la plus rapide
    if( av <= vmax2 ){// et de la 2e plus rapide
      vmax = vmax2;
      nvmax = nvmax2;
      vmax2 = 1;
    }
    else
      vmax = av;
  }
  else if( av > vmax2 ){
    if( av < vmax ){
      vmax2 = av;
      nvmax2 = i;
    }
    else if( av > vmax ){
      vmax2 = vmax;
      nvmax2 = nvmax;
      vmax = av;
      nvmax = i;
    }
  }
}

static void dM(int k){
  int fx = 0;
  int fy = 0;
  struct particule pk = part[k];
  //  int x = part[k].x;
  //  int y = part[k].y;
  int l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  //  int vx = part[k].vx;
  //  int vy = part[k].vy;
  int xi = pk.x; // position initiale
  int yi = pk.y;
  int li = l;

  // Somme des forces (hors chocs) s'exercant sur la particule k

  if( force_on ) champs_force(pk.x,pk.y,&fx,&fy);  // champs de force

  if( map[ l+1 ] > -1 )
    fx -= PR; // forces repulsives entre particules
  if( map[ l-1] > -1 ) 
    fx += PR; // proportionnelles a la temperature T
  if( map[ l+W ] > -1 ) 
    fy -= PR;
  if( map[ l-W ] > -1 )
    fy += PR;
  if( map[ l+W+1 ] > -1 ){
    fx -= PR;
    fy -= PR;
  }
  if( map[ l-W+1] > -1 ){
    fx -= PR;
    fy += PR;
  }
  if( map[ l+W-1 ] > -1 ){
    fx += PR;
    fy -= PR;
  }
  if( map[ l-W-1 ] > -1 ){
    fx += PR;
    fy += PR;
  }
  
  #ifdef DENS
  if( expan_on ){
    fx -= ( p3[l+1] - p3[l-1]) * (p3[l] );
    fy -= ( p3[l+W] - p3[l-W]) * (p3[l] );
  }
  #endif

  #ifdef VISC
  if( visc_on ){
    int dvnx = flux_x[l-W] + flux_x[l+W] - 2*pk.vx;
    int dvny = flux_y[l-1] + flux_y[l+1] - 2*pk.vy;
    fx += dvnx*VISC;
    fy += dvny*VISC;
  }
  #endif
  
  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  pk.vx *= vmax;
  pk.vx += fx;
  pk.vx /= vmax;

  pk.vy *= vmax;
  pk.vy += fy;
  pk.vy /= vmax;

  checkv(k,&pk.vx,&pk.vy);
  
  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  pk.x *= vmax;
  pk.x += (pk.vx << SUBD_EXP) + r;
  pk.x /= vmax;

  pk.y *= vmax;
  pk.y += (pk.vy << SUBD_EXP) + r;
  pk.y /= vmax;

  // chocs avec une autre particule
    
  l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  if( l != li ){
    if( map[l] != -1 ){
      #ifdef STATS
      nchocs++;
      #endif
      int m,le;
      int j = map[l];
      if( j >= 0 ){
        choc( xi - part[j].x, yi - part[j].y, &pk.vx, &pk.vy, &part[j].vx, &part[j].vy );
        checkv( k, &pk.vx, &pk.vy );
        checkv( j, &part[j].vx, &part[j].vy );
        pk.x = xi;
        pk.y = yi;
        l = li;
      }
      else{
        switch( j ){
        case -3 : 
          pk.vx = -pk.vx;
          pk.x = xi;
          pk.y = yi;
          l = li;
          break;
        case -2 :
          pk.vy = -pk.vy;
          //pk.vx /= 2;
          pk.x = xi;
          pk.y = yi;
          l = li;
          break;
        case -4 :
          pk.vx = -pk.vx;
          pk.vy = -pk.vy;
          pk.x = xi;
          pk.y = yi;
          l = li;
          break;
        case -5 :
          mur5(&pk, k);
          break;
        case -6 :
          mur6(&pk, k);
          break;
        }
      }
    }
    else{
      map[li] = -1; // la particule change de pixel
      map[l] = k;
    }
  }

  // sauvegarde finale des nouvelles positions et vitesses
  part[k] = pk;
  #ifdef VISC
  flux_x[ l ] = pk.vx; // champs des vitesses
  flux_y[ l ] = pk.vy;
  #endif
  #ifdef DENS
  p1[l] += 21;     // champs des densit�s
  #endif
  #ifdef STATS
  count++;
  if( vmax > vmaxmax ) vmaxmax = vmax;
  vxmean += (float)abs(pk.vx);
  vymean += (float)abs(pk.vy);
  vmaxmean += (float)vmax;
  #endif
}

#ifdef DENS
static void diffusion( int *a1, int *a2 ){
  // Le champs scalaire a1 diffuse dans le champs a2
  int h,l;
  l = 0;
  *(a2+l) = *(a1+W+1) * 9; // les lignes de bordures ne sont pas prises en compte
  for( l=1; l < W-1; l++)
    *(a2+l) = ( *(a1+l+W-1) + *(a1+l+W) + *(a1+l+W+1) ) * 3;
  *(a2+l) = *(a1+l+W-1) * 9;
  for( h=W; h < (H-1)*W; h+=W ){ 
    l++;
    *(a2+l) = ( *(a1+l-W+1) + *(a1+l+1) + *(a1+l+W+1) ) * 3;
    for( l = h+1 ; l < h+W-1; l++ )
      *(a2+l) = *(a1+l-W-1) + *(a1+l-W) + *(a1+l-W+1)
        + *(a1+l-1) + *(a1+l) + *(a1+l+1)
        + *(a1+l+W-1) + *(a1+l+W) + *(a1+l+W+1);
    *(a2+l) = ( *(a1+l-W-1) + *(a1+l-1) + *(a1+l+W-1) ) * 3;
  }
  l++;
  *(a2+l) = *(a1-W+1) * 9;
  for( l=(H-1)*W+1; l < H*W-1; l++)
    *(a2+l) = ( *(a1+l-W-1) + *(a1+l-W) + *(a1+l-W+1) ) * 3;
  *(a2+l) = *(a1+l-W-1) * 9;
}

static void attenuation( int *a1 ){
  unsigned int S,l;
  for( l = 0; l < W*H; l++ ){ 
    S = ( a1[l] * 3 ) >> 8;   // coefficient d'attenuation : 9*9*3/(2^8)
    a1[l] = S;
    p3[l] = S;
  }
}
#endif

void move(){
  int i,bt;
  static unsigned int n;
  bt = rand()%(9*N);
  for( i=0; i<N; i++ ){
    n += alea[ i + bt ];
    dM( n%N );
  }
  #ifdef DENS
  if( iterations % 5 == 0 ){
    diffusion(p1,p2);
    diffusion(p2,p1);
    attenuation(p1);
    //     ptemp = p1;
    //p1 = p2;
    //p2 = ptemp;
  }
  #endif
  iterations++;
}

void print_stats( int tf ){
#ifdef STATS
  int ms=tf%1000;
  int h=tf/(60*60*1000);
  int mn=(tf/(60*1000))%60;
  int s=(tf/1000)%60;
  int xmean = 0;
  int ymean = 0;
  int i;
  for( i=0; i<N; i++ ) xmean += part[i].x;
  for( i=0; i<N; i++ ) ymean += part[i].y;
  xmean /= N*SUBD;
  ymean /= N*SUBD;
  printf("\nNombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", ((float)N)/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf("\nNombre total d'iterations : %u\n",iterations);
  printf("Temps ecoule : %i h %i mn %i s %i ms\n",h,mn,s,ms);
  printf("Iterations par seconde : %f\n",1000.0*iterations/tf);
  // printf("Images par seconde : %f\n\n",1000.0*frame/tf);
  printf("Moyenne des abscisses (derni�re it�ration) : %i\n",xmean);
  printf("Moyenne des ordonn�es (derni�re it�ration) : %i\n",ymean);
  printf("Vitesse x moyenne : %f\n", vxmean/count);
  printf("Vitesse y moyenne : %f\n", vymean/count);
  printf("Vitesse max moyenne : %f\n", vmaxmean/count);
  printf("Record de vitesse toute iterations confondues : %i\n", vmaxmax);
  printf("Nombre moyen de chocs par iteration : %f\n", ((float)nchocs)/iterations);
#endif
}
