extern void init_fluide();
extern void move();
extern void print_stats(int);

struct particule{ // structure d'une particule
  int x;
  int y;
  int vx;
  int vy;
};
extern struct particule *part; // les particules...
extern int *map;   // carte des pixels
extern int *flux_x;// champs des vitesses (abscisses) 
extern int *flux_y;// champs des vitesses (ordonn�es)
extern int *p3;    // p3 conserve le champs des densit�s courant
extern int vmax;   // vitesse maximum (pour la norme max)
extern int nvmax;  // numero de la particule qui a la vitesse vmax
extern int force_on; // champs de force activ� ou non
extern int visc_on;  // la viscosit� est activ�e ou non
extern int expan_on; // expansion bas�e sue la densit� activ�e ou non
extern unsigned int iterations; // compte les iterations
