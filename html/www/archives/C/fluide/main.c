#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include "local_consts.h"
#include "main.h"
#include "exec_flow.h"
#include "fluid.h"
#include "video.h"
#include "context.h"

volatile int loop = 1;
int *map;               // carte des pixels
struct particule *part; // les particules
int force_on = 0;       // champs de force activ� ou non

static void check_malloc(void *malloc_p){
  if( malloc_p == NULL ){
    fprintf(stderr, "\nProbl�me de m�moire !\n");
    exit(1);
  }
}

static void init_main(){
  int i, l;

  // carte des pixels : map
  map = malloc( W*H*sizeof(map[0]) );
  check_malloc(map);
  for ( l=0; l<W*H; l++ )
    map[l] = LAST_PART;

  // les particules : part
  part = malloc( N * sizeof(struct particule) );
  check_malloc(part);

  init_particules(); // definie dans le fichier context-XXX.c

  for ( i=0; i<N; i++ ){ // mettre les numeros des particules sur la carte
    l = ( part[i].x >> SUBD_EXP ) + ( part[i].y >> SUBD_EXP ) * W;
    if ( map[l] == LAST_PART ){
      map[l] = i;
      part[i].next = LAST_PART;
    }
    else {
      part[i].next = map[l];
      map[l] = i;
    }
  }
}

int main( int narg, char **arg ){

  init_main();      // utilise la fonction init_particules
  init_fluid();
  init_video();     // initialisation en particulier de SDL
  init_exec_flow(); // initialisation des threads

  Uint32 t0 = SDL_GetTicks();

  // D�marrage des threads :
  start_fluid();
  start_video();
  
  SDL_Event event;
  while( loop ){
    SDL_WaitEvent( &event );
    switch(event.type){
    case SDL_MOUSEBUTTONDOWN :
      loop = 0;	
      break;
    case SDL_KEYDOWN:
      switch( event.key.keysym.sym ){
      case SDLK_ESCAPE :
      case SDLK_q :
        loop = 0;
        break;
      case SDLK_f :
        force_on = 1 - force_on;
        break;
      case SDLK_SPACE :
        video_mode++;
        break;
      }
      break;
    }
  }

  end_video();
  end_fluid();

#ifdef STATS
  print_stats_video( SDL_GetTicks() - t0 );
  print_stats_context();
  print_stats_fluid();
  print_stats_exec( SDL_GetTicks() - t0 );
#else // STATS
  print_stats_min( SDL_GetTicks() - t0 );
#endif // STATS
  return 0;
}
