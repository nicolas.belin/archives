#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
//#define NDEBUG 1  // pas de controle avec assert() si defini
#include <assert.h>
#include <SDL/SDL.h>
#include "defines.h"
#include "fluid.h"
#include "context.h"
#include "local_consts.h"

extern int loop;
SDL_Thread *thread_struct_ptr[THREAD_NR];
uint *densite; // champs de densit�s
int *map;  // carte des pixels
int vmax;  // vitesse maximum (pour la norme max)
int force_on = 0; // champs de force activ� ou non
uint line_sep[THREAD_NR+1];
static uint thread_id[THREAD_NR];
struct particule *part;
static uint *part_list;  // liste des particules, par bandes
static uint v9_strip[THREAD_NR]; // 97e centile pour chaque bande
static uint bline[H+1];
static int *alea; // liste de nombres aleatoires
static uint seeds[THREAD_NR];
static const int near[] = { -W-1, -W, -W+1, -1, 0, 1, W-1, W, W+1 };
static SDL_mutex *in_sep_lock[THREAD_NR];

#ifdef V_FIELD
struct vector *v_field;
#endif // V_FIELD

#ifdef STATS
static uint mutex_locked_nr = 0;
static uint mutex_wait_nr = 0;
static uint mutex_locked[THREAD_NR+1];
static uint vmaxmax = 0; // le max des vmax
static uint slowed = 0; // nombre de particules ralenties
static uint nchocs = 0; // nombre de chocs
static uint64_t count_per_thread[THREAD_NR];
#endif

static void check_malloc(void *malloc_p){
  if( malloc_p == NULL ){
    fprintf(stderr, "\nProbl�me de m�moire !\n");
    exit(1);
  }
}

static inline void check_lock( int r ){
  if( r != 0 ){
    fprintf(stderr, "\nIncapable de lock/unlock un mutex !\n");
    exit(-1);
  }
}

static inline void print_dens( uint l, int h ){
  assert( l-(W>>DT)-1 >= 0 &&  l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  densite[ l ] += h;
  densite[ l + 1 ] += h;
  densite[ l - 1 ] += h;
  densite[ l + (W>>DT) ] += h;
  densite[ l - (W>>DT) ] += h;
  densite[ l + (W>>DT) + 1 ] += h;
  densite[ l - (W>>DT) + 1 ] += h;
  densite[ l + (W>>DT) - 1 ] += h;
  densite[ l - (W>>DT) - 1 ] += h;
}

#ifdef V_FIELD
static inline void print_v( uint l, int vx, int vy ){
  assert( l-(W>>DT)-1 >= 0 && l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  v_field[ l ].x += vx;
  v_field[ l ].y += vy;
  v_field[ l + 1 ].x += vx;
  v_field[ l + 1 ].y += vy;
  v_field[ l - 1 ].x += vx;
  v_field[ l - 1 ].y += vy;
  v_field[ l + (W>>DT) ].x += vx;
  v_field[ l + (W>>DT) ].y += vy;
  v_field[ l - (W>>DT) ].x += vx;
  v_field[ l - (W>>DT) ].y += vy;
  v_field[ l + (W>>DT) + 1 ].x += vx;
  v_field[ l + (W>>DT) + 1 ].y += vy;
  v_field[ l - (W>>DT) + 1 ].x += vx;
  v_field[ l - (W>>DT) + 1 ].y += vy;
  v_field[ l + (W>>DT) - 1 ].x += vx;
  v_field[ l + (W>>DT) - 1 ].y += vy;
  v_field[ l - (W>>DT) - 1 ].x += vx;
  v_field[ l - (W>>DT) - 1 ].y += vy;
  
}
#endif // V_FIELD

static inline void swap_uint( uint *a, uint *b ){
  uint temp;
  temp = *a;
  *a = *b;
  *b = temp;
}

static inline int mod_int( int a , int b ){
  int t = a % b;
  return ( t >= 0 ) ? t : t+b;
}

void init_fluid(){
  int i,j;
  for( i=0; i<THREAD_NR; i++ )
    thread_id[i] = i;

  // carte des pixels : map
  map = malloc( W*H*sizeof(int) );
  check_malloc(map);
  for(i=0; i<W*H; i++)
    map[i] = LAST_PART;

  // champs des densit�s : densite
  densite = malloc( (W>>DT)*(H>>DT)*sizeof(uint) );
  check_malloc(densite);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    densite[i] = 0;

  // table de nombres al�atoires : alea
  alea = malloc( N*10*sizeof(int) );
  check_malloc(alea);
  srand( time(NULL));               // liste de nombres al�atoires
  for(i=0; i<10*N; i++)
    alea[i] = ( (double)rand() / RAND_MAX ) * 3000;

  // seeds pour la fonction rand_r(), adapt�e au multithreading
  for( i=0; i<THREAD_NR; i++ )
    seeds[i] = rand();

#ifdef V_FIELD
  // champs des vitesses : v_field
  v_field = malloc( (W>>DT) * (H>>DT) * sizeof(struct vector) );
  check_malloc(v_field);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    v_field[i].x = v_field[i].y = 0;
#endif // V_FIELD

  // les particules : part
  part = malloc( N * sizeof(struct particule) );
  check_malloc(part);
  init_particules(); // definie dans le fichier context-XXX.c

  // liste des particules, class�es par lignes
  uint sb;
  part_list = malloc( N * sizeof(uint) );
  check_malloc( part_list );
  for( i=0; i<N; i++ )
    part_list[i] = i;
  for( i=0, sb=0; i<H; i++ ){
    bline[i] = sb;
    for( j=sb; j<N; j++ ){
      if( part[ part_list[j] ].y >> SUBD_EXP == i ){
        swap_uint( part_list + sb, part_list + j );
        sb++;
      }
    }
  }
  bline[H] = N;

  for( i=0, j=0; i<THREAD_NR; i++ ){
    line_sep[i] = j;
    while ( bline[++j] < (i+1)*N/THREAD_NR );
  }
  line_sep[THREAD_NR] = H;

  /*
  for( i=0; i<=THREAD_NR; i++ )
    printf( "bound: %i     line: %i\n",
            strip_boundary[i], line_sep[i] );
  exit(0);
  */

  for( i=0; i<THREAD_NR; i++ )
    v9_strip[i] = V9_INIT;

  for(i=0; i<N; i++) // mettre les numeros des particules sur la carte
    map[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = i;

  for(i=0; i<N; i++)
    print_dens( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT), 1 );

#ifdef V_FIELD
  for(i=0; i<N; i++)
    print_v( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT),
             part[i].vx, part[i].vy );
#endif // V_FIELD

  vmax = ( ( CVMAX_NUM * V9_INIT ) / CVMAX_DEN ) >> 16;
  if( vmax > VMAX_MAX )
    vmax = VMAX_MAX;
  if( vmax < VMAX_MIN )
    vmax = VMAX_MIN;

#ifdef STATS
  for( i=0; i<=THREAD_NR; i++ )
    mutex_locked[i] = 0;
#endif // STATS

}

void start_fluid(){ // mise en place des threads g�rant les mouvements des particules
  int i;
  for( i=0; i<THREAD_NR; i++ )
    in_sep_lock[i] = SDL_CreateMutex();
  for( i=0; i<THREAD_NR; i++ ){
    thread_struct_ptr[i] = SDL_CreateThread( move_fluid, thread_id + i );
    if( thread_struct_ptr[i] == NULL ){
      fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
      exit(1);
    }
  }
}

void end_fluid(){ // On attend que toutes les threads soient �teintes
  int i;
  for( i=0; i<THREAD_NR; i++ )
    SDL_WaitThread( thread_struct_ptr[i], NULL );
  for( i=0; i<THREAD_NR; i++ )
    SDL_DestroyMutex( in_sep_lock[i] );
}

#ifdef CHOCS
static void choc( int a, int  b, struct particule *p1, struct particule *p2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int vx1 = p1->vx;
  int abvx1 = ab * vx1; // evite de faire ces produits deux fois.
  int vy1 = p1->vy;
  int abvy1 = ab * vy1;
  int vx2 = p2->vx;
  int abvx2 = ab * vx2;
  int vy2 = p2->vy;
  int abvy2 = ab * vy2;
  int vx1_p = b2*vx1 - abvy1  + a2*vx2 + abvy2;
  int vy1_p = -abvx1 + a2*vy1 + abvx2  + b2*vy2;
  int vx2_p = a2*vx1 + abvy1  + b2*vx2 - abvy2;
  int vy2_p = abvx1  + b2*vy1 - abvx2  + a2*vy2;
  p1->vx = vx1_p / S;
  p1->vy = vy1_p / S;
  p2->vx = vx2_p / S;
  p2->vy = vy2_p / S;
#ifdef STATS
  nchocs++;
#endif // STATS
}
#endif // CHOCS

static inline void checkv( struct particule *p ){
  int vx = p->vx;
  int vy = p->vy;
  if( ABS(vx) > vmax || ABS(vy) > vmax ){
    int av = sqrt( SQ(vx) + SQ(vy) );
    p->vx = (vx*vmax) / av;
    p->vy = (vy*vmax) / av;
#ifdef STATS
    slowed++;
#endif // STATS
  }
}

static inline struct particule dM( const struct particule *pki_ptr , uint *v9_ptr ){
  struct particule pki = *pki_ptr;
  struct particule pk = pki;
  int fx = 0;
  int fy = 0;
  
  uint l = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
  assert( l-W-1 >= 0 && l+W+1 < H*W );
  uint li = l;

    /*
    printf( "* Thread no. %i \n", thread );
    printf( " lim_inf: %i   true: %i\n", lim_inf, bline[ line_sep[thread] ] );
    printf( " lim_sup: %i   true: %i\n", lim_sup, bline[ line_sep[thread+1] ] );
    printf(" strip_locked: %i\n", strip_locked );
    printf("list_k: %i\n", list_k );
    loop = 0;
    */

  uint li4 = ( pki.x>>(SUBD_EXP+DT) ) + ( pki.y>>(SUBD_EXP+DT) ) * (W>>DT);
  uint l4 = li4;
  int h;
  
  // Somme des forces (hors chocs) s'exer�ant sur la particule k

  if( force_on )
    champs_force(pki.x,pki.y,&fx,&fy);  // champs de force

  /*  
  int dens = densite[ l4 ];
  
  int densr = max( densite[ l4 + 1 ], 1 ); // on va diviser par ces densit�s
  int densl = max( densite[ l4 - 1 ], 1 );
  int densu = max( densite[ l4 + (W>>DT) ], 1 );
  int densd = max( densite[ l4 - (W>>DT) ], 1 );
  */

  /*
  int densr = densite[ l4 + 1 ];
  int densl = densite[ l4 - 1 ];
  int densu += densite[ l4 +  (W>>DT) ];
  int dens8 += densite[ l4 -  (W>>DT) ];
  dens8 += densite[ l4 +  (W>>DT) + 1 ];
  dens8 += densite[ l4 +  (W>>DT) - 1 ];
  dens8 += densite[ l4 -  (W>>DT) + 1 ];
  dens8 += densite[ l4 -  (W>>DT) - 1 ];
  */

  
  // Agitation particulaire locale (agitation thermique)
  // la temp�rature est li�e � la densit� particulaire
  // cette relation est l'une des �quation d'�tat du fluide :
  int T = T_MIN;// + ( (T_MAX-T_MIN) * dens ) / DENS_MAX;
  
  if( map[ l+1 ] >= 0 )
    fx -= T;
  if( map[ l-1] >= 0 ) 
    fx += T; 
  if( map[ l+W ] >= 0 ) 
    fy -= T;
  if( map[ l-W ] >= 0 )
    fy += T;
  if( map[ l+W+1 ] >= 0 ){
    fx -= T;
    fy -= T;
  }
  if( map[ l-W+1] >= 0 ){
    fx -= T;
    fy += T;
  }
  if( map[ l+W-1 ] >= 0 ){
    fx += T;
    fy -= T;
  }
  if( map[ l-W-1 ] >= 0 ){
    fx += T;
    fy += T;
  }
  
  /*
  int i, dx, dy, ds2, ml;
  for( i=0; i<9; i++ ){
    ml = map[l + near[i] ];
    while( ml != LAST_PART && ml != k ){
      dx = part[ml].x - pk.x;
      dy = part[ml].y - pk.y;
      ds2 = SQ(dx) + SQ(dy);
      if ( ds2 == 0 ){
        ds2 = 1;
      }
      fx += ( (dx<<SUBD_EXP) / ds2) * 2*T;
      fy += ( (dy<<SUBD_EXP) / ds2) * 2*T;
      ml = part[ml].next;
    }
  }
  */
  /*
  fx -= ENT*(pk.vx - vit[l4].x / dens); // force d'entrainement du courant local
  fy -= ENT*(pk.vy - vit[l4].y / dens); 
  */
  
  // flux de quantit� de mouvement
  //  int vx2r = ( vit[ l4 + 1 ].x * vit[ l4 + 1 ].x ) / densr;
  //int vx2l = ( vit[ l4 - 1 ].x * vit[ l4 - 1 ].x ) / densl;
  //  int vy2r = ( vit[ l4 + 1 ].y * vit[ l4 + 1 ].y ) / densr;
  //  int vy2l = ( vit[ l4 - 1 ].y * vit[ l4 - 1 ].y ) / densl;
  // int vxyr = ( vit[ l4 + 1 ].x * vit[ l4 + 1 ].y ) / densr;
  //int vxyl = ( vit[ l4 - 1 ].x * vit[ l4 - 1 ].y ) / densl;
  //  int vx2u = ( vit[ l4 + (W>>DT) ].x * vit[ l4 + (W>>DT) ].x ) / densu;
  //  int vx2l = ( vit[ l4 - (W>>DT) ].x * vit[ l4 - (W>>DT) ].x ) / densd;
  //int vy2u = ( vit[ l4 + (W>>DT) ].y * vit[ l4 + (W>>DT) ].y ) / densu;
  //int vy2d = ( vit[ l4 - (W>>DT) ].y * vit[ l4 - (W>>DT) ].y ) / densd;
  //int vxyu = ( vit[ l4 + (W>>DT) ].x * vit[ l4 + (W>>DT) ].y ) / densu;
  //int vxyd = ( vit[ l4 - (W>>DT) ].x * vit[ l4 - (W>>DT) ].y ) / densd;
  //fx -= ( vx2r - vx2l + vxyu - vxyd ) / dens;
  //fy -= ( vxyr - vxyl + vy2u - vy2d ) / dens;

  /*
  // gradient de densit� -> gradient de pression
  int gradddiag1 = densite[ l4 + 1 + (W>>DT) ] - densite[ l4 - 1 - (W>>DT) ];
  int gradddiag2 = densite[ l4 + 1 - (W>>DT) ] - densite[ l4 - 1 + (W>>DT) ];
  int graddx = gradddiag1 + gradddiag2;
  int graddy = gradddiag1 - gradddiag2;
  graddx *= 3; // approximation de 1/sqrt(2) : 3/4
  graddy *= 3;
  graddx >>= 2;
  graddy >>= 2;
  graddx += densite[ l4 + 1 ] - densite[ l4 - 1 ];
  graddy += densite[ l4 + (W>>DT) ] - densite[ l4 - (W>>DT) ];
  #define ADP 10000
  #define BPD 1000
  int coeff_pd = ADP + BPD * dens; // �quation d'etat du fluide
  int gradpx = coeff_pd * graddx;  // grad(P) = coeff_pd(d) * grad(d) 
  int gradpy = coeff_pd * graddy;
  fx -= gradpx / dens; // concervation de la quantit� de mouvement : -grad(P)
  fy -= gradpy / dens;
  */  

  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  pk.vx *= vmax;
  pk.vx += INV_M*fx;
  pk.vx /= vmax;

  pk.vy *= vmax;
  pk.vy += INV_M*fy;
  pk.vy /= vmax;

  /*    
  int rot = vit[ l4 + 1 + (W>>DT) ].x - vit[ l4 - 1 - (W>>DT) ].x;
  rot += -vit[ l4 + 1 + (W>>DT) ].y + vit[ l4 - 1 - (W>>DT) ].y;
  rot += vit[ l4 - 1 + (W>>DT) ].x - vit[ l4 + 1 - (W>>DT) ].x;
  rot += vit[ l4 - 1 + (W>>DT) ].y - vit[ l4 + 1 - (W>>DT) ].y;
  rot *= 3; // approximation de 1/sqrt(2) : 3/4
  rot >>= 2;
  rot += vit[ l4 + (W>>DT) ].x - vit[ l4 - (W>>DT) ].x;
  rot += vit[ l4 - 1].y - vit[ l4 + 1].y;
  rot /= dens8;
  rot -= pk.w;
  pk.w *= vmax;
  pk.w += rot*100;
  pk.w /= vmax;

  float ca = cos( ((float) pk.w) / vmax );
  float sa = sin( ((float) pk.w) / vmax );
  int vx = pk.vx * ca - pk.vy * sa;
  pk.vy = pk.vx * sa + pk.vy * ca;
  pk.vx = vx;
  */

  checkv( &pk );
  assert( ABS(pk.vx) <= VMAX_MAX && ABS(pk.vy) <= VMAX_MAX );

  
  // la suite des v9 converge vers le 97e centile des vitesses
  //  uint v9 = v9_strip[thread];
  
  if( (ABS(pk.vx)<<16) >= *v9_ptr || (ABS(pk.vy)<<16) >= *v9_ptr )
    *v9_ptr += 31*(1<<11);
  else
    *v9_ptr -= 1*(1<<11);
  //  v9_strip[thread] = v9;
  

  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  pk.x *= vmax;
  pk.x += (pk.vx << SUBD_EXP) + r;
  pk.x /= vmax;

  pk.y *= vmax;
  pk.y += (pk.vy << SUBD_EXP) + r;
  pk.y /= vmax;

  assert( pk.x >= 0 && pk.y >= 0 && (pk.x >> SUBD_EXP) < W && (pk.y >> SUBD_EXP) < H );
  assert( ABS( (pk.x >> SUBD_EXP) - (pki.x >> SUBD_EXP) ) <= 1 ); 
  assert( ABS( (pk.y >> SUBD_EXP) - (pki.y >> SUBD_EXP) ) <= 1 ); 

 translated:
  l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  assert( l-W-1 >= 0 && l+W+1 < H*W );
  if ( l != li ){ // si la particule change de pixel...
    int mapl = map[l];
    if( mapl >= 0 || mapl == LAST_PART ){ // si la place est occup�e par une autre particule ou vide...
      
#ifdef CHOCS
      if( mapl != LAST_PART ){
        // chocs avec les particules pr�sente sur le nouveau pixel :
        h = mapl;
#ifdef V_FIELD
        int parthvi_x = 0;
        int parthvi_y = 0;
#endif // V_FIELD
        do{
#ifdef V_FIELD
          parthvi_x -= part[h].vx;
          parthvi_y -= part[h].vy;
#endif // V_FIELD          
          choc( pki.x - part[h].x, pki.y - part[h].y, &pk, &part[h] );
          checkv( &pk );
          checkv( &part[h] );
          h = part[h].next;
#ifdef V_FIELD
          parthvi_x += part[h].vx;
          parthvi_y += part[h].vy;
#endif // V_FIELD
        }
        while( h != LAST_PART );
#ifdef V_FIELD
        print_v( li4, parthvi_x, parthvi_y );
#endif // V_FIELD
      }
#endif // CHOCS

      // pixel que l'on quitte :
      if( pki.prev != FIRST_PART )  // si k n'�tait pas en t�te de liste
        part[ pki.prev ].next = pki.next;
      else
        map[li] = pki.next;
      if( pki.next != LAST_PART )
        part[ pki.next ].prev = pki.prev;

      //pixel o� l'on arrive
      if( mapl != LAST_PART )
        part[ mapl ].prev = k;
      pk.next = mapl;
      pk.prev = FIRST_PART;       // devient t�te de liste
      map[l] = k;
    }
    else{
      switch( mapl ){ // si la place est occup�e par autre chose...
      case FRAME_V : 
        pk.vx = -pk.vx;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case FRAME_H :
        pk.vy = -pk.vy;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case TRANSLATE_PART :
        translation( &pk, &pki );
        goto translated;
      case FORBIDDEN :
        wall1( &pk, &pki );
      }
    }
    l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
    if( li4 != l4 ){ // modifier le champs des densit�s
      print_dens( li4, -1 );
      print_dens( l4, 1 );
    }
    
    /*
    // modification d'une ligne de partage ?
    int b, a, line_k;
    if( strip_locked != -1 ){
      line_k = pk.y >> SUBD_EXP;
      if ( line_k >= line_sep[thread+1] ){ 
        b = 2 * bline[ line_sep[thread+1] ] - bline[ line_sep[thread] ] - bline[ line_sep[thread+2] ];  
        a = bline[ line_sep[thread+1] ] - bline[ line_sep[thread+1] - 1 ];  
        if ( b > a){
          line_sep[thread+1]--;
        }
      }
      else if ( line_k < line_sep[thread] ){
        b = -2 * bline[ line_sep[thread] ] + bline[ line_sep[thread+1] ] + bline[ line_sep[thread-1] ];
        a = bline[ line_sep[thread] + 1 ] - bline[ line_sep[thread] ];  
        if ( b > a ){
          line_sep[thread]++;
        }
      }
    }
    */
  }
 
#ifdef V_FIELD     
  //  if( l4 == li4 )  // mise � jour du champs des vitesses
  //  print_v( li4, pk.vx - pki.vx, pk.vy - pki.vy );
  // else{
  print_v( li4, -pki.vx, -pki.vy );
  print_v( l4, pk.vx, pk.vy );
    // }
#endif // V_FIELD

  return pk;
}

/* 
 * Cette fonction (re-entrante) est lanc�e
 * dans chaque thread
 */
int move_fluid( void *thread_ptr ){ 
  int i, j, bt, strip_locked;
  uint v9 = V9_INIT;
  uint64_t v9_mean;
  uint64_t count = 0;
  uint k, list_k, thread = *(int*)thread_ptr;    // numero de la thread actuelle
  uint n = rand_r( seeds + thread );
  struct particule pk;
  struct particule pki; // sauvegarde de l'�tat initial
  while( loop ){
    if( count % (N/THREAD_NR) == 0 ){ 
      bt = rand_r( seeds + thread ) % (9*N);
      i = 0;
      v9_strip[thread] = v9;
      if( thread == 0 ){
        v9_mean = 0;
        for( j=0; j<THREAD_NR; j++ )
          v9_mean += (uint64_t)v9_strip[j] * ( bline[ line_sep[j+1] ] - bline[ line_sep[j] ] );
        v9_mean /= N;
        uint vmax_temp;
        vmax_temp = ( (CVMAX_NUM*v9_mean)/CVMAX_DEN ) >> 16;
        vmax_temp = MAX( vmax_temp, VMAX_MIN );
        vmax_temp = MIN( vmax_temp, VMAX_MAX );
        vmax = vmax_temp;
#ifdef STATS
        vmaxmax = MAX( vmax, vmaxmax );
#endif // STATS
      }
    }
    n += alea[ i++ + bt ];
    list_k =  bline[ line_sep[thread] ]
      + n % ( bline[ line_sep[thread+1] ] - bline[ line_sep[thread] ]);
    k = part_list[ list_k ];
    strip_locked = -1;
    if( list_k < bline[line_sep[thread]+2] ){ // la particule est dans une zone frontiere entre 2 bandes
#ifdef STATS
      if ( mutex_locked[thread] != 0 )
        mutex_wait_nr++;
#endif // STATS
      check_lock( SDL_LockMutex( in_sep_lock[thread] ) );
      strip_locked = thread;
      if ( list_k < bline[ line_sep[thread] ] ) // cas extr�mement rare, mais possible
        list_k = bline[ line_sep[thread] ];
#ifdef STATS
      mutex_locked[thread] = 1;
#endif // STATS
    }
    else if( list_k >= bline[line_sep[thread+1]-2] ){
#ifdef STATS
      if ( mutex_locked[thread+1] != 0 )
        mutex_wait_nr++;
#endif // STATS
      check_lock( SDL_LockMutex( in_sep_lock[thread+1] ) );
      strip_locked = thread + 1;
      if ( list_k >= bline[ line_sep[thread+1] ] ) // cas extr�mement rare, mais possible
        list_k = bline[ line_sep[thread+1] ] - 1;
#ifdef STATS
      mutex_locked[thread+1] = 1;
#endif // STATS
    }

    pki = part[k];
    pk = dM( &pki, &v9 );                          // calcul de la nouvelle position de la part.
    count++;
    part[k] = pk;

    // changement de ligne :
    uint yi = pki.y >> SUBD_EXP;
    uint y = pk.y >> SUBD_EXP;
    if ( y > yi ){
      bline[y]--;
      swap_uint( part_list + list_k, part_list + bline[y] );
    }
    else if ( y < yi ){
      swap_uint( part_list + list_k, part_list + bline[yi] );
      bline[yi]++;
    }

    if( strip_locked != -1 ){ // la particule est dans une zone frontiere entre 2 bandes
#ifdef STATS
      mutex_locked[strip_locked] = 0;
      mutex_locked_nr++;
#endif // STATS
      check_lock( SDL_UnlockMutex( in_sep_lock[ strip_locked ] ) );
      strip_locked = -1;
    }
  }

#ifdef STATS
  count_per_thread[thread] = count;
#endif // STATS
  return 0;
}

#ifdef STATS
static float v_smoothness( int c ){
  int k,k_random,l,x,y,hxy;
  double sk, sk_random, s = 0;
  double s_random = 0;
  struct particule pk, p_random;
  for( k=0; k<N; k++ ){
    pk = part[k];
    l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
    sk = 0;
    sk_random = 0;
    for( x=-c; x<=c; x++ )
      for( y=-c; y<=c; y++ ){
        hxy = map[ l + x + y*W ];
        if( hxy >= 0 )
          do{
            k_random = rand()%N;
            sk += sqrt( SQ(pk.vx-part[hxy].vx) + SQ(pk.vy-part[hxy].vy) );
            sk_random += sqrt( SQ(pk.vx-part[k_random].vx) + SQ(pk.vy-part[k_random].vy) );
            hxy = part[hxy].next;
          }
          while( hxy != LAST_PART );
      }
    s += sk;
    s_random += sk_random;
  }
  return log(s_random/s); // log=ln
}

void print_stats_fluid( int tf ){
  int ms=tf%1000;
  int h=tf/(60*60*1000);
  int mn=(tf/(60*1000))%60;
  int s=(tf/1000)%60;
  int i;
  uint64_t total_count = 0;
  printf("\nSTATISTIQUES fluide.c ======================\n");
  printf("\nNombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", (float)N/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf("Nombre de particules trait�es par thread :\n" );
  for( i=0; i<THREAD_NR; i++ ){
    printf( "* Thread no. %d : %"PRIu64"\n", i, count_per_thread[i] );
    total_count += count_per_thread[i];
  }
  printf( "Total : %"PRIu64"\n", total_count );
  printf("Dur�e r�elle : %i h %i mn %i s %i ms\n",h,mn,s,ms);
  printf("Particules par ms : %f\n", (double)total_count / tf );
  printf("Maximum des vmax : %i\n", vmaxmax);
  printf("Nombre moyen de chocs par iteration : %f\n", (float)nchocs / (total_count/N) );
  printf("Proportion de particules ralenties : %f \%\n",
         (double)slowed / total_count * 100.0 );
  printf("Nombre de mutex pris : %i\n", mutex_locked_nr );
  printf("Nombre d'arr�t � un mutex lock : %i\n", mutex_wait_nr );
  printf("\nDERNIERE ITERATION :\n");
  uint dens_max = 0;
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    dens_max = MAX( dens_max, densite[i] );
  printf("Densit� max : %i\n", dens_max );
  uint l, p, k, max_stack = 0;
  int  nextp;
  uint greater_s = 0;
  uint wrong_stack_nr = 0;
  uint loop_nr = 0;
  uint s_nr = 0;
  for( i=0; i<W*H; i++ ){
    if( map[i] >= 0){
      s_nr++;
      k = 1;
      p = map[i];
      l = (part[p].y>>SUBD_EXP)*W + (part[p].x>>SUBD_EXP);
      nextp = map[i];
      while( part[nextp].next != LAST_PART ){
        if( k == 1 )
          greater_s++;
        nextp = part[nextp].next;
        k++;
        if( k > 90 ){
          printf( " %i ", nextp );
          if ( k > 100 ){
            loop_nr++;
            printf( "\n\n" );
            break;
          }
        }
        if ( (part[nextp].y>>SUBD_EXP)*W + (part[nextp].x>>SUBD_EXP) != l ){
          wrong_stack_nr++;
          break;
        }
      }
    }
    max_stack = MAX( max_stack, k );
  }
  printf( "Emp�lement maximum : %i\n", max_stack );
  printf( "stacks > 1 : %i\n", greater_s );
  printf( "Proportion de p�les > 1 : %f \%\n", 100.0 * (double)greater_s / s_nr );
  if( wrong_stack_nr > 0 || loop_nr > 0 ){
    printf( "\nWRONG STACKS NR : %i !!\n", wrong_stack_nr );
    printf( "LOOP NR : %i !!\n\n", loop_nr );
  }
  int vxmean = 0;
  int vymean = 0;
  for( i=0; i<N; i++ ) vxmean += ABS( part[i].vx );
  for( i=0; i<N; i++ ) vymean += ABS( part[i].vy );
  printf("Vitesse x moyenne : %i\n", vxmean/N);
  printf("Vitesse y moyenne : %i\n", vymean/N);
  printf("vmax = %i\n", vmax );
  uint64_t v9_mean = 0;
  
  for( i=0; i<THREAD_NR; i++ ){
    printf("Thread %i : v9 = %f\n", i, (double)v9_strip[i] / (1<<16) );
    v9_mean += (uint64_t)v9_strip[i] * ( bline[ line_sep[i+1] ] - bline[ line_sep[i] ] );
  }
  v9_mean /= N;
   printf("v9 moyen : %f\n", (double)v9_mean / (1<<16) );
  
  uint count = 0;
  for( i=0; i<N; i++ )
    if( (MAX(ABS( part[i].vx ), ABS( part[i].vy ))<<16) <= v9_mean )
      count++;
  printf("Proportion de particules de vitesse < v9 : %f \%\n", (double)count / N * 100.0 );

  printf("R�gularit� du champs des vitesses : %f\n", v_smoothness(1) );
  for ( i=0; i<THREAD_NR; i++ ){
    printf( "thread %i - line_sep: %i    lenght: %i\n", i, line_sep[i], bline[line_sep[i+1]]-bline[line_sep[i]] );
   
  }

#ifdef V_FIELD
  int dt,it,vt=0;
  for( i=0; i<(H>>DT)*(W>>DT); i++ )
    if( (SQ(v_field[i].x)+SQ(v_field[i].y)) > vt ){
      vt = SQ(v_field[i].x)+SQ(v_field[i].y); 
      it = i;
    }
  dt = densite[it];
  vt = SQ(v_field[it].x / dt)+SQ(v_field[it].y / dt);
  printf("vfield_x = %i\n",v_field[it].x);
  printf("vfield_y = %i\n",v_field[it].y);
  printf("s SQ = %i\n",vt);
  printf("dens = %i\n",dt);
  printf("sqrt %f\n", sqrt(vt));
#endif // V_FIELD

  /*
  int ii = 0;
  int bug = 0;
  int l;
  for( l=0; l<W*H; l++){
    if(map[l] >= 0){
      h = map[l];
      do{
        h = part[h].next;
        ii++;
        if( ii>N ){
          bug++;
          break;
        }
      }
      while( h != LAST_PART );
    }
  }
  printf( "bad lists : %i\n", bug );
  */
}
#endif
