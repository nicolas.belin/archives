#ifndef FLUID_H
#define FLUID_H

// *************** FONCTIONS EXPORTEES ***********
extern void init_fluid();
extern void print_stats_fluid();
extern struct particule dM( struct particule );

// **************** CONSTANTES *******************

#define LAST_PART -1
#define FRAME_V -3
#define FRAME_H -4
#define TRANSLATE_PART -5
#define FORBIDDEN -6


// *********** VARIABLES EXPORTEES ******************
extern struct vector *v_field; // le champs des vitesses
extern uint *densite; // le champs des densit�s
extern int *map;   // carte des pixels
extern int vmax;   // vitesse maximum (pour la norme max)

#endif // FLUID_H
