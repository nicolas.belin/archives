#ifndef MAIN_H
#define MAIN_H

// ********** variables exportées :
extern volatile int loop;
extern int *map;
extern struct particule *part;
extern int force_on; // champs de force activé ou non

// ********** macros :
#define FRAME_V -3
#define FRAME_H -4
#define TRANSLATE_PART -5
#define FORBIDDEN -6
#define LINEAR_POS(A)  ( ( (A)->x >> SUBD_EXP ) + ( (A)->y >> SUBD_EXP ) * W )

// ********** types :
struct particule{
  int x;
  int y;
  int vx;
  int vy;
  int next;
};

#endif // MAIN_H
