#ifndef EXEC_FLOW_H 
#define EXEC_FLOW_H

// *************** FONCTIONS EXPORTEES ***********
extern void init_exec_flow();
extern void start_fluid();
extern void end_fluid();
extern int move_fluid( void* );
extern void print_stats_exec(int);
extern void print_stats_min(int);

// **************** CONSTANTES *******************
#define THREAD_NR 4
#define VMAX_MAX ( 1 << ( sizeof(int)*4 - 1 ) - 1 )
#define VMAX_MIN 1000
#define V9_INIT (1000 << 16) 
#define LAST_PART -1

// gcc build-in functions (atomic + full barrier) :
#define ATOMIC_FETCH_AND_OR __sync_fetch_and_or
#define ATOMIC_FETCH_AND_ADD __sync_fetch_and_add
#define ATOMIC_FETCH_AND_SUB __sync_fetch_and_sub

// semaphore
#define INC_SEM(A) ( ATOMIC_FETCH_AND_ADD( (A), 1 ) )
#define DEC_SEM(A) ( ATOMIC_FETCH_AND_SUB( (A), 1 ) )

// le bit de gauche sert de lock
#define GET_LOCK(A) ( ATOMIC_FETCH_AND_OR( (A), ( 1 << ( 8*sizeof(*(A))-1 )) ) \
                      & ( 1 << ( 8*sizeof(*(A))-1 )) )
#define SPIN_LOCK(A) while ( GET_LOCK(*(A)) )
#define SET_LOCK_BIT(A) ( (A) | ( 1 << ( 8*sizeof(A)-1 )) )
#define UNSET_LOCK_BIT(A) ( (A) & ~( 1 << ( 8*sizeof(A)-1 )) )
#define RMV_LOCK_BIT(A) ( ( (A) << 1 ) >> 1 )

// *********** VARIABLES EXPORTEES ******************
extern int vmax;   // vitesse maximum (pour la norme max)
//extern uint v9; // 9e d�cile des vitesses
extern uint k_sep[THREAD_NR+1]; // lignes de separtion des threads
extern uint64_t total_count;

#endif // EXEC_FLOW_H
