#ifndef DEFINES_H
#define DEFINES_H

typedef unsigned long ulong;
typedef unsigned int uint; 

#define PI 3.14159265359

#define MAX(A,B) ( (A) > (B) ? (A) : (B) )
#define MIN(A,B) ( (A) < (B) ? (A) : (B) )
#define ABS(X) ( (X)>=0 ? (X) : -(X) )
#define SQ(X) ( (X)*(X) )
#define MOD(X,Y) ( (X)%(Y) >= 0 ? (X)%(Y) : ((X)%(Y) + (Y)))

#endif // DEFINES_H
