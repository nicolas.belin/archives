#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <GL/gl.h>	// API GPU
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "fluide-local.h"
#include "fluide.h"

#define OPENGL_INFO 1 // donne des informations sur l'implementation opengl
#define PI 3.14159265359

#define IM_SEC 40       // nombre max d'images par seconde 

#define max(A,B) ((A) > (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))
#define sq(A) ( (A)*(A) )

static Uint32 *video;          // pointeur sur la memoire video

static int *palette1; // palette de couleurs
static int *palette2; // palette circulaire
static void Make_palette();
static void init_GL();
static void display();
static void makevid1();
static void makevid2();
static void makevid3();
static void makevid4();
static void makevid5();
static void makevid6();
static void MemError();
// ********* main() ******************************
int main(){
  // ******** initialisation video SDL et OpenGL
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
  SDL_Surface *screen;	// pointeur sur la structure 'surface'
  screen = SDL_SetVideoMode(W ,H ,0, SDL_OPENGL);
  SDL_Event event;
  if ( !screen ){
    printf("Impossible d'obtenir la r�solution souhait�e :%s\n",SDL_GetError());
    exit(1);
  }
  video = (Uint32*)screen->pixels;
  init_GL();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho( 0, W*SUBD, 0, H*SUBD, -1, 1 );
 // ********* allocations memoire
 palette1 = malloc( 0x800*sizeof(int) );
 palette2 = malloc( 0x800*sizeof(int) );
 if( !(palette1 && palette2) ) MemError();

 // ********* initialisations
 Make_palette();  // fabrication d'une palette de couleurs
 int vid_mode = 0; // mode de visualisation
 init_fluide(); // initialisations diverses; utilise la fonction init_particules

 #ifdef STATS
 Uint32 t0,t1,t3;	          // temps
 unsigned int frame = 0;    // compte les images affichees
 t0 = SDL_GetTicks();
 #endif

 Uint32 t2 = SDL_GetTicks();
 int i;
 int loop = 1;
 int *ptemp;

 glEnableClientState( GL_VERTEX_ARRAY );
 glVertexPointer( 2, GL_INT, sizeof(struct particule), part );
 const GLfloat point_size = 5.0;
 const GLfloat colorR = 1.0 / sq( point_size );
 glPointSize( point_size );
 glEnable(GL_BLEND);
 glBlendFunc( GL_ONE, GL_ONE );
 glClearAccum( 0.0, 0.0, 0.0, 0.0 ); 
 // glEnable(GL_POINT_SMOOTH);
 //********** boucle principale
 while( loop /* && (SDL_GetTicks()-t0<60000)*/ ){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE) loop = 0;
       if(event.key.keysym.sym==SDLK_f) force_on = 1 - force_on; 
       if(event.key.keysym.sym==SDLK_SPACE){
         if ( (++vid_mode) > 5 ) vid_mode = 0;
         if ( vid_mode == 2 );
       }
       #ifdef VISC
       if(event.key.keysym.sym==SDLK_v) visc_on = 1 - visc_on; 
       #endif
       #ifdef DENS
       if(event.key.keysym.sym==SDLK_e) expan_on = 1 - expan_on; 
       #endif
       break;
     }
   }
   if( SDL_GetTicks()-t2 > 1000/IM_SEC ){
     SDL_GL_SwapBuffers();
     glClear(GL_COLOR_BUFFER_BIT|GL_ACCUM_BUFFER_BIT);
     glDrawBuffer(GL_BACK);
     glColor3f( colorR, colorR, colorR );
     glDrawArrays( GL_POINTS, 0, N ); 
     glReadBuffer( GL_BACK );
     glAccum( GL_ACCUM, 4.0 );
     glClear(GL_COLOR_BUFFER_BIT);
     glAccum( GL_RETURN, 1.0 );
     /*
     glBegin(GL_POINTS);
     for(i=0; i<N; i++)
       glVertex2i( part[i].x, part[i].y );
     glColor3f( 1.0, 0.0, 0.0 );
     glVertex2i( part[nvmax].x, part[nvmax].y );
     glEnd();
     */
     /*
     switch( vid_mode ){
     case 0 : makevid1();
       break;
     case 1 : makevid2();
       break;
     case 2 : makevid3();
       break;
     case 3 : makevid4();
       break;
     case 4 : makevid5();
       break;
     case 5 : makevid6();
       break;
     }
     */
     //     glFlush();
     t2 = SDL_GetTicks();
   }
   move();
 }
 #ifdef OPENGL_INFO
 GLint temp;
 printf("\nInfo sur l'impl�mentation Opengl :\n");
 printf("sizeof(GLint) = %i bytes\n", sizeof( GLint ));
 printf("sizeof(GLfloat) = %i bytes\n", sizeof( GLfloat ));
 printf("sizeof(GLdouble) = %i bytes\n", sizeof( GLdouble ));
 glGetIntegerv( GL_RED_BITS, &temp );
 printf("GL_RED_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_GREEN_BITS, &temp );
 printf("GL_GREEN_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_BLUE_BITS, &temp );
 printf("GL_BLUE_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_ALPHA_BITS, &temp );
 printf("GL_ALPHA_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_ACCUM_RED_BITS, &temp );
 printf("GL_ACCUM_RED_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_ACCUM_GREEN_BITS, &temp );
 printf("GL_ACCUM_GREEN_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_ACCUM_BLUE_BITS, &temp );
 printf("GL_ACCUM_BLUE_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_ACCUM_ALPHA_BITS, &temp );
 printf("GL_ACCUM_ALPHA_BITS = %i bits\n", (int)temp );
 glGetIntegerv( GL_AUX_BUFFERS, &temp );
 printf("GL_AUX_BUFFERS = %i\n", (int)temp );
 #endif // OPENGL_INFO
 print_stats( SDL_GetTicks() - t0 );
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************

void init_particules(){ // initialisation des positions des positions
  int i,x,y,l;            // et vitesses initiales des particules
  int X = W/2; // largeur du rectangle
  int Y = H/8; // decalage par rapport au bas
  if( X*(H-Y) < N ){
    printf("Probl�me pour le placement initial des points.");
    exit(1);
  }
  for(i=0; i<N; i++){ // positions initiales des particules
    x = ( (W-X)/2 + i % X ) * SUBD + SUBD/2;
    y = (H - Y - i / X ) * SUBD + SUBD/2;
    l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
    map[ l ] = i;
  }

  // bordures de l'�cran
  for( i=0; i<W; i++ ){ //barriere horizontale 
    map[ i ] = -2;
    map[ W*(H-1) + i ] =-2;
  }
  for( i=0; i<H; i++ ){ // barriere verticale
    map[ W*i ] = -3;
    map[ W-1 + W*i ] = -3;
  }
  map[ 0 ] = -4;               // coins
  map[ W-1 ] = -4;
  map[ W*H - 1 ] = -4;
  map[ W*(H-1) ] = -4;
}

void champs_force( int x, int y, int *fx, int *fy){
  //*fy += 50000; // gravite
#define L_CHAMPS 200 // largueur du champs
#define H_CHAMPS 100 // hauteur du champs
#define I_CHAMPS -15000 // intensit� du champs
  if( ( x >= (W/5)*SUBD ) && ( x <= (W/5+L_CHAMPS)*SUBD ) && (y >= H/4*SUBD) && (y <= (3*H/4+H_CHAMPS)*SUBD ) )
    *fy += I_CHAMPS;
}

void champs2( int x, int y, int *fx, int *fy){
  static int theta_old,co,si,fxs,fys;
  int f = 150000;   // intensite du champs
  int exp = 14;     // x et y doivent etre <= 180223
  int R = 100*SUBD; // longueur
  int l = 20*SUBD;  // largeur
  R <<= exp;
  l <<= exp;

  if( iterations == 0 ) theta_old = -1;
  int theta = (iterations/4) % 360; // angle en degres
  if( theta != theta_old ){
    co = (int)( cos( theta*PI/180 )*( 1<<exp ) );
    si = (int)( sin( theta*PI/180 )*( 1<<exp ) );
    fxs = (int)( -sin( theta*PI/180 ) * f );
    fys = (int)( cos( theta*PI/180 ) * f );
    theta_old = theta;
  }
  x -= SUBD*W/2; // centre de rotation
  y -= SUBD*H/2;
  int p1 = -x*si + y*co;
  if( ( p1 <= l) && ( p1 >= -l ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= 0 ) ){
      *fx += fxs;
      *fy += fys;
    }
  }
}

void mur5(){}

void mur6(){}

static void init_GL(){
  glShadeModel(GL_FLAT); // facon de colorer les objets de bases
  glClearColor(0,0,0,0); // on efface avec du noir
  glDisable(GL_DEPTH_TEST); // pas de test de profondeur
}

static void display(){
  glClear(GL_COLOR_BUFFER_BIT);
  glDrawBuffer(GL_BACK);

  glFlush();
  SDL_GL_SwapBuffers();
}

static void makevid1(){
  int i;
  for(i=0; i<N; i++)
    video[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = 0xffffff;
  video[ ( part[nvmax].x>>SUBD_EXP ) + ( part[nvmax].y>>SUBD_EXP ) * W ] = 0xff0000;
}

static void makevid2(){ // la couleur depend de la direction du vecteur vitesse.
  int i,x,y;
  double theta;
  for(i=0; i<N; i++){
    x = part[i].vx;
    y = part[i].vy;
    if( (x==0) && (y==0) ) continue; // pas affich�e si immobile
    if( x == 0 ){
      if( y > 0 ) theta = PI/2;
      else theta = -PI/2;
    }
    else{
      theta = atan( ((double)y)/((double)x) );
      if( x < 0 ) theta += PI;
    }
    theta += PI/2; // maintenant, 0 <= theta <= 2*PI
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ]
      = palette2[ (int)(theta*2047/(2*PI)) ];
  }
}

static void makevid3(){ // parcours d'une seule particule : rapide
  video[ part[N/2].x/SUBD + (part[N/2].y/SUBD) * W ] = 0x00ffff;
  video[ part[2*(N/3)].x/SUBD + (part[2*(N/3)].y/SUBD) * W ] = 0xff00ff;
  video[ part[N/3].x/SUBD + (part[N/3].y/SUBD) * W ] = 0xffff00;
}

static void makevid4(){ // couleurs fonctions de la vitesse
  unsigned int i,ci;
  int vxt,vyt;
  for(i=0; i<N; i++){
    vxt = part[i].vx;
    vyt = part[i].vy;
    ci = max( abs(vxt), abs(vyt) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ] = ci;
  }
}

static void makevid5(){
  #ifdef VISC
  unsigned int l,ci;
  for(l=0; l<W*H; l++){
    ci = max( abs( flux_x[l] ), abs( flux_y[l] ) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ l ] = ci;
  }
  #endif
}

static void makevid6(){
#ifdef DENS
  unsigned int l;
  if( (iterations-1) % 5 == 0 ){
    for(l=0; l<W*H; l++)
      video[ l ] =  palette1[ p3[l] & 0x7ff ];
  }
#endif
}

static void MemError(){
  printf("Probleme de zero !\n");
  exit(1);
}

static void Make_palette(){
  int *palette;
  if( !( palette = malloc( 0x6fa*sizeof(int)))) MemError();
  int i;
  int c = 0x0;
  for(i = 0; i<0x100 ; i++)
    palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c = 0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){
    c -= 0x1;
    palette[i] = c;
  }
  c = 0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){
    c += 0x10000;
    palette[i] = c;
  }
  c = 0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){
    c -= 0x100;
    palette[i] = c;
  }
  c = 0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){
    c += 0x1;
    palette[i] = c;
  }
  c = 0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c=0xffffff;
  for( i=0; i < 0x800; i++)
    palette1[i] = palette[(0x6f9*i)/0x7ff];

    // palette circulaire :
  c = 0xffffff;
  for(i = 0; i<0x100 ; i++){
    c-=0x10100;
    palette[i]=c;
  }
  for(i = 0 ; i < 0x800 ; i++)
    palette2[i] = palette[(0x6f9*i)/0x7ff];
    
  free(palette);
}
