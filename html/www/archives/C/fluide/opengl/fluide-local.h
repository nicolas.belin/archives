// paramètres de compilations locaux

#define N 40000    // nombre de particules
#define PR 200000  // force repulsive (statique) entre particules
//#define DENS 500   // densité et compressibilité
//#define VISC 20    // viscosité
#define SUBD_EXP 5 // SUBD = 2 ^ SUBD_EXP
#define SUBD 32    // nombre de subdivisions d'un pixel
#define W 500	// largueur (width)
#define H 400	// hauteur (height)
// (pour une répartition initiale symétrique, prendre N divisible par W)
#define STATS  1  // collecte de stats ou non ?

