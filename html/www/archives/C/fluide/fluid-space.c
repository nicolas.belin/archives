#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
//#define NDEBUG  // pas de controle avec assert() si defini
#include <assert.h>
#include <SDL/SDL.h>
#include "defines.h"
#include "fluid.h"
#include "context.h"
#include "local_consts.h"

// gcc build-in functions :
#define ATOMIC_FETCH_AND_OR __sync_fetch_and_or

// le 32e bit sert de lock
#define GET_LOCK(A) ( ATOMIC_FETCH_AND_OR( (A), ( 1 << ( 8*sizeof(int)-1 )) )\
                      & ( 1 << ( 8*sizeof(int)-1 )) )
#define LOCKED ( 1 << ( 8*sizeof(int)-1 ))
#define SPIN_LOCK(A) while ( GET_LOCK(A) == LOCKED )
#define SET_LOCK_BIT(A) ( (A) | ( 1 << ( 8*sizeof(int)-1 )) )
#define UNSET_LOCK_BIT(A) ( *(A) &= ~( 1 << ( 8*sizeof(int)-1 )) )
#define RMV_LOCK_BIT(A) ( ( (A) << 1 ) >> 1 )

extern int loop;
SDL_Thread *thread_struct_ptr[THREAD_NR];
uint *densite; // champs de densit�s
int *map;  // carte des pixels
int vmax;  // vitesse maximum (pour la norme max)
int force_on = 0; // champs de force activ� ou non
uint line_sep[THREAD_NR+1];
static uint thread_id[THREAD_NR];
struct particule *part;
static uint *part_list;  // liste des particules, par bandes
static uint v9_thread[THREAD_NR]; // le 97e centile des vitesses
static uint bline[H+1];
static int *alea; // liste de nombres aleatoires
static const int near[] = { -W-1, -W, -W+1, -1, 0, 1, W-1, W, W+1 };
static uint64_t count_per_thread[THREAD_NR];
static uint64_t previous_count[THREAD_NR];
static long d_count[THREAD_NR];

#ifdef V_FIELD
struct vector *v_field;
#endif // V_FIELD

#ifdef STATS
static uint vmaxmax = 0; // le max des vmax
static uint slowed = 0; // nombre de particules ralenties
static uint nchocs = 0; // nombre de chocs
#endif

static void check_malloc(void *malloc_p){
  if( malloc_p == NULL ){
    fprintf(stderr, "\nProbl�me de m�moire !\n");
    exit(1);
  }
}

static inline void print_dens( uint l, int h ){
  assert( l-(W>>DT)-1 >= 0 &&  l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  densite[ l ] += h;
  densite[ l + 1 ] += h;
  densite[ l - 1 ] += h;
  densite[ l + (W>>DT) ] += h;
  densite[ l - (W>>DT) ] += h;
  densite[ l + (W>>DT) + 1 ] += h;
  densite[ l - (W>>DT) + 1 ] += h;
  densite[ l + (W>>DT) - 1 ] += h;
  densite[ l - (W>>DT) - 1 ] += h;
}

#ifdef V_FIELD
static inline void print_v( uint l, int vx, int vy ){
  assert( l-(W>>DT)-1 >= 0 && l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  v_field[ l ].x += vx;
  v_field[ l ].y += vy;
  v_field[ l + 1 ].x += vx;
  v_field[ l + 1 ].y += vy;
  v_field[ l - 1 ].x += vx;
  v_field[ l - 1 ].y += vy;
  v_field[ l + (W>>DT) ].x += vx;
  v_field[ l + (W>>DT) ].y += vy;
  v_field[ l - (W>>DT) ].x += vx;
  v_field[ l - (W>>DT) ].y += vy;
  v_field[ l + (W>>DT) + 1 ].x += vx;
  v_field[ l + (W>>DT) + 1 ].y += vy;
  v_field[ l - (W>>DT) + 1 ].x += vx;
  v_field[ l - (W>>DT) + 1 ].y += vy;
  v_field[ l + (W>>DT) - 1 ].x += vx;
  v_field[ l + (W>>DT) - 1 ].y += vy;
  v_field[ l - (W>>DT) - 1 ].x += vx;
  v_field[ l - (W>>DT) - 1 ].y += vy;
  
}
#endif // V_FIELD

static inline void swap_uint( uint *a, uint *b ){
  uint temp;
  temp = *a;
  *a = *b;
  *b = temp;
}

void init_fluid(){
  int i,j;
  for( i=0; i<THREAD_NR; i++ )
    thread_id[i] = i;

  // carte des pixels : map
  map = malloc( W*H*sizeof(int) );
  check_malloc(map);
  for(i=0; i<W*H; i++)
    map[i] = LAST_PART;

  // champs des densit�s : densite
  densite = malloc( (W>>DT)*(H>>DT)*sizeof(uint) );
  check_malloc(densite);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    densite[i] = 0;

  // table de nombres al�atoires : alea
  alea = malloc( N*10*sizeof(int) );
  check_malloc(alea);
  srand( time(NULL));               // liste de nombres al�atoires
  for(i=0; i<10*N; i++)
    alea[i] = ( (double)rand() / RAND_MAX ) * 3000;

#ifdef V_FIELD
  // champs des vitesses : v_field
  v_field = malloc( (W>>DT) * (H>>DT) * sizeof(struct vector) );
  check_malloc(v_field);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    v_field[i].x = v_field[i].y = 0;
#endif // V_FIELD

  // les particules : part
  part = malloc( N * sizeof(struct particule) );
  check_malloc(part);
  init_particules(); // definie dans le fichier context-XXX.c

  // liste des particules, class�es par lignes
  uint sb;
  part_list = malloc( N * sizeof(uint) );
  check_malloc( part_list );
  for( i=0; i<N; i++ )
    part_list[i] = i;
  for( i=0, sb=0; i<H; i++ ){
    bline[i] = sb;
    for( j=sb; j<N; j++ ){
      if( part[ part_list[j] ].y >> SUBD_EXP == i ){
        swap_uint( part_list + sb, part_list + j );
        sb++;
      }
    }
  }
  bline[H] = N;

  for( i=0, j=0; i<THREAD_NR; i++ ){
    line_sep[i] = j;
    while ( bline[++j] < (i+1)*N/THREAD_NR );
  }
  line_sep[THREAD_NR] = H;

  for(i=0; i<N; i++) // mettre les numeros des particules sur la carte
    map[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = i;

  for(i=0; i<N; i++)
    print_dens( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT), 1 );

#ifdef V_FIELD
  for(i=0; i<N; i++)
    print_v( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT),
             part[i].vx, part[i].vy );
#endif // V_FIELD

  for ( i=0; i<THREAD_NR; i++ )
    v9_thread[i] = V9_INIT;

  vmax = ( ( CVMAX_NUM * V9_INIT ) / CVMAX_DEN ) >> 16;
  if( vmax > VMAX_MAX )
    vmax = VMAX_MAX;
  if( vmax < VMAX_MIN )
    vmax = VMAX_MIN;

  for(i=0; i<W*H; i++)
    UNSET_LOCK_BIT( map + i ); // aucun LOCK

}

void start_fluid(){ // mise en place des threads g�rant les mouvements des particules
  int i;
  for( i=0; i<THREAD_NR; i++ ){
    thread_struct_ptr[i] = SDL_CreateThread( move_fluid, thread_id + i );
    if( thread_struct_ptr[i] == NULL ){
      fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
      exit(1);
    }
  }
}

void end_fluid(){ // On attend que toutes les threads soient �teintes
  int i;
  for( i=0; i<THREAD_NR; i++ )
    SDL_WaitThread( thread_struct_ptr[i], NULL );
}

#ifdef CHOCS
static void choc( int a, int  b, struct particule *p1, struct particule *p2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int vx1 = p1->vx;
  int abvx1 = ab * vx1; // evite de faire ces produits deux fois.
  int vy1 = p1->vy;
  int abvy1 = ab * vy1;
  int vx2 = p2->vx;
  int abvx2 = ab * vx2;
  int vy2 = p2->vy;
  int abvy2 = ab * vy2;
  int vx1_p = b2*vx1 - abvy1  + a2*vx2 + abvy2;
  int vy1_p = -abvx1 + a2*vy1 + abvx2  + b2*vy2;
  int vx2_p = a2*vx1 + abvy1  + b2*vx2 - abvy2;
  int vy2_p = abvx1  + b2*vy1 - abvx2  + a2*vy2;
  p1->vx = vx1_p / S;
  p1->vy = vy1_p / S;
  p2->vx = vx2_p / S;
  p2->vy = vy2_p / S;
#ifdef STATS
  nchocs++;
#endif // STATS
}
#endif // CHOCS

static inline void checkv( struct particule *p ){
  int vx = p->vx;
  int vy = p->vy;
  if( ABS(vx) > vmax || ABS(vy) > vmax ){
    int av = sqrt( SQ(vx) + SQ(vy) );
    p->vx = (vx*vmax) / av;
    p->vy = (vy*vmax) / av;
#ifdef STATS
    slowed++;
#endif // STATS
  }
}

static inline struct particule dM( struct particule pki ){
  struct particule pk = pki;
  int fx = 0;
  int fy = 0;
  uint l = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
  uint li = l;
  uint li4 = ( pki.x>>(SUBD_EXP+DT) ) + ( pki.y>>(SUBD_EXP+DT) ) * (W>>DT);
  uint l4 = li4;
  int h;
  assert( l-W-1 >= 0 && l+W+1 < H*W );

  // Somme des forces (hors chocs) s'exer�ant sur la particule k
  if( force_on )
    champs_force(pki.x,pki.y,&fx,&fy);  // champs de force
  
  // Agitation particulaire locale (agitation thermique)
  // la temp�rature est li�e � la densit� particulaire
  // cette relation est l'une des �quation d'�tat du fluide :
  int T = T_MIN;// + ( (T_MAX-T_MIN) * dens ) / DENS_MAX;
  
  if( RMV_LOCK_BIT(map[ l+1 ]) >= 0 )
    fx -= T;
  if( RMV_LOCK_BIT(map[ l-1 ]) >= 0 ) 
    fx += T; 
  if( RMV_LOCK_BIT(map[ l+W ]) >= 0 ) 
    fy -= T;
  if( RMV_LOCK_BIT(map[ l-W ]) >= 0 )
    fy += T;
  if( RMV_LOCK_BIT(map[ l+W+1 ]) >= 0 ){
    fx -= T;
    fy -= T;
  }
  if( RMV_LOCK_BIT(map[ l-W+1]) >= 0 ){
    fx -= T;
    fy += T;
  }
  if( RMV_LOCK_BIT(map[ l+W-1 ]) >= 0 ){
    fx += T;
    fy -= T;
  }
  if( RMV_LOCK_BIT(map[ l-W-1 ]) >= 0 ){
    fx += T;
    fy += T;
  }
  
  /*
  int i, dx, dy, ds2, ml;
  for( i=0; i<9; i++ ){
    ml = map[l + near[i] ];
    while( ml != LAST_PART && ml != k ){
      dx = part[ml].x - pk.x;
      dy = part[ml].y - pk.y;
      ds2 = SQ(dx) + SQ(dy);
      if ( ds2 == 0 ){
        ds2 = 1;
      }
      fx += ( (dx<<SUBD_EXP) / ds2) * 2*T;
      fy += ( (dy<<SUBD_EXP) / ds2) * 2*T;
      ml = part[ml].next;
    }
  }
  */

  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  pk.vx *= vmax;
  pk.vx += INV_M*fx;
  pk.vx /= vmax;

  pk.vy *= vmax;
  pk.vy += INV_M*fy;
  pk.vy /= vmax;

  checkv( &pk );
  assert( ABS(pk.vx) <= VMAX_MAX && ABS(pk.vy) <= VMAX_MAX );

  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  pk.x *= vmax;
  pk.x += (pk.vx << SUBD_EXP) + r;
  pk.x /= vmax;

  pk.y *= vmax;
  pk.y += (pk.vy << SUBD_EXP) + r;
  pk.y /= vmax;

  assert( pk.x >= 0 && pk.y >= 0 && (pk.x >> SUBD_EXP) < W && (pk.y >> SUBD_EXP) < H );
  assert( ABS( (pk.x >> SUBD_EXP) - (pki.x >> SUBD_EXP) ) <= 1 ); 
  assert( ABS( (pk.y >> SUBD_EXP) - (pki.y >> SUBD_EXP) ) <= 1 ); 

 translated:
  l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  assert( l-W-1 >= 0 && l+W+1 < H*W );
  if ( l != li ){ // si la particule change de pixel...
    int mapl = RMV_LOCK_BIT( map[l] );
    if( mapl >= 0 || mapl == LAST_PART ){ // si la place est occup�e par une autre particule ou vide...
      
#ifdef CHOCS
      if( mapl != LAST_PART ){
        // chocs avec les particules pr�sente sur le nouveau pixel :
        h = mapl;
#ifdef V_FIELD
        int parthvi_x = 0;
        int parthvi_y = 0;
#endif // V_FIELD
        do{
#ifdef V_FIELD
          parthvi_x -= part[h].vx;
          parthvi_y -= part[h].vy;
#endif // V_FIELD          
          choc( pki.x - part[h].x, pki.y - part[h].y, &pk, &part[h] );
          checkv( &pk );
          checkv( &part[h] );
          h = part[h].next;
#ifdef V_FIELD
          parthvi_x += part[h].vx;
          parthvi_y += part[h].vy;
#endif // V_FIELD
        }
        while( h != LAST_PART );
#ifdef V_FIELD
        print_v( li4, parthvi_x, parthvi_y );
#endif // V_FIELD
      }
#endif // CHOCS

    }
    else{
      switch( mapl ){ // si la place est occup�e par autre chose...
      case FRAME_V : 
        pk.vx = -pk.vx;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case FRAME_H :
        pk.vy = -pk.vy;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case TRANSLATE_PART :
        translation( &pk, &pki );
        goto translated;
      case FORBIDDEN :
        wall1( &pk, &pki );
      }
    }
    l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
    if( li4 != l4 ){ // modifier le champs des densit�s
      print_dens( li4, -1 );
      print_dens( l4, 1 );
    }
  }
 
#ifdef V_FIELD     
  //  if( l4 == li4 )  // mise � jour du champs des vitesses
  //  print_v( li4, pk.vx - pki.vx, pk.vy - pki.vy );
  // else{
  print_v( li4, -pki.vx, -pki.vy );
  print_v( l4, pk.vx, pk.vy );
    // }
#endif // V_FIELD

  return pk;
}

static inline void update_vmax(){
  uint64_t v9_mean = 0;
  int j;
  for( j=0; j<THREAD_NR; j++ )
    v9_mean += (uint64_t)(v9_thread[j])
      * ( RMV_LOCK_BIT(bline[ line_sep[j+1] ]) - RMV_LOCK_BIT(bline[ line_sep[j] ]) );
  v9_mean /= N;
  uint vmax_temp = ( (CVMAX_NUM*v9_mean)/CVMAX_DEN ) >> 16;
  vmax_temp = MAX( vmax_temp, VMAX_MIN );
  vmax_temp = MIN( vmax_temp, VMAX_MAX );
  vmax = vmax_temp;
#ifdef STATS
  vmaxmax = MAX( vmax, vmaxmax );
#endif // STATS
}

static inline int lock_or_not( uint thread, uint list_k ){
  // la particule se situe dans l'interface entre 2 bandes ?
  if( list_k < RMV_LOCK_BIT(bline[line_sep[thread]+2]) ){ // la particule est dans une zone frontiere entre 2 bandes
    return thread;
  }
  else if( list_k >= RMV_LOCK_BIT(bline[line_sep[thread+1]-2]) ){
    return thread + 1;
    }
  return -1;
}

static inline void set_counts(){
  // d_count[] indique la "vitesse" de chaque thread
  // les calculs doivent �tre r�aliser en m�me temps pour que les
  // d_count aient la m�me base de temps
  int i;
  for ( i=0; i<THREAD_NR; i++ ){
    d_count[i] = (int64_t)count_per_thread[i] - (int64_t)previous_count[i];
    previous_count[i] = count_per_thread[i];
  }
}

static inline long discr_count( uint i, uint j ){
  // faut-il modifier la ligne de partage ?
  long Ni = RMV_LOCK_BIT(bline[ line_sep[i+1] ]) - RMV_LOCK_BIT(bline[ line_sep[i] ]);
  long Nj = RMV_LOCK_BIT(bline[ line_sep[j+1] ]) - RMV_LOCK_BIT(bline[ line_sep[j] ]);
  return d_count[j] * Ni - d_count[i] * Nj;
}

static inline void change_line_sep( uint thread, int strip_locked, uint y ){
  /* Changement �ventuel d'une ligne de partage.
   * Dans chaque thread, la proba qu'une part.
   * soit choisie durant un temps dt doit �tre
   * la m�me.
   * Les line_sep[] fluctuent beaucoup, mais
   * c'est une bonne chose.
   */
  int b, a, j;
  if ( strip_locked == thread + 1 ){
    if ( y == line_sep[thread+1] - 1 ){
      j = thread + 1;
      b = discr_count( thread, j );
      if ( b > 0 ){
        a = ( d_count[thread] + d_count[j] ) 
          * ( RMV_LOCK_BIT(bline[ line_sep[thread+1] ]) - RMV_LOCK_BIT(bline[ line_sep[thread+1] - 1 ]) );
        if ( a < 2*b ){
          if ( line_sep[thread+1] > line_sep[thread] + 6 )
            line_sep[thread+1]--;
        }
      }
    }
  }
  else if ( strip_locked == thread ){
    if ( y == line_sep[thread] ){
      j = thread - 1;
      b = discr_count( thread, j );
      if ( b > 0 ){
        a = ( d_count[thread] + d_count[j] ) 
          * ( RMV_LOCK_BIT(bline[ line_sep[thread] + 1 ]) - RMV_LOCK_BIT(bline[ line_sep[thread] ]) );
        if ( a < 2*b ){
          if ( line_sep[thread+1] > line_sep[thread] + 6 )
            line_sep[thread]++;
        }
      }
    }
  }
}

/* 
 * Cette fonction (re-entrante) est lanc�e
 * dans chaque thread
 */
int move_fluid( void *thread_ptr ){ 
  int i, bt, strip_locked;
  uint64_t count = 0;
  uint k, l, li, list_k, thread = *(int*)thread_ptr;    // numero de la thread actuelle
  uint v9 = V9_INIT;
  uint seed = rand();
  uint n = rand_r(&seed);
  count_per_thread[thread] = 0;
  
  struct particule pk;
  struct particule pki; // sauvegarde de l'�tat initial
  while( loop ){
    if( count % (N/THREAD_NR) == 0 ){ 
      bt = rand_r(&seed) % (9*N);
      i = 0;
      v9_thread[thread] = v9;
      if ( thread == 0 ){
        update_vmax();
        set_counts();
      }
    }
    n += alea[ i++ + bt ];
    list_k =  RMV_LOCK_BIT(bline[ line_sep[thread] ])
      + n % ( RMV_LOCK_BIT(bline[ line_sep[thread+1] ]) - RMV_LOCK_BIT(bline[ line_sep[thread] ]));
    strip_locked = lock_or_not( thread, list_k );
    assert( strip_locked != 0 );
    assert( strip_locked < THREAD_NR );
    k = part_list[ list_k ];
    pki = part[k];

    li = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
    if ( GET_LOCK( map + li ) == LOCKED )
      continue;
    int mapl, mapli = RMV_LOCK_BIT( map[li] );
    mapl = mapli;
    pki = part[k];
    if ( li != ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W ){
      map[li] = mapli;
      continue;
    }

    pk = dM( pki );                     // nouvelle position

    //   li = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
    l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
    if ( l != li ){ // la particule change de pixel, et donc de p�le

      // on lock la nouvelle p�le
      if ( GET_LOCK( map + l ) == LOCKED ){
        UNSET_LOCK_BIT( &mapli );
        map[li] = mapli;
        continue;
      }
      
      // p�le que l'on quitte :
      int j = mapli;
      if( j == k )       // si k est t�te de liste
        mapli = pki.next;
      else {             
        int prevp;
        do               // on descend la p�le
          prevp = j;
        while ( (j = part[prevp].next) != k );
        part[prevp].next = pki.next;
      }
      UNSET_LOCK_BIT( &mapli );       // mapli peut �tre n�gative
      map[li] = mapli;                // la p�le de map[li] n'est plus lock�e

      //p�le o� l'on arrive
      pk.next = RMV_LOCK_BIT( map[l] ); 
      mapl = k;

      // changement de ligne :
      uint yi = pki.y >> SUBD_EXP;
      uint y = pk.y >> SUBD_EXP;
      uint blinet;
      if ( y > yi ){
        SPIN_LOCK( bline + y );
        blinet = RMV_LOCK_BIT( bline[y] );
        blinet--;
        swap_uint( part_list + list_k, part_list + blinet );
        bline[y] = blinet;                             // unlock bline[y]
      }
      else if ( y < yi ){
        SPIN_LOCK( bline + yi );
        blinet = RMV_LOCK_BIT( bline[yi] );
        swap_uint( part_list + list_k, part_list + blinet );
        blinet++;
        bline[yi] = blinet;                             // unlock bline[yi]
      }
    
      // modification d'une ligne de partage ?
      change_line_sep( thread, strip_locked, y );
    }

    part[k] = pk;                    // sauvegarde finale

    map[l] = mapl;                   // la p�le de map[l] n'est plus lock�e

    count++;
    #define STEP 10
    if ( count % STEP == 0 ){ // op�rations qu'il est inutile de r�p�ter pour chaque particules
      count_per_thread[thread] += STEP;
      // la suite des v9 converge vers le 97e centile des vitesses
      if( (ABS(pk.vx)<<16) >= v9 || (ABS(pk.vy)<<16) >= v9 )
        v9 += 31*(1<<11);
      else
        v9 -= 1*(1<<11);
    }

  }
  // avant de finir cette thread :
  count_per_thread[thread] = count;
  return 0;
}

void print_stats_min( int tf ){ // donn�es statistiques minimales
  uint64_t total_count = 0;
  int i;
  for( i=0; i<THREAD_NR; i++ )
    total_count += count_per_thread[i];
  printf( "Nombre de particules trait�es : %"PRIu64"\n", total_count );
  printf( "Dur�e r�elle : %i ms\n", tf );
  printf( "Particules par ms : %f\n", (double)total_count / tf );
}

#ifdef STATS
static float v_smoothness( int c ){
  int k,k_random,l,x,y,hxy;
  double sk, sk_random, s = 0;
  double s_random = 0;
  struct particule pk, p_random;
  for( k=0; k<N; k++ ){
    pk = part[k];
    l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
    sk = 0;
    sk_random = 0;
    for( x=-c; x<=c; x++ )
      for( y=-c; y<=c; y++ ){
        hxy = RMV_LOCK_BIT(map[ l + x + y*W ]);
        if( hxy >= 0 )
          do{
            k_random = rand()%N;
            sk += sqrt( SQ(pk.vx-part[hxy].vx) + SQ(pk.vy-part[hxy].vy) );
            sk_random += sqrt( SQ(pk.vx-part[k_random].vx) + SQ(pk.vy-part[k_random].vy) );
            hxy = part[hxy].next;
          }
          while( hxy != LAST_PART );
      }
    s += sk;
    s_random += sk_random;
  }
  return log(s_random/s); // log=ln
}

void print_stats_fluid( int tf ){
  int ms=tf%1000;
  int h=tf/(60*60*1000);
  int mn=(tf/(60*1000))%60;
  int s=(tf/1000)%60;
  int i;
  uint64_t total_count = 0;
  printf("\nfluid.c ======================\n");
  printf("\nStatistiques : MECANIQUE\n");
  printf("Nombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", (float)N/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf("Nombre de particules trait�es par thread :\n" );
  for( i=0; i<THREAD_NR; i++ )
    total_count += count_per_thread[i];
  printf( "Nombre de particules trait�es : %"PRIu64"\n", total_count );
  printf("Dur�e r�elle : %i h %i mn %i s %i ms\n",h,mn,s,ms);
  printf("Particules par ms : %f\n", (double)total_count / tf );
  printf("Maximum des vmax : %i\n", vmaxmax);
  printf("Nombre moyen de chocs par iteration : %f\n", (float)nchocs / (total_count/N) );
  printf("Proportion de particules ralenties : %f \%\n",
         (double)slowed / total_count * 100.0 );
  printf("Etat final :\n");
  uint dens_max = 0;
  uint dens_count = 0;
  for(i=0; i<(W>>DT)*(H>>DT); i++){
    dens_max = MAX( dens_max, densite[i] );
    dens_count += densite[i];
  }
  printf("  Densit� max : %i\n", dens_max );
  if ( dens_count != 9*N )
    printf( "\nDENSITE INCONSISTANTE : total = %u  9*N = %u\n\n", dens_count, 9*N );
  int vxmean = 0;
  int vymean = 0;
  for( i=0; i<N; i++ ) vxmean += ABS( part[i].vx );
  for( i=0; i<N; i++ ) vymean += ABS( part[i].vy );
  printf("  Vitesse x moyenne : %i\n", vxmean/N);
  printf("  Vitesse y moyenne : %i\n", vymean/N);
  printf("  vmax = %i\n", vmax );
  uint64_t v9_mean = 0;
  for( i=0; i<THREAD_NR; i++ )
    v9_mean += (uint64_t)v9_thread[i] * ( bline[ line_sep[i+1] ] - bline[ line_sep[i] ] );
  v9_mean /= N;
  printf("  v9 moyen : %f\n", (double)v9_mean / (1<<16) );
  uint count = 0;
  for( i=0; i<N; i++ )
    if( (MAX(ABS( part[i].vx ), ABS( part[i].vy ))<<16) <= v9_mean )
      count++;
  printf("  Proportion de particules de vitesse < v9 moyen : %f \%\n", (double)count / N * 100.0 );
  printf("  R�gularit� du champs des vitesses : %f\n", v_smoothness(1) );
#ifdef V_FIELD
  int dt,it,vt=0;
  for( i=0; i<(H>>DT)*(W>>DT); i++ )
    if( (SQ(v_field[i].x)+SQ(v_field[i].y)) > vt ){
      vt = SQ(v_field[i].x)+SQ(v_field[i].y); 
      it = i;
    }
  dt = densite[it];
  vt = SQ(v_field[it].x / dt)+SQ(v_field[it].y / dt);
  printf("vfield_x = %i\n",v_field[it].x);
  printf("vfield_y = %i\n",v_field[it].y);
  printf("s SQ = %i\n",vt);
  printf("dens = %i\n",dt);
  printf("sqrt %f\n", sqrt(vt));
#endif // V_FIELD

#ifdef STATS_EXEC
  printf("\nStatistiques : EXECUTION FLOW\n");
  printf("Nombre de threads : %i\n", THREAD_NR );
  printf("Nombre de particules trait�es par thread :\n");
  for( i=0; i<THREAD_NR; i++ ){
    printf( "* Thread no. %d : %"PRIu64"\n", i, count_per_thread[i] );
  }
  printf("Etat final :\n");
  for ( i=0; i<THREAD_NR; i++ ){
    printf( "  * Thread %i :  ligne: %i   #part. : %i  v97 = %f   proba : %f\n", i,
            line_sep[i],
            bline[ line_sep[i+1] ] - bline[ line_sep[i] ],
            (double)v9_thread[i] / (1<<16),
            (double)d_count[i] / ( bline[ line_sep[i+1] ] - bline[ line_sep[i] ] ) );
  }
#endif // STATS_EXEC

#ifdef STATS_STACKS
  uint l, p, k, max_stack = 0;
  int  nextp;
  uint greater_s = 0;
  uint wrong_stack_nr = 0;
  uint loop_nr = 0;
  uint s_nr = 0;
  for ( i=0; i<W*H; i++ ){
    if ( ( nextp = RMV_LOCK_BIT(map[i]) ) >= 0 ){
      s_nr++;
      k = 1;
      l = (part[nextp].y>>SUBD_EXP)*W + (part[nextp].x>>SUBD_EXP);
      while( part[nextp].next != LAST_PART ){
        if( k == 1 )
          greater_s++;
        nextp = part[nextp].next;
        k++;
        if( k > 90 ){
          printf( " %i ", nextp );
          if ( k > 100 ){
            loop_nr++;
            printf( "\n\n" );
            break;
          }
        }
        if ( (part[nextp].y>>SUBD_EXP)*W + (part[nextp].x>>SUBD_EXP) != l ){
          wrong_stack_nr++;
          break;
        }
      }
    }
    max_stack = MAX( max_stack, k );
  }
  printf( "\nStatistiques : EMPILEMENTS\n" );
  printf( "Emp�lement maximum : %i\n", max_stack );
  printf( "Etat final :\n" );
  printf( "  stacks > 1 : %i\n", greater_s );
  printf( "  Proportion de p�les > 1 : %f \%\n", 100.0 * (double)greater_s / s_nr );
  if( wrong_stack_nr > 0 || loop_nr > 0 ){
    printf( "\n  WRONG STACKS NR : %i !!\n", wrong_stack_nr );
    printf( "  LOOP NR : %i !!\n\n", loop_nr );
  }
#endif // STATS_STACKS
}
#endif
