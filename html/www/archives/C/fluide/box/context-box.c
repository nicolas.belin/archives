#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "defines.h"
#include "main.h"
#include "local_consts.h"
#include "fluid.h"

void init_particules(){ // initialisation des positions des positions
  int i,j,x,y,l;           // et vitesses initiales des particules

  
  const int X = W/2; // largeur du rectangle
  const int Y = H/2; // longueur du rectangle
  const int sqrt_N = floor( sqrt(N) );
  float dl = sqrt( (float)(X*Y*SUBD*SUBD) / N );
  for ( i=0; i<N; i++ ){ // positions initiales des particules
    x = floor( (W-X)*SUBD/2 + (i % sqrt_N ) * dl );
    y = floor( (H-Y)*SUBD/2 + (i / sqrt_N ) * dl );
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
  }

  // bordures de l'�cran
  const int lb = 4;         // largeur de la bordure de l'�cran
  for ( j=0; j<lb; j++ )
    for ( i=0; i<W; i++ ){  // barriere horizontale 
      map[ i + j*W ] = FRAME_H;
      map[ W*(H-j-1) + i ] = FRAME_H;
    }

  for ( j=0; j<lb; j++ )
    for ( i=0; i<H; i++ ){  // barriere verticale
      map[ W*i + j ] = FRAME_V;
      map[ W*(i+1) - j - 1  ] = FRAME_V;
    }
}

void champs_force( int x, int y, int *fx, int *fy ){
  //*fy += 50000; // gravite
  const int l_field = 200;    // largueur du champs
  const int h_field = 100;    // hauteur du champs
  const int i_field = -50000; // intensit� du champs
  if( ( x >= (W/5) * SUBD ) &&
      ( x <= ( W/5 + l_field ) * SUBD ) &&
      (y >= H/4 * SUBD ) &&
      (y <= ( 3*H/4 + h_field ) * SUBD ) )
    *fy += i_field;
}

void champs_force2( int x, int y, int *fx, int *fy ){
  static int co,si,fxs,fys;
  const int f = 150000;   // intensite du champs
  const int exp = 14;     // x et y doivent etre <= 180223
  int R = 100*SUBD; // longueur
  int l = 20*SUBD;  // largeur
  R <<= exp;
  l <<= exp;

  int theta = 45;// (iterations/4) % 360; // angle en degres
  co = (int)( cos( theta*PI/180 )*( 1<<exp ) );
  si = (int)( sin( theta*PI/180 )*( 1<<exp ) );
  fxs = (int)( -sin( theta*PI/180 ) * f );
  fys = (int)( cos( theta*PI/180 ) * f );
  x -= SUBD*W/2; // centre de rotation
  y -= SUBD*H/2;
  int p1 = -x*si + y*co;
  if( ( p1 <= l) && ( p1 >= 0 ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= -R ) ){
      *fx += fxs;
      *fy += fys;
    }
    //   if( ( p2 <= 0 ) && ( p2 >= -R ) ){
    // *fx -= fxs;
    // *fy -= fys;
    //}
  }
  if( ( p1 <= 0) && ( p1 >= -l ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= -R ) ){
      *fx -= fxs;
      *fy -= fys;
    }
    //if( ( p2 <= 0 ) && ( p2 >= -R ) ){
    // *fx += fxs;
    // *fy += fys;
    //}
  }
}

void translation( struct particule *pk, struct particule *pki ){} // types de particules fixes
void wall1( struct particule *pk, struct particule *pki ){}

void print_stats_context(){}
