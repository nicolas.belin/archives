#ifndef LOCAL_CONSTS_H
#define LOCAL_CONSTS_H

#define CHOCS
//#define V_FIELD
//#define STATS
#define STATS_EXEC
#define STATS_STACKS
// ************ CONSTANTES SYMBOLIQUES ****************

#define N 1000000               // nombre de particules
#define INV_M 1               // inverse de la masse d'une particule
#define T_MIN 100000          // force repulsive (statique) entre particules a densit� nulle
#define T_MAX 1600000         // force repulsive (statique) entre particules a densit� maximale
#define DT 2                  // largeur et longueur des taches de densit� = 3*2^DT
#define CVMAX_NUM 4           // vmax est �gale � CVMAX_NUM * v9 / CVMAX_DEN
#define CVMAX_DEN 3
#define DENS_MAX (3*(1<<DT)*3*(1<<DT)) // densit� maximale : ( 3*2^DT )^2
#define ENT 20                // coefficient d'entrainement du flux des vitesses 
#define SUBD_EXP 5            // SUBD = 2 ^ SUBD_EXP
#define SUBD (1<<SUBD_EXP)    // nombre de subdivisions d'un pixel
// (pour une r�partition initiale sym�trique, prendre N divisible par W)
#define W 1000                 // largueur (width)
#define H 1000                 // hauteur (height)

#endif // LOCAL_CONSTS_H
