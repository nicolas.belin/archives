#define IM_SEC 20             // nombre d'images par seconde 

extern void init_video();
extern void start_video();
extern void end_video();
extern void print_stats_video( Uint32 );
extern Uint32 video_mode;
