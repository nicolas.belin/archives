#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include <assert.h>
//#define NDEBUG  // pas de controle avec assert() si defini
#include "defines.h"
#include "local_consts.h"
#include "main.h"
#include "exec_flow.h"
#include "fluid.h"
#include "context.h"

extern volatile int loop;
uint *densite; // champs de densit�s
static const int near[] = { -W-1, -W, -W+1, -1, 0, 1, W-1, W, W+1 };

#ifdef V_FIELD
struct vector *v_field;
#endif // V_FIELD

#ifdef STATS
static uint slowed = 0; // nombre de particules ralenties
static uint nchocs = 0; // nombre de chocs
#endif

static void check_malloc(void *malloc_p){
  if( malloc_p == NULL ){
    fprintf(stderr, "\nProbl�me de m�moire !\n");
    exit(1);
  }
}

static inline void print_dens( uint l, int h ){
  assert( l-(W>>DT)-1 >= 0 &&  l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  densite[ l ] += h;
  densite[ l + 1 ] += h;
  densite[ l - 1 ] += h;
  densite[ l + (W>>DT) ] += h;
  densite[ l - (W>>DT) ] += h;
  densite[ l + (W>>DT) + 1 ] += h;
  densite[ l - (W>>DT) + 1 ] += h;
  densite[ l + (W>>DT) - 1 ] += h;
  densite[ l - (W>>DT) - 1 ] += h;
}

#ifdef V_FIELD
static inline void print_v( uint l, int vx, int vy ){
  assert( l-(W>>DT)-1 >= 0 && l+(W>>DT)+1 < (W>>DT)*(H>>DT) );
  v_field[ l ].x += vx;
  v_field[ l ].y += vy;
  v_field[ l + 1 ].x += vx;
  v_field[ l + 1 ].y += vy;
  v_field[ l - 1 ].x += vx;
  v_field[ l - 1 ].y += vy;
  v_field[ l + (W>>DT) ].x += vx;
  v_field[ l + (W>>DT) ].y += vy;
  v_field[ l - (W>>DT) ].x += vx;
  v_field[ l - (W>>DT) ].y += vy;
  v_field[ l + (W>>DT) + 1 ].x += vx;
  v_field[ l + (W>>DT) + 1 ].y += vy;
  v_field[ l - (W>>DT) + 1 ].x += vx;
  v_field[ l - (W>>DT) + 1 ].y += vy;
  v_field[ l + (W>>DT) - 1 ].x += vx;
  v_field[ l + (W>>DT) - 1 ].y += vy;
  v_field[ l - (W>>DT) - 1 ].x += vx;
  v_field[ l - (W>>DT) - 1 ].y += vy;
  
}
#endif // V_FIELD

void init_fluid(){
  int i,j;

  // champs des densit�s : densite
  densite = malloc( (W>>DT)*(H>>DT)*sizeof(densite[0]) );
  check_malloc(densite);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    densite[i] = 0;

#ifdef V_FIELD
  // champs des vitesses : v_field
  v_field = malloc( (W>>DT) * (H>>DT) * sizeof(struct vector) );
  check_malloc(v_field);
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    v_field[i].x = v_field[i].y = 0;
#endif // V_FIELD

  for(i=0; i<N; i++)
    print_dens( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT), 1 );

#ifdef V_FIELD
  for(i=0; i<N; i++)
    print_v( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT),
             part[i].vx, part[i].vy );
#endif // V_FIELD
}

#ifdef CHOCS
static void choc( int a, int  b, struct particule *p1, struct particule *p2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int vx1 = p1->vx;
  int abvx1 = ab * vx1; // evite de faire ces produits deux fois.
  int vy1 = p1->vy;
  int abvy1 = ab * vy1;
  int vx2 = p2->vx;
  int abvx2 = ab * vx2;
  int vy2 = p2->vy;
  int abvy2 = ab * vy2;
  int vx1_p = b2*vx1 - abvy1  + a2*vx2 + abvy2;
  int vy1_p = -abvx1 + a2*vy1 + abvx2  + b2*vy2;
  int vx2_p = a2*vx1 + abvy1  + b2*vx2 - abvy2;
  int vy2_p = abvx1  + b2*vy1 - abvx2  + a2*vy2;
  p1->vx = vx1_p / S;
  p1->vy = vy1_p / S;
  p2->vx = vx2_p / S;
  p2->vy = vy2_p / S;
#ifdef STATS
  nchocs++;
#endif // STATS
}
#endif // CHOCS

static inline void checkv( struct particule *p ){
  int vx = p->vx;
  int vy = p->vy;
  if( ABS(vx) > vmax || ABS(vy) > vmax ){
    int av = sqrt( SQ(vx) + SQ(vy) );
    p->vx = (vx*vmax) / av;
    p->vy = (vy*vmax) / av;
#ifdef STATS
    slowed++;
#endif // STATS
  }
}

inline struct particule dM( struct particule pki ){
  struct particule pk = pki;
  int fx = 0;
  int fy = 0;
  uint l = ( pki.x>>SUBD_EXP ) + ( pki.y>>SUBD_EXP ) * W;
  uint li = l;
  uint li4 = ( pki.x>>(SUBD_EXP+DT) ) + ( pki.y>>(SUBD_EXP+DT) ) * (W>>DT);
  uint l4 = li4;
  int h;
  assert( l-W-1 >= 0 && l+W+1 < H*W );

  // Somme des forces (hors chocs) s'exer�ant sur la particule k
  if( force_on )
    champs_force(pki.x,pki.y,&fx,&fy);  // champs de force
  
  // Agitation particulaire locale (agitation thermique)
  // la temp�rature est li�e � la densit� particulaire
  // cette relation est l'une des �quation d'�tat du fluide :
  int T = T_MIN;// + ( (T_MAX-T_MIN) * dens ) / DENS_MAX;
  
  if( RMV_LOCK_BIT(map[ l+1 ]) >= 0 )
    fx -= T;
  if( RMV_LOCK_BIT(map[ l-1 ]) >= 0 ) 
    fx += T; 
  if( RMV_LOCK_BIT(map[ l+W ]) >= 0 ) 
    fy -= T;
  if( RMV_LOCK_BIT(map[ l-W ]) >= 0 )
    fy += T;
  if( RMV_LOCK_BIT(map[ l+W+1 ]) >= 0 ){
    fx -= T;
    fy -= T;
  }
  if( RMV_LOCK_BIT(map[ l-W+1]) >= 0 ){
    fx -= T;
    fy += T;
  }
  if( RMV_LOCK_BIT(map[ l+W-1 ]) >= 0 ){
    fx += T;
    fy -= T;
  }
  if( RMV_LOCK_BIT(map[ l-W-1 ]) >= 0 ){
    fx += T;
    fy += T;
  }

  /*
  int i, dx, dy, ds2, ml;
  for( i=0; i<9; i++ ){
    ml = map[l + near[i] ];
    while( ml != LAST_PART && ml != k ){
      dx = part[ml].x - pk.x;
      dy = part[ml].y - pk.y;
      ds2 = SQ(dx) + SQ(dy);
      if ( ds2 == 0 ){
        ds2 = 1;
      }
      fx += ( (dx<<SUBD_EXP) / ds2) * 2*T;
      fy += ( (dy<<SUBD_EXP) / ds2) * 2*T;
      ml = part[ml].next;
    }
  }
  */

  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  int vmax_loc = vmax;
  pk.vx *= vmax_loc;
  pk.vx += INV_M*fx;
  pk.vx /= vmax_loc;

  pk.vy *= vmax_loc;
  pk.vy += INV_M*fy;
  pk.vy /= vmax_loc;

  checkv( &pk );
  assert( ABS(pk.vx) <= VMAX_MAX && ABS(pk.vy) <= VMAX_MAX );

  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax_loc >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  pk.x *= vmax_loc;
  pk.x += (pk.vx << SUBD_EXP) + r;
  pk.x /= vmax_loc;

  pk.y *= vmax_loc;
  pk.y += (pk.vy << SUBD_EXP) + r;
  pk.y /= vmax_loc;

  assert( pk.x >= 0 && pk.y >= 0 && (pk.x >> SUBD_EXP) < W && (pk.y >> SUBD_EXP) < H );
  assert( ABS( (pk.x >> SUBD_EXP) - (pki.x >> SUBD_EXP) ) <= 1 ); 
  assert( ABS( (pk.y >> SUBD_EXP) - (pki.y >> SUBD_EXP) ) <= 1 ); 

  l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  assert( l-W-1 >= 0 && l+W+1 < H*W );
  if ( l != li ){ // si la particule change de pixel...
    int mapl = RMV_LOCK_BIT( map[l] );
    if( mapl >= 0 || mapl == LAST_PART ){ // si la place est occup�e par une autre particule ou vide...
      
#ifdef CHOCS
      if( mapl != LAST_PART ){
        // chocs avec les particules pr�sente sur le nouveau pixel :
        h = mapl;
#ifdef V_FIELD
        int parthvi_x = 0;
        int parthvi_y = 0;
#endif // V_FIELD
        do{
#ifdef V_FIELD
          parthvi_x -= part[h].vx;
          parthvi_y -= part[h].vy;
#endif // V_FIELD          
          choc( pki.x - part[h].x, pki.y - part[h].y, &pk, &part[h] );
          checkv( &pk );
          checkv( &part[h] );
          h = part[h].next;
#ifdef V_FIELD
          parthvi_x += part[h].vx;
          parthvi_y += part[h].vy;
#endif // V_FIELD
        }
        while( h != LAST_PART );
#ifdef V_FIELD
        print_v( li4, parthvi_x, parthvi_y );
#endif // V_FIELD
      }
#endif // CHOCS

    }
    else{
      switch( mapl ){ // si la place est occup�e par autre chose...
      case FRAME_V : 
        pk.vx = -pk.vx;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case FRAME_H :
        pk.vy = -pk.vy;
        pk.x = pki.x;
        pk.y = pki.y;
        break;
      case TRANSLATE_PART :
        translation( &pk );
        break;
      case FORBIDDEN :
        wall1( &pk, &pki );
        break;
      }
    }
    l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
    if( li4 != l4 ){ // modifier le champs des densit�s
      print_dens( li4, -1 );
      print_dens( l4, 1 );
    }
  }
 
#ifdef V_FIELD     
  //  if( l4 == li4 )  // mise � jour du champs des vitesses
  //  print_v( li4, pk.vx - pki.vx, pk.vy - pki.vy );
  // else{
  print_v( li4, -pki.vx, -pki.vy );
  print_v( l4, pk.vx, pk.vy );
    // }
#endif // V_FIELD

  return pk;
}

#ifdef STATS
static float v_smoothness( int c ){
  int k,k_random,l,x,y,hxy;
  double sk, sk_random, s = 0;
  double s_random = 0;
  struct particule pk, p_random;
  for( k=0; k<N; k++ ){
    pk = part[k];
    l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
    sk = 0;
    sk_random = 0;
    for( x=-c; x<=c; x++ )
      for( y=-c; y<=c; y++ ){
        hxy = RMV_LOCK_BIT(map[ l + x + y*W ]);
        if( hxy >= 0 )
          do{
            k_random = rand()%N;
            sk += sqrt( SQ(pk.vx-part[hxy].vx) + SQ(pk.vy-part[hxy].vy) );
            sk_random += sqrt( SQ(pk.vx-part[k_random].vx) + SQ(pk.vy-part[k_random].vy) );
            hxy = part[hxy].next;
          }
          while( hxy != LAST_PART );
      }
    s += sk;
    s_random += sk_random;
  }
  return log(s_random/s); // log=ln
}

void print_stats_fluid(){
  int i;
  printf("\nfluid.c ======================\n");
  printf("\nStatistiques : MECANIQUE\n");
  printf("Nombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", (float)N/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf("Nombre moyen de chocs par iteration : %f\n", (float)nchocs / (total_count/N) );
  printf("Proportion de particules ralenties : %f \%\n",
         (double)slowed / total_count * 100.0 );
  printf("Etat final :\n");
  uint dens_max = 0;
  uint dens_count = 0;
  for(i=0; i<(W>>DT)*(H>>DT); i++){
    dens_max = MAX( dens_max, densite[i] );
    dens_count += densite[i];
  }
  printf("  Densit� max : %i\n", dens_max );
  if ( dens_count != 9*N )
    printf( "\nDENSITE INCONSISTANTE : total = %u  9*N = %u\n\n", dens_count, 9*N );
  int vxmean = 0;
  int vymean = 0;
  for( i=0; i<N; i++ ) vxmean += ABS( part[i].vx );
  for( i=0; i<N; i++ ) vymean += ABS( part[i].vy );
  printf("  Vitesse x moyenne : %i\n", vxmean/N);
  printf("  Vitesse y moyenne : %i\n", vymean/N);
  printf("  vmax = %i\n", vmax );
  printf("  R�gularit� du champs des vitesses : %f\n", v_smoothness(1) );
#ifdef V_FIELD
  int dt,it,vt=0;
  for( i=0; i<(H>>DT)*(W>>DT); i++ )
    if( (SQ(v_field[i].x)+SQ(v_field[i].y)) > vt ){
      vt = SQ(v_field[i].x)+SQ(v_field[i].y); 
      it = i;
    }
  dt = densite[it];
  vt = SQ(v_field[it].x / dt)+SQ(v_field[it].y / dt);
  printf("vfield_x = %i\n",v_field[it].x);
  printf("vfield_y = %i\n",v_field[it].y);
  printf("s SQ = %i\n",vt);
  printf("dens = %i\n",dt);
  printf("sqrt %f\n", sqrt(vt));
#endif // V_FIELD

}
#endif // STATS
