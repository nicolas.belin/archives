#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "context_local.h"
#include "fluide.h"
#include "video.h"

#define PI 3.14159265359

int loop = 1;

// ********* main() ******************************
int main(){
  init_fluid(); // initialisations diverses; utilise la fonction init_particules
  init_video(); // initialisation en particulier de SDL

  // tous les calculs relatifs au mouvements des particules
  // se font dans un fils d'execution s�par� :
  SDL_Thread *fluid_thread;
  fluid_thread = SDL_CreateThread( move_fluid, NULL );
  if( fluid_thread == NULL ){
    fprintf(stderr, "Impossible d'initialiser un thread: %s\n", SDL_GetError());
    exit(1);
  }

#ifdef STATS
  Uint32 t0 = SDL_GetTicks();
#endif

  SDL_Event event;
  while( loop ){
    //    SDL_Delay( SDL_DEFAULT_REPEAT_DELAY );
    SDL_WaitEvent( &event );
    switch(event.type){
    case SDL_MOUSEBUTTONDOWN :
      loop = 0;	
      break;
    case SDL_KEYDOWN:
      switch( event.key.keysym.sym ){
      case SDLK_ESCAPE :
      case SDLK_q :
        loop = 0;
        break;
      case SDLK_f :
        force_on = 1 - force_on;
        break;
      case SDLK_SPACE :
        video_mode++;
        break;
      }
      break;
    }
  }
  
  SDL_WaitThread( fluid_thread, NULL );

#ifdef STATS
 print_stats_video( SDL_GetTicks() - t0 );
 print_stats_fluid( SDL_GetTicks() - t0 );
#endif // STATS
 return 0;
}

// ****************************** fonctions **********************************

void init_particules(){ // initialisation des positions des positions
  int i,j,x,y,l;           // et vitesses initiales des particules
  int X = W/2; // largeur du rectangle
  //int X = 200;
  int Y = H/6; // decalage par rapport au haut
  if( X*(H-Y) < N ){
    fprintf(stderr,"Probl�me pour le placement initial des particules.");
    exit(1);
  }
  for(i=0; i<N; i++){ // positions initiales des particules
    x = ( (W-X)/2 + i % X ) * SUBD + SUBD/2;
    y = ( Y + i / X ) * SUBD + SUBD/2;
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
    part[i].w = 0;
  }

  // bordures de l'�cran
  #define LB 4 // largeur de la bordure de l'�cran

  for( j=0; j<LB; j++ )
    for( i=0; i<W; i++ ){ //barriere horizontale 
      map[ i + j*W ] = -2;
      map[ W*(H-j-1) + i ] =-2;
    }

  for( j=0; j<LB; j++ )
    for( i=0; i<H; i++ ){ // barriere verticale
      map[ W*i + j ] = -3;
      map[ W*(i+1) - j - 1  ] = -3;
    }
}

void champs_force( int x, int y, int *fx, int *fy){
  //*fy += 50000; // gravite
#define L_CHAMPS 200 // largueur du champs
#define H_CHAMPS 100 // hauteur du champs
#define I_CHAMPS -50000 // intensit� du champs
  if( ( x >= (W/5)*SUBD ) && ( x <= (W/5+L_CHAMPS)*SUBD ) && (y >= H/4*SUBD) && (y <= (3*H/4+H_CHAMPS)*SUBD ) )
    *fy += I_CHAMPS;
}

void champs_force2( int x, int y, int *fx, int *fy){
  static int co,si,fxs,fys;
  int f = 150000;   // intensite du champs
  int exp = 14;     // x et y doivent etre <= 180223
  int R = 100*SUBD; // longueur
  int l = 20*SUBD;  // largeur
  R <<= exp;
  l <<= exp;

  int theta = 45;// (iterations/4) % 360; // angle en degres
  co = (int)( cos( theta*PI/180 )*( 1<<exp ) );
  si = (int)( sin( theta*PI/180 )*( 1<<exp ) );
  fxs = (int)( -sin( theta*PI/180 ) * f );
  fys = (int)( cos( theta*PI/180 ) * f );
  x -= SUBD*W/2; // centre de rotation
  y -= SUBD*H/2;
  int p1 = -x*si + y*co;
  if( ( p1 <= l) && ( p1 >= 0 ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= -R ) ){
      *fx += fxs;
      *fy += fys;
    }
    //   if( ( p2 <= 0 ) && ( p2 >= -R ) ){
    // *fx -= fxs;
    // *fy -= fys;
    //}
  }
  if( ( p1 <= 0) && ( p1 >= -l ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= -R ) ){
      *fx -= fxs;
      *fy -= fys;
    }
    //if( ( p2 <= 0 ) && ( p2 >= -R ) ){
    // *fx += fxs;
    // *fy += fys;
    //}
  }
}

void mur5(struct particule *pk, int k){} // types de particules fixes
void mur6(struct particule *pk, int k){}
