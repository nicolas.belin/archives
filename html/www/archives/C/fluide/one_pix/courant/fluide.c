#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "fluide.h"
#include "context_local.h"

#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) < (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))
#define sq(A) ((A)*(A))

// fonctions d�finies dans le programme appelant :
extern void champs_force(int,int,int*,int*);
extern void init_particules(); // initialisations
extern void mur5(struct particule *pk, int k); // types de particules fixes
extern void mur6(struct particule *pk, int k);

extern int loop;

struct particule *part;
struct champs_vitesse *vit;
int *densite; // champs de densit�s
int *map;  // carte des pixels
int vmax;  // vitesse maximum (pour la norme max)
unsigned int v9 = 1000 << 16; // 97e centile des vitesses
int force_on = 0; // champs de force activ� ou non
unsigned int iterations = 0; // compte les iterations
static int *alea; // liste de nombres aleatoires
#ifdef STATS
static int vmaxmax = 0; // le max des vmax
static unsigned int ralenties = 0; // nombre de particules ralenties
static unsigned int nchocs = 0; // nombre de chocs
static double temps_fluide = 0; // temps du fluide
#endif

static void MemErrorF(){
  printf("Probl�me de m�moire !");
  exit(1);
}

static void print_dens( int l, int h ){
  densite[ l ] += h;
  densite[ l + 1 ] += h;
  densite[ l - 1 ] += h;
  densite[ l + (W>>DT) ] += h;
  densite[ l - (W>>DT) ] += h;
  densite[ l + (W>>DT) + 1 ] += h;
  densite[ l - (W>>DT) + 1 ] += h;
  densite[ l + (W>>DT) - 1 ] += h;
  densite[ l - (W>>DT) - 1 ] += h;
}

static void print_v( int l, int vx, int vy ){
  vit[ l ].x += vx;
  vit[ l ].y += vy;
  vit[ l + 1 ].x += vx;
  vit[ l + 1 ].y += vy;
  vit[ l - 1 ].x += vx;
  vit[ l - 1 ].y += vy;
  vit[ l + (W>>DT) ].x += vx;
  vit[ l + (W>>DT) ].y += vy;
  vit[ l - (W>>DT) ].x += vx;
  vit[ l - (W>>DT) ].y += vy;
  vit[ l + (W>>DT) + 1 ].x += vx;
  vit[ l + (W>>DT) + 1 ].y += vy;
  vit[ l - (W>>DT) + 1 ].x += vx;
  vit[ l - (W>>DT) + 1 ].y += vy;
  vit[ l + (W>>DT) - 1 ].x += vx;
  vit[ l + (W>>DT) - 1 ].y += vy;
  vit[ l - (W>>DT) - 1 ].x += vx;
  vit[ l - (W>>DT) - 1 ].y += vy;
}

void init_fluid(){
  int i;
  part = malloc( N*sizeof(struct particule) );
  densite = malloc( (W>>DT)*(H>>DT)*sizeof(int) );
  vit = malloc( (W>>DT)*(H>>DT)*sizeof(struct champs_vitesse) );
  map = malloc( W*H*sizeof(int) );
  alea = malloc( N*10*sizeof(int) );
  if( !(alea && map && part && densite ) ) MemErrorF();
  for(i=0; i<W*H; i++) map[i] = -1; // effacer la carte ( -1 -> vide )
  for(i=0; i<(W>>DT)*(H>>DT); i++) densite[i] = 0;
  struct champs_vitesse v;
  v.x = 0;
  v.y = 0;
  for(i=0; i<(W>>DT)*(H>>DT); i++)
    vit[i] = v;
  srand( time(NULL));               // liste de nombres al�atoires
  for(i=0; i<10*N; i++) alea[i] = rand()%400;

  init_particules(); // d�l�gu� au programme appelant

  for(i=0; i<N; i++) // mettres les numeros des particules sur la carte
    map[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = i;

  for(i=0; i<N; i++)
    print_dens( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT), 1 );

  for(i=0; i<N; i++)
    print_v( ( part[i].x>>(SUBD_EXP+DT) ) + ( part[i].y>>(SUBD_EXP+DT) ) * (W>>DT),
             part[i].vx, part[i].vy );

  vmax = (CVMAX_NUM*v9)/CVMAX_DEN;
  vmax >>= 16;
  if( vmax > VMAX_ABS ) vmax = VMAX_ABS;
}

static void choc( int a, int  b, struct particule *p1, struct particule *p2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int vx1 = p1->vx;
  int abvx1 = ab * vx1; // evite de faire ces produits deux fois.
  int vy1 = p1->vy;
  int abvy1 = ab * vy1;
  int vx2 = p2->vx;
  int abvx2 = ab * vx2;
  int vy2 = p2->vy;
  int abvy2 = ab * vy2;
  int vx1_p = b2*vx1 - abvy1  + a2*vx2 + abvy2;
  int vy1_p = -abvx1 + a2*vy1 + abvx2  + b2*vy2;
  int vx2_p = a2*vx1 + abvy1  + b2*vx2 - abvy2;
  int vy2_p = abvx1  + b2*vy1 - abvx2  + a2*vy2;
  p1->vx = vx1_p / S;
  p1->vy = vy1_p / S;
  p2->vx = vx2_p / S;
  p2->vy = vy2_p / S;
#ifdef STATS
  nchocs++;
#endif
}

static void checkv( struct particule *p ){
  int vx = p->vx;
  int vy = p->vy;
  if( abs(vx) > vmax || abs(vy) > vmax ){
    int av = sqrt( sq(vx) + sq(vy) );
    p->vx = (vx*vmax) / av;
    p->vy = (vy*vmax) / av;
#ifdef STATS
    ralenties++;
#endif // STATS
  }
}

static void dM(int k){
  int fx = 0;
  int fy = 0;
  struct particule pk = part[k];
  struct particule pki = pk; // sauvegarde de l'�tat initial
  int l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  int l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
  int li = l;
  int li4 = l4;

  // Somme des forces (hors chocs) s'exer�ant sur la particule k

  if( force_on ) champs_force(pk.x,pk.y,&fx,&fy);  // champs de force

  
  int dens = densite[ l4 ];
  /*
  int densr = max( densite[ l4 + 1 ], 1 ); // on va diviser par ces densit�s
  int densl = max( densite[ l4 - 1 ], 1 );
  int densu = max( densite[ l4 + (W>>DT) ], 1 );
  int densd = max( densite[ l4 - (W>>DT) ], 1 );
  */
  /*
  int densr = densite[ l4 + 1 ];
  int densl = densite[ l4 - 1 ];
  int densu += densite[ l4 +  (W>>DT) ];
  int dens8 += densite[ l4 -  (W>>DT) ];
  dens8 += densite[ l4 +  (W>>DT) + 1 ];
  dens8 += densite[ l4 +  (W>>DT) - 1 ];
  dens8 += densite[ l4 -  (W>>DT) + 1 ];
  dens8 += densite[ l4 -  (W>>DT) - 1 ];
  */

  // Agitation particulaire locale (agitation thermique)
  // la temp�rature est li�e � la densit� particulaire
  // cette relation est l'une des �quation d'�tat du fluide :
  int T = T_MIN;// + ( (T_MAX-T_MIN) * dens ) / DENS_MAX;
  if( map[ l+1 ] > -1 )
    fx -= T;
  if( map[ l-1] > -1 ) 
    fx += T; 
  if( map[ l+W ] > -1 ) 
    fy -= T;
  if( map[ l-W ] > -1 )
    fy += T;
  if( map[ l+W+1 ] > -1 ){
    fx -= T;
    fy -= T;
  }
  if( map[ l-W+1] > -1 ){
    fx -= T;
    fy += T;
  }
  if( map[ l+W-1 ] > -1 ){
    fx += T;
    fy -= T;
  }
  if( map[ l-W-1 ] > -1 ){
    fx += T;
    fy += T;
  }
  
  
  fx -= ENT*(pk.vx - vit[l4].x / dens); // force d'entrainement du courant local
  fy -= ENT*(pk.vy - vit[l4].y / dens); 

  
  // flux de quantit� de mouvement
  //  int vx2r = ( vit[ l4 + 1 ].x * vit[ l4 + 1 ].x ) / densr;
  //int vx2l = ( vit[ l4 - 1 ].x * vit[ l4 - 1 ].x ) / densl;
  //  int vy2r = ( vit[ l4 + 1 ].y * vit[ l4 + 1 ].y ) / densr;
  //  int vy2l = ( vit[ l4 - 1 ].y * vit[ l4 - 1 ].y ) / densl;
  // int vxyr = ( vit[ l4 + 1 ].x * vit[ l4 + 1 ].y ) / densr;
  //int vxyl = ( vit[ l4 - 1 ].x * vit[ l4 - 1 ].y ) / densl;
  //  int vx2u = ( vit[ l4 + (W>>DT) ].x * vit[ l4 + (W>>DT) ].x ) / densu;
  //  int vx2l = ( vit[ l4 - (W>>DT) ].x * vit[ l4 - (W>>DT) ].x ) / densd;
  //int vy2u = ( vit[ l4 + (W>>DT) ].y * vit[ l4 + (W>>DT) ].y ) / densu;
  //int vy2d = ( vit[ l4 - (W>>DT) ].y * vit[ l4 - (W>>DT) ].y ) / densd;
  //int vxyu = ( vit[ l4 + (W>>DT) ].x * vit[ l4 + (W>>DT) ].y ) / densu;
  //int vxyd = ( vit[ l4 - (W>>DT) ].x * vit[ l4 - (W>>DT) ].y ) / densd;
  //fx -= ( vx2r - vx2l + vxyu - vxyd ) / dens;
  //fy -= ( vxyr - vxyl + vy2u - vy2d ) / dens;

  // gradient de densit� -> gradient de pression
  int gradddiag1 = densite[ l4 + 1 + (W>>DT) ] - densite[ l4 - 1 - (W>>DT) ];
  int gradddiag2 = densite[ l4 + 1 - (W>>DT) ] - densite[ l4 - 1 + (W>>DT) ];
  int graddx = gradddiag1 + gradddiag2;
  int graddy = gradddiag1 - gradddiag2;
  graddx *= 3; // approximation de 1/sqrt(2) : 3/4
  graddy *= 3;
  graddx >>= 2;
  graddy >>= 2;
  graddx += densite[ l4 + 1 ] - densite[ l4 - 1 ];
  graddy += densite[ l4 + (W>>DT) ] - densite[ l4 - (W>>DT) ];
  #define ADP 10000
  #define BPD 1000
  int coeff_pd = ADP + BPD * dens; // �quation d'etat du fluide
  int gradpx = coeff_pd * graddx;  // grad(P) = coeff_pd(d) * grad(d) 
  int gradpy = coeff_pd * graddy;
  fx -= gradpx / dens; // concervation de la quantit� de mouvement : -grad(P)
  fy -= gradpy / dens;
  
  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  pk.vx *= vmax;
  pk.vx += INV_M*fx;
  pk.vx /= vmax;

  pk.vy *= vmax;
  pk.vy += INV_M*fy;
  pk.vy /= vmax;

  /*    
  int rot = vit[ l4 + 1 + (W>>DT) ].x - vit[ l4 - 1 - (W>>DT) ].x;
  rot += -vit[ l4 + 1 + (W>>DT) ].y + vit[ l4 - 1 - (W>>DT) ].y;
  rot += vit[ l4 - 1 + (W>>DT) ].x - vit[ l4 + 1 - (W>>DT) ].x;
  rot += vit[ l4 - 1 + (W>>DT) ].y - vit[ l4 + 1 - (W>>DT) ].y;
  rot *= 3; // approximation de 1/sqrt(2) : 3/4
  rot >>= 2;
  rot += vit[ l4 + (W>>DT) ].x - vit[ l4 - (W>>DT) ].x;
  rot += vit[ l4 - 1].y - vit[ l4 + 1].y;
  rot /= dens8;
  rot -= pk.w;
  pk.w *= vmax;
  pk.w += rot*100;
  pk.w /= vmax;

  float ca = cos( ((float) pk.w) / vmax );
  float sa = sin( ((float) pk.w) / vmax );
  int vx = pk.vx * ca - pk.vy * sa;
  pk.vy = pk.vx * sa + pk.vy * ca;
  pk.vx = vx;
  */

  checkv( &pk );

  // la suite des v9 converge vers le 97e centile des vitesses
  if( (abs(pk.vx)<<16) >= v9 ||  (abs(pk.vy)<<16) >= v9 )
    v9 += 31*(1<<11);
  else
    v9 -= 1*(1<<11);

  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  pk.x *= vmax;
  pk.x += (pk.vx << SUBD_EXP) + r;
  pk.x /= vmax;

  pk.y *= vmax;
  pk.y += (pk.vy << SUBD_EXP) + r;
  pk.y /= vmax;

  l = ( pk.x>>SUBD_EXP ) + ( pk.y>>SUBD_EXP ) * W;
  l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
  if( l != li ){ // si la particule veut changer de pixel...
    if( map[l] != -1 ){ // et si la place n'est pas libre...
      int m,le;
      int j = map[l];
      if( j >= 0 ){ // si la place est occup�e par une autre particule...
        int vxj = part[j].vx;
        int vyj = part[j].vy;
        choc( pki.x - part[j].x, pki.y - part[j].y, &pk, &part[j] );
        checkv( &pk );
        checkv( &part[j] );
        print_v( l4, part[j].vx - vxj, part[j].vy - vyj );
        pk.x = pki.x;
        pk.y = pki.y;
        l4 = li4;
      }
      else{
        switch( j ){ // si la place est occup�e par un mur...
        case -3 : 
          pk.vx = -pk.vx;
          pk.x = pki.x;
          pk.y = pki.y;
          l4 = li4;
          break;
        case -2 :
          pk.vy = -pk.vy;
          pk.x = pki.x;
          pk.y = pki.y;
          l4 = li4;
          break;
        case -4 :
          pk.vx = -pk.vx;
          pk.vy = -pk.vy;
          pk.x = pki.x;
          pk.y = pki.y;
          l4 = li4;
          break;
        case -5 :
          print_dens( li4, -1 );
          mur5(&pk, k);
          l4 = ( pk.x>>(SUBD_EXP+DT) ) + ( pk.y>>(SUBD_EXP+DT) ) * (W>>DT);
          print_dens( l4, 1 );
          break;
        case -6 :
          mur6(&pk, k);
          l4 = li4;
          break;
        }
      }
    }
    else{
      map[li] = -1; // la particule change de pixel
      map[l] = k;
      if( li4 != l4 ){ // imprimer la densit�
        print_dens( li4, -1 );
        print_dens( l4, 1 );
      }
    }
  }
  if( l4 == li4 )  // mise � jour du champs des vitesses
    print_v( li4, pk.vx - pki.vx, pk.vy-pki.vy );
  else{
    print_v( li4, -pki.vx, -pki.vy );
    print_v( l4, pk.vx, pk.vy );
  }
  part[k] = pk; // sauvegarde finale des nouvelles positions et vitesses
}

static void diffusion( int *a1, int *a2 ){
  // Le champs scalaire a1 diffuse dans le champs a2
  int h,l;
  l = 0;
  *(a2+l) = *(a1+W+1) * 9; // les lignes de bordures ne sont pas prises en compte
  for( l=1; l < W-1; l++)
    *(a2+l) = ( *(a1+l+W-1) + *(a1+l+W) + *(a1+l+W+1) ) * 3;
  *(a2+l) = *(a1+l+W-1) * 9;
  for( h=W; h < (H-1)*W; h+=W ){ 
    l++;
    *(a2+l) = ( *(a1+l-W+1) + *(a1+l+1) + *(a1+l+W+1) ) * 3;
    for( l = h+1 ; l < h+W-1; l++ )
      *(a2+l) = *(a1+l-W-1) + *(a1+l-W) + *(a1+l-W+1)
        + *(a1+l-1) + *(a1+l) + *(a1+l+1)
        + *(a1+l+W-1) + *(a1+l+W) + *(a1+l+W+1);
    *(a2+l) = ( *(a1+l-W-1) + *(a1+l-1) + *(a1+l+W-1) ) * 3;
  }
  l++;
  *(a2+l) = *(a1-W+1) * 9;
  for( l=(H-1)*W+1; l < H*W-1; l++)
    *(a2+l) = ( *(a1+l-W-1) + *(a1+l-W) + *(a1+l-W+1) ) * 3;
  *(a2+l) = *(a1+l-W-1) * 9;
}

int move_fluid( void *unused ){
  int i,bt;
  unsigned int n;
  while( loop ){
    vmax = (CVMAX_NUM*v9)/CVMAX_DEN;
    vmax >>= 16;
    if( vmax > VMAX_ABS ) vmax = VMAX_ABS;
#ifdef STATS
    if( vmax > vmaxmax ) vmaxmax = vmax;
    temps_fluide += 1.0 / (double)vmax;
#endif // STATS
    bt = rand()%(9*N);
    for( i=0; i<N; i++ ){
      n += alea[ i + bt ];
      dM( n%N );
    }
    iterations++;
  }
  return 0;
}


void print_stats_fluid( int tf ){
#ifdef STATS
  int ms=tf%1000;
  int h=tf/(60*60*1000);
  int mn=(tf/(60*1000))%60;
  int s=(tf/1000)%60;
  int i;
  printf("\nNombre de particules : %i\n", N);
  printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
  printf("Densit� moyenne : %f part./pixel\n", (float)N/(W*H));
  printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
  printf("\nNombre total d'iterations : %u\n",iterations);
  printf("Dur�e r�elle : %i h %i mn %i s %i ms\n",h,mn,s,ms);
  printf("Dur�e de l'�coulement : %E\n", temps_fluide ); 
  printf("Iterations par seconde : %f\n",1000.0*iterations/tf);
  printf("Record de vitesse toute iterations confondues : %i\n", vmaxmax);
  printf("Nombre moyen de chocs par iteration : %f\n", (float)nchocs/iterations);
  printf("Nombre de particules ralenties par it�ration : %f\n", (float)ralenties/iterations );
  printf("\nDerni�re it�ration :\n");
  int vxmean = 0;
  int vymean = 0;
  int wmean = 0;
  for( i=0; i<N; i++ ) vxmean += abs( part[i].vx );
  for( i=0; i<N; i++ ) vymean += abs( part[i].vy );
  for( i=0; i<N; i++ ) wmean += abs( part[i].w );
  printf("Vitesse x moyenne : %i\n", vxmean/N);
  printf("Vitesse y moyenne : %i\n", vymean/N);
  printf("vitesse angulaire moyenne : %i\n", wmean/N);
  int count = 0;
  for( i=0; i<N; i++ )
    if( (max(abs( part[i].vx ), abs( part[i].vy ))<<16) <= v9 )
      count++;
  printf("Proportion de particules de vitesse < v9 : %f\n", ((float)count) / N );
  printf("Limite sup�rieure de la vitesse (vmax) : %i\n", vmax);
#endif
}
