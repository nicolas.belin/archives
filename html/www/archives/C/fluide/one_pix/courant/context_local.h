// param�tres de compilations locaux de courant.c

#define N 40000    // nombre de particules
#define INV_M 2    // inverse de la masse d'une particule
#define T_MIN 40000 // force repulsive (statique) entre particules a densit<E9> nulle
#define T_MAX 100000 // force repulsive (statique) entre particules a densit<E9> maximale
#define DT 2        // largeur et longueur des taches de densit� = 3*2^DT
#define CVMAX_NUM 4 // vmax est �gale � CVMAX_NUM * v9 / CVMAX_DEN
#define CVMAX_DEN 3
#define DENS_MAX (3*3*4*4) // densit� maximale : ( 3*2^DT )^2
#define ENT 50 // coefficient d'entrainement du flux des vitesses 
#define SUBD_EXP 5 // SUBD = 2 ^ SUBD_EXP
#define SUBD 32    // nombre de subdivisions d'un pixel
#define W 800	// largueur (width)
#define H 300	// hauteur (height)
#define IM_SEC 20 // nombre d'images par seconde
 
// Optionnels :
#define STATS  1  // collecte de stats ou non ?
