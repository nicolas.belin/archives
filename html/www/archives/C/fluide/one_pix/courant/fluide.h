#define VMAX_ABS 0x7fff // vitesse maximum pouvant �tre g�r�e par le programme
                        // �gale � 2^( sizeof(int)/2 - 1 ) - 1
extern void init_fluid();
extern int move_fluid( void* );
extern void print_stats_fluid(int);

struct particule{ // structure d'une particule
  int x;
  int y;
  int vx;
  int vy;
  int w;
};
struct champs_vitesse{ // structure de champs des vitesses
  int x;
  int y;
};

extern struct particule *part; // les particules...
extern struct champs_vitesse *vit; // le champs des vitesses
extern int *densite; // le champs des densit�s
extern int *map;   // carte des pixels

extern int vmax;   // vitesse maximum (pour la norme max)
extern int nvmax;  // numero de la particule qui a la vitesse vmax
extern unsigned int v9; // 9e d�cile des vitesses
extern int force_on; // champs de force activ� ou non
extern unsigned int iterations; // compte les iterations
