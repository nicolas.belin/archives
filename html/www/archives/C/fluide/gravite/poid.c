#include <stdio.h>	// entrees/sorties avec la console 
#include <stdlib.h>     // malloc, exit...
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random
#include "fluide-local.h"
#include "fluide.h"

#define PI 3.14159265359

#define IM_SEC 40       // nombre max d'images par seconde 

#define max(A,B) ((A) > (B) ? (A) : (B))
#define abs(A) ((A) > 0 ? (A) : -(A))

static Uint32 *video;          // pointeur sur la memoire video

static int *palette1; // palette de couleurs
static int *palette2; // palette circulaire
static void Make_palette();

static void clear_screen();
static void makevid1();
static void makevid2();
static void makevid3();
static void makevid4();
static void makevid5();
static void makevid6();
static void MemError();
// ********* main() ******************************
int main(){
  // ******** initialisation video SDL
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
 
 SDL_Surface *screen;
 SDL_Event event;
 
 screen = SDL_SetVideoMode(W, H,32
                           ,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( !screen ){
   printf("Impossible d'obtenir la r�solution souhait�e :%s\n",SDL_GetError());
   exit(1);
 }
 video = (Uint32*)screen->pixels;

 // ********* allocations memoire
 palette1 = malloc( 0x800*sizeof(int) );
 palette2 = malloc( 0x800*sizeof(int) );
 if( !(palette1 && palette2) ) MemError();

 // ********* initialisations
 Make_palette();  // fabrication d'une palette de couleurs
 int vid_mode = 0; // mode de visualisation
 init_fluide(); // initialisations diverses; utilise la fonction init_particules

 force_on = 1;

 #ifdef STATS
 Uint32 t0,t1,t3;	          // temps
 unsigned int frame = 0;    // compte les images affichees
 t0 = SDL_GetTicks();
 #endif

 Uint32 t2 = SDL_GetTicks();
 int i;
 int loop = 1;
 int *ptemp;

 //********** boucle principale
 while( loop /* && (SDL_GetTicks()-t0<60000)*/ ){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE) loop = 0;
       if(event.key.keysym.sym==SDLK_f) force_on = 1 - force_on; 
       if(event.key.keysym.sym==SDLK_SPACE){
         if ( (++vid_mode) > 5 ) vid_mode = 0;
         if ( vid_mode == 2 ) clear_screen();
       }
       #ifdef VISC
       if(event.key.keysym.sym==SDLK_v) visc_on = 1 - visc_on; 
       #endif
       #ifdef DENS
       if(event.key.keysym.sym==SDLK_e) expan_on = 1 - expan_on; 
       #endif
       break;
     }
   }
   if( SDL_GetTicks()-t2 > 1000/IM_SEC ){
     switch( vid_mode ){
     case 0 : makevid1();
       break;
     case 1 : makevid2();
       break;
     case 2 : makevid3();
       break;
     case 3 : makevid4();
       break;
     case 4 : makevid5();
       break;
     case 5 : makevid6();
       break;
     }
     
     SDL_Flip(screen);
     t2 = SDL_GetTicks();
     #ifdef STATS
     frame++;
     #endif
   }
   move();
 }

 print_stats( SDL_GetTicks() - t0 );
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************

void init_particules(){ // initialisation des positions des positions
  int i,x,y,l;            // et vitesses initiales des particules
  int X = W/2; // largeur du rectangle
  int Y = H/8; // decalage par rapport au bas
  if( X*(H-Y) < N ){
    printf("Probl�me pour le placement initial des points.");
    exit(1);
  }
  for(i=0; i<N; i++){ // positions initiales des particules
    x = ( (W-X)/2 + i % X ) * SUBD + SUBD/2;
    y = (H - Y - i / X ) * SUBD + SUBD/2;
    l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
    part[i].x = x;
    part[i].y = y;
    part[i].vx = 0;
    part[i].vy = 0;
    map[ l ] = i;
  }

  // bordures de l'�cran
  for( i=0; i<W; i++ ){ //barriere horizontale 
    map[ i ] = -2;
    map[ W*(H-1) + i ] =-2;
  }
  for( i=0; i<H; i++ ){ // barriere verticale
    map[ W*i ] = -3;
    map[ W-1 + W*i ] = -3;
  }
  map[ 0 ] = -4;               // coins
  map[ W-1 ] = -4;
  map[ W*H - 1 ] = -4;
  map[ W*(H-1) ] = -4;
}

void champs_force( int x, int y, int *fx, int *fy){
  *fy += 5000; // gravite
}

void mur5(){}

void mur6(){}

static void clear_screen(){
  int i;
  for(i=0; i<W*H; i++)
    video[i]=0;
}

static void makevid1(){
  clear_screen();
  int i;
  for(i=0; i<N; i++)
    video[ ( part[i].x>>SUBD_EXP ) + ( part[i].y>>SUBD_EXP ) * W ] = 0xffffff;
  video[ ( part[nvmax].x>>SUBD_EXP ) + ( part[nvmax].y>>SUBD_EXP ) * W ] = 0xff0000;
}

static void makevid2(){ // la couleur depend de la direction du vecteur vitesse.
  clear_screen();
  int i,x,y;
  double theta;
  for(i=0; i<N; i++){
    x = part[i].vx;
    y = part[i].vy;
    if( (x==0) && (y==0) ) continue; // pas affich�e si immobile
    if( x == 0 ){
      if( y > 0 ) theta = PI/2;
      else theta = -PI/2;
    }
    else{
      theta = atan( ((double)y)/((double)x) );
      if( x < 0 ) theta += PI;
    }
    theta += PI/2; // maintenant, 0 <= theta <= 2*PI
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ]
      = palette2[ (int)(theta*2047/(2*PI)) ];
  }
}

static void makevid3(){ // parcours d'une seule particule : rapide
  video[ part[N/2].x/SUBD + (part[N/2].y/SUBD) * W ] = 0x00ffff;
  video[ part[2*(N/3)].x/SUBD + (part[2*(N/3)].y/SUBD) * W ] = 0xff00ff;
  video[ part[N/3].x/SUBD + (part[N/3].y/SUBD) * W ] = 0xffff00;
}

static void makevid4(){ // couleurs fonctions de la vitesse
  clear_screen();
  unsigned int i,ci;
  int vxt,vyt;
  for(i=0; i<N; i++){
    vxt = part[i].vx;
    vyt = part[i].vy;
    ci = max( abs(vxt), abs(vyt) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ (part[i].x>>SUBD_EXP) + (part[i].y>>SUBD_EXP) * W ] = ci;
  }
}

static void makevid5(){
  #ifdef VISC
  unsigned int l,ci;
  for(l=0; l<W*H; l++){
    ci = max( abs( flux_x[l] ), abs( flux_y[l] ) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ l ] = ci;
  }
  #endif
}

static void makevid6(){
#ifdef DENS
  unsigned int l;
  if( (iterations-1) % 5 == 0 ){
    for(l=0; l<W*H; l++)
      video[ l ] =  palette1[ p3[l] & 0x7ff ];
  }
#endif
}

static void MemError(){
  printf("Probleme de zero !\n");
  exit(1);
}

static void Make_palette(){
  int *palette;
  if( !( palette = malloc( 0x6fa*sizeof(int)))) MemError();
  int i;
  int c = 0x0;
  for(i = 0; i<0x100 ; i++)
    palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c = 0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){
    c -= 0x1;
    palette[i] = c;
  }
  c = 0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){
    c += 0x10000;
    palette[i] = c;
  }
  c = 0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){
    c -= 0x100;
    palette[i] = c;
  }
  c = 0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){
    c += 0x1;
    palette[i] = c;
  }
  c = 0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){
    c += 0x100;
    palette[i] = c;
  }
  c=0xffffff;
  for( i=0; i < 0x800; i++)
    palette1[i] = palette[(0x6f9*i)/0x7ff];

    // palette circulaire :
  c = 0xffffff;
  for(i = 0; i<0x100 ; i++){
    c-=0x10100;
    palette[i]=c;
  }
  for(i = 0 ; i < 0x800 ; i++)
    palette2[i] = palette[(0x6f9*i)/0x7ff];
    
  free(palette);
}
