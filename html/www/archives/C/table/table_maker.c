#include <stdio.h>
#include <math.h>

#define	N 100		/* nombre de points */
#define column 5	/* nombre de colonnes de coordonnees*/	

double f(double x) {
  return sin(x*3.1415);
}

double float_part(double x) {
  double fp;
  fp = x - floor(x);
  return fp;
}
	
int main() {
	int	i;
	double	x_min,x_max;
	double	x;

	x_min = -1;
	x_max = 1;
	
	printf("{");
	for(i=1;i<=N;i++) {	
        x= x_min + ( (double)i - 1 ) * ( x_max - x_min ) / ( N - 1 );
        printf("{%.3f,%.3f}",x,f(x));
        if ( i != N ) {
          printf(",");
        };
        if ( float_part( (double)i / column ) < 0.1 && i < N ) {
          printf("%\n");
        }
	}
	printf("}\n");
}

