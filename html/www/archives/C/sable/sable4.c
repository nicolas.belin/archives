// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <stdio.h>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <math.h>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random

#define STATS  1        // collecte de stats ou non ?

#define W 800	// largueur (width)
#define H 400	// hauteur (height)
// (pour une r�partition initiale sym�trique, prendre N divisible par W)

#define PI 3.14159265359

#define IM_SEC 40       // nombre max d'images par seconde 

Uint32 *video;          // pointeur sur la memoire video

// creation d'une palette de 2048 couleurs :
int palette2[2047];
void Make_palette();

#define N 40000   // nombre de particules
#define PR 200000 // force repulsive (statique) entre particules
#define VISC 10   // viscosit�
#define REPART 50 // Lors d'un choc, repartition (en %) de la composante normale des vitesses :
                  // - si REPART = 0, choc elastique (echange total);
                  // - si REPART = 100, aucun echange, la particule est seulement arret�e.

#define SUBD 32 // nombre de subdivisions d'un pixel
#define SUBD_EXP 5 // SUBD = 2 ^ SUBD_EXP
int *pos;  // positions des particules
int *map;  // carte des pixels
int *v;    // vitesses
int *flux_x; // champs des vitesses (abscisses) 
int *flux_y; // champs des vitesses (ordonn�es)
int *p1;     // champs scalaire des densit�s (ie pression statique)
int *p2;     // p1 et p2 alternent quand ils sont regularis�s
int vmax;  // vitesse maximum (pour la norme max)
int nvmax; // numero de la particule qui a la vitesse vmax
int vmax2; // deuxieme vitesse la plus elevee
int nvmax2;
int *alea; // liste de nombres aleatoires
unsigned int iterations = 0; // compte les iterations
#ifdef STATS
float vxmean = 0; // vitesse moyenne
float vymean = 0;
unsigned int count = 0; // nombre de particules trait�es
float vmaxmean = 0; // vitesse maximale moyenne
int vmaxmax = 0; // le max des vmax
unsigned int nchocs = 0; // nombre de chocs
#endif

void MakeAlea();
void initvars();
void dM(int k);
void choc1( int a, int  b,int *vx1, int *vy1, int *vx2, int *vy2 );
void choc2( int a, int  b,int *vx1, int *vy1, int *vx2, int *vy2 );
void champs_force( int x, int y, int *fx, int *fy);
void champs_force2( int x, int y, int *fx, int *fy);
void checkv(int i, int *vx, int *vy);
void clear_screen();
void makevid1();
void makevid2();
void makevid3();
void makevid4();
void makevid5();
void makevid6();
void MemError();
void Error(int l);
int max(int a,int b);
int abso(int a);

// ********* main() ******************************
int main(){
  // ******** initialisation video SDL
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
 
 SDL_Surface *screen;
 SDL_Event event;
 
 screen = SDL_SetVideoMode(W, H,32
                           ,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( !screen ){
   printf("Impossible d'obtenir la r�solution souhait�e :%s\n",SDL_GetError());
   exit(1);
 }
 video = (Uint32*)screen->pixels;

 // ********* allocations memoire
 map = malloc( W*H*sizeof(int) );
 alea = malloc( N*10*sizeof(int) );
 pos = malloc( 2*N*sizeof(int) );
 v = malloc( 2*N*sizeof(int) );
 flux_x = malloc( W*H*sizeof(int) );
 flux_y = malloc( W*H*sizeof(int) );
 p1 = malloc( W*H*sizeof(int) );
 p2 = malloc( W*H*sizeof(int) );
 if( !(alea && map && pos && v && flux_x && flux_y && p1 && p2) ) MemError();

 // ********* initialisations
 Make_palette();  // fabrication d'une palette de couleurs
 int vid_mode = 0; // mode de visualisation
 MakeAlea();      // fabication d'une liste de nombre aleatoire
 initvars();

 #ifdef STATS
 Uint32 t0,t1,t3;	          // temps
 unsigned int frame = 0;    // compte les images affichees
 t0 = SDL_GetTicks();
 #endif

 Uint32 t2 = SDL_GetTicks();
 int i,bt;
 unsigned int n = 0;
 int loop = 1;

 //********** boucle principale
 while( loop ){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE) loop = 0;
       if(event.key.keysym.sym==SDLK_SPACE){
         if ( (++vid_mode) > 5 ) vid_mode = 0;
         if ( vid_mode == 2 ) clear_screen();
       }
       break;
     }
   }
   if( SDL_GetTicks()-t2 > 1000/IM_SEC ){
          switch( vid_mode ){
     case 0 : makevid1();
       break;
     case 1 : makevid2();
       break;
     case 2 : makevid3();
       break;
     case 3 : makevid4();
       break;
     case 4 : makevid5();
       break;
     case 5 : makevid6();
       break;
       }
     SDL_Flip(screen);
     t2 = SDL_GetTicks();
     #ifdef STATS
     frame++;
     #endif
   }
   bt = rand()%(9*N);
   for( i=0; i<N; i++ ){
     n += alea[ i+bt ];
     dM( n%N );
   }
   #ifdef STATS
   iterations++;
   #endif
 }

#ifdef STATS
 t1=SDL_GetTicks();
 int tf=t1-t0;
 int ms=tf%1000;
 int h=tf/(60*60*1000);
 int mn=(tf/(60*1000))%60;
 int s=(tf/1000)%60;
 int xmean = 0;
 int ymean = 0;
 for( i=0; i<N; i++ ) xmean += pos[ 2*i ];
 for( i=0; i<N; i++ ) ymean += pos[ 2*i+1 ];
 xmean /= N*SUBD;
 ymean /= N*SUBD;
 printf("\nNombre de particules : %i\n", N);
 printf("Nombre de pixels : %i x %i = %i\n", W,H,H*W);
 printf("\nNombre total d'iterations : %u\n",iterations);
 printf("Temps ecoule : %i h %i mn %i s %i ms\n",h,mn,s,ms);
 printf("Iterations par seconde : %f\n",1000.0*iterations/tf);
 printf("Images par seconde : %f\n\n",1000.0*frame/tf);
 printf("Densit� moyenne : %f part./pixel\n", ((float)N)/(W*H));
 printf("  Avec le champs statique : %f part./pixel\n", (9.0*N)/(W*H));
 printf("Moyenne des abscisses (derni�re it�ration) : %i\n",xmean);
 printf("Moyenne des ordonn�es (derni�re it�ration) : %i\n",ymean);
 printf("Vitesse x moyenne : %f\n", vxmean/count);
 printf("Vitesse y moyenne : %f\n", vymean/count);
 printf("Vitesse max moyenne : %f\n", vmaxmean/count);
 printf("Record de vitesse toute iterations confondues : %i\n", vmaxmax);
 printf("Nombre moyen de chocs par iteration : %f\n", ((float)nchocs)/iterations);
#endif
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************
void MakeAlea(){
  int i;
  srand( time(NULL));
  for(i=0; i<10*N; i++) alea[i] = rand()%400;
}

void initvars(){
  int i,x,y,l;

  for(i=0; i<W*H; i++) map[i] = -1; // effacer la carte ( -1 -> vide )
  for(i=0; i<W*H; i++) p1[i] = 0; 
  for(i=0; i<W*H; i++) p2[i] = 0; 

  for(i=0; i<N; i++){ // positions initiales des particules
    if( (W/2)*(H-2) < N ){
      printf("Probl�me pour le placement initial des points.");
      exit(1);
    }
    x = ( 1 + i % (W-2) ) * SUBD + SUBD/2;
    y = (H -2 - i / (W-2) ) * SUBD + SUBD/2;
    l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
    pos[2*i] = x;
    pos[2*i+1] = y;
    map[ l ] = i;
    v[2*i] = 0;
    v[2*i+1] = 0;
  }

  // bordures en particules fixes de tres grande masse
  for( i=0; i<W; i++ ){ //barriere horizontale 
    map[ i ] = -2;
    map[ W*(H-1) + i ] =-2;
  }
  for( i=0; i<H; i++ ){ // barriere verticale
    map[ W*i ] = -3;
    map[ W-1 + W*i ] = -3;
  }
  map[ 0 ] = -4;               // coins
  map[ W-1 ] = -4;
  map[ W*H - 1 ] = -4;
  map[ W*(H-1) ] = -4;

  vmax = 500;
  vmax2 = 1;
  nvmax = -1;
  nvmax2 = -1;
  int av;
  for(i=0; i<2*N; i++){   // determine les deux plus grandes vitesses initiales
    av = abso(v[i]);
    if( av > vmax2 ){
      if( av > vmax ){
        vmax = av;
        nvmax = i/2;
      }
      else {
        vmax2 = av;
        nvmax2 = i/2;
      }
    }
  }
}

void dM(int k){
  int fx = 0;
  int fy = 0;
  int x = pos[2*k];
  int y = pos[2*k+1];
  int l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
  int vx = v[2*k];
  int vy = v[2*k+1];
  int xi = x; // position initiale
  int yi = y;
  int li = l;

  // Somme des forces (hors chocs) s'exercant sur la particule k

  champs_force(x,y,&fx,&fy);  // champs de force

  if( map[ l+1 ] != -1 ) fx -= PR; // forces repulsives entre particules
  if( map[ l-1] != -1 ) fx += PR; // proportionnelles a la temperature T
  if( map[ l+W ] != -1 ) fy -= PR;
  if( map[ l-W ] != -1 ) fy += PR;
  if( map[ l+W+1 ] != -1 ){
    fx -= PR;
    fy -= PR;
  }
  if( map[ l-W+1] != -1 ){
    fx -= PR;
    fy += PR;
  }
  if( map[ l+W-1 ] != -1 ){
    fx += PR;
    fy -= PR;
  }
  if( map[ l-W-1 ] != -1 ){
    fx += PR;
    fy += PR;
  }

  /*
  int dvnx = flux_x[l-W] + flux_x[l+W] - 2*vx;
  int dvny = flux_y[l-1] + flux_y[l+1] - 2*vy;
  fx += dvnx*VISC;
  fy += dvny*VISC;
  */

  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  vx *= vmax;
  vx += fx;
  vx /= vmax;

  vy *= vmax;
  vy += fy;
  vy /= vmax;

  checkv(k,&vx,&vy);
  
  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  int r = vmax >> 1;  // n�c�ssaire car les tronquatures avantagent les vitesses n�gatives 
  x *= vmax;
  x += (vx << SUBD_EXP) + r;
  x /= vmax;

  y *= vmax;
  y += (vy << SUBD_EXP) + r;
  y /= vmax;

  // chocs avec une autre particule
    
  l = ( x>>SUBD_EXP ) + ( y>>SUBD_EXP ) * W;
  if( (l != li) && ( map[l] != -1 ) ){
    #ifdef STATS
    nchocs++;
    #endif
    int m,le;
    int j = map[l];
    if( j >= 0 ){
      choc2( xi-x , yi-y, &vx, &vy, v+2*j, v+2*j+1 );  // faux mais efficace
      //choc2( xi - pos[2*j], yi - pos[2*j+1], &vx, &vy, v+2*j, v+2*j+1 );
      checkv( k, &vx, &vy );
      checkv( j, v+2*j, v+2*j+1 );
    }
    else{
      switch( j ){
      case -3 : 
        vx = -vx;
        break;
      case -2 :
        vy = -vy;
        break;
      case -4 :
        vx = -vx;
        vy = -vy;
        break;
      }
    }
    x = xi;
    y = yi;
    l = li;
  }

  // sauvegarde finale des nouvelles positions et vitesses
  pos[2*k] = x;
  pos[2*k+1] = y;
  map[li] = -1;
  map[l] = k;
  v[2*k] = vx;
  v[2*k+1] = vy;
  flux_x[ l ] = vx; // champs des vitesses
  flux_y[ l ] = vy;
  p1[l] += 1;     // champs des densit�s
  
#ifdef STATS
  count++;
  if( vmax > vmaxmax ) vmaxmax = vmax;
  vxmean += (float)abso(vx);
  vymean += (float)abso(vy);
  vmaxmean += (float)vmax;
#endif
}

void choc1( int a, int  b,int *vx1, int *vy1, int *vx2, int *vy2 ){
  // CHOC ENTRE DEUX PARTICULES DE MEME MASSE.
  // version simplifi�e : la position relative des particules est r�duite � celle
  // de leur pixels. Plus rapide que choc2.

  int dl = ( a>>SUBD_EXP ) + ( b>>SUBD_EXP ) * W;
  dl = abso(dl);
  int vtemp;
  if( dl == 1 ){
    vtemp = *vx1;
    *vx1 = *vx2;
    *vx2 = vtemp;
  }
  else if( dl == W ){
    vtemp = *vy1;
    *vy1 = *vy2;
    *vy2 = vtemp;
  }
  else{
    vtemp = *vx1;
    *vx1 = *vx2;
    *vx2 = vtemp;
    vtemp = *vy1;
    *vy1 = *vy2;
    *vy2 = vtemp;
  }
}

void choc2( int a, int  b,int *vx1, int *vy1, int *vx2, int *vy2 ){
  // CHOC ELASTIQUE ENTRE DEUX PARTICULES DE MEME MASSE.
  
  // la particule i a pour position (xi;yi) et pour vitesse (vxi;vyi)
  // On d�finit a=x1-x2 et b=y1-y2 (seule importe la direction du vecteur (a;b))
  // Les vitesses apr�s le choc sont donn�es par :
  
  //  /vx1'\      1       /  b^2  -a*b   a^2   a*b  \    /vx1\
  //  |vy1'| = ------- * |  -a*b   a^2   a*b   b^2   | * |vy1|
  //  |vx2'|   a^2+b^2   |   a^2   a*b   b^2  -a*b   |   |vx2|
  //  \vy2'/              \  a*b   b^2  -a*b   a^2  /    \vy2/
  int a2 = a*a;
  int b2 = b*b;
  int S = a2+b2;
  int ab = a*b;
  int abvx1 = ab*(*vx1); // evite de faire ces produits deux fois.
  int abvy1 = ab*(*vy1);
  int abvx2 = ab*(*vx2);
  int abvy2 = ab*(*vy2);
  int vx1_p = b2*(*vx1) - abvy1     + a2*(*vx2) + abvy2;
  int vy1_p = -abvx1    + a2*(*vy1) + abvx2     + b2*(*vy2);
  int vx2_p = a2*(*vx1) + abvy1     + b2*(*vx2) - abvy2;
  int vy2_p = abvx1     + b2*(*vy1) - abvx2     + a2*(*vy2);
  int q = REPART;
  int p = 100-q;
  *vx1 = ( p*vx1_p )/S + q*(*vx1);
  *vy1 = ( p*vy1_p )/S + q*(*vy1);
  *vx2 = ( p*vx2_p )/S + q*(*vx2);
  *vy2 = ( p*vy2_p )/S + q*(*vy2);
  *vx1 /= 100;
  *vy1 /= 100;
  *vx2 /= 100;
  *vy2 /= 100;
}

void champs_force( int x, int y, int *fx, int *fy){
  //*fy += 50000; // gravite
#define L_CHAMPS 200 // largueur du champs
#define H_CHAMPS 100 // hauteur du champs
#define I_CHAMPS -15000 // intensit� du champs
  if( ( x >= (W/5)*SUBD ) && ( x <= (W/5+L_CHAMPS)*SUBD ) && (y >= H/4*SUBD) && (y <= (3*H/4+H_CHAMPS)*SUBD ) )
    *fy += I_CHAMPS;
}

void champs_force2( int x, int y, int *fx, int *fy){
  static int theta_old,co,si,fxs,fys;
  int f = 150000;   // intensite du champs
  int exp = 14;     // x et y doivent etre <= 180223
  int R = 100*SUBD; // longueur
  int l = 20*SUBD;  // largeur
  R <<= exp;
  l <<= exp;

  if( iterations == 0 ) theta_old = -1;
  int theta = (iterations/4) % 360; // angle en degres
  if( theta != theta_old ){
    co = (int)( cos( theta*PI/180 )*( 1<<exp ) );
    si = (int)( sin( theta*PI/180 )*( 1<<exp ) );
    fxs = (int)( -sin( theta*PI/180 ) * f );
    fys = (int)( cos( theta*PI/180 ) * f );
    theta_old = theta;
  }
  x -= SUBD*W/2; // centre de rotation
  y -= SUBD*H/2;
  int p1 = -x*si + y*co;
  if( ( p1 <= l) && ( p1 >= -l ) ){
    int p2 = x*co + y*si;
    if( ( p2 <= R ) && ( p2 >= 0 ) ){
      *fx += fxs;
      *fy += fys;
    }
  }
}

void checkv(int i, int *vx, int *vy){
  
  // majoration de la vitesse a 0x8000
  int av = max( abso(*vx) , abso(*vy) );
  if( av >= 0x8000 ){
    if( *vx>=0x8000 ) *vx = 0x7fff;
    if( *vy>=0x8000 ) *vy = 0x7fff;
    if( *vx<=-0x8000 ) *vx = -0x7fff;
    if( *vy<=-0x8000 ) *vy = -0x7fff;
    av = 0x7fff;
  }

  // vmax est la vitesse de la particule la plus rapide
  if( (i==nvmax) ){
    if( av>vmax2 ) vmax = av;
    else {
      vmax = vmax2;
      vmax2 = 1;
      nvmax = nvmax2;
    }
  }
  
  if( av > vmax2 ){
    if( av > vmax ){
      vmax = av;
      nvmax = i;
    }
    else {
      vmax2 = av;
      nvmax2 = i;
    }
  }
}

void clear_screen(){
  int i;
  for(i=0; i<W*H; i++) video[i]=0;
}

void makevid1(){
  clear_screen();
  int i;
  for(i=0; i<N; i++)
    video[ pos[2*i]/SUBD + (pos[2*i+1]/SUBD) * W ] = 0xffffff;
  video[ pos[2*nvmax]/SUBD + (pos[2*nvmax+1]/SUBD) * W ] = 0xff0000;
}

void makevid2(){ // la couleur depend de la direction du vecteur vitesse.
  clear_screen();
  int i,x,y;
  double theta;
  for(i=0; i<N; i++){
    x = v[ 2*i ];
    y = v[ 2*i + 1 ];
    if( (x==0) && (y==0) ) continue; // pas affich�e si immobile
    if( x == 0 ){
      if( y > 0 ) theta = PI/2;
      else theta = -PI/2;
    }
    else{
      theta = atan( ((double)y)/((double)x) );
      if( x < 0 ) theta += PI;
    }
    theta += PI/2; // maintenant, 0 <= theta <= 2*PI
    video[ pos[2*i]/SUBD + (pos[2*i+1]/SUBD) * W ]
      = palette2[ (int)(theta*2047/(2*PI)) ];
  }
}

void makevid3(){ // parcours d'une seule particule : rapide
  video[ pos[ 2*(N/2) ]/SUBD + (pos[ 2*(N/2)+1 ]/SUBD) * W ] = 0xffffff;
  video[ pos[ 2*(N/3) ]/SUBD + (pos[ 2*(N/3)+1 ]/SUBD) * W ] = 0xffff00;
  video[ pos[ 4*(N/3) ]/SUBD + (pos[ 4*(N/3)+1 ]/SUBD) * W ] = 0x00ffff;
}

void makevid4(){ // couleurs fonctions de la vitesse
  clear_screen();
  unsigned int i,ci;
  for(i=0; i<N; i++){
    ci = max( abso(v[2*i]), abso(v[2*i+1]) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ pos[2*i]/SUBD + (pos[2*i+1]/SUBD) * W ]
      = ci;
  }
}

void makevid5(){
  unsigned int l,ci;
  for(l=0; l<W*H; l++){
    ci = max( abso( flux_x[l] ), abso( flux_y[l] ) );
    ci = (ci * 0xff ) / vmax;
    ci *= 0x10101;
    video[ l ] = ci;
  }
}

void makevid6(){
  unsigned int l;
  for(l=0; l<W*H; l++){
    video[ l ] = ( (p1[l]*0xff) / iterations) * 0x10101;
  }
}

void MemError(){
  printf("Probleme de zero !\n");
  exit(1);
}

void Error(int l){
  printf("Probleme !\n l=%i\n",l);
  exit(1);
}

int max(int a,int b){
  return (a>=b) ? a : b;
}

int abso(int a){
  return (a<0) ? -a : a;
  }

void Make_palette(){
  int *palette;
  if( !( palette = malloc( 0x6fa*sizeof(int)))) MemError();
  int i;
  int c = 0xffffff;          // palette circulaire
  for(i = 0; i<0x100 ; i++){c-=0x10100; palette[i]=c;}
  //  for(i = 0; i<0x100 ; i++) palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
  c=0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
  c=0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
  c=0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
  c=0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
  c=0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
  c=0xffffff;
  for(i = 0 ; i < 0x800 ; i++) palette2[i]=palette[(0x6f9*i)/0x7ff];
  free(palette);
}

void Scale_palette(int *palette,int *palette2){
  int i;
}
