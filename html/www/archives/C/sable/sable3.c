// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <stdio.h>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
//#include <cmath>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random

#define screen_w 800	// screen width (largueur)
#define screen_h 480	// screen height (hauteur)

//using namespace std;

const float PI =3.14159265359;

int palette2[2047];
const unsigned int f_rate = 40;		// renouvellement de l'affichage/seconde


Uint32 *video;                                    // pointeur sur la memoire video
void Make_palette(int *palette);                // creation d'une premiere palette de 1786 couleurs
void Scale_palette(int *palette,int *palette2); // Etirement de la palette precedente
                                                // pour avoir 2048=2^11 couleurs.

#define N 40000    // nombre de particules
#define subd 32 // nombre de subdivisions d'un pixel = 2^5
#define subd_exp 5 // subd = 2 ^ subd_exp
unsigned int pos[2*N];  // positions des particules
int *map;
int v[2*N];    // vitesses
int vmax;    // abscisse ou ordonnee de vitesse maximum
float vxmean = 0; // vitesse moyenne
float vymean = 0;
int count = 0;
float vmaxmean = 0; // vitesse maximale moyenne
int nvmax;   // numero de la particule qui a la vitesse vmax
int vmax2;   // deuxieme vitesse la plus elevee
int nvmax2;
int m[N];      // masses
int t;         // temps
int side = 1;  // cot� du premier test
int *table;   // table de nombres aleatoires

void MakeTable();
void initvars();
void dM(int k);
void makevid();
void MemError();
int max(int a,int b);

// ********************************** main() ******************************************

int main(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
 
 SDL_Surface *screen;
 SDL_Event event;
 
 screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( screen == NULL ){
   printf("Impossible d'obtenir la r�solution souhait�e : %s\n",SDL_GetError());
   exit(1);
 }

 video = (Uint32*)screen->pixels;
  
 // couleurs
 int palette[1785];
 Make_palette(palette);
 Scale_palette(palette,palette2);
 
 Uint32 t0,t1,t2,t3;	// temps
 unsigned int frame=0;	// compte les frames
 unsigned int iterations=0; // compte les iterations
 t0 = SDL_GetTicks();
 t2=SDL_GetTicks();
 
 map = malloc( screen_w*screen_h*sizeof(int) );
 table = malloc( N*10*sizeof(int) );
 if( (table == 0) || (map == 0) ) MemError();

 MakeTable();
 unsigned int n = 0;
 initvars();
 int i,bt;

 int loop = 1;
 while((loop==1)){// &&  (SDL_GetTicks()-t0<=40000)){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE){
         loop = 0;
       }
       break;						
     }
   }
   if(SDL_GetTicks()-t2 > 1000/f_rate){
     makevid();
     SDL_Flip(screen);
     frame++;
     t2 = SDL_GetTicks();
   }

   bt = rand()%(9*N);
   for(i = 0;i<N;i++){
     n += table[i+bt];
     dM( n%N );
   }

   iterations++;
   //   SDL_Delay(2);
 }

 t1=SDL_GetTicks();
 int tf=t1-t0;
 int ms=tf%1000;
 int h=tf/(60*60*1000);
 int mn=(tf/(60*1000))%60;
 int s=(tf/1000)%60;
 printf("\nNombre total d'iterations : %u\n",iterations);
 printf("Temps ecoule : %i h %i mn %i s %i ms\n",h,mn,s,ms);
 printf("iterations par seconde : %f\n",1000.0*iterations/tf);
 printf("frames par seconde : %f\n\n",1000.0*frame/tf);

 printf("Nombre de particules : %i\n", N);
 printf("Vitesse x moyenne : %f\n", ((float)vxmean)/count);
 printf("Vitesse y moyenne : %f\n", ((float)vymean)/count);
 printf("Vitesse max moyenne : %f\n", ((float)vmaxmean)/count);
 
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************

void Make_palette(int *palette){
  int c,i;
  for(i = 0; i<0x100 ; i++) palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
  c=0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
  c=0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
  c=0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
  c=0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
  c=0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2){
  int i;
  for(i = 0 ; i < 0x800 ; i++){
    palette2[i]=palette[(0x6f9*i)/0x7ff];
  }
}

void MakeTable(){
  int i;
  srand( time(NULL));
  for(i=0; i<10*N; i++) table[i] = rand()%400;
}

void initvars(){
  int i;

  for(i=0; i<N; i++){   // positions initiales des particules
    pos[2*i] = (300 + i%200) * subd;
    pos[2*i+1] = (screen_h -1 - i/200 - 2) * subd;
    v[2*i] = 0;
    v[2*i+1] = 0;
  }

  for(i=0; i<screen_w*screen_h; i++) map[i]=0; // effacer la carte

  vmax = 500;
  vmax2 = 1;
  nvmax = -1;
  nvmax2 = -1;
  int av;
  for(i=0; i<2*N; i++){   // determine les deux plus grandes vitesses initiales
    av = abs(v[i]);
    if( av > vmax2 ){
      if( av > vmax ){
        vmax = av;
        nvmax = i/2;
      }
      else {
        vmax2 = av;
        nvmax2 = i/2;
      }
    }
  }
  return;
}

void dM(int k){
  const int pr = 100*1000;
  int fx = 0;
  int fy = 0;
  int x = pos[2*k];
  int y = pos[2*k+1];
  int vx = v[2*k];
  int vy = v[2*k+1];
  int c = 0;

  // Somme des forces (hors chocs) s'exercant sur la particule k

  //  fy += 10000; // gravite

  int addr = ( x>>subd_exp ) + ( y>>subd_exp ) * screen_w;
  if( map[addr + 1] != 0 ) fx -= pr; // forces repulsives entre particules
  if( map[addr - 1] != 0 ) fx += pr; // proportionnelles a la temperature T
  if( map[addr + screen_w] != 0 ) fy -= pr;
  if( map[addr - screen_w] != 0 ) fy += pr;
  if( map[addr + screen_w + 1] != 0 ){
    fx -= pr;
    fy -= pr;
    c++;
  }
  if( map[addr - screen_w + 1] != 0 ){
    fx -= pr;
    fy += pr;
    c++;
  }
  if( map[addr + screen_w - 1] != 0 ){
    fx += pr;
    fy -= pr;
    c++;
  }
  if( map[addr - screen_w - 1] != 0 ){
    fx += pr;
    fy += pr;
    c++;
  }
  if( c == 4 ){
    v[2*k] = 0;
    v[2*k+1] = 0;
    return;
  }

  // calcul de la nouvelle vitesse
  // relation fondamentale de la dynamique : dv = f * dt
  vx *= vmax;
  vx += fx;
  vx /= vmax;

  vy *= vmax;
  vy += fy;
  vy /= vmax;
  
  // majoration de la vitesse a 0x8000
  int av = max( abs(vx) , abs(vy) );
  if( av >= 0x8000 ){
    if( vx>=0x8000 ) vx = 0x7fff;
    if( vy>=0x8000 ) vy = 0x7fff;
    if( vx<=-0x8000 ) vx = -0x7fff;
    if( vy<=-0x8000 ) vy = -0x7fff;
  }

  // vmax doit contenir la vitesse de la particule la plus rapide
  int vtemp;
  if( (k==nvmax) ){
    if( av>vmax2 ) vmax = av;
    else {
      vmax = vmax2;
      vmax2 = 1;
      nvmax = nvmax2;
    }
  }

  if( av > vmax2 ){
    if( av > vmax ){
      vmax = av;
      nvmax = k;
    }
    else {
      vmax2 = av;
      nvmax2 = k;
    }
  }

  // statistiques
  count++;
  vxmean += (float)abs(vx);
  vymean += (float)abs(vy);
  vmaxmean += (float)vmax;

  map[ addr ] = 0;

  // calcul de la nouvelle position de la particule avec dM = v * dt
  // vmax sert comme unite temporelle
  // une particule ne doit pas se deplacer de plus d'un pixel a la fois.
  x *= vmax;
  x += vx << subd_exp;
  x /= vmax;

  y *= vmax;
  y += vy << subd_exp;
  y /= vmax;


  // reflexion contre les parois
  if( (x>>subd_exp) == 0 ){
    x = pos[2*k];
    vx = -vx;
  }
  if( (x>>subd_exp) == screen_w-1 ){
    x = pos[2*k];
    vx = -vx;
  }
  if( (y>>subd_exp) == 0 ){
    y = pos[ 2*k+1 ];
    vy = -vy;
  }
  if( (y>>subd_exp) == screen_h-1 ){
    y = pos[ 2*k+1 ];
    vy = -vy;
  }

  // sauvegarde finale des nouvelles positions et vitesse
  pos[2*k] = x;
  pos[2*k+1]= y;
  v[2*k] = vx;
  v[2*k+1] = vy;
  map[ (x>>subd_exp) + (y>>subd_exp) * screen_w ] = 0xffffff;

  return;
}

void makevid(){
  int i;
  for(i=0; i<screen_w*screen_h; i++) video[i]=0;
  for(i=0; i<N; i++) video[ pos[2*i]/subd + (pos[2*i+1]/subd) * screen_w ] = 0xffffff;
  //video[ pos[30000]/subd + (pos[30001]/subd) * screen_w ] = 0xffffff;
}

void MemError(){
  printf("Probleme de memoire !\n");
  exit(1);
}

int max(int a,int b){
  if(a>=b) return a;
  else return b;
}

int abs(int a){
  if(a<0) return -a;
  return a;
}
