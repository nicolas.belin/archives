// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <stdio.h>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
//#include <cmath>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random

#define screen_w 800	// screen width (largueur)
#define screen_h 480	// screen height (hauteur)

//using namespace std;

const float PI =3.14159265359;

int palette2[2047];
const unsigned int f_rate = 40;		// renouvellement de l'affichage/seconde


Uint32 *map;                                    // pointeur sur la memoire video
void Make_palette(int *palette);                // creation d'une premiere palette de 1786 couleurs
void Scale_palette(int *palette,int *palette2); // Etirement de la palette precedente
                                                // pour avoir 2048=2^11 couleurs.

#define N 80000
int pos[N];  // positions des particules
int side = 1; // cot� du premier test
int *table;   // table de nombres aleatoires

void MakeTable();
void init_pos();
void init_map();
void move(int k);
void pos2map();
void cls();
void MemError();

// ********************************** main() ******************************************

int main(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
 
 SDL_Surface *screen;
 SDL_Event event;
 
 screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( screen == NULL ){
   printf("Impossible d'obtenir la r�solution souhait�e : %s\n",SDL_GetError());
   exit(1);
 }

 map = (Uint32*)screen->pixels;
  
 // couleurs
 int palette[1785];
 Make_palette(palette);
 Scale_palette(palette,palette2);
 
 Uint32 t0,t1,t2,t3;	// temps
 unsigned int frame=0;	// compte les frames
 t0 = SDL_GetTicks();
 t2=SDL_GetTicks();
 
 table = malloc( N*10*sizeof(int) );
 if(table == 0) MemError();
 MakeTable();
 init_pos();
 cls();

 pos2map();
 init_map();
 unsigned int n = 0;

 int loop = 1;
 while(loop==1){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE){
         loop = 0;
       }
       break;						
     }
   }
   if(SDL_GetTicks()-t2 > 1000/f_rate){
     SDL_Flip(screen);
     t2 = SDL_GetTicks();
   }
   int bt = rand()%(9*N);
   int i;
   for(i = 0;i<N;i++){
     n += table[i+bt];
     move(n % N);
   }
   frame++;
   //   SDL_Delay(1);
 }

 t1=SDL_GetTicks();
 int t=t1-t0;
 int ms=t%1000;
 int h=t/(60*60*1000);
 int mn=(t/(60*1000))%60;
 int s=(t/1000)%60;
 printf("Nombre total d'iterations : %u\n",frame);
 printf("Temps ecoule : %i h %i mn %i s %i ms\n",h,mn,s,ms);
 printf("iterations par seconde : %f\n",1000.0*frame/t);
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************

void Make_palette(int *palette){
  int c,i;
  for(i = 0; i<0x100 ; i++) palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
  c=0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
  c=0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
  c=0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
  c=0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
  c=0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2){
  int i;
  for(i = 0 ; i < 0x800 ; i++){
    palette2[i]=palette[(0x6f9*i)/0x7ff];
  }
}

void MakeTable(){
  int i;
  srand( time(NULL));
  for(i=0; i<10*N; i++) table[i] = rand()%400;
}

void init_pos(){
  int i;
  for(i=0; i<N; i++){
    pos[i] = 300 + i%200 + (screen_h -1 - i/200 - 1) * screen_w;
  }
}

void init_map(){
  int i;
  for(i=screen_w*(screen_h-1); i<screen_w*screen_h-1; i++) map[i]=0xff0000;
}

void move(int k){
  int l = pos[k];
  side = -side;
  if( map[ l+screen_w ] == 0 ){
    pos[ k ] = l+screen_w;
    map[ l ] = 0;
    map[ l+screen_w ] = 0xffffff;
    return;
  }
  if( map[ l + screen_w + side ] == 0 ){
    pos[k] = l + screen_w + side;
    map[ l ] = 0;
    map[ l + screen_w + side ] = 0xffffff;
    return;
  }
  if( map[ l + screen_w - side ] == 0 ){
    pos[k] = l + screen_w - side;
    map[ l ] = 0;
    map[ l + screen_w - side ] = 0xffffff;
    return;
  }
  return;
}

void pos2map(){
  int i;
  for(i=0; i<N; i++){
    map[ pos[i] ] = 0xffffff;
  }
}

void cls(){
  int i;
  for(i=0; i<screen_w*screen_h; i++) map[i]=0;
}

void MemError()
{	printf("Probleme de memoire !\n");
	exit(1);
}
