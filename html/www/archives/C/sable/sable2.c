// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <stdio.h>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
//#include <cmath>   	// fonction sinux, cosinus...
#include <time.h>	      // pour initialiser la fonction random

#define screen_w 800	// screen width (largueur)
#define screen_h 480	// screen height (hauteur)

//using namespace std;

const float PI =3.14159265359;

int palette2[2047];
const unsigned int f_rate = 40;		// renouvellement de l'affichage/seconde


Uint32 *map;                                    // pointeur sur la memoire video
void Make_palette(int *palette);                // creation d'une premiere palette de 1786 couleurs
void Scale_palette(int *palette,int *palette2); // Etirement de la palette precedente
                                                // pour avoir 2048=2^11 couleurs.

#define N 10000    // nombre de particules
#define subd 32 // nombre de subdivisions d'un pixel = 2^5
unsigned int pos[2*N];  // positions des particules
int v[2*N];    // vitesses
int vmax=0;    // abscisse ou ordonnee de vitesse maximum
int nvmax;     // numero de la particule qui a la vitesse vmax
int vmax2=0;   // deuxieme vitesse la plus elevee
int nvmax2;
int m[N];      // masses
int t;         // temps
int side = 1;  // cot� du premier test
int *table;   // table de nombres aleatoires

void MakeTable();
void initvars();
void move(int k);
void dv(int k);
void pos2map();
void cls();
void MemError();
int max(int a,int b);

// ********************************** main() ******************************************

int main(){
  if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ){
    fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(SDL_Quit);
 
 SDL_Surface *screen;
 SDL_Event event;
 
 screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
 if ( screen == NULL ){
   printf("Impossible d'obtenir la r�solution souhait�e : %s\n",SDL_GetError());
   exit(1);
 }

 map = (Uint32*)screen->pixels;
  
 // couleurs
 int palette[1785];
 Make_palette(palette);
 Scale_palette(palette,palette2);
 
 Uint32 t0,t1,t2,t3;	// temps
 unsigned int frame=0;	// compte les frames
 t0 = SDL_GetTicks();
 t2=SDL_GetTicks();
 
 table = malloc( N*10*sizeof(int) );
 if(table == 0) MemError();
 MakeTable();
 cls();

 //pos2map();
 unsigned int n = 0;

 /* pos[0]=200*subd;
 pos[1]=100*subd;
 v[0]=1000;
 v[1]=0;

 pos[2]=200*subd;
 pos[3]=100*subd;
 v[2]=-1000;
 v[3]=-500;
 pos[4]=200*subd;
 pos[5]=100*subd;
 v[4]=700;
 v[5]=-1000;
 m[0]=1000;*/
 initvars();

 int i;

 int loop = 1;
 while(loop==1){
   if( SDL_PollEvent(&event)==1 ){
     switch(event.type){
     case SDL_MOUSEBUTTONDOWN :
       loop = 0;	
       break;
     case SDL_KEYDOWN:
       if(event.key.keysym.sym==SDLK_ESCAPE){
         loop = 0;
       }
       break;						
     }
   }
   if(SDL_GetTicks()-t2 > 1000/f_rate){
     SDL_Flip(screen);
     t2 = SDL_GetTicks();
   }
   int bt = rand()%(9*N);
   int i,m;
   for(i = 0;i<N;i++){
   n += table[i+bt];
   m = n % N;
   dv( m );
   move( m );
   }

   frame++;
   //   SDL_Delay(5);
 }

 t1=SDL_GetTicks();
 int tf=t1-t0;
 int ms=tf%1000;
 int h=tf/(60*60*1000);
 int mn=(tf/(60*1000))%60;
 int s=(tf/1000)%60;
 printf("Nombre total d'iterations : %u\n",frame);
 printf("Temps ecoule : %i h %i mn %i s %i ms\n",h,mn,s,ms);
 printf("iterations par seconde : %f\n",1000.0*frame/tf);
 SDL_Quit();
 return 0;
}

// ****************************** fonctions **********************************

void Make_palette(int *palette){
  int c,i;
  for(i = 0; i<0x100 ; i++) palette[i]=i;
  c=0xff;
  for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
  c=0xffff;
  for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
  c=0xff00;
  for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
  c=0xffff00;
  for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
  c=0xff0000;
  for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
  c=0xff00ff;
  for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2){
  int i;
  for(i = 0 ; i < 0x800 ; i++){
    palette2[i]=palette[(0x6f9*i)/0x7ff];
  }
}

void MakeTable(){
  int i;
  srand( time(NULL));
  for(i=0; i<10*N; i++) table[i] = rand()%400;
}

void initvars(){
  int i;
  for(i=0; i<N; i++){
    pos[2*i] = (300 + i%200) * subd;
    pos[2*i+1] = (screen_h -1 - i/200 - 1) * subd;
    v[2*i] = 2000 - (rand() % 4000);
    v[2*i+1] = 2000 - (rand() % 4000);
  }
  //  for(i=screen_w*(screen_h-1); i<screen_w*screen_h-1; i++) map[i]=0xff0000;

  for(i=0; i<2*N; i++){   // determine les deux plus grandes vitesses initiales
    int vxy = abs(v[i]);
    if( vxy > vmax2 ){
      if( vxy > vmax ){
        vmax = vxy;
        nvmax = i/2;
      }
      else {
        vmax2 = vxy;
        nvmax2 = i/2;
      }
    }
  }
  return;
}

void move(int k){
/*
  int l = pos[k];
  side = -side;
  if( map[ l+screen_w ] == 0 ){
    pos[ k ] = l+screen_w;
    map[ l ] = 0;
    map[ l+screen_w ] = k;
    return;
  }
  if( map[ l + screen_w + side ] == 0 ){
  pos[k] = l + screen_w + side;
  map[ l ] = 0;
  map[ l + screen_w + side ] = k;
  return;
  }
  if( map[ l + screen_w - side ] == 0 ){
  pos[k] = l + screen_w - side;
  map[ l ] = 0;
  map[ l + screen_w - side ] = k;
  return;
  }
  return;
*/
  int x = pos[2*k];
  int y = pos[2*k+1];
  map[ x/subd + (y/subd) * screen_w ] = 0;
  x = x*vmax + v[2*k]*subd;
  y = y*vmax + v[2*k+1]*subd;
  x /= vmax;
  y /= vmax;
  if( x/subd == -1 ){
    x = pos[2*k];
    v[2*k] = -v[2*k];
  }
  if( x/subd == screen_w ){
    x = pos[2*k];
    v[2*k] = -v[2*k];
  }
  if( y/subd == -1 ){
    y = pos[ 2*k+1 ];
    v[2*k+1] = -v[2*k+1];
  }
  if( y/subd == screen_h ){
    y = pos[ 2*k+1 ];
    v[2*k+1] = -v[2*k+1];
  }
  pos[2*k] = x;
  pos[2*k+1]= y;
  map[ x/subd + (y/subd) * screen_w ] = 0xffffff;
  
  return;
}

void dv(int k){
  int vx = v[2*k]*vmax;
  int vy = v[2*k+1]*vmax;// + 100*1000;
  int vtemp;
  vx /= vmax;
  vy /= vmax;
  v[2*k] = vx;
  v[2*k+1] = vy;

  int av = max( abs(vx) , abs(vy) );
  if( av >= 0x8000 ){   // vitesse limitee a 0x8000
    if( vx>=0x8000 ) vx = 0x7fff;
    if( vy>=0x8000 ) vy = 0x7fff;
    if( vx<=-0x8000 ) vx = -0x7fff;
    if( vy<=-0x8000 ) vy = -0x7fff;
  }
  if( (k==nvmax) ){     // determination de l'umite temporelle
    if( av<vmax ){
      vtemp = vmax;
      vmax = vmax2;
      vmax2 = av;
      nvmax = nvmax2;
      nvmax2 = k;
    }
  }
  if( av>vmax2 ){
    if( av>vmax ){
      vmax2 = vmax;
      vmax = av;
      nvmax2 = nvmax;
      nvmax = k;
    }
    else {
      vmax2 = av;
      nvmax2 = k;
    }
  }
  return;
}

void pos2map(){
  int i;
  for(i=0; i<N; i++){
    map[ pos[2*i]/subd + (pos[2*i+1]/subd) * screen_w ] = 0xffffff;
  }
}

void cls(){
  int i;
  for(i=0; i<screen_w*screen_h; i++) map[i]=0;
}

void MemError(){
  printf("Probleme de memoire !\n");
  exit(1);
}

int max(int a,int b){
  if(a>=b) return a;
  else return b;
}

int abs(int a){
  if(a<0) return -a;
  return a;
}
