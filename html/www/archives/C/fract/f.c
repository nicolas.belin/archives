// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
//#include <cmath>	// fonction sinux, cosinus...
#include <cstdlib>
//#include <ctime>	// random
#include <complex>

using namespace std;

const unsigned int screen_w = 1200;	// screen width (largueur)
const unsigned int screen_h = 900;	// screen height (hauteur)
const double zoom = 1.5;
const double x_min = -(screen_w*zoom*1.0)/screen_h;
const double x_max = (screen_w*zoom*1.0)/screen_h;
const double y_min = -zoom;
const double y_max = zoom;
const float PI =3.14159265359;

int palette[1785];
Uint32 *bitmap;

void Make_palette(int *palette);                // creation d'une palette de 1786 couleurs
void Draw_pixel(int x,int y,double color);
complex<double> f(complex<double> z);
complex<double> fn(complex<double> z);

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	bitmap = (Uint32*)screen->pixels;	

	// couleurs
        Make_palette(palette);

	for(int xp = 0 ; xp < screen_w ; xp++)
	{
	SDL_Flip(screen);
	if(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN) break;
	for(int yp = 0 ; yp < screen_h ; yp++)
	{	double x = x_min + (xp * ( x_max - x_min )) / screen_w;
		double y = y_min + (yp * ( y_max - y_min )) / screen_h;
		complex<double> z(x , y);
		z = fn(z);
		Draw_pixel(xp,yp,real(z)); 
	}
	}

	SDL_Flip(screen);

        while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN)){
        }

	//cin.get();
	SDL_Quit();
	return 0;
}

// ****************************** fonctions **********************************

complex<double> f(complex<double> z)
{	return z*(z-1.0);
}

complex<double> fn(complex<double> z)
{	int i = 0;
	while( i < 30 && abs(z)<10)
	{	z = f(z);
		i++;
	}
	return z;
}

void Make_palette(int *palette)
{       int c,i;
        for(i = 0; i<0x100 ; i++) palette[i]=i;
        c=0xff;
        for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
        c=0xffff;
        for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
        c=0xff00;
        for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
        c=0xffff00;
        for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
        c=0xff0000;
        for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
        c=0xff00ff;
        for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Draw_pixel(int x,int y,double color)
{	int col = int( color * 1785 );
	if(col < 0) col = 0;
	if(col > 1785) col = 1785;
	bitmap[(y*screen_w)+x] = palette[col];
}
