section .data

extern	palette2
screen_w equ 640	; largueur de l'ecran
screen_h equ 480	; longueur de l'ecran

section .text 

global dif_ASM,copbuf_ASM
align 16

;*********************************************
;***  diffusion avec bords neutres ***********
;*********************************************
dif_ASM2:mov esi,[esp+4]		;hot1
	mov edi,[esp+8]		;hot2
	push ebp		;sauvegarde de ebp
	mov ebp,3		;divisions par 3
	xor ecx,ecx		;index en octets
; pixel du coin superieur gauche
	mov eax,[esi+4]
	add eax,[esi+screen_w*4]
	shr eax,1
	mov [edi],eax
	add ecx,4
; ligne horizontale superieure
.absi1:	mov eax,[esi+ecx-4]
	add eax,[esi+ecx+4]
	xor edx,edx
	add eax,[esi+ecx+screen_w*4]
	div ebp
	mov [edi+ecx],eax
	add ecx,4
	cmp ecx,(screen_w-1)*4
	jne .absi1
; pixel du coin superieur droit
	mov eax,[esi+ecx-4]
	add eax,[esi+ecx+screen_w*4]
	shr eax,1
	mov [edi+ecx],eax
	add ecx,4
; ligne verticale gauche
.ord:	mov eax,[esi+ecx+4]
	add eax,[esi+ecx-screen_w*4]
	xor edx,edx
	add eax,[esi+ecx+screen_w*4]
	div ebp
	mov [edi+ecx],eax
	add ecx,4
	xor ebx,ebx
; points interieurs
.absi:	mov eax,[esi+ecx+4]
	add eax,[esi+ecx-4]
	add eax,[esi+ecx+screen_w*4]
	add ebx,1
	add eax,[esi+ecx-screen_w*4]
	shr eax,2
	mov [edi+ecx],eax
	add ecx,4
	cmp ebx,screen_w-2
	jne .absi
; ligne verticale droite
	mov eax,[esi+ecx-4]
	add eax,[esi+ecx-screen_w*4]
	xor edx,edx
	add eax,[esi+ecx+screen_w*4]
	div ebp
	mov [edi+ecx],eax
	add ecx,4
	cmp ecx,screen_w*(screen_h-1)*4
	jne .ord
; pixel du coin inferieur gauche
	mov eax,[esi+ecx+4]
	add eax,[esi+ecx-screen_w*4]
	shr eax,1
	mov [edi+ecx],eax
	add ecx,4
; ligne horizontale inferieure
.absi2:	mov eax,[esi+ecx-4]
	add eax,[esi+ecx+4]
	xor edx,edx
	add eax,[esi+ecx-screen_w*4]
	div ebp
	mov [edi+ecx],eax
	add ecx,4
	cmp ecx,screen_h*screen_w*4-4
	jne .absi2
; pixel du coin inferieur droit
	mov eax,[esi+ecx-4]
	add eax,[esi+ecx-screen_w*4]
	shr eax,1
	mov [edi+ecx],eax
	pop ebp
	ret

;*********************************************
;***  diffusion avec bords froids ***********
;*********************************************
dif_ASM:mov esi,[esp+4]	;hot1
	mov edi,[esp+8]		;hot2
	mov ecx,(screen_w+1)*4
.ord:	xor ebx,ebx
.absi:	mov eax,[esi+ecx+4]
	add eax,[esi+ecx-4]
	add eax,[esi+ecx+screen_w*4]
	add eax,[esi+ecx-screen_w*4]
	shr eax,2
	mov [edi+ecx],eax
	add ebx,1
	add ecx,4
	cmp ebx,screen_w-2
	jne .absi
	add ecx,8
	cmp ecx,screen_w*(screen_h-1)*4+4
	jne .ord
	ret

;*********************************************
;***  diffusion avec bords froids SSE2 *******
;*********************************************
dif_ASM4:mov esi,[esp+4]		;hot1
	mov edi,[esp+8]		;hot2
	mov ecx,screen_w*4
.ord:	;xor ebx,ebx
.absi:	movdqu xmm0,[esi+ecx+4]
	movdqu xmm1,[esi+ecx-4]
	paddd xmm0,xmm1
	paddd xmm0,[esi+ecx-screen_w*4]
	paddd xmm0,[esi+ecx+screen_w*4]
	psrld xmm0,2
	movdqa [edi+ecx],xmm0
	add ecx,16
;	cmp ebx,screen_w*(screen_h-5)
;	jne .absi
;	add ecx,8*4
	cmp ecx,screen_w*(screen_h-1)*4
	jne .ord
	ret

;************************************************
;mise en place des couleurs dans la memoire video
;************************************************
copbuf_ASM:
	mov esi,[esp+4]
	mov edi,[esp+8]
	mov ecx,screen_w*4
.cop:	mov eax,[esi+ecx]
	shr eax,18
	mov edx,[palette2+eax*4]
	add ecx,4
	mov [edi+ecx],edx
	cmp ecx,screen_w*(screen_h-1)*4
	jne .cop
	ret
