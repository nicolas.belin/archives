// compilation : g++ `sdl-config --cflags` -c %1.c
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>		// input/output avec la console
#include <SDL/SDL.h>	// API video, clavier, timer, souris...
#include <GL/gl.h>		// API carte 3D
#include <cmath>	// fonction sinux, cosinus...

using namespace std;

const float pi = 3.141592;

//-----------------------------------------------------------
//---------------------  GENERAL  ---------------------------
//-----------------------------------------------------------
void MemError();	// erreur d'allocation de memoire

//-----------------------------------------------------------
//---------------------  DIFFUSION  -------------------------
//-----------------------------------------------------------
const int n_w = 640;	// taille de la matrice d'elements finis
const int n_h = 512;	// (redefinis dans regul.asm)
// nombre maximum pour eviter les debordements :
const unsigned int max_hot = ( ~(unsigned int)0x0 )>>2;
extern "C" void dif_ASM( int* , int* );
void dif_C( int* , int* );
void source( int* , unsigned int );//sources de chaleur

//-----------------------------------------------------------
//---------------------  COULEURS  --------------------------
//-----------------------------------------------------------
int palette2[0x800];
void Make_palette();
extern "C" void graphic_ASM( int* );
void graphic_C( int* );

//------------------------------------------------------------
//-------------------  OPENGL et SDL  ------------------------
//------------------------------------------------------------ 
const int screen_w = 640;
const int screen_h = 512;
const unsigned int f_rate = 40;	// max frame rate 
float Near=1.5;
void init_GL();	// initialisation de opengl (parametres globaux)
void display3D();
void myLookAt(float eyex,float eyey,float eyez,float centerx,
		float centery,float centerz,float upx,float upy,float upz);

//------------------------------------------------------------
//-------------------  VISUALISATION 3D  ---------------------
//------------------------------------------------------------
const int p_w = n_w/2;	// taille de la matrice affichee
const int p_h = n_h/2;	// (redefinis dans regul.asm)
int xyzhot[3*p_w*p_h];		// coordonnees des points de l'espace
int colors[p_w*p_h];
//------------------------------------------------------------
//---------------------  MAIN() ------------------------------
//------------------------------------------------------------
int main( int argc , char **argv )
{	Make_palette(); // fabrication de la palette des couleurs

	int *hot1 = new int[n_h*n_w];
	int *hot2 = new int[n_h*n_w];
	if( (hot1 == 0) || (hot1 == 0) ) MemError();
	for( int i=0 ; i<n_h*n_w ; i++ )
	{	hot1[i]=0;
		hot2[i]=0;
	}

	int *xyz = xyzhot;
	for( int z = 0 ; z < n_h ; z+=2 )
	for( int x = 0 ; x < n_w ; x+=2 )
		{	*(xyz) = x;
			xyz += 2;
			*(xyz) = z;
			xyz++;
		}

	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	cerr	<< "Impossible d'initialiser SDL : " << SDL_GetError();
		exit(1);
	}
	atexit(SDL_Quit);

	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
											SDL_DEFAULT_REPEAT_INTERVAL);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
	SDL_Surface *screen;
	screen = SDL_SetVideoMode(screen_w,screen_h, 0,
									SDL_OPENGL|SDL_FULLSCREEN);
	if ( screen == NULL )
	{	cerr << "Impossible de passer en " << screen_w;
		cerr << "x" << screen_h << " en 32 bpp : ";
		cerr << SDL_GetError() << endl;
   	    exit(1);
	}
	init_GL();		

//-------------------------------------------------------------
//---------------------  representation 3D  -------------------
//-------------------------------------------------------------
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0*screen_w/screen_h,1.0*screen_w/screen_h,
												-1.0,1.0 , Near,10000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// viewing transformation :
	myLookAt( 50.0,200,n_h , 0.0,0.0,0.0 , 0.0,1.0,0.0 );
	const float y_max = 150.0;
	glScalef( 1.0 , y_max/max_hot , 1.0 );
	glPushMatrix();

	SDL_Event event;
	int frame = 0;
	int iter = 0;
	bool b = 0;
	Uint32 t0,t1,t2,t3;	// temps
	t2 = SDL_GetTicks();
	t0 = SDL_GetTicks();
	bool loop = true;
	while(loop)
	{	if(b==0)
		{	source(hot1,iter);
			dif_ASM(hot1,hot2);
			if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	graphic_ASM( hot2 );
				display3D();
				t2 = SDL_GetTicks();
				frame++;
			}
		}
		else
		{	source(hot2,iter);
			dif_ASM(hot2,hot1);
			if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	graphic_ASM( hot1 );
				display3D();
				t2 = SDL_GetTicks();
				frame++;
			}
		 }
		b=!b;
		iter++;
		
		if( SDL_PollEvent(&event)==1 )
		{	switch(event.type)
			{	case SDL_MOUSEBUTTONDOWN :
					loop = false;	
					break;
				case SDL_KEYDOWN:
    				if(event.key.keysym.sym==SDLK_ESCAPE)
					{	loop=false;
					}
					break;						
			}
		}
	}

	t1=SDL_GetTicks();

	delete [] hot2;
	delete [] hot1;

	SDL_Quit();

	int t=t1-t0;
	int ms=t%1000;
	int hour=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << iter << endl;
	cout << "Nombre de frames : " << frame << endl;
	cout << "Temps ecoule : " <<hour<<" h : " <<mn<<" mn : "<<s
											<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*iter/t << endl;
	cout << "frames par seconde : " << 1000.0*frame/t << endl;

	return 0;
}

void init_GL()
{	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0,0,0,0);	// on efface avec du noir
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
}

void graphic_C( int *hot )
{	int *y = xyzhot + 1;
	unsigned int c = 0;
	unsigned int b,a;

	while( c != p_w*p_h )
	{	for(b = 0; b < p_w ; b++ )
		{	a = *hot;
			a += *( hot + 1 );
			a += *( hot + n_w );
			a += *( hot + n_w + 1 );
			a >>= 2;
			*y = a;
			a >>= 19;
			colors[c++] = palette2[a];
			hot += 2;
			y += 3;
		}
			hot += n_w;
	}
}

void dif_C( int *hot1 , int *hot2 )
{	unsigned int S; 
	hot1 += n_w+1;
	hot2 += n_w+1;
	for( int y=1 ; y<n_h-1 ; y++ )
	{	for( int x=1 ; x<n_w-1 ; x++ )
		{	S = *( hot1-1 );
			S += *( hot1+1 );
			S += *( hot1-n_w );
			S += *( hot1+n_w );
			S >>= 2;
			*hot2 = S;
			hot1++;
			hot2++;
		}
		hot1 += 2;
		hot2 += 2;
	}
}

void display3D()
{	// viewing transformation :
	glPopMatrix();
	glPushMatrix();

	// modeling tranformation :
	glRotatef( SDL_GetTicks()/100.0 , 0.0 , 1.0 , 0.0 );
	glTranslatef(-n_w/2.0,0.0,-n_h/2.0);
	GLubyte *col = (GLubyte*)colors;
	int *xyz = xyzhot;
	for( int i = 0 ; i < p_h-1 ; i++ )
	{	glBegin(GL_TRIANGLE_STRIP);
		for( int x = 0 ; x < p_w ; x++ )
		{	glColor3ubv( col+p_w*4 );
			glVertex3iv( xyz+p_w*3 );
			glColor3ubv( col );
			glVertex3iv( xyz );
			col += 4;
			xyz += 3;
		}
		glEnd();
	}
	glFlush();
	SDL_GL_SwapBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void myLookAt(float eyex,float eyey,float eyez,float centerx,float centery,
					float centerz,float upx,float upy,float upz)
{	float wx = -centerx+eyex;	// a l'envers car la camera regarde vers
	float wy = -centery+eyey;	// les z negatifs
	float wz = -centerz+eyez;
	float N = sqrt( wx*wx + wy*wy + wz*wz ); //norme de w
	wx /= N;	// on normalise w
	wy /= N;
	wz /= N;
	float s = wx*upx + wy*upy + wz*upz; // produit scalaire de w avec up
	float vx = upx - s*wx; // on enleve dans up sa composante sur w;
	float vy = upy - s*wy; // ainsi, ils sont orthogonaux.
	float vz = upz - s*wz;
	N = sqrt( vx*vx + vy*vy + vz*vz ); // norme de v
	vx /= N; // on normalise v
	vy /= N;
	vz /= N;
	float ux = wz*vy - wy*vz; // produit vectoriel de v par w.
	float uy = -wz*vx + wx*vz ; // ainsi, (u,v,w) est une base
	float uz = wy*vx - wx*vy; //  orthonormee directe de l'espace
	
	float M[16];	// la matrice de la transformation (en colonne)
	// 1e colonne : 
	M[0] = ux;	// matrice inverse :
	M[1] = -(uy*wz - uz*wy);// transposee de la matrice des cofacteurs.
	M[2] = uy*vz - uz*vy;	// determinant=1 car c'est une isometrie
	M[3] = 0;
	// 2e colonne :
	M[4] = uy;
	M[5] = ux*wz - uz*wx;
	M[6] = -(ux*vz - uz*vx);
	M[7] = 0;
	// 3e colonne :
	M[8] = uz;
	M[9] = -(ux*wy - uy*wx);
	M[10] = ux*vy - uy*vx;
	M[11] = 0;
	// 4e colonne :
	M[12] = 0;
	M[13] = 0;
	M[14] = 0;
	M[15] = 1;
	glMultMatrixf(M);	// multiplication de la matrice courante par M
	glTranslatef( -eyex , -eyey , -eyez );
}

void Make_palette()
{	int *palette = new int[0x6fa];	
	if( palette == NULL ) MemError();

	unsigned int c,i;
	for(i = 0; i<0x100 ; i++) palette[i]=i;
	c=0xff;
	for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
	c=0xffff;
	for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
	c=0xff00;
	for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
	c=0xffff00;
	for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
	c=0xff0000;
	for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
	c=0xff00ff;
	for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
	
	unsigned int b,r;
	for(int j=0 ; j<0x6fa ; j++)
	{	c = palette[j];
		b = ( 0xff & c ) << 16;
		r = ( 0xff0000 & c ) >> 16;
		c &= 0xff00ff00;
		c |= b|r;
		palette[j] = c;
	}
	for(int i = 0 ; i < 0x800 ; i++)
				palette2[i]=palette[(0x6f9*i)/0x7ff];
	delete [] palette;	
}

void source(int hot[n_h*n_w],unsigned int n)
{	for(int xs=n_w/4 ; xs<=(3*n_w)/4 ; xs++)
		hot[ (3*n_h)/13*n_w + xs ] = max_hot;
	
	const int r = 50;
	const int w = 10;
	float t=( (n%6000)*2*pi ) / 6000;
	int xc = int( n_w/2 + ( n_w/2-2*r - 4 )*sin(t));
	int yc = 9*(n_h/13);
	int xp = int( xc + r*cos(w*t) );
	int yp = int( yc + r*sin(w*t) );
	hot[ yp*n_w + xp ] = max_hot;
	hot[ (yp+1)*n_w + xp ] = max_hot;
	hot[ (yp-1)*n_w + xp ] = max_hot;
	hot[ yp*n_w + xp + 1 ] = max_hot;
	hot[ yp*n_w + xp - 1 ] = max_hot;

	int nb=n%(2*n_h-4);
	hot[ (1 + nb/2)*n_w + (11*n_w)/13 ] = max_hot;
	hot[ (1 + nb/2)*n_w + 1 + (11*n_w)/13 ] = max_hot;
	hot[ (1 + nb/2)*n_w + 2 + (11*n_w)/13 ] = max_hot;
}		

void MemError()
{	cerr << "Probleme de memoire !" << endl;
	exit(1);
}
