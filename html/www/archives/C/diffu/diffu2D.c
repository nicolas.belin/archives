// compilation : g++ `sdl-config --cflags` -c %1.c
// linkage : g++ `sdl-config --libs` regul.o %1.o

#include <iostream>		// input/output avec la console
#include <SDL/SDL.h>	// API video, clavier, timer, souris...
#include <GL/gl.h>		// API carte 3D
#include <cmath>	// fonction sinux, cosinus...

using namespace std;

const float pi = 3.141592;

//-----------------------------------------------------------
//---------------------  GENERAL  ---------------------------
//-----------------------------------------------------------
void MemError();	// erreur d'allocation de memoire

//-----------------------------------------------------------
//---------------------  DIFFUSION  -------------------------
//-----------------------------------------------------------
const int n_w = 640;	// taille de la matrice d'elements finis
const int n_h = 512;
// nombre maximum pour eviter les debordements :
const unsigned int max_hot = ( ~(unsigned int)0x0 )>>2 ;
void dif_C(int hot1[n_h*n_w],int hot2[n_h*n_w]);	
extern "C" void dif_ASM(int a[n_h*n_w],int b[n_h*n_w]);
void source(int hot[n_h*n_w],unsigned int frame);//sources de chaleur

//-----------------------------------------------------------
//---------------------  COULEURS  --------------------------
//-----------------------------------------------------------
int palette2[0x800];
void Make_palette();
int *image;
extern "C" void copbuf_ASM(int a[n_w*n_h], int *c);

//------------------------------------------------------------
//-------------------  OPENGL et SDL  ------------------------
//------------------------------------------------------------ 
const int screen_w = 1280;
const int screen_h = 1024;
const unsigned int f_rate = 40;	// max frame rate 
float Near=1.5;
void init_GL();	// initialisation de opengl (parametres globaux)
void display2D( float zoom );

//------------------------------------------------------------
//---------------------  MAIN() ------------------------------
//------------------------------------------------------------
int main( int argc , char **argv )
{	Make_palette(); // fabrication de la palette des couleurs

	image = new int[n_w*n_h];
	if( image == NULL ) MemError();

	int *hot1 = new int[n_h*n_w];
	int *hot2 = new int[n_h*n_w];
	if( (hot1 == 0) || (hot1 == 0) ) MemError();
	for(int i=0;i<n_h*n_w;i++)
	{	hot1[i]=0;
		hot2[i]=0;
	}

	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	cerr	<< "Impossible d'initialiser SDL : " << SDL_GetError();
		exit(1);
	}
	atexit(SDL_Quit);

	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
									SDL_DEFAULT_REPEAT_INTERVAL);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
	SDL_Surface *screen;
	screen = SDL_SetVideoMode(screen_w,screen_h, 0,
									SDL_OPENGL|SDL_FULLSCREEN);
	if ( screen == NULL )
	{	cerr << "Impossible de passer en " << screen_w;
		cerr << "x" << screen_h << " en 32 bpp : ";
		cerr << SDL_GetError() << endl;
   	    exit(1);
	}

	init_GL();		

//-------------------------------------------------------------
//---------------------  representation 2D  -------------------
//-------------------------------------------------------------
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,screen_w,0,screen_h,-1,1);

	SDL_Event event;
	int frame = 0;
	int iter = 0;
	bool b = 0;
	Uint32 t0,t1,t2,t3;	// temps
	t0 = SDL_GetTicks();
	t2 = t0;
	float zoom = 1.0;
	bool loop = true;
	while(loop)
	{	if(b==0)
		{	source(hot1,iter);
			dif_ASM(hot1,hot2);
			if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	copbuf_ASM(hot2,image);
				display2D( zoom );
				t2 = SDL_GetTicks();
				frame++;
			}
		}
		else
		{	source(hot2,iter);
			dif_ASM(hot2,hot1);
			if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	copbuf_ASM(hot1,image);
				display2D( zoom );
				t2 = SDL_GetTicks();
				frame++;
			}
		 }
		b=!b;
		iter++;
		
		if( SDL_PollEvent(&event)==1 )
		{	switch(event.type)
			{	case SDL_MOUSEBUTTONDOWN :
					loop = false;	
					break;
				case SDL_KEYDOWN:
    				if(event.key.keysym.sym==SDLK_ESCAPE)
					{	loop=false;
					}
  					else if(event.key.keysym.sym==SDLK_UP)
					{	zoom *= 1.01010101;
					}
    				else if(event.key.keysym.sym==SDLK_DOWN)
					{	zoom *= 0.99;
					}
					break;						
			}
		}
	}

	delete [] hot2;
	delete [] hot1;

	t1 = SDL_GetTicks();

	SDL_Quit();

	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << iter << endl;
	cout << "Nombre de frames : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s
										<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*iter/t << endl;
	cout << "frames par seconde : " << 1000.0*frame/t << endl;

	return 0;
}

void init_GL()
{	glShadeModel(GL_FLAT);
	glDisable(GL_DEPTH_TEST);
	glClearColor(0,0,0,0);	// on efface avec du noir
}

void display2D( float zoom )
{	glClear(GL_COLOR_BUFFER_BIT);
	glDrawBuffer(GL_BACK);
	glPixelZoom(zoom,-zoom);
	glRasterPos2i( int( screen_w/2 - n_w*zoom/2 ) ,
										int( screen_h/2 + n_h*zoom/2 ) );
	glDrawPixels( n_w, n_h, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glFlush();
	SDL_GL_SwapBuffers();
}

void Make_palette()
{	int *palette = new int[0x6fa];	
	if( palette == NULL ) MemError();

	unsigned int c,i;
	for(i = 0; i<0x100 ; i++) palette[i]=i;
	c=0xff;
	for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
	c=0xffff;
	for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
	c=0xff00;
	for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
	c=0xffff00;
	for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
	c=0xff0000;
	for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
	c=0xff00ff;
	for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
	
	unsigned int b,r;
	for(int j=0 ; j<0x6fa ; j++)
	{	c = palette[j];
		b = ( 0xff & c ) << 16;
		r = ( 0xff0000 & c ) >> 16;
		c &= 0xff00ff00;
		c |= b|r;
		palette[j] = c;
	}
	for(int i = 0 ; i < 0x800 ; i++) palette2[i]=palette[(0x6f9*i)/0x7ff];
	delete [] palette;	
}

void source(int hot[n_h*n_w],unsigned int n)
{	for(int xs=n_w/4 ; xs<=(3*n_w)/4 ; xs++)
		hot[ (3*n_h)/13*n_w + xs ] = max_hot;
	
	const int r = 50;
	const int w = 10;
	float t=( (n%6000)*2*pi ) / 6000;
	int xc = int( n_w/2 + ( n_w/2-2*r - 4 )*sin(t));
	int yc = 9*(n_h/13);
	int xp = int( xc + r*cos(w*t) );
	int yp = int( yc + r*sin(w*t) );
	hot[ yp*n_w + xp ] = max_hot;
	hot[ (yp+1)*n_w + xp ] = max_hot;
	hot[ (yp-1)*n_w + xp ] = max_hot;
	hot[ yp*n_w + xp + 1 ] = max_hot;
	hot[ yp*n_w + xp - 1 ] = max_hot;

	int nb=n%(2*n_h-4);
	hot[ (1 + nb/2)*n_w + (11*n_w)/13 ] = max_hot;
	hot[ (1 + nb/2)*n_w + 1 + (11*n_w)/13 ] = max_hot;
	hot[ (1 + nb/2)*n_w + 2 + (11*n_w)/13 ] = max_hot;
}		

void MemError()
{	cerr << "Probleme de memoire !" << endl;
	exit(1);
}
