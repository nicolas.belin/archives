// compilation : g++ `sdl-config --cflags` -c %1.c
// assemblage : nasm -f elf regul.asm
// linkage : g++ `sdl-config --libs` regul.o %1.o

/********************************************************************************
Equation de la chaleur discretisee :

               C * T(x,y,t) + T(x+1,y,t) + T(x-1,y,t) + T(x,y+1,t) + T(x,y-1,t)
T(x,y,t+1) = --------------------------------------------------------------------
                                             C + 4

ou C=dx^2/dt=dy^2/dt est un nombre caracteristique de la vitesse de diffusion.
Dans ce programme, on prend C=0, i.e une vitesse "infinie" (maximum, en fait).
*********************************************************************************/

#include <iostream>	// entrees/sorties avec la console 
#include <SDL/SDL.h>	// API graphique (video)
#include <cmath>	// fonction sinux, cosinus...

#define screen_w 640	// screen width (largueur)
#define screen_h 480	// screen height (hauteur)

using namespace std;

const float PI =3.14159265359;
const unsigned int max_int = 0x7fffffff;	// nombre maximum	
const unsigned int max_bitmap = max_int/4;	// nombre maximum pour eviter les debordements
const unsigned int f_rate = 40;		// renouvellement de l'affichage/seconde

int palette2[2047];	

void Make_palette(int *palette);		// creation d'une premiere palette de 1786 couleurs
			
void Scale_palette(int *palette,int *palette2);	// Etirement de la palette precedente
						// pour avoir 2048=2^11 couleurs.
// C ou assembleur ?
#define dif dif_C
#define copbuf copbuf_C


void dif_C					// routine diffusion, version C
	(	int hot1[screen_h][screen_w],	// diffusion de hot1 dans hot2
		int hot2[screen_h][screen_w]
	);	
	
extern "C" void dif_ASM				// routine diffusion, version ASM
	(	int a[screen_h][screen_w],	// diffusion de a dans b
		int b[screen_h][screen_w]
	);

void copbuf_C
	(	int hot1[screen_h][screen_w],	// indexe hot1 dans la palette	
		Uint32 *bitmap			// couleurs et copie dans le memoire video
	);


extern "C" void copbuf_ASM			// idem, version ASM
	(	int a[screen_h][screen_w],	
		Uint32 *c
	);
	
void source					// genere les sources de chaleur
	(	int hot[screen_h][screen_w],	// dans hot1
		unsigned int frame		// variations calees sur le temps du programme
	);

// ********************************** main() ******************************************

int main()
{	if ( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0 ) 
	{	fprintf(stderr, "Impossible d'initialiser SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);
	
	SDL_Surface *screen;
	SDL_Event event;
	
	screen = SDL_SetVideoMode(screen_w, screen_h,32,SDL_SWSURFACE|SDL_DOUBLEBUF);
	if ( screen == NULL )
	{	cout<< "Impossible de passer en 640x480 en 32 bpp: "<< SDL_GetError();
        	exit(1);
	}
	Uint32 *bitmap;
	bitmap = (Uint32*)screen->pixels;	

	Uint32 t0,t1,t2,t3;	// temps
	unsigned int frame=0;	// compte les frames
	
	// couleurs
	int palette[1785];	
	Make_palette(palette);
	Scale_palette(palette,palette2);	
		
	int hot1[screen_h][screen_w];
	int hot2[screen_h][screen_w];
	for(int yf=0;yf<screen_h;yf++)for(int xf=0;xf<screen_w;xf++)
	{	hot1[yf][xf]=0;
		hot2[yf][xf]=0;
	}
	

	bool b=0;			
	t0 = SDL_GetTicks();
	t2=SDL_GetTicks();

	while(!(SDL_PollEvent(&event) && event.type ==SDL_MOUSEBUTTONDOWN))
	{	if(b==0)
		{	source(hot1,frame);
			dif(hot1,hot2);
			if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	copbuf(hot2,bitmap);
				SDL_Flip(screen);
				t2 = SDL_GetTicks();
			}
		}
		else
		{	source(hot2,frame);
			dif(hot2,hot1);
		 	if(SDL_GetTicks()-t2 > 1000/f_rate)
			{	copbuf(hot1,bitmap);
				SDL_Flip(screen);
				t2 = SDL_GetTicks();
			}
		 }
		b=!b;
		frame++;
	}		
	
	t1=SDL_GetTicks();
	int t=t1-t0;
	int ms=t%1000;
	int h=t/(60*60*1000);
	int mn=(t/(60*1000))%60;
	int s=(t/1000)%60;
	cout << "Nombre total d'iterations : " << frame << endl;
	cout << "Temps ecoule : " <<h<<" h : " <<mn<<" mn : "<<s<<" s : "<<ms<< " ms" << endl;
	cout << "iterations par seconde : " << 1000.0*frame/t << endl;
	
	//cin.get();
	SDL_Quit();
	return 0;
}


// ****************************** fonctions **********************************

void dif_C(int hot1[screen_h][screen_w]
		,int hot2[screen_h][screen_w])
{	for(int y=1;y<screen_h-1;y++)
	{	for(int x=1;x<screen_w-1;x++)
		{	hot2[y][x]=(hot1[y][x-1]+hot1[y][x+1]
				+hot1[y-1][x]+hot1[y+1][x])>>2;
		}
	}
}

void copbuf_C(int hot1[screen_h][screen_w],Uint32 *bitmap)
{	int *hot;
	hot = &hot1[0][0];
	for(int p = screen_w ; p < screen_w*(screen_h-2) ; p++)
		bitmap[p] = palette2[hot[p]/0x40000];
}

void Make_palette(int *palette)
{	int c,i;
	for(i = 0; i<0x100 ; i++) palette[i]=i;
	c=0xff;
	for(i = 0x100 ; i < 0x1ff ; i++){c+=0x100; palette[i]=c;}
	c=0xffff;
	for(i = 0x1ff ; i < 0x2fe ; i++){c-=0x1; palette[i]=c;}
	c=0xff00;
	for(i = 0x2fe ; i < 0x3fd ; i++){c+=0x10000; palette[i]=c;}
	c=0xffff00;
	for(i = 0x3fd ; i < 0x4fc ; i++){c-=0x100; palette[i]=c;}
	c=0xff0000;
	for(i = 0x4fc ; i < 0x5fb ; i++){c+=0x1; palette[i]=c;}
	c=0xff00ff;
	for(i = 0x5fb ; i < 0x6fa ; i++){c+=0x100; palette[i]=c;}
}

void Scale_palette(int *palette,int *palette2)
{	for(int i = 0 ; i < 0x800 ; i++)
	{	palette2[i]=palette[(0x6f9*i)/0x7ff];
	}
}

void source(int hot[screen_h][screen_w],unsigned int n)
{	for(int xs=screen_w/4 ; xs<=(3*screen_w)/4 ; xs++)
		hot[(3*screen_h)/13][xs]=max_bitmap;
	
	const int r = 50;
	const int w = 10;
	float t=((n%6000)*2*PI)/6000;
	int xc = int(screen_w/2+(screen_w/2-2*r-4)*sin(t));
	int yc = 9*(screen_h/13);
	int xp = int(xc+r*cos(w*t));
	int yp = int(yc+r*sin(w*t));
	hot[yp][xp]=max_bitmap;
	hot[yp+1][xp]=max_bitmap;
	hot[yp-1][xp]=max_bitmap;
	hot[yp][xp+1]=max_bitmap;
	hot[yp][xp-1]=max_bitmap;
	
	
	int nb=n%(2*screen_h-4);
	
	hot[1+nb/2][(11*screen_w)/13]=max_bitmap;
	hot[1+nb/2][1+(11*screen_w)/13]=max_bitmap;
	hot[1+nb/2][2+(11*screen_w)/13]=max_bitmap;
}		

/* *********** La fonction dif_C optimisee par g++ -O :
	  
	  55                push ebp
	  89E5              mov ebp,esp
	  57                push edi
	  56                push esi
	  53                push ebx
	  83EC08            sub esp,byte +0x8
	  8B7508            mov esi,[ebp+0x8]		; esi pointe sur la source
	  C745F001000000    mov dword [ebp-0x10],0x1    ; [ebp-0x10]= ligne

00000F51  BB01000000        mov ebx,0x1 		; numero colonne : ebx 
	  8B55F0            mov edx,[ebp-0x10]
	  8D0492            lea eax,[edx+edx*4]
	  89C1              mov ecx,eax
	  C1E107            shl ecx,0x7
	  894DEC            mov [ebp-0x14],ecx
	  89C7              mov edi,eax
	  C1E709            shl edi,0x9

00000F69  8B45EC            mov eax,[ebp-0x14]
	  01D8              add eax,ebx
	  8B5486FC          mov edx,[esi+eax*4-0x4]
	  03548604          add edx,[esi+eax*4+0x4]
	  8D0C9F            lea ecx,[edi+ebx*4]
	  03943100F6FFFF    add edx,[ecx+esi+0xfffff600]
	  039431000A0000    add edx,[ecx+esi+0xa00]
	  C1FA02            sar edx,0x2
	  8B4D0C            mov ecx,[ebp+0xc]		; ici, ecx pointe sur la dest.
	  891481            mov [ecx+eax*4],edx
	  43                inc ebx
	  81FB7E020000      cmp ebx,0x27e
	  7ED0              jng 0xf69

	  FF45F0            inc dword [ebp-0x10]
	  817DF0DE010000    cmp dword [ebp-0x10],0x1de
	  7EAC              jng 0xf51

	  83C408            add esp,byte +0x8
	  5B                pop ebx
	  5E                pop esi
	  5F                pop edi
	  5D                pop ebp
	  C3                ret
*/

