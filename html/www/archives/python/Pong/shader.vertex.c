#version 100

precision mediump float;

attribute vec2 a_position;
attribute vec4 a_color;

uniform vec4 u_window;
uniform float u_z;
uniform vec2 u_translation;
//uniform float u_angle;

varying vec4 v_color;

void main() {
  float l = u_window.x; // left
  float r = u_window.y; // right
  float b = u_window.z; // bottom
  float t = u_window.w; // top

  float x = a_position.x;
  float y = a_position.y;
  float z = u_z;
    
  x += u_translation.x;
  y += u_translation.y;

  x = 2.0 * ( x - l ) / ( r - l ) - 1.0;
  y = 2.0 * ( y - b ) / ( t - b ) - 1.0;
  
  gl_Position = vec4( x, y, z, 1.0 );
  v_color = a_color;
}
