info_touches ="""
Touches :
q :               Quitter
f :               Plein écran
Ctl/Alt gauches : Palet du haut
Ctl/Alt droits :  Palet du bas
"""

import math
import random
from interface_élève import *

def construit_fond(inter):
    def bandeh(y):
        bande_couleur = (0, 0, 64)
        d = 0.2
        return [{'genre':'triangle', 'A':(-7., y+d), 'B':(-7., y-d), 'C':(7., y+d),
                 'couleur':bande_couleur},
                {'genre':'triangle', 'A':(7., y+d), 'B':(7., y-d), 'C':(-7., y-d),
                 'couleur':bande_couleur}]

    def bandev(x):
        bande_couleur = (0, 0, 64)
        d = 0.2
        return [{'genre':'triangle', 'A':(x+d, -4.), 'B':(x-d, -4.), 'C':(x+d, 4.),
                 'couleur':bande_couleur},
                {'genre':'triangle', 'A':(x+d, 4.), 'B':(x-d, 4.), 'C':(x-d, -4.),
                 'couleur':bande_couleur}]

    inter.couleur_initiale(0, 0, 50)

    fond_a = ['fond']
    for i in range(-4, 5):
        fond_a += bandeh(i)
    for i in range(-7, 8):
        fond_a += bandev(i)

    return inter.nouvel_objet(fond_a)

def construit_palets(inter):
    lr = 1.     # demi-longueur
    er = 0.18   # demi-épaisseur
    c = (255, 255, 255, 150)
    palet_a = ('palet_haut',
               {'genre':'triangle', 'A':(-lr, -er), 'B':(lr, -er), 'C':(lr, er),
                'couleur': c},
               {'genre':'triangle', 'A':(-lr, er), 'B':(lr, er), 'C':(-lr, -er),
                'couleur': c})

    haut = inter.nouvel_objet(palet_a)
    haut.l = lr
    haut.e = er
    haut.s = -1.0

    bas = inter.copie_objet(haut, 'palet_bas')
    bas.l = lr
    bas.e = er
    bas.s = 1.0

    touches = {'ctl_gauche':{'scancode':37, 'état':False},
               'alt_gauche':{'scancode':64, 'état':False},
               'ctl_droit':{'scancode':108, 'état':False},
               'alt_droit':{'scancode':105, 'état':False}}

    def action_touche(nom):
        def act(t):
            touches[nom]['état'] = t
        return act

    for nom in touches:
        inter.sdl.register_key(action_touche(nom),
                               scancode = touches[nom]['scancode']) 
    haut.gauche, haut.droite = touches['ctl_gauche'], touches['alt_gauche']
    bas.gauche, bas.droite = touches['ctl_droit'], touches['alt_droit']
    return haut, bas

def construit_bille(inter):
    rb = 0.2    # rayon
    nb = 20     # nombre de triangles
    bille_a = ['bille']
    angle = lambda i: i/nb*math.pi*2.0
    for i in range(nb):
        bille_a.append({'genre':'triangle',
                        'A':(0., 0.), 'B':(rb*math.cos(angle(i)), rb*math.sin(angle(i))),
                        'C':(rb*math.cos(angle(i+1)), rb*math.sin(angle(i+1))),
                        'couleur': (255, 255, 255, 150)})
    bl = interface.nouvel_objet(bille_a)
    bl.r = rb
    bl.v_0 = 1.5
    bl.v_10 = 4.0
    return bl

def initialise_objets(pth, ptb, bl):
    pth.x = random.uniform(-7.0, 7.0)
    pth.y = 4.0 - 2.0 * pth.e
    pth.vx = 0.0
    pth.cible_x = None
    pth.auto = True
    pth.position(pth.x, pth.y)

    ptb.x = random.uniform(-7.0, 7.0)
    ptb.y = -4.0 + 2.0 * ptb.e
    ptb.vx = 0.0
    ptb.cible_x = None
    ptb.auto = True
    ptb.position(ptb.x, ptb.y)

    pth.auto_droite, pth.auto_gauche = False, False    
    ptb.auto_droite, ptb.auto_gauche = False, False    

    x = random.uniform(-7.0 / 2.0, 7.0 / 2.0)
    y = 4.0 * (random.randint(0, 1) * 2 - 1)
    l = math.sqrt(x * x + y * y)
    v0 = bl.v_0
    bl.vx, bl.vy = v0 * x / l, v0 * y / l
    bl.x, bl.y = 0.0, 0.0
    bl.n = 0
    
def anime_palet(pt, dt):
    if pt.gauche['état'] or pt.auto_gauche:
        pt.vx -= 5.0 * dt
    if pt.droite['état'] or pt.auto_droite:
        pt.vx += 5.0 * dt
    pt.vx -= pt.vx * dt
    pt.x += pt.vx * dt
    if pt.x > 7.0 - pt.l :
        pt.x = 7.0 - pt.l
        pt.vx = 0.0
    if pt.x < -7.0 + pt.l:
        pt.x = -7.0 + pt.l
        pt.vx = 0.0
    pt.position(pt.x, pt.y)

def anime_bille(bl,  dt):
    bl.x += bl.vx * dt
    bl.y += bl.vy * dt
    """
    if bl.y >= 4.0 - bl.r:
        bl.y = 2.0 * (4.0 - bl.r) - bl.y
        bl.vy *= -1.0
    elif bl.y <= -4.0 + bl.r:
        bl.y = 2.0 * (bl.r - 4.0) - bl.y
        bl.vy *= -1.0
    """
    if bl.y >= 4.0 - bl.r or  bl.y <= -4.0 + bl.r:
        perdu(bl)
    if bl.x >= 7.0 - bl.r:
        bl.x = 2.0 * (7.0 - bl.r) - bl.x
        bl.vx *= -1.0
    elif bl.x <= -7.0 + bl.r:
        bl.x = 2.0 * (-7.0 + bl.r) - bl.x
        bl.vx *= -1.0

def interactions_bille_palet(bl, pt):
    if pt.s * (bl.y - pt.y) <= bl.r + pt.e:
        if -bl.r - pt.l <= pt.x - bl.x <= bl.r + pt.l:
            if - pt.l <= pt.x - bl.x <= pt.l:
                bl.vy *= -1.0
                bl.y = 2.0 * (pt.y + pt.s * (pt.e + bl.r)) - bl.y
            else:
                if pt.e > pt.y - bl.y > -pt.e:
                    bl.vx *= -1.0
                    if bl.x > pt.x + pt.l:
                        bl.x = 2.0 * (pt.x + pt.l + bl.r) - bl.x 
                    else:
                        bl.x = 2.0 * (pt.x - pt.l - bl.r) - bl.x 
                else:
                    if bl.x > pt.x + pt.l:
                        xc = pt.x + pt.l
                    else: # bl.x < pt.x - pt.l
                        xc = pt.x - pt.l
                    yc = pt.y + pt.s * pt.e
                    dx, dy = bl.x - xc, bl.y - yc
                    if dx * dx + dy * dy < bl.r * bl.r:
                        vx, vy = bl.vx, bl.vy
                        v2 = vx * vx + vy * vy
                        s = (dx * vx + dy * vy) / v2
                        t = bl.r * bl.r - dx * dx - dy * dy
                        t /= v2
                        t += s * s
                        t = math.sqrt(t) + s
                        dx -= vx * t
                        dy -= vy * t
                        ux, uy = dx / bl.r, dy / bl.r
                        a = uy * uy - ux * ux
                        b = -2.0 * ux * uy
                        bl.vx, bl.vy = a * vx + b * vy, b * vx - a * vy
                        bl.x, bl.y = xc + dx + bl.vx * t, yc + dy + bl.vy * t
                
    bl.position(bl.x, bl.y)
    
def auto(pt, bl):
    if not pt.auto:
        return
    if pt.droite['état'] or pt.gauche['état']:
        pt.auto, pt.auto_gauche, pt.auto_droite = False, False, False
        return
    if pt.cible_x == None:
        if bl.vy * pt.s >= 0.0:
            pt.cible_x = 0.0
        else:
            x = bl.x + bl.vx * (pt.y + pt.s * pt.e - bl.y) / bl.vy
            r, n = math.modf((x + 7.0) / 14.0)
            n = int(n)
            if r < 0.0:
                r += 1.0
                n -= 1
            nx = r * 14.0 - 7.0
            if n % 2 == 1:
                nx *= -1.0
            pt.cible_x = nx
    if -14.0/100.0 < pt.x - pt.cible_x < 14.0/100.0:
        pt.auto_droite = False
        pt.auto_gauche = False
    elif pt.x > pt.cible_x:
        pt.auto_droite = False
        pt.auto_gauche = True
    else:
        pt.auto_droite = True
        pt.auto_gauche = False

def rebond(bl, pth, ptb):
    bl.n += 1
    pth.cible_x = None
    ptb.cible_x = None
    vx, vy = bl.vx, bl.vy
    v = math.sqrt(vx * vx + vy * vy)
    nv = bl.v_0 + math.sqrt(bl.n / 10) * (bl.v_10 - bl.v_0)
    bl.vx = nv * vx / v
    bl.vy = nv * vy / v
        
def anime(t0, t1):
    dt = (t0 - t1) / 1000.0
    auto(palet_haut, bille)
    auto(palet_bas, bille)
    anime_palet(palet_haut, dt)
    anime_palet(palet_bas, dt)
    anime_bille(bille, dt)
    vx1, vy1 = bille.vx, bille.vy
    interactions_bille_palet(bille, palet_haut)
    interactions_bille_palet(bille, palet_bas)
    if vx1 != bille.vx or vy1 != bille.vy:
        rebond(bille, palet_haut, palet_bas)
    
def perdu(bl):
    print("\n")
    if bl.y > 0:
        print("La bille est sortie par le haut !")
    else:
        print("La bille est sortie par le bas !")
    print("* Nombre de rebonds : " + str(bl.n))
    print("* Vitesse finale : {0:.2f}".format(math.sqrt(bl.vx * bl.vx + bl.vy * bl.vy)))
    initialise_objets(palet_haut, palet_bas, bille)

print(info_touches)
random.seed()
interface = Interface(-7., 7., -4., 4., 'Pong')
fond = construit_fond(interface)
palet_haut, palet_bas = construit_palets(interface)
bille = construit_bille(interface)
initialise_objets(palet_haut, palet_bas, bille)

interface.fonction_par_image(anime)

interface.sdl.loop()
interface.sdl.quit()



