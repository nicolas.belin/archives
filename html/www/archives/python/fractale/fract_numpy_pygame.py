import numpy as np
import pygame

def render(w, h):
    xmin = -2.1
    ymin = -1.5
    xmax = 0.9
    ymax = 1.5
    nmax = 500
    x = np.linspace(xmin, xmax, w)
    y = np.linspace(ymin, ymax, h).reshape(h,1)
    c = (x+y*1j).reshape(h*w)
    z = np.zeros(c.shape, dtype=complex)
    r = np.zeros(c.shape, dtype=int)
    d = (r == 0)
    for i in range(nmax):
        z[d] = z[d]**2 + c[d]
        nd = ( z*np.conj(z) > 4 ) & d
        r[nd] = i
        d = ~nd & d
    
    rgb = np.array([ 1, 5, 10 ], dtype=int)
    r.resize(w*h, 1)
    rgb = rgb * r
    rgb.resize(h,w,3)
    return rgb % 256
 
w = 500
h = 400

pygame.display.init()
screen = pygame.display.set_mode([w, h])

#pixels = pygame.PixelArray(screen)
#pixels[:] = 0xFF00FF
#pixels.close()

array = render(w, h)
array = np.transpose(array, (1, 0, 2))
pygame.surfarray.blit_array(screen, array)

pygame.display.flip()


loop = True
while loop == True:
    ev = pygame.event.wait()
    if ev.type == pygame.QUIT:
        loop = False
    elif ev.type == pygame.MOUSEBUTTONDOWN:
        pass
    
pygame.quit()
