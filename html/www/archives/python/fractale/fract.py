import pygame
import math              
import threading

def point(a):
    u = 0.0 + 0.0*1j
    n = 0
    while abs(u) < 2 and n < nmax:
        u = u*u + a
        n += 1
    if n == nmax:   # convergence
        return 0x000000
    else:           # divergence :
        return pygame.Color(n%256, (5*n)%256, (10*n)%256)

def stop_render():
    global rendering
    if render_thread.is_alive():
        rendering = False
        render_thread.join()
    
def render(surf, rect):
    xmin = rect['xmin']
    ymin = rect['ymin']
    xmax = rect['xmax']
    ymax = rect['ymax']
    w, h = surf.get_size()
    if w * (ymax - ymin) > h * (xmax - xmin):
        xmin = (xmin + xmax) / 2 - w / h * (ymax - ymin) / 2
        xmax = xmin + w / h * (ymax - ymin)
    else:
        ymin = (ymin + ymax) / 2 - h / w * (xmax - xmin) / 2
        ymax = ymin + h / w * (xmax - xmin)
        
    p = 1
    for z in range(math.floor(math.log2(max(w, h))), -1, -1):
        colors = []
        d = 2 ** z
        for y in range(0, h, d):
            p = 1 - p
            if not rendering:
                break
            for x in range(p * d, w, (1 + p) * d):
                colors.append(point(xmin + x*(xmax-xmin)/w + (ymin + y*(ymax-ymin)/h) * 1j))
        if rendering:
            ev = pygame.event.Event(pygame.USEREVENT, utype='draw', z=z, w=w, h=h, colors=colors )
            pygame.event.post(ev)
            p = 0
        else:
            break
    if z == 0:
        pygame.time.set_timer(pygame.USEREVENT+1, 30000)
        
width = 400
height = 400

area_index = 0
areas = [{"xmin":-2.1,"xmax":0.9,"ymin":-1.5,"ymax":1.5,"nmax":500},
         {"xmin":-0.66563,"xmax":-0.42656,"ymin":-0.73594,"ymax":-0.49687,"nmax":500},
         {"xmin":-1.82748,"xmax":-1.69336,"ymin":-0.06589,"ymax":0.06723,"nmax":500},
         {"xmin":-0.55925929,"xmax":-0.559218592,"ymin":-0.643894062,"ymax":-0.643858307,"nmax":1000},
         {"xmin":-0.559255823585,"xmax":-0.559255769174,"ymin":-0.643875237782,"ymax":-0.643875183983,"nmax":2000}]

pygame.display.init()
screen = pygame.display.set_mode((width, height), pygame.RESIZABLE)
pygame.display.set_caption('Mandelbrot')
pygame.mouse.set_visible(False)
    
pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype='start_render'))


loop = True
is_fullscreen = False
while loop:
    ev = pygame.event.wait()
    if ev.type == pygame.QUIT:
        stop_render()
        loop = False
    elif ev.type == pygame.KEYDOWN:
        if ev.unicode == 'f':
            if is_fullscreen:
                is_fullscreen = False
                stop_render()
                rendering = True
                screen = pygame.display.set_mode((w_old,h_old), pygame.RESIZABLE)
                pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype='start_render' ))

            else:
                is_fullscreen = True
                stop_render()
                w_old, h_old = screen.get_size()
                rendering = True
                screen = pygame.display.set_mode(pygame.display.list_modes(0,pygame.FULLSCREEN)[0], pygame.FULLSCREEN)
                pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype='start_render' ))
        elif ev.unicode == 'q':
            pygame.event.post(pygame.event.Event(pygame.QUIT))
    elif ev.type == pygame.USEREVENT:
        if ev.utype == 'draw':
            p = 0
            i = 0
            d = 2 ** ev.z
            for y in range(0, ev.h, d):
                p = 1 - p
                for x in range(p * d, ev.w, (1 + p) * d):
                    screen.fill(ev.colors[i], (x, y, x+d, y+d))
                    i += 1

            pygame.display.flip()
        elif ev.utype == 'start_render':
            global nmax
            nmax = areas[area_index]['nmax']
            rendering = True
            render_thread = threading.Thread(target = render, args = (screen, areas[area_index]), daemon = True)
            render_thread.start()
            
    elif ev.type == pygame.USEREVENT+1:
        area_index = (area_index + 1) % len(areas)
        pygame.time.set_timer(pygame.USEREVENT+1, 0)
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype='start_render' ))
        
    elif ev.type == pygame.VIDEORESIZE:
        stop_render()
        screen = pygame.display.set_mode(ev.size, pygame.RESIZABLE)
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype='start_render' ))
    
pygame.quit()

