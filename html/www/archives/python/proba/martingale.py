import random

def play(x:int):
    # joue avec une mise x
    if random.randint(0,1) == 0:
        return 2*x
    return 0

def un_par_victoire(d:int, n:int):
    # gain de 1 par martingale
    return d + 1

def un_par_jeu(d, n):
    # gain de 1 par jeu
    return d + n + 1

#mise = un_par_victoire
mise = un_par_jeu

def martingale():
    d = 0.0 # débit
    m = 1.0 # mise
    n = 0 # nombre de partie
    g = 0.0 # gain
    perdue = True
    while perdue:
        g = play(m)
        d += m
        n += 1
        if (g == 0):
            m = mise(d, n)
        else:
            perdue = False
    return [n, d, g-d]

D = 0
N = 0
G = 0
for i in range(100):
    m = martingale()
    N += m[0]
    G += m[2]
    D = max(D, m[1])
    
print('Nombre de parties :{0}\n Débit maximum :{1}\nGain total :{2}'.format(N, D, G))
