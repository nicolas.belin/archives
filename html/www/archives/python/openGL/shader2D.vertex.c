#version 100

precision mediump float;

attribute vec2 a_position;
attribute vec4 a_color;

uniform vec4 u_window;
uniform float u_z;
uniform vec2 u_translation;
uniform float u_angle;

varying vec4 v_color;

void main() {
  float l = u_window.x; // left
  float r = u_window.y; // right
  float b = u_window.z; // bottom
  float t = u_window.w; // top
  vec2 p = a_position;
  float z = u_z;
    
  mat2 R;
  vec2 c = vec2(cos(u_angle), sin(u_angle));
  R[0] = c;
  R[1] = vec2(-c.y, c.x);
  p = R * p + u_translation;

  float x = 2.0 * ( p.x - l ) / ( r - l ) - 1.0;
  float y = 2.0 * ( p.y - b ) / ( t - b ) - 1.0;

  gl_Position = vec4( x, y, z, 1.0 );
  v_color = a_color;
}
