
from interface_prof import Sdl, Wgl, GLchain, VAO, VBO, Uniform

class Objet:
    def __init__(self, si, a, vbo = None):
        self.si = si
        self.sdl = si.sdl
        self.wgl = si.wgl
        self.name = a[0]
        if not vbo:
            self.vbo = self.array2vbo(a)
        else:
            self.vbo = vbo
        
        tri_vao = VAO(self.name + "_triangles", si.chain, 'triangles')
        tri_vao.add_vbo(self.vbo)
        tri_vao.add_uniform(si.win_u)

        #z_u = Uniform('u_z', 'float')
        #z = 0.0 if 'z' not in d else d['z']
        #z_u.set(self.wgl.float.conv((z,)))
        #tri_vao.add_uniform(z_u)
        
        self.pos_xy = self.wgl.float.conv((0., 0.))
        translation_u = Uniform('u_translation', 'vec2')
        translation_u.set(self.pos_xy)
        tri_vao.add_uniform(translation_u)
        si.chain.add_vao(tri_vao)

    def position(self, x, y):
        self.pos_xy[0] = x
        self.pos_xy[1] = y

    def array2vbo(self, a):
        triangles = []
        for i in range(1, len(a)):
            d = a[i]
            otype = d['genre']
            if otype == 'triangle':
                color = d['couleur']
                if len(color) == 3:
                    color = (*color, 255)
                triangles.append((d['A'], color))
                triangles.append((d['B'], color))
                triangles.append((d['C'], color))
            else:
                print("Objet '" + name + "' contient un élément de genre inconnu :'" + otype + "'")
                self.sdl.quit()
        vbo_a = VBO('attributes')
        vbo_a.add_attribute('a_position', 'float', 2)
        vbo_a.add_attribute('a_color', 'ubyte', 4)
        vbo_a.load(triangles)
        return vbo_a
        
    def dup(self, name):
        return Objet(self.si, (name,), vbo = self.vbo)
        
class Interface:
    frame_rate = 60 # rafraichissement par seconde
    frame = 0
    
    def __init__(self, xmin, xmax, ymin, ymax, title):
        self.sdl = Sdl(xmin, xmax, ymin, ymax, title)
        self.wgl = Wgl(self.sdl)

        chain_d = {'name':'bare',
          'vertex_shader_filename':'shader.vertex.c',
          'fragment_shader_filename':'shader.fragment.c'}
        self.chain = GLchain(self.wgl, chain_d)
        if not self.chain.is_ready:
            sdl.quit()
        self.wgl.add_chain(self.chain)

        self.win_u = Uniform('u_window', 'vec4')
        self.win_u.set(self.wgl.float.conv((xmin, xmax, ymin, ymax)))

        self.sdl.register_drawing(self.wgl)
        self.t_init = self.sdl.get_time()

    def couleur_initiale(self, r, g, b, a = 255):
        self.wgl.set_clear_color(r/255, g/255, b/255, a/255)
        
    def nouvel_objet(self, a):
        return Objet(self, a)

    def copie_objet(self, objet, nouveau_nom):
        return objet.dup(nouveau_nom)
    
    def fonction_par_image(self, f):
        
        def action(t0, t1):
            f(t0, t1)
            """
            self.frame += 1;
            if self.frame % 900 == 0:
                print(str(self.frame * 1000.0 / (self.sdl.get_time() - self.t_init)))
            """
            self.sdl.refresh()

        self.sdl.set_timer(action, 1000 // self.frame_rate)

    
