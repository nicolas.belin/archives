#version 100

precision mediump float;

attribute vec2 a_position;
attribute vec4 a_color;

uniform vec4 u_fullcrum;

varying vec4 v_color;

void main() {

  gl_Position = vec4( x, y, z, 1.0 );
  v_color = a_color;
}
