var w, h, xmin, xmax, ymin, ymax;
var nmax = NaN; // set by a message from the main thread

function color_component(c) {
    var s = "";
    if ( c<16 )
        s += "0";
    s += c.toString(16);
    return s;
}

function get_color( ax, ay ) {
    var a = xmin + ax * ( xmax-xmin ) / w;
    var b = ymin + ay * ( ymax-ymin ) / h;
    var X1, X = 0;
    var Y1, Y = 0;
    var n = 0;
    while ( X*X + Y*Y < 4 ) {
        X1 = X*X - Y*Y + a;
        Y1 = 2*X*Y + b;
        X = X1;
        Y = Y1;
        n++;
        if ( n == nmax ) // convergence
            return "000000";
    }
    // divergence :
    var s = "";
    s += color_component( (n) % 0x100 );
    s += color_component( (5*n) % 0x100 );
    s += color_component( (10*n) % 0x100 );
    return s;
}

function render(){
    var x = 0;
    var y = 0;
    var str = "";
    var i = 0;
    do {
        str +=  get_color( x, y );
        i++;
        if ( i >= 300 ){
            postMessage( str );
            str = "";
            i = 0;
        }
        x++;
        if ( x >= w ){
            y++;
            x = 0;
        }
    }
    while ( y < h );
    if ( str.length > 0 ) {
        postMessage( str );
    }
    postMessage( "1" );
}

function entry(event) {
    var data = event.data;
    switch( data.cmd ) {
    case "start" :
        xmin = data.rect.xmin;
        xmax = data.rect.xmax;
        ymin = data.rect.ymin;
        ymax = data.rect.ymax;
        w = data.w;
        h = data.h;
        render();
        break;
    case "change_nmax" :
        nmax = data.nmax;
        break;
    }
}

self.addEventListener('message', entry );
