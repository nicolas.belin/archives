"use strict";

var wgl;
var mouse = {};
var aim = { theta:0, phi:0 };

function main() {

    function $(e) {
        return document.getElementById(e);
    }

    var width = Math.round(
        0.8 * Math.min(
            window.innerWidth, window.innerHeight*4/3 ) );
    var canvas, vertices, shader, coords, indexes, chain;
    var vBuffer, iBuffer, nBuffer, normals;
    var i, l, lk, d, m, j, k, gl;
    
    $('second_col').style.width = width + 'px'; 

    canvas = $("canvas_3d");
    canvas.width = width;
    canvas.height = Math.round( width*3/4 );

    wgl = new WebGL(canvas);
    gl = wgl.gl;
    if ( ! gl ){
        $('no_webgl').hidden = null;
        return;
    }
    gl.cullFace( gl.BACK );
    gl.enable( gl.CULL_FACE );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    gl.clear( gl.COLOR_BUFFER_BIT );
    wgl.resize();
    
    shader = new Shader( 'Basic', wgl );
    if ( ! shader.make( util.loaded['shader.vertex.c'],
                        util.loaded['shader.fragment.c'] ) ) {
        return;
    }
    delete util.loaded;

    chain = new Chain(shader);
    chain.addUniform( 'u_projection',
                      wgl.fustrum( 40.0, wgl.w / wgl.h,
                                    3.0, 2.5, -10.0 ) );
    wgl.camMat = wgl.camera( -0.5, aim.theta, aim.phi );
    chain.addUniform( 'u_camera', wgl.camMat );

    vertices = sphere.make(12);
    l = vertices.length;
    var a = 0.3;

    for ( i=0; i<l; i++ ){
        sphere.glide( vertices[i],
                      { p:function( x, y, z ) {
                          return x*x + 3*y*y + 2*z*z - 0.8; },
                        x:function( x, y, z ) {
                        return 2*x; },
                        y:function( x, y, z ) {
                            return 6*y; },
                        z:function( x, y, z ) {
                            return 4*z; } } );
    }
    
    coords = [];
    for ( i=0; i<l; i++ ){
        coords.push( vertices[i].x );
        coords.push( vertices[i].y );
        coords.push( vertices[i].z );
    }
    vBuffer = wgl.buffer( coords, 'attributes', 3 );
    chain.addAttribute( 'a_position', vBuffer );
/*
    normals = [];
    for ( i=0; i<l; i++ ){
        normals.push( vertices[i].nx );
        normals.push( vertices[i].ny );
        normals.push( vertices[i].nz );
    }
    nBuffer = wgl.buffer( normals, 'attributes', 3 );
    chain.addAttribute( 'a_normal', nBuffer );
*/
    indexes = [];
    for ( i=0; i<l; i++ ){
        lk = vertices[i].lk;
        d = lk.length;
        for ( k=0; k<d; k++ ){
            j = lk[k].index;
            m = lk[ (k+1) % d ].index;
            if ( j>i && m>i ){
                indexes.push(i);
                indexes.push(j);
                indexes.push(m);
            }
        }
    }
    iBuffer = wgl.buffer( indexes, 'indexes' );
    chain.addIndexes(iBuffer);
    chain.setPrimitiveType('triangles');
    
    window.requestAnimationFrame =
        window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;
    
    window.requestAnimationFrame(
        getDraw(
            chain.render.bind(chain), performance.now() ) );
    window.addEventListener( 'resize', resize, false );
    canvas.addEventListener(
        'mouseup', stopTrackingMouse, false );
    canvas.addEventListener(
        'mouseout', stopTrackingMouse, false );
    canvas.addEventListener(
        'mousedown', clickingOnCanvas, false );
}

function getDraw( render, prevTime ) {
    return function draw(t) {
        aim.theta += ( t - prevTime ) / 100;
        aim.phi += ( t - prevTime ) / 300;
        wgl.camera( -0.5, aim.theta, aim.phi, wgl.camMat );
        prevTime = t;
        wgl.gl.clear( wgl.gl.COLOR_BUFFER_BIT );
        render();
        window.requestAnimationFrame(draw);
    };
}

function mouseTracking(event) {
    var rect = wgl.canvas.getBoundingClientRect();
    var x = Math.round( event.clientX - rect.left );
    var y = Math.round( event.clientY - rect.top );
    if ( mouse.x !== undefined ){
        aim.phi += x - mouse.x;
        aim.theta += y - mouse.y;
    }
    mouse.x = x;
    mouse.y = y;
}

function resize(event){
    wgl.resize();
}

function stopTrackingMouse(event){
    wgl.canvas.removeEventListener(
        'mousemove', mouseTracking, false );
}

function clickingOnCanvas(event) {
    switch ( event.button ){
    case 0 : // left button
        delete mouse.x;
        wgl.canvas.addEventListener( 'mousemove', mouseTracking, false );
        break;
    case 2 : // right button
        break;
    }
}
