#version 100

precision mediump float;

attribute vec3 a_position;
//attribute vec3 a_normal;

uniform mat4 u_projection;
uniform mat4 u_camera;

varying vec3 v_pos_abs;
//varying vec3 v_pos_rel;
//varying vec3 v_normal;

void main() {
  vec4 m = vec4( a_position, 1.0 );
  m = u_camera * m;
  gl_Position = u_projection * m;
  v_pos_abs = a_position;

  /*
  vec4 n = vec4( a_normal, 0.0 );
  n = u_camera * n;
  v_normal = n.xyz;
  */
}
