#version 100
precision mediump float;

varying vec3 v_pos_abs;
//varying vec3 v_pos_rel;
//varying vec3 v_normal;

void main() {
  /*
  vec3 m = v_pos_rel - vec3( 0.0, 0.0, 3.0 );
  vec3 n = normalize(v_normal);
  m = reflect( m, n );
  m = normalize(m);
  float z = exp( m.z - 1.0 );
  float a = abs( m.x/z );
  float b = abs( m.y/z );
  if ( a < 1.0 && b < 1.0 ) {
    a = 1.1;
  }
  else {
    a = 1.0;
  }
  */
  
  vec3 p = v_pos_abs * 3.0;
  float s = sin( p.x * 3.0 );
  p.x *= p.y;
  p *= 2.0;
  p.z = p.z * s;
  p *= p;
  p.y /= ( 1.0 + p.z );
  p.x = clamp( p.x, 0.0, 1.0 );
  p.y = clamp( p.y, 0.0, 1.0 );
  p.z = clamp( p.z, 0.0, 1.0 );
  p = vec3(1.0) - p;
  gl_FragColor = vec4 ( p, 1.0 );
}
