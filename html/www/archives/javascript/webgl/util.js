"use strict";

var util = {};

util.preloadTextFiles = function(doAfter) {
    this.doAfter = doAfter;
    this.toBeLoaded =
        Array.prototype.slice.call( arguments, 1 );
    this.loaded = {};
    this.q = new XMLHttpRequest();
    this.q.overrideMimeType('text/plain');
    this.q.addEventListener( 'load', this );
    this.loadNext();
};

util.loadNext = function() {
    if ( this.toBeLoaded[0] ) {
        this.q.open( 'get', this.toBeLoaded[0], true );
        this.q.send();
    }
    else {
        this.doAfter();
    }
};

util.handleEvent = function(event) {
    switch(event.type) {
    case 'load':
        this.loaded[ this.toBeLoaded.shift() ] =
            this.q.responseText;
        this.loadNext();
        break;
    }
};
