#version 100

precision mediump float;

attribute vec2 a_position;

uniform vec4 u_frame;
uniform vec4 u_color;

varying vec4 v_color;

void main() {
  vec2 m = a_position;
  vec4 f = u_frame;

  m.x -= ( f[1] + f[0] ) / 2.0;
  m.y -= ( f[3] + f[2] ) / 2.0;
  m.x *= 2.0 / ( f[1] - f[0] ); 
  m.y *= 2.0 / ( f[3] - f[2] ); 

  gl_Position = vec4( m, -0.9, 1.0 );
  v_color = u_color;
}
