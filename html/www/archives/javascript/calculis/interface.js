"use strict";

function Interface() {
    var i, s, l, n, h, e;

    this.devMode = true;
    this.zIndexMax = 10;
    
    switch( navigator.language.substr( 0, 2 ) ) {
    case 'fr':
        this.lang = 'fr';
        break;
    default:
        this.lang = 'eng';
        break;
    }
    core.send(
        { cmd:'set', comma:this.getText('comma'),
          sep:this.getText('separator') } );
    for ( i in this.text ) {
        if ( this.text.hasOwnProperty(i) &&
             this.text[i].type === 'TITLE' ) {
            $(i).setAttribute( 'title', this.text[i][this.lang] );
        }
    }
    console.info( "User Agent : " + window.navigator.userAgent );
    if ( window.navigator.userAgent.indexOf("Firefox") != -1 ) {
        this.usingMathJax = false;
        console.info( "Math renderer : native MathML" );
    }
    else {
        h = document.getElementsByTagName("head")[0];         
        e = document.createElement('script');
        e.type = 'text/javascript';
        e.src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_HTMLorMML';
        e.onload = function() {
            if ( MathJax === undefined ) {
                inter.usingMathJax = false;
                console.info ( "Math renderer : MathJax unavailable," +
                           " backing to native mathML..." );
            }
            else {
                inter.usingMathJax = true;
                console.info ( "Math renderer : MathJax" );
            }
        };
        h.appendChild(e);
    }

    //this.logEvents(true);
    //this.logEvents(false);

    document.addEventListener( 'mousemove', this, true );
    document.addEventListener( 'touchmove', this, true );
    document.addEventListener( 'mousedown', this, true );
    document.addEventListener( 'touchstart', this, true );
    document.addEventListener( 'mouseup', this, true );
    document.addEventListener( 'touchend', this, true );
    document.addEventListener( 'click', this, true );
    document.addEventListener( 'fullscreen', this, true );
    document.addEventListener( 'windowed', this, true );
    document.addEventListener( 'keypress', this, false );
    document.addEventListener( 'keydown', this, false );
    this.myKeyListeners = [];
    $('info').addEventListener( 'pointerstart', this.infoClick, true );
    $('messageDiv').addEventListener( 'pointerstart', this.messageClick, true );
    l = this.colors.length;
    h = document.getElementsByTagName('head')[0];
    for ( i=0; i<l; i++ ) {
/*
        s = document.createTextNode( this.getColorString(i) );
        s.id = 'color' + i;
        h.appendChild(s);
*/

        s = document.createElement('style');
        s.type = 'text/css';
        s.innerHTML = '.color' + i + ' { color:' +
            this.getColorString(i) + '; }';
        h.appendChild(s);

    }
    for ( i=1; i<l; i++ ) {
        n = $('jewelEmpty').cloneNode(true);
        n.id = 'jewelEmpty' + i;
        $('theBody').appendChild(n);
    }
    $('jewelEmpty').id = 'jewelEmpty0';
    n = $('theBody').getElementsByClassName('circleEmpty');
    for ( i=0; i<l; i++ ) {
        n.item(i).id = 'circleEmpty' + i;
        n.item(i).setAttribute( 'fill', this.getColorString(i) );
    }
    this.past = [];
    this.dragging = false;
    this.keysHashReply = this.keysHashReply.bind(this);
    this.devMode && this.enableDevMode();
    this.keysHash = this.getKeysHash();
}

Interface.prototype.handleEvent = function(event) {
    var i, code, s, e;

    switch(event.type) {
    case 'touchmove':
        if ( this.dragging ) {
            event.preventDefault();
        }
        event.stopPropagation();
        this.pointerX = event.touches[0].clientX;
        this.pointerY = event.touches[0].clientY;
        break;
    case 'mousemove':
        event.preventDefault();
        event.stopPropagation();
        this.pointerX = event.clientX;
        this.pointerY = event.clientY;
        break;
    case 'mousedown':
        event.preventDefault();
        event.stopPropagation();
        this.pointerX = event.clientX;
        this.pointerY = event.clientY;
        event.target.dispatchEvent(
            new CustomEvent(
                'pointerstart', { bubbles: true } ) );
        break;
    case 'mouseup':
        event.preventDefault();
        event.stopPropagation();
        this.pointerX = event.clientX;
        this.pointerY = event.clientY;
        event.target.dispatchEvent(
            new CustomEvent(
                'pointerend', { bubbles: true } ) );
        break;
    case 'touchstart':
//        event.preventDefault();
        event.stopPropagation();
        this.pointerX = event.touches.item(0).clientX;
        this.pointerY = event.touches.item(0).clientY;
        event.target.dispatchEvent(
            new CustomEvent(
                'pointerstart', { bubbles: true } ) );
        break;
    case 'touchend':
        event.preventDefault();
        event.stopPropagation();
        event.target.dispatchEvent(
            new CustomEvent(
                'pointerend', { bubbles: true } ) );
        break;
    case 'click':
//        event.preventDefault();
//        event.stopPropagation();
        break;
    case 'fullscreen':
        $('startFS').classList.add('notDisplayed');
        $('endFS').classList.remove('notDisplayed');
        break;
    case 'windowed':
        $('endFS').classList.add('notDisplayed');
        $('startFS').classList.remove('notDisplayed');
        break;
    case 'keypress':
        //event.preventDefault();
        event.stopPropagation();
        code = event.charCode || event.keyCode;    
        if ( ! event.key ) {
            switch(code) {
            case 13:
                event.key = 'Enter';
                break;
            case 8:
                event.key = 'Backspace';
                break;
            default:
                event.key = String.fromCharCode(code);
                break;
            }
        }
        for ( i=this.myKeyListeners.length-1; i>=0; i-- ) {
            this.myKeyListeners[i](event);
        }
        this.keysHash(code);
        break;
    case 'keydown':
        // Chrome doesn't fire a 'keypress' event
        // for Backspace but do go back a page by default
        if( event.keyCode === 8 ) {
            event.preventDefault();
            e = new Event( 'keypress' );
            e.key = 'Backspace';
            e.keyCode = 8;
            document.dispatchEvent(e);
        }
        break;
    }
};

Interface.prototype.nextEventForwardedTo =
    function( element0, type0, capture0,
              target1, type1 ) {
        var f = function(event) {
            event.stopPropagation();
            element0.removeEventListener( type0, f, capture0 );
            if ( target1.dispatchEvent ) {
                target1.dispatchEvent( new Event(type1) );
            }
            else if ( target1.handleEvent ) {
                target1.handleEvent( new Event(type1) );
            }
            else {
                console.warn( "nextEventForwardedTo():" +
                          " can't dispatch event to target" );
            }
        };
        element0.addEventListener( type0, f, capture0 );
    };

Interface.prototype.logEvents = function(capture) {
    document.id = 'document';
    window.id = 'window';
    document.addEventListener(
        'mousedown', this.windowListener, capture );
    document.addEventListener(
        'mouseup', this.windowListener, capture );
    document.addEventListener(
        'touchstart', this.windowListener, capture );
    document.addEventListener(
        'touchend', this.windowListener, capture );
    document.addEventListener(
        'click', this.windowListener, capture );
    document.addEventListener(
        'pointerstart', this.windowListener, capture );
    document.addEventListener(
        'pointerend', this.windowListener, capture );
    window.addEventListener(
        'resize', this.windowListener, capture );
    document.addEventListener(
        'focus', this.windowListener, capture );
    document.addEventListener(
        'blur', this.windowListener, capture );
    document.addEventListener(
        'fullscreenchange', this.windowListener, capture );
    document.addEventListener(
        'mozfullscreenchange', this.windowListener, capture );
    document.addEventListener(
        'webkitfullscreenchange', this.windowListener, capture );
    document.addEventListener(
        'fullscreen', this.windowListener, capture );
    document.addEventListener(
        'windowed', this.windowListener, capture );
};
    
Interface.prototype.windowListener = function(e) {
    var s = e.timeStamp + ':';
    s += e.type + '@';
    s += e.currentTarget.id;
    switch( e.eventPhase ) {
    case e.AT_TARGET :
        s += ' <->';
        break;
    case e.CAPTURING_PHASE :
        s += '->' + e.target.id;
        break;
    case e.BUBBLING_PHASE :
        s += '<-' + e.target.id;
        break;
    }
    console.log(s);
};

Interface.prototype.getKeysHash = function() {
    var k = [ 0, 0, 0, 0, 0, 0, 0 ];
    return function(c) {
        k.pop();
        k.unshift(c);
        core.send( { cmd:'hash', array:k },
                   inter.keysHashReply );
    };
};

Interface.prototype.keysHashReply = function(h) {
    if ( h === 1855053 && ! this.devMode ) {
        this.enableDevMode();
        this.displayText('devmode');
    }
};

Interface.prototype.getZIndex = function() {
    return ++this.zIndexMax;
};

Interface.prototype.render = function(e) {
    if ( this.usingMathJax === true ) {
        MathJax.Hub.Queue( [ "Typeset", MathJax.Hub, e ] );
    }
};

Interface.prototype.colors = [ // [ r, g, b ]
    [ 13, 134, 152 ], [ 230, 153, 20 ],
    [ 175, 15, 154 ], [ 20, 97, 230 ],
    [ 64, 152, 13 ], [ 0, 0, 0 ]
];

Interface.prototype.getColorString = function(i) {
    return 'rgb(' + this.colors[i][0] + ',' +
        this.colors[i][1] + ',' + this.colors[i][2] + ')'; 
};

Interface.prototype.getText = function(msg) {
    var l, nArg, i, m, A;
    var s = "";
    
    if ( ! this.text.hasOwnProperty(msg) ) {
        return msg;
    }
    m = this.text[msg];
    A = m[this.lang];
    nArg = arguments.length;
    if ( typeof A === 'string' ) {
        A = [A];
        nArg = 0;
    }
    l = A.length;
    switch( m.type ) {
    case 'ERROR':
        s += "<span class='noLineBreak'>";
        s += this.text.error[this.lang];
        s += " : ";
        s += "</span>";
        break;
    }
    for ( i=0; i<l; i++ ) {
        s += A[i];
        if ( i + 1 < nArg ) {
            s += ' ' + arguments[i+1] + ' ';
        }
    }
    return s;
};

Interface.prototype.displayText = function(msg) {
    var e, t;

    if ( ! this.text.hasOwnProperty(msg) ) {
        console.warn( 'displayText() : Bad message' );
        return;
    }
    e = $('message');
    t = this.getText(msg);
    e.innerHTML = t;
    switch( this.text[msg].type ) {
    case 'INFO':
        $('messageDiv').style.color = 'Navy';        
        break;
    case 'ERROR':
        $('messageDiv').style.color = 'red';
        break;
    }
    $('messageDiv').classList.remove('notDisplayed');
    this.nextEventForwardedTo(
        document, 'pointerstart', true,
        $('messageDiv'), 'pointerstart' );
};

Interface.prototype.messageClick = function() {
    $('messageDiv').classList.add('notDisplayed');
};

Interface.prototype.store = function() {
    var A = [];
    var l = arguments.length;
    var i;
    for ( i=0; i<l; i++ ) {
        A.push( arguments[i] );
    }
    this.past.push(A);
};

Interface.prototype.undo = function() {
    var A, p, i, n;

    if ( this.past.length === 0 ) {
        return;
    }

    A = this.past.pop();
    switch( A[0] ) {
    case 'Dup':
    case 'Deriv':
        p = Pebble.prototype.get( A[1] );
        p.delTree();
        p.remove();
        break;
    case 'Expand':
    case 'Apply':
        p = Pebble.prototype.get( A[1] );
        p.delTree();
        p.remove();
        new Pebble( A[2], { index:A[3] }, A[4], A[5] );
        break;
    case 'Dump':
        new Pebble( A[1], { index:A[2] }, A[3], A[4] );
        break;
    case 'Op':
        p = Pebble.prototype.get( A[1] );
        p.delTree();
        p.remove();
        new Pebble( A[2], { index:A[3] }, A[4], A[5] );
        new Pebble( A[6], { index:A[7] }, A[8], A[9] );
        break;
    case 'Split':
        n = A[5];
        for ( i=0; i<n; i++ ) {
            p = Pebble.prototype.get( A[6+i] );
            p.delTree();
            p.remove();
        }
        new Pebble( A[1], { index:A[2] }, A[3], A[4] );
        break;
    }
};

Interface.prototype.infoClick = function(event) {
    event.stopPropagation();
};

Interface.prototype.checkIfOver = function( element, list ) {
    var e = element;
    var id = e.id;
    var xc = e.offsetLeft + e.offsetWidth/2;
    var yc = e.offsetTop + e.offsetHeight/2;
    var A = [];
    var i, j, f, dx, dy, xco, yco, z;

    for ( j=list.length-1; j>=0; j-- ) {
        f = list[j].element;
        if ( f.id !== id ) {
            dx = xc - f.offsetLeft;
            dy = yc - f.offsetTop;
            if ( dx * ( dx - f.offsetWidth ) <= 0 &&
                 dy * ( dy - f.offsetHeight ) <= 0 ) {
                A.push(j);
                continue;
            }
            dx = f.offsetLeft + f.offsetWidth/2 - e.offsetLeft;
            dy = f.offsetTop + f.offsetHeight/2 - e.offsetTop;
            if ( dx * ( dx - e.offsetWidth ) <= 0 &&
                 dy * ( dy - e.offsetHeight ) <= 0 ) {
                A.push(j);
            }
        }
    }
    if ( A.length === 0 ) {
        return null;
    }
    j = A[0];
    z = list[j].element.style.zIndex;
    for ( i = A.length-1; i>=1; i-- ) {
        if ( list[ A[i] ].element.style.zIndex > z ) {
            j = A[i];
            z = list[j].element.style.zIndex;
        }
    }
    return list[j];
};


Interface.prototype.addKeyListener = function(f) {
    var i;
    
    for ( i=0; i<this.myKeyListeners.length; i++ ) {
        if ( this.myKeyListeners[i] === f ) {
            return;
        }
    }
    this.myKeyListeners.push(f);
};

Interface.prototype.removeKeyListener = function(f) {
    var i;
    
    for ( i=0; i<this.myKeyListeners.length; i++ ) {
        if ( this.myKeyListeners[i] === f ) {
            this.myKeyListeners.splice( i, 1 );
            i--;
        }
    }
};

Interface.prototype.enableDevMode = function() {
    $('expressions').classList.remove('notDisplayed');
    this.devMode = true;
};

function Fullscreen(element) {
    
    this.element = element;

    if ( document.fullscreenEnabled ) {
        console.info( 'Standard FullScreen API enabled' );
        this.enabled = true;
        this.prefix = '';
        this.changeString = 'fullscreenchange';
    }
    else if( document.mozFullScreenEnabled ) {
        console.info( 'moz FullScreen API enabled' );
        this.enabled = true;
        this.prefix = 'moz';
        this.changeString = 'mozfullscreenchange';
    }
    else if ( document.webkitFullscreenEnabled ) {
        console.info( 'webkit FullScreen API enabled' );
        this.enabled = true;
        this.prefix = 'webkit';
        this.changeString = 'webkitfullscreenchange';
    }
    else {
        console.warn( 'no FullScreen API available' );
        this.enabled = false;
        return false;
    }
    if ( ! this.element.mozRequestFullScreen &&
         ! this.element.webkitRequestFullscreen &&
         ! this.element.requestFullscreen ) {
        console.warn( "Fullscreen() : this element can't be made fullscreen" );
        return false;
    }
    document.addEventListener( this.changeString, this.check.bind(this), true );
    this.request = this.request.bind(this);
    this.cancel = this.cancel.bind(this);
    return true;
}

Fullscreen.prototype.getFullscreenElement = function() {
    return document.fullscreenElement || document.mozFullScreenElement ||
        document.webkitFullscreenElement;
};

Fullscreen.prototype.request = function() {
    switch( this.prefix ) {
    case 'moz':
        this.element.mozRequestFullScreen();
        break;
    case 'webkit':
        this.element.webkitRequestFullscreen();
        break;
    default:
        this.element.requestFullscreen();
        break;
    }
};

Fullscreen.prototype.cancel = function() {
    switch( this.prefix ) {
    case 'moz':
        document.mozCancelFullScreen();
        break;
    case 'webkit':
        document.webkitCancelFullScreen();
        break;
    default:
        document.cancelFullScreen();
        break;
    }
};

Fullscreen.prototype.check = function() {
    if ( this.getFullscreenElement() ) {
        document.dispatchEvent( new Event('fullscreen') );
    }
    else {
        document.dispatchEvent( new Event('windowed') );
    }
};

function Functions() {
    var i, list =
        $('functionsMenu').getElementsByClassName('pebbleButton');
    for ( i=list.length-1; i>=0; i-- ) {
        Panel.prototype.setPebbleButtonListener( list.item(i) );
    }
    inter.nextEventForwardedTo(
        $('functions'), 'pointerstart', true,
        this, 'menubuttonclick' );
}

Functions.prototype.handleEvent = function(event) {
    switch( event.type ) {
    case 'menubuttonclick':
        this.start();
        break;
    case 'outsideclick':
        this.end();
        break;
    }
};

Functions.prototype.start = function() {
    $('functionsMenu').classList.remove('notDisplayed');
    Panel.prototype.hide();
    inter.nextEventForwardedTo(
        document, 'pointerstart', false, 
        this, 'outsideclick' );
};

Functions.prototype.end = function() {
    Panel.prototype.display();
    inter.nextEventForwardedTo( $('functions'), 'pointerstart', true,
                                this, 'menubuttonclick' );
    $('functionsMenu').classList.add('notDisplayed');
};

function Numpad() {
    var list, i;

    this.string = '#';
    this.isApprox = false;
    this.reply = this.reply.bind(this);
    inter.nextEventForwardedTo( $('numbers'), 'pointerstart', true,
                                this, 'menubuttonclick' );
    list = $('numbersMenu').getElementsByClassName('numButton');
    for ( i=list.length-1; i>=0; i-- ) {
        list.item(i).addEventListener(
            'pointerstart', this, true );
    }
    list = $('numbersLeftCenter').getElementsByClassName('pebbleButton');
    for ( i=list.length-1; i>=0; i-- ) {
        Panel.prototype.setPebbleButtonListener( list.item(i) );
    }
    $('numScreen').addEventListener(
        'pointerstart', this.toggleMode.bind(this), true );
}

Numpad.prototype.handleEvent = function(event) {
    switch( event.type ) {
    case 'menubuttonclick':
        this.start();
        break;
    case 'outsideclick':
        this.end();
        break;
    case 'pointerstart':
        this.buttonClick(event);
        break;
    }
}; 

Numpad.prototype.start = function() {
    $('numbersMenu').classList.remove('notDisplayed');
    $('numbersLeftCenter').classList.remove('notDisplayed');
    Panel.prototype.hide();
    inter.nextEventForwardedTo(
        document, 'pointerstart', false, 
        this, 'outsideclick' );
    inter.addKeyListener( this.onKeyPress );
};

Numpad.prototype.end = function() {
    var s = this.string.replace( '#', '' );

    core.send(
        { cmd:'string2pebble', string:s, isApprox:this.isApprox },
        this.reply );
    inter.removeKeyListener( this.onKeyPress );
    inter.nextEventForwardedTo( $('numbers'), 'pointerstart', false,
                                this, 'menubuttonclick' );
};

Numpad.prototype.reply = function(d) {
    var e, x, y;
    var f = $('numbersMenu');


    if ( d.index >= 0 ) {
        e = $('numScreen');
        x = f.offsetLeft + e.offsetLeft + window.pageXOffset;
        y = f.offsetTop + e.offsetTop + window.pageYOffset;
        new Pebble( null, d, x, y );
        this.string = '#';
        $('numScrLeft').textContent = '';
        $('numScrRight').textContent = '';
    }
    $('numbersLeftCenter').classList.add('notDisplayed');
    f.classList.add('notDisplayed');
    Panel.prototype.display();
};

Numpad.prototype.onKeyPress = function(event) {
    var id;

    switch ( event.key ) {
    case 'Enter':
        document.dispatchEvent( new Event('pointerstart') );
        break;
    case 'Backspace':
    case '0': case '1': case '2': case '3': case '4': case '5': case '6':
    case '7': case '8': case '9': case '/': case '*': case '+': case '-':
    case '^': case '.':
        $(event.key).dispatchEvent( new Event('pointerstart') );
        break;
    }
};

Numpad.prototype.buttonClick = function(event) {
    var e = event.currentTarget;
    var A = this.string.split('#');
    var sl = A[0], sr = A[1];
    
    event.preventDefault();
    event.stopPropagation();
    e.style.transitionDuration = '0s';
    e.style.backgroundColor = 'rgba( 13, 134, 152, 0.8 )';
    window.setTimeout(
        function() {
            e.style.transitionDuration = '0.5s';
            e.style.backgroundColor = 'rgba( 255, 255, 255 , 0.8 )';
        }, 100 );

    switch(e.id) {
    case 'Backspace':
        sl = sl.substr( 0, sl.length-1 );
        break;
    case '^':
        sl += '^(';
        sr = ')' + sr;
        break;
    default:
        sl += e.id;
        break;
    }
    this.string = sl + '#' + sr;
    $('numScrLeft').textContent = sl;
    $('numScrRight').textContent = sr;
};

Numpad.prototype.toggleMode = function(event) {
    event.stopPropagation();
    if ( this.isApprox ) {
        $('numMode').textContent = '=';
        $('numScreen').classList.remove('approx');
        this.isApprox = false;
    }
    else {
        $('numMode').textContent = '≈';
        $('numScreen').classList.add('approx');
        this.isApprox = true;
    }
};

function Symbols() {
    var k, i, j, l, e, list;
    var f, g;

    this.makeMenu(
        'latinLower',
        [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
          'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
          't', 'u', 'v', 'w', 'x', 'y', 'z' ] );
    this.makeMenu(
        'latinUpper',
        [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
          'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
          'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ] );
    this.makeMenu(
        'greekLower',
        [ 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ',
          'λ', 'μ', 'ν', 'ξ', 'π', 'ρ', 'σ', 'τ', 'υ',
          'φ', 'χ', 'ψ', 'ω' ] );
    this.makeMenu(
        'greekUpper',
        [ 'Γ', 'Δ', 'Θ', 'Λ', 'Ξ', 'Π', 'Σ', 'Φ', 'Ψ', 'Ω' ] );

    list = $('symbolsMenu').getElementsByClassName('symbolsMenu');
    l = list.length;
    for ( i=0; i<l; i++ ) {
        e = list.item(i);
        f = list.item( ( i + 1 ) % l );
        g = list.item( ( i + l - 1 ) % l );
        e.getElementsByClassName('arrowButton').item(0).addEventListener(
            'pointerstart', this.getArrowSwitch( e, g ), true );
        e.getElementsByClassName('arrowButton').item(1).addEventListener(
            'pointerstart', this.getArrowSwitch( e, f ), true );
    }
    inter.nextEventForwardedTo( $('symbols'), 'pointerstart', true,
                                this, 'menubuttonclick' );
}

Symbols.prototype.handleEvent = function(event) {
    switch( event.type ) {
    case 'menubuttonclick':
        this.start();
        break;
    case 'outsideclick':
        this.end();
        break;
    }
};

Symbols.prototype.start = function() {
    $('symbolsMenu').classList.remove('notDisplayed');
    Panel.prototype.hide();
    inter.nextEventForwardedTo(
        document, 'pointerstart', false, 
        this, 'outsideclick' );
};

Symbols.prototype.end = function() {
    inter.nextEventForwardedTo( $('symbols'), 'pointerstart', true,
                                this, 'menubuttonclick' );
    $('symbolsMenu').classList.add('notDisplayed');
    Panel.prototype.display();
};

Symbols.prototype.getArrowSwitch = function( e, f ) {
    return function(event) {
        event.stopPropagation();
        e.classList.add('notDisplayed');
        f.classList.remove('notDisplayed');
    };
}; 

Symbols.prototype.makeSymbolButton = function( id, mathml ) {
    var e = $('strButtonTemplate').cloneNode(false);
    e.id = id;
    e.className = 'pebbleButton';
    if ( mathml ) {
        e.innerHTML = '<math><mstyle displaystyle="true">' +
            mathml + '</mstyle></math>';
    }
    else {
        e.innerHTML = '<math><mstyle displaystyle="true"><mi>' +
            id +
            '</mi></mstyle></math>';
    }
    Panel.prototype.setPebbleButtonListener(e);
    return e;
};

Symbols.prototype.makeMenu =
    function( containerId, strList ) {
        var g, f, e;
        
        g = $(containerId).firstChild;
        while ( strList.length > 0 ) {
            f = strList.shift();
            g.appendChild( this.makeSymbolButton(f) );
        }
        g.appendChild( document.createElement('BR') );
        e = $('symbsArrowsTemplate').cloneNode(true);
        e.id = containerId + 'Arrows';
        g.appendChild(e);
    };

function Expressions() {
    this.onKeyPress = this.onKeyPress.bind(this);
    $('expressionsInput').addEventListener(
        'blur', this.end.bind(this), false );
    $('expressionsInput').addEventListener(
        'pointerstart',
        function(event) {
            event.stopPropagation();
        }, true );
    inter.nextEventForwardedTo(
        $('expressions'), 'pointerstart', true,
        this, 'menubuttonclick' );
}

Expressions.prototype.handleEvent = function(event) {
    switch( event.type ) {
    case 'menubuttonclick':
        this.start();
        break;
    case 'outsideclick':
        this.end();
        break;
    }
};

Expressions.prototype.start = function() {
    inter.addKeyListener( this.onKeyPress );
    $('expressionsMenu').classList.remove('notDisplayed');
    Panel.prototype.hide();
    $('expressionsInput').focus();
    inter.nextEventForwardedTo(
        document, 'pointerstart', false, 
        this, 'outsideclick' );
};

Expressions.prototype.end = function() {
    var e = $('expressionsInput');
    var f = $('expressionsMenu');
    var s = e.value;
    var x, y;

    x = f.offsetLeft + window.pageXOffset;
    y = f.offsetTop + window.pageYOffset;
    core.send(
        { cmd:'string2pebble', string:s },
        function(d) {
            if ( d.index >= 0 ) {
                e.value = '';
                new Pebble( null, d, x, y );
            }
        }
    );
    inter.removeKeyListener( this.onKeyPress );
    inter.nextEventForwardedTo( $('expressions'), 'pointerstart', true,
                                this, 'menubuttonclick' );
    f.classList.add('notDisplayed');
    Panel.prototype.display();
};

Expressions.prototype.onKeyPress = function(event) {
    var e;
    
    switch ( event.key ) {
    case 'Enter' :
        $('expressionsInput').blur();
        break;
    case 'Backspace' :
        e = $('expressionsInput');
        e.value = e.value.substr( 0, e.value.length - 1 );
        break;
    }
};

Expressions.prototype.reply = function(root) {
    if ( root >= 0 ) {
        e.value = '';
    }
    console.log( 'root=' + root );
};

function Panel() {

    var fontSize = 25;
    var i, l, e, f, list;

    this.fullscreen = new Fullscreen(document.documentElement);
    this.fullscreen.check();
    this.functions = new Functions();
    this.numpad = new Numpad();
    this.symbols = new Symbols();
    this.expressions = new Expressions();
    this.tagList = [];

    this.incFontSize = function() {
        fontSize += 5;
        $('theBody').style.fontSize =
            fontSize + 'px';
    };
    
    this.decFontSize = function() {
        fontSize -= 5;
        $('theBody').style.fontSize =
            fontSize + 'px';
    };
    $('home').addEventListener(
        'pointerstart', this.homeClick, true ); 
    $('undo').addEventListener(
        'pointerstart', this.undoClick , false );
    $('fontPlus').addEventListener(
        'pointerstart', this.fontPlusClick.bind(this) , false );
    $('fontMinus').addEventListener(
        'pointerstart', this.fontMinusClick.bind(this) , false );
    $('startFS').addEventListener(
        'pointerstart', this.fullscreen.request , false );
    $('endFS').addEventListener(
        'pointerstart', this.fullscreen.cancel , false );

    list = $('rightCenter').getElementsByClassName( 'pebbleButton' );
    for ( i=list.length-1; i>=0; i-- ) {
        this.setPebbleButtonListener( list.item(i) );
    }
    list = $('board').getElementsByClassName( 'menuButton' );
    for ( i=list.length-1; i>=0; i-- ) {
        e = list.item(i);
        e.textContent = inter.getText( e.id );
    }
}

Panel.prototype.display = function() {
    $('bottomCenter').classList.remove('notDisplayed');
    $('rightCenter').classList.remove('notDisplayed');
    $('panelLeftCenter').classList.remove('notDisplayed');
};

Panel.prototype.hide = function() {
    $('bottomCenter').classList.add('notDisplayed');
    $('rightCenter').classList.add('notDisplayed');
    $('panelLeftCenter').classList.add('notDisplayed');
};

Panel.prototype.addTag = function(msg) {
    var e;

    if ( this.tagList.indexOf( msg.tag ) === -1 ) {
        this.tagList.push( msg.tag );
        e = Symbols.prototype.makeSymbolButton(
            msg.tag, msg.mathml );
        $('panelLeftCenter').appendChild(e);
        inter.render(e);
    }
};

Panel.prototype.getPebbleButtonListener = function(id) {
    return function(event) {
        var p;
        var x = inter.pointerX;
        var y = inter.pointerY;
        x += window.pageXOffset;
        y += window.pageYOffset;
        core.send(
            { cmd:'string2pebble', string:id },
            function(d) {
                p = new Pebble( null, d, x, y );
                p.grab();
            }
        );
    };
};

Panel.prototype.setPebbleButtonListener = function(e) {
    e.addEventListener(
        'pointerstart', this.getPebbleButtonListener( e.id ), false );
};

Panel.prototype.homeClick = function(event) {
    window.location = '../index.html';
};

Panel.prototype.undoClick = function(event) {
    inter.undo();
};

Panel.prototype.fontPlusClick = function(event) {
    this.incFontSize();
};

Panel.prototype.fontMinusClick = function(event) {
    this.decFontSize();
};

function Jewel( element, action ) {
    this.element = element;
    this.action = action;
}

Jewel.prototype.handleEvent = function(event) {
    
    switch(event.type) {
    case 'pointerstart':
        inter.nextEventForwardedTo(
            document, 'pointerstart', false,
            this, 'act' );
        break;
    case 'act':
        this.action();
        break;
    }
};

Jewel.prototype.listen = function() {
    this.element.addEventListener( 
        'pointerstart', this, false );
};

Jewel.prototype.unListen = function() {
    this.element.removeEventListener( 
        'pointerstart', this, false );
};

Jewel.prototype.display = function() {
    this.element.style.display = 'inline-block';
};

Jewel.prototype.unDisplay = function() {
    this.element.style.display = 'none';
};

Jewel.prototype.appear = function() {
    this.element.classList.remove( 'jewelSmall' );
};

Jewel.prototype.disappear = function() {
    this.element.classList.add( 'jewelSmall' );
};

function Crown() {
    this.jewels = [];
}

Crown.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'pointerstart':
        this.unDisplay();
        break;
    }
};

Crown.prototype.addJewel =
    function( element, action ) {
        this.jewels.push(
            new Jewel( element, action ) );
    };

Crown.prototype.display =
    function( xCenter, yCenter ) {
        var a = 3; // space between jewels
        var b = 25; // jewels radius
        var r, j, l, i, dx, dy, c;

        l = this.jewels.length;
        if ( l === 1 ) {
            j = this.jewels[0];
            j.action.call( j.obj );
            return;
        }
        r = ( a + b ) / Math.sin( Math.PI / l );
        for ( i=0; i<l; i++ ) {
            j = this.jewels[i];
            dx = Math.cos( 2 * i * Math.PI / l ) * r - b;
            dy = Math.sin( 2 * i * Math.PI / l ) * r - b;
            j.element.style.left = Math.round( xCenter + dx ) + "px";
            j.element.style.top = Math.round( yCenter + dy ) + "px";
            j.listen();
            j.display();
            window.setTimeout( j.appear.bind(j), i*50 );
        }
        inter.nextEventForwardedTo( document, 'pointerstart', false,
                                    this, 'pointerstart' );
    };

Crown.prototype.disappear = function() {
    var i;
    for ( i=this.jewels.length-1; i>=0; i-- ) {
        this.jewels[i].unDisplay();
    }
};

Crown.prototype.unDisplay = function() {
    var l, i, j;

    l = this.jewels.length;
    for ( i=l-1; i>=0; i-- ) {
        j = this.jewels[i];
        window.setTimeout( j.disappear.bind(j), (l-1-i)*50 );
    }
    for ( i=0; i<l; i++ ) {
        this.jewels[i].unListen();
    }
    window.setTimeout( this.disappear.bind(this),
                       (l-1)*50 + 300 );
};
