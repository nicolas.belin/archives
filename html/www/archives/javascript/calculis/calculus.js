var calc = {};

calc.testSign = function(rN) {
    var i, p;
    if ( rN.isMult ) {
	  p = 1;
	  for ( i=rN.a.length-1; i>=0; i-- ) {
		p *= calc.testSign( rN.a[i] );
	  }
	  return p;
    }
    if ( rN.isNum ) {
	  if  ( rN.get('v') >= 0 ) {
		return 1;
	  }
	  else {
		return -1;
	  }
    }
    return 0;
};

calc['int'] = function( left, right, f, error ) {
    // Adaptive Simpson's method
    
    function aux( a, b, eps, S, fa, fb, fc, bottom ) {
        var c = (a + b)/2, h = b - a;
        var d = (a + c)/2, e = (c + b)/2;
        var fd = f(d), fe = f(e);
        var Sleft = h / 12 * ( fa + 4*fd + fc );
        var Sright = h / 12 * ( fc + 4*fe + fb );
        var S2 = Sleft + Sright;

        if ( bottom <= 0 || Math.abs( S2 - S ) <= 15*eps ) {
            // magic 15 comes from error analysis
            return S2 + (S2 - S) / 15;
        }
        return aux( a, c, eps/2, Sleft, fa, fc, fd, bottom-1 ) +
            aux( c, b, eps/2, Sright, fc, fb, fe, bottom-1 );
    }

    var maxRecursionDepth = 100;
    var fl = f(left), fr = f(right);
    var fce = f( ( left + right ) / 2 );

    return aux( left, right, error,
                ( right - left ) * ( fl + 4*fce + fr ) / 6,
                fl, fr, fce, maxRecursionDepth ); 
};

calc.log = function(x) {
    return Math.log(x)/Math.LN10;
};

calc.H = function(x) {
    if ( x === 0 ) {
        return 0.5;
    }
    return x > 0 ? 1 : 0;
};

calc.δ = function(x) {
    return x === 0 ? Number.MAX_VALUE : 0;
};

calc.derivFunction = function(rN) {
    var x, f;

    if ( rN.isEq ) {
        //rN = rN.dup(null);
        x = rN.a[1].a[0].dup(rN.a[0]);
        rN.a[0].a[0] = x;
        rN = calc.derivative( rN, x.id ).ret(rN.r);
        rN.a[0].a.length = 0;
        return rN.act();
    }
    else {
        return calc.derivative( rN, rN.a[0].id );
    }
};

calc.derivative = function( rN, id ) {
    var i, p, f, g, t, aN;

    if ( rN.isAtypical ) {
        if ( rN.isSymb && rN.id === '∀' ) {
            return alg.forAll(
                rN.a[0].dup(null), this.derivative( rN.a[1], id ) );
        }
        if ( rN.isSymb && rN.id === '↦' ) {
            return alg.mapsTo(
                rN.a[0].dup(null), this.derivative(
                    rN.a[1].dup(null), id ) );
        }
        if ( rN.isEq ) {
            rN.a[0] =
                this.derivative( rN.a[0].dup(null), id ).ret(rN);
            rN.a[1] =
                this.derivative( rN.a[1].dup(null), id ).ret(rN);
            return rN;
        }
    }
    t = [];
    rN.scanUp(
        function() {
            if ( this.isSymb && this.id === id ) {
                t.push(this);
            }
            if ( this.isSymb && this.id === '↦' ) {
                return [];
            }
            return this.a;
        } );
    var l = t.length;
    var s = alg.numConst(0);
    
    for ( i=0; i<l; i++ ) {
        p = alg.numConst(1);
        f = t[i].r;
        g = t[i];
        while ( f !== null ) {
            p = f.d( p, f.a.indexOf(g) );
            g = f;
            f = f.r;
        }
        s = alg.add( s, p );
    }
    return s;
};

Symb.prototype.d = function( p, n ) {
    var i, id, f, first;
    var dd;

    if ( ! this.g.df ) {
        this.g.df = [];
        for ( i=0; i<this.nArg; i++ ) {
            this.g.df[i] = null;
        }
    }
    if ( this.g.df[n] === null ) {
        dd = [];
        if ( ! this.dd ) {
            for ( i=0; i<this.nArg; i++ ) {
                dd.push(0);
            }
            first = this.g;
        }
        else {
            for ( i=0; i<this.nArg; i++ ) {
                dd.push( this.dd[i] );
            }
            first = this.first;
        }
        dd[n]++;
        id = first.id;
        for ( i=0; i<this.nArg; i++ ) {
            id += '_' + dd[i];
        }
        f = alg.symb( id, null, false, true );
        f.setNArg( this.nArg );
        f.g.dd = dd;
        f.g.first = first;
        f.g.prev = this.g;
        this.g.df[n] = f.g;
        worker.sendTag(f.g);
    }
    else {
        f = this.g.df[n].dup(null);
    }
    for ( i=this.a.length-1; i>=0; i-- ) {
        f.a[i] = this.a[i].dup(f);
    }
    if (p) {
        f = alg.mult( p, f ); 
    }
    return f;
};

Pow.prototype.d = function( p, n ) {
    var a, b, c;

    a = this.a[0].dup(null);
    if ( n === 0 ) {
        b = this.a[1].dup(null);
        c = b.dup(null);
        c = alg.add( c, alg.numConst(-1) );
        a = alg.pow( a, c );
        a = alg.mult( b, a );
        return alg.mult( p, a );
    }
    else { // n == 1
        return alg.mult(
            p, alg.mult(
                alg.symb( 'ln', a ), this.dup(null) ) );
    }
};

Mult.prototype.d = function( q, n ) {
    var l = this.a.length;
    var p, i, j;

    if ( n === 0 ) {
        p = this.a[1].dup(null);
        i = 2;
    }
    else {
        p = this.a[0].dup(null);
        i = 1;
    }
    for ( j=i; j<l; j++ ) {
        if ( j != n ) {
            p = alg.mult( p, this.a[j].dup(null) );
        }
    }
    return alg.mult( q, p );
};

Add.prototype.d = function( p, n ) {
    return p;
};

Vect.prototype.d = function( p, n ) {
    var l = this.a.length;
    var A = [];
    var i;

    for ( i=0; i<l; i++ ) {
        if ( i === n ) {
            A.push(p);
        }
        else {
            A.push( alg.numConst(0) );
        }
    }
    return alg.vect(A);
};

Symb.prototype.specials['mapsTo'].d = function( p, n ) {
    if ( n === 0 ) {
        return alg.numConst(0);
    }
    // n === 1
    return alg.mapsTo( this.a[0].dup(null), p );
};

Symb.prototype.specials['cos'].d = function( p, n ) {
    var f = this.a[0].dup(null);
    return alg.mult( p, alg.mult( alg.numConst(-1),
                                  alg.symb( 'sin', f ) ) );
};

Symb.prototype.specials['sin'].d = function( p, n ) {
    var f = this.a[0].dup(null);
    return alg.mult( p, alg.symb( 'cos', f ) );
};

Symb.prototype.specials['tan'].d = function( p , n ) {
    var f = this.a[0].dup(null);
    return alg.div(
        p, alg.pow( alg.symb( 'cos', f ),
                    alg.numConst(2) ) );
};


Symb.prototype.specials['ln'].d = function( p , n ) {
    var f = this.a[0].dup(null);
    return alg.div( p, f );
};

Symb.prototype.specials['log'].d = function( p , n ) {
    var f = this.a[0].dup(null);
    f = alg.mult( alg.symb( 'ln', alg.numConst(10) ), f );
    return alg.div( p, f );
};

Symb.prototype.specials['abs'].d = function( p, n ) {
    var f = this.a[0].dup(null);
    f = alg.mult( alg.numConst(2), alg.symb( 'heaviside', f ) );
    return alg.mult( p, alg.add( f, alg.numConst(-1) ) );
};

Symb.prototype.specials['heaviside'].d = function( p, n ) {
    var f = this.a[0].dup(null);
    return alg.mult( p, alg.symb( 'dirac', f ) );
};

Symb.prototype.specials['int'].d = function( p, n ) {
    var aN;

    switch(n) {
    case 0:
        aN = this.a[2].a[1].dup(null);
        aN = alg.findAndReplace(
            this.a[2].a[0].id, aN, this.a[0] );
        aN = alg.mult( aN, alg.numConst(-1) );
        return alg.mult( p, aN );
    case 1:
        aN = this.a[2].a[1].dup(null);
        aN = alg.findAndReplace(
            this.a[2].a[0].id, aN, this.a[1] );
        return alg.mult( p, aN );
    case 2:
        aN = alg.vect(
            [ this.a[0].dup(null), this.a[1].dup(null), p ] );
        return alg.symb( 'int', aN );
    }
};
