"use strict";

function Pebble( id, t, x, y ) {
    var s, e, i, l, p;

    if ( id === null ) {
        id = "p" + this.index;
        Pebble.prototype.index++;
    }
    this.id = id;
    e = $('pebbleTemplate').cloneNode(false);
    this.element = e;
    this.element.title = id;
    this.element.id = id;
    this.xc = x;
    this.yc = y;
    this.topping = null;
    this.isGrabbed = false;
    this.justGrabbed = false;
    this.add();
    e.addEventListener( 'pointerstart', this, true );
    e.addEventListener( 'pointerend', this, true );
    if ( t.isReady ) {
        this.whenReady(t);
    }
    else {
        core.send( { cmd:'prepare', index:t.index }, this.whenReady.bind(this) );
    }
}

Pebble.prototype.index = 0;
Pebble.prototype.list = [];

Pebble.prototype.get = function(id) {
    var i;
    for ( i=Pebble.prototype.list.length-1; i>=0; i-- ) {
        if ( Pebble.prototype.list[i].id === id ) {
            return Pebble.prototype.list[i];
        }
    }
    console.error(
        "Pebble.prototype.get() : can't find pebble by id=" +
            id );
    return null;
};

Pebble.prototype.add = function() {
    $('board').appendChild( this.element );
    Pebble.prototype.list.push(this);
};

Pebble.prototype.remove = function() {
    var i = Pebble.prototype.list.indexOf(this);
    Pebble.prototype.list.splice( i, 1 );
    $('board').removeChild( this.element );
};

Pebble.prototype.handleEvent = function(event) {

    event.preventDefault();
    event.stopPropagation();
    switch( event.type ) {
    case 'pointerstart':
        this.grab(event);
        break;
    case 'pointerend':
        this.ungrab();
        break;
    case 'outsideInfoClick':
        $('info').classList.add('notDisplayed');
        break;
    }
};

Pebble.prototype.whenReady = function(t) {
    this.t = t;
    this.render();
    this.setCrowns();
    if ( t.hasName ) {
        this.graph = null;
    }
};

Pebble.prototype.render = function() {
    var e = this.element;
    var s = "<math><mstyle displaystyle='true'><mrow>";
    
    s += this.t.mathml;
    s += "</mrow></mstyle></math>";

    e.innerHTML = s;
    this.x = this.xc - Math.round( e.offsetWidth / 2 );
    this.y = this.yc - Math.round( e.offsetHeight / 2 );
    e.style.left = this.x + 'px';
    e.style.top = this.y + 'px';
    e.style.zIndex = inter.getZIndex();

    inter.render( this.element ); 
};

Pebble.prototype.grab = function() {
    var i, left, top, dx, dy;

    dx = inter.pointerX -
        this.x + window.pageXOffset;
    dy = inter.pointerY -
        this.y + window.pageYOffset;
    inter.dragging = true;
    this.isGrabbed = true;
    this.justGrabbed = true;
    this.scroll( dx, dy );
    this.element.style.zIndex = inter.getZIndex();
    this.element.classList.add('grabbedPebble');
    window.setTimeout( this.notJustGrabbed.bind(this), 200 );
    inter.nextEventForwardedTo(
        document, 'pointerend', true,
        this.element, 'pointerend' );
};

Pebble.prototype.ungrab = function() {
  
    var id = null;
    var p;

    if ( this.isGrabbed ) {
        this.isGrabbed = false;
        inter.dragging = false;
        this.element.classList.remove("grabbedPebble");
        this.x = this.element.offsetLeft;
        this.y = this.element.offsetTop;
        p = inter.checkIfOver(
            this.element, Pebble.prototype.list );
        if ( p !== null ) {
            p.topping = this;
            p.actUponCrown.display( inter.pointerX, inter.pointerY );
        }
        else if ( this.justGrabbed ) {
            this.selfActCrown.display( inter.pointerX, inter.pointerY );
        }
        if ( this.graph === null ) {
            p = inter.checkIfOver(
                this.element, Plot.prototype.list );
            if ( p !== null ) {
                p.add(this);
            }
        }
    }
};

Pebble.prototype.notJustGrabbed = function() {
    this.justGrabbed = false;
    if ( this.graph ) {
        this.graph.plot.remove(this);
    }
};

Pebble.prototype.setCrowns = function() {
    var i, l;

    this.selfActCrown = new Crown();
    this.actUponCrown = new Crown();
    l = this.t.lacunaeNr;
    if ( l > 0 ) {
        l = Math.min( l, inter.colors.length );
        for ( i=0; i<l; i++ ) {
            this.actUponCrown.addJewel(
                $( 'jewelEmpty' + i ), this.getFillLacuna(i).bind(this) );
        }
    }
    if ( this.t.canBeSplit ) {
        this.selfActCrown.addJewel(
            $('jewelSplit'), this.split.bind(this) );
    }
    if ( this.t.isMapsTo || this.t.isDefining ) { 
        if ( l === 0 ) {
            this.actUponCrown.addJewel(
                $('jewelDup'), this.apply.bind(this) );
            this.selfActCrown.addJewel(
                $('jewelDeriv'), this.deriv.bind(this) );
            this.selfActCrown.addJewel(
                $('jewelPlot'), this.setupPlot.bind(this) );
        }
        this.selfActCrown.addJewel(
            $('jewelDump'), this.dump.bind(this) );
        this.selfActCrown.addJewel(
            $('jewelDup'), this.dup.bind(this) );
        if ( inter.devMode ) {
            this.selfActCrown.addJewel(
                $('jewelInfo'), this.showInfo.bind(this) );
        }
        return;
    }
    this.selfActCrown.addJewel(
        $('jewelDump'), this.dump.bind(this) );
    this.selfActCrown.addJewel(
        $('jewelDup'), this.dup.bind(this) );
    if ( inter.devMode ) {
        this.selfActCrown.addJewel(
            $('jewelInfo'), this.showInfo.bind(this) );
    }
    if ( this.t.canBeExpanded ) {
        this.selfActCrown.addJewel(
            $('jewelExpand'), this.expand.bind(this) );
    }
    if ( this.t.isAdd ) {
        this.selfActCrown.addJewel(
            $('jewelFactorize'), this.factorize.bind(this) );
    }
    if ( l === 0 ) {
        this.actUponCrown.addJewel(
            $('jewelAdd'), this.op('+').bind(this) );
        this.actUponCrown.addJewel(
            $('jewelSub'), this.op('-').bind(this) );
        this.actUponCrown.addJewel(
            $('jewelMult'), this.op('*').bind(this) );
        this.actUponCrown.addJewel(
            $('jewelDiv'), this.op('/').bind(this) );
        if ( ! info.isVect ) {
            this.actUponCrown.addJewel(
                $('jewelPow'), this.op('^').bind(this) );
        }
    }
};

Pebble.prototype.dup = function() {
    core.send(
        { cmd:'dup', index:this.t.index },
        this.dupReply.bind(this) );
};

Pebble.prototype.dupReply = function(d) {
    var p = new Pebble( null, d, this.getCenterX() + 10,
                        this.getCenterY() + 10 );
    inter.store( 'Dup', p.id );
};

Pebble.prototype.dump = function() {
    inter.store( 'Dump', this.id, this.t.index,
                 this.getCenterX(), this.getCenterY() );
    this.remove();
};

Pebble.prototype.deriv = function() {
    core.send(
        { cmd:'deriv', index:this.t.index },
        this.derivReply.bind(this) );
};

Pebble.prototype.derivReply = function(d) {
    var p = new Pebble( null, d, this.getCenterX() + 20,
                        this.getCenterY() + 20 );
    inter.store( 'Deriv', p.id );
};

Pebble.prototype.expand = function() {
    core.send(
        { cmd:'expand', index:this.t.index },
        this.expandReply.bind(this) );
};

Pebble.prototype.expandReply = function(d) {
    var p;

    if ( d.index >= 0 ) {
        p = new Pebble( null, d, this.getCenterX(), this.getCenterY() );
        inter.store( 'Expand', p.id, this.id, this.t.index,
                     this.getCenterX(), this.getCenterY() );
        this.remove();
    }
};

Pebble.prototype.factorize = function() {
    core.send(
        { cmd:'factorize', index:this.t.index },
        this.factorizeReply.bind(this) );
};

Pebble.prototype.factorizeReply = function(d) {
    var p;

    if ( d.index >= 0 ) {
        p = new Pebble( null, d, this.getCenterX(), this.getCenterY() );
        inter.store( 'Expand', p.id, this.id, this.t.index,
                     this.getCenterX(), this.getCenterY() );
        this.remove();
    }
};

Pebble.prototype.split = function() {
    core.send(
        { cmd:'split', index:this.t.index },
        this.splitReply.bind(this) );
};

Pebble.prototype.splitReply = function(d) {
    var i, a, p;
    var l = d.trees.length;
    var x = this.getCenterX();
    var y = this.getCenterY();
    if ( l > 0 ) {
        a = [ 'Split', this.id, this.t.index,
              this.getCenterX(), this.getCenterY(), 0 ];
        for ( i=0; i<l; i++ ) {
            if ( d.trees[i].index > 0 ) {
                a[5]++;
                p = new Pebble( null, d.trees[i], x, y + 20*i );
                a.push( p.id );
            }
        }
        inter.store.apply( inter, a );
        this.remove();
    }
};

Pebble.prototype.showInfo = function() {
    core.send( { cmd:'print', index:this.t.index },
               this.showInfoReply.bind(this) );
    inter.nextEventForwardedTo(
        document, 'pointerstart', false,
        this, 'outsideInfoClick');
};

Pebble.prototype.showInfoReply = function(text) {
    var i, e = $('info');
    var s = "id='" + this.id + "'";
    
    s += ' x=' + this.getCenterX();
    s += ' y=' + this.getCenterY();
    for ( i in this.t ) {
        if ( i !== 'mathml' ) {
            s += ' ' + i + '=' + this.t[i];
        }
    }
    s += '\n';
    s += text;
    s += '\ninner HTML :\n';
    s += this.element.innerHTML;
    e.textContent = s;
    e.classList.remove('notDisplayed');
    e.style.top = this.element.offsetTop + 'px';
    e.style.left = ( this.element.offsetLeft +
                     this.element.offsetWidth + 10 ) + 'px';
};

Pebble.prototype.op = function(str) {
    return (function() {
        core.send(
            { cmd:'op', op:str, left:this.t.index, right:this.topping.t.index },
            this.opReply.bind(this) );
    } ).bind(this);
};

Pebble.prototype.opReply = function(d) {
    var p;
    
    if ( d.index >= 0 ) {
        p = new Pebble(
            null, d,
            this.getCenterX(), this.getCenterY() );
        inter.store(
            'Op', p.id, this.id, this.t.index, this.getCenterX(),
            this.getCenterY(), this.topping.id, this.topping.t.index,
            this.topping.getCenterX(), this.topping.getCenterY() );
        this.topping.remove();
        this.remove();
    }
};

Pebble.prototype.getFillLacuna = function(k) {
    return ( function() {
        core.send(
            { cmd:'lacuna', index:k, inc:this.t.index, by:this.topping.t.index },
            this.fillLacunaReply.bind(this) );
    } ).bind(this);
};

Pebble.prototype.fillLacunaReply = function(d) {
    var p;

    if ( d.index >= 0 ) {
        p = new Pebble(
            null, d,
            this.getCenterX(), this.getCenterY() );
        inter.store(
            'Op', p.id, this.id, this.t.index, this.getCenterX(),
            this.getCenterY(), this.topping.id, this.topping.t.index,
            this.topping.getCenterX(), this.topping.getCenterY() );
        this.topping.remove();
        this.remove();
    }
};

Pebble.prototype.apply = function() {
    core.send(
        { cmd:'apply', mapsTo:this.t.index, arg:this.topping.t.index },
        this.applyReply.bind(this) );
};

Pebble.prototype.applyReply = function(d) {
    var p;
    
    if ( d.index >= 0 ) {
        if ( d.isMapsTo ) {
            new Pebble(
                null, d,
                this.topping.getCenterX(),
                this.topping.getCenterY() +
                    this.topping.element.offsetHeight );
        }
        else {
            p = new Pebble(
                null, d, this.topping.getCenterX(),
                this.topping.getCenterY() );
            inter.store( 'Apply', p.id, this.topping.id,
                         this.topping.t.index,
                         this.topping.getCenterX(),
                         this.topping.getCenterY() );
            this.topping.remove();
        }
    }
};

Pebble.prototype.setupPlot = function() {
    var p = new Plot( this.x, this.y );
    
    if ( p !== null ) {
        p.add(this);
    }
};

Pebble.prototype.getCenterX = function() {
    var e = this.element;
    return Math.round( e.offsetLeft + e.offsetWidth/2 );
};

Pebble.prototype.getCenterY = function() {
    var e = this.element;
    return Math.round( e.offsetTop + e.offsetHeight/2 );
};

Pebble.prototype.scroll = function( dx, dy ) {
    var self = this;
    var animate = function() {
        if ( self.isGrabbed ) {
            self.element.style.left =
                ( inter.pointerX - dx +
                  window.pageXOffset ) + 'px';
            self.element.style.top =
                ( inter.pointerY - dy +
                  window.pageYOffset ) + 'px';
            window.requestAnimationFrame(animate);
        }
    };
    animate();
};

Pebble.prototype.delTree = function() {
    core.send( { cmd:'delTree', index:this.t.index } );
};
