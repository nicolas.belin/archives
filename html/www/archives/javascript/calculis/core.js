"use strict";

var core, inter, panel;

function $(e) {
    return document.getElementById(e);
}

core = {};

core.start = function() {
    var blob, url, i, l;
    this.defActList = new Array();
    this.scriptsNames =
        [ 'worker.js', 'nodes.js', 'numerical.js',
          'hash.js', 'algebra.js', 'act.js', 'tree.js',
          'mathml.js', 'calculus.js', 'script.js' ];
    l = this.scriptsNames.length;
    if ( l > 1 ) {
        // concatenate worker scripts,
        // then create the worker
        this.scripts = [];
        this.lineNrs = [];
        for ( i=0; i<l; i++ ) {
            this.scripts[i] =
                this.loaded[ this.scriptsNames[i] ];
            this.lineNrs[i] =
                this.getLineNr( this.scripts[i] );
            delete this.loaded[ this.scriptsNames[i] ];
        }
        blob = new Blob(
            [ String.prototype.concat.apply(
                '', this.scripts ) ],
            { type: "application/javascript" } );
        url = URL.createObjectURL( blob );
        delete this.scripts;
    }
    else {
        url = this.scriptsNames[0];
    }
    this.worker = new Worker( url );
    this.worker.addEventListener(
        'message', this.receive.bind(this)  );
    this.worker.addEventListener(
        'error', this.workererror.bind(this) );
    inter = new Interface();
    panel = new Panel();

    l = this.queue.length;
    this.worker.postMessage( { cmd:'start' } );
    for ( i=0; i<l; i++ ) {
        this.send.apply( this, this.queue[i] );
    }
    core.send(
        { cmd:'string2pebble',
//          string:'mapsTo( x, cos(x) )' },
          string:'(x+1)^2 + x + 1' },
        function(d) {
            if ( d.index >= 0 ) {
                new Pebble( null, d, 200, 100 );
            }
        } );
};

core.handleEvent = function(event) {
    var blob, url, code;

    switch(event.type) {
    case 'load':
        this.loaded[
            Array.prototype.shift.call( this.toBeLoaded ) ] =
            this.q.responseText;
        this.loadNext();
        break;
    }
};

core.getLineNr = function(text) {
    return text.split('\n').length;
};

core.workererror = function(event) {
    var s;
    var l = 0;
    var p = event.lineno;
    var i = 0;

    if ( this.lineNrs ) {
        for (; i<this.lineNrs.length; i++ ) {
            if ( p < this.lineNrs[i] ) {
                break;
            }
            p -= this.lineNrs[i];
        }
        p += i;
    }
    s = 'Worker@' + this.scriptsNames[i];
    s += '(' + p + ') -> ';
    s += event.message;
    console.error(s);
    event.preventDefault();
};

core.receive = function(event) {
    // receive data from worker
    var msg = event.data;
    
    switch( msg.cmd ) {
    case 'log':
        console.log( 'Worker: ' + msg.txt );
        break;
    case 'deferredAction':
        if ( msg.act in this.defActList ) {
            this.defActList[msg.act](msg.arg);
            delete this.defActList[msg.act];
        }
        else {
            console.warn( 'Worker asks for undefined action' );
        }
        break;
    case 'tag':
        panel.addTag(msg);
        break;
    case 'text':
        inter.displayText( msg.txt );
        break;
    }
};

core.queue = [];

core.send = function( obj, defAct ) {
    var r;

    if ( this.worker ) {
        r = this.defActList.length;
        if ( defAct ) {
            this.defActList.push(defAct);
        }
        else {
            r = -1;
        }
        this.worker.postMessage( { msg:obj, act:r } );
    }
    else { // the worker is not set yet
        this.queue.push( [ obj, defAct ] );
    }
}

core.preload = function() {
    this.toBeLoaded = arguments;
    this.loaded = {};
    this.q = new XMLHttpRequest();
    this.q.overrideMimeType('text/plain');
    this.q.addEventListener( 'load', this );
    this.loadNext();
};

core.loadNext = function() {
    if ( this.toBeLoaded[0] ) {
        this.q.open( 'get', this.toBeLoaded[0], true );
        this.q.send();
    }
    else {
        this.start();
    }
};

