"use strict";

var global = this;
var worker = {};
var hash;

function Msg(event) {
    if ( event.data.msg ) {
        this.msg = event.data.msg;
        this.act = event.data.act;
    }
    else {
        this.msg = event.data;
        this.act = -1;
    }
}

Msg.prototype.reply = function(arg) {
    postMessage( { cmd:'deferredAction', act:this.act, arg:arg } );
};

worker.receive = function(event) {
    var m = new Msg(event);
    var msg = m.msg;
    var a, b, root;

    switch( msg.cmd ) {
    case 'start' :
        worker.log( 'up and running !' ); 
        hash = new Hash();
        break;
    case 'set':
        mathml.comma = msg.comma;
        mathml.sep = msg.sep;
        break;
    case 'hash':
        a = msg.array;
        b = hash.add( hash.mult ( hash.pow( hash.add( hash.mult( a[0], a[4] ), a[6] ), a[3]+a[2] ), a[5] ), a[1] );
        m.reply(b);
        break;
    case 'delTree':
        tree.del( msg.index );
        break;
    case 'print':
        m.reply( worker.printTree( tree.getRoot( msg.index ) ) );
        break;
    case 'prepare':
        a = { index:msg.index };
        worker.prepare(a);
        m.reply(a);
        break;
    case 'dup':
        root = tree.getRoot( msg.index ).dup(null);
        worker.root2pebble( m, root );
        break;
    case 'string2pebble':
        root = tree.str2tree( msg.string );
        if ( root && msg.isApprox ) {
            root.isApprox = true;
        }
        worker.root2pebble( m, root );
        break;
    case 'op':
        a = tree.getRoot( msg.left ).dup(null);
        b = tree.getRoot( msg.right ).dup(null);
        switch( msg.op ) {
        case '+':
            root = alg.add( a, b );
            break;
        case '-':
            root = alg.sub( a, b );
            break;
        case '*':
            root = alg.mult( a, b );
            break;
        case '/':
            root = alg.div( a, b );
            break;
        case '^':
            root = alg.pow( a, b );
            break;
        }
        worker.root2pebble( m, root );
        break;
    case 'lacuna':
        root = tree.getRoot( msg.inc ).dup(null);
        root = alg.replaceLacuna(
            root.findLacunae()[ msg.index ],
            tree.getRoot( msg.by ) );
        worker.root2pebble( m, root );
        break;
    case 'apply':
        root = tree.getRoot( msg.mapsTo );
        if ( root.isEq ) {
            root = root.a[1].apply( tree.getRoot( msg.arg ) );
        }
        else {
            root = root.apply( tree.getRoot( msg.arg ) );
        }
        worker.root2pebble( m, root );
        break;
    case 'deriv':
        root = tree.getRoot( msg.index );
        root = calc.derivFunction(root);
        worker.root2pebble( m, root );
        break;
    case 'expand':
        root = tree.getRoot( msg.index );
        worker.root2pebble( m, alg.expand( root.dup(null) ) );
        break;
    case 'factorize':
        root = tree.getRoot( msg.index );
        worker.root2pebble( m, alg.factorize( root.dup(null) ) );
        break;
   case 'split':
        worker.doSplit(m);
        break;
    case 'frame':
        worker.getFrame(m);
        break;
    case 'graph':
        worker.setGraph(m);
        break;
    }
};

worker.root2pebble = function( m, root ) {
    var t;

    if ( root === null ) {
        m.reply( { index:-1 } );
    }
    else {
        t = { index:tree.addRoot(root) };
        worker.prepare(t);
        m.reply(t);
    }
};

worker.prepare = function(t) {
    var root = tree.getRoot(t.index);
    var list, l, i, b, c;

    list = root.findLacunae();
    l = list.length;
    for ( i=0; i<l; i++ ) {
        list[i].index = i;
    }
    t.lacunaeNr = l;
    t.mathml = mathml.get(root);
    if ( root.isVect ) {
        t.isVect = true;
/*
        l = root.a.length;
        b = true;
        for ( i=0; i<l; i++ ) {
            c = root.a[i];
            if ( ! ( c.isSymb && ! c.nArg && ! c.isDefined ) ) {
                b = false;
                break;
            }
        }
        if (b) {
            t.isPureSymb = true;
        }
*/
    }
    if ( root.isSymb ) {
        if ( root.id === '↦' ) {
            t.isMapsTo = true;
            t.nArg = root.a[0].isVect ? root.a[0].a.length : 1;
            if ( root.name ) {
                t.hasName = true;
            }
        }
/*
        else if ( ! root.nArg && ! root.isDefined ) {
            t.isPureSymb = true;
        }
*/
    }
    if ( root.get('v') !== null ) {
        t.hasValue = true;
        t.v = root.get('v');
    }
    if ( root.isNum ) {
        t.isNum = true;
        if ( root.isApprox ) {
            t.isApprox = true;
        }
    }
    if ( root.isEq && root.isDefining ) {
        t.isDefining = true;
    }
    if ( root.isMult ) {
        for ( i=root.a.length-1; i>=0; i-- ) {
            if ( root.a[i].isAdd ||
                 ( root.a[i].isPow && root.a[i].a[1].isNum &&
                   root.a[i].a[0].isAdd && root.a[i].a[1].get('v') > 0 &&
                   worker.isInt( root.a[i].a[1].get('v') ) ) ) {
                t.canBeExpanded = true;
                break;
            }
        }
    }
    else if ( root.isPow && root.a[1].isNum &&
              root.a[0].isAdd &&
              worker.isInt( root.a[1].get('v') ) &&
              root.a[1].get('v') > 0 ) {
        t.canBeExpanded = true;
    }
    else if ( root.isAdd ) {
        t.isAdd =true;
    }
    l = root.a.length;
    if ( l > 0 ) {
        for ( i=0; i<l; i++ ) {
            if ( ! ( root.a[i].isSymb && root.a[i].id === '?' ) ) {
                break;
            }
        }
        if ( i < l ) {
            t.canBeSplit = true;
        }
    }
    t.isReady = true;
};

worker.sendTag = function(symb) {
    if ( symb.getTag ) {
        postMessage( { cmd:'tag', tag:symb.getTag(), mathml:symb.getMathml(true) } );
    }
};

worker.log = function(s) {
    postMessage( { cmd:'log', txt:s } );
};

worker.displayText = function(s) {
    postMessage( { cmd:'text', txt:s } );
};

worker.doSplit = function(m) {
    var msg = m.msg;
    var root = tree.getRoot( msg.index );
    var b, i, a = tree.split(root);
    if ( a.length > 0 ) {
        for ( i=0; i<a.length; i++ ) {
            b = { index:tree.addRoot(a[i]) };
            worker.prepare(b);
            a[i] = b;
        }
    }
    m.reply( { trees:a, orig:msg.index } );
};

worker.getFrame = function(m) {
    var msg = m.msg;
    var f = u[ tree.getRoot( msg.tree ).name ];
    var d = {};
    var fmin = Infinity, fmax = -Infinity;
    
    var xmin = -10;
    var xmax = 10;
    var fx, i, n = 100;
    for ( i=0; i<n; i++ ) {
        fx = f( xmin + i*(xmax-xmin)/(n-1) );
        if ( fx > fmax ) {
            fmax = fx;
        }
        if ( fx < fmin ) {
            fmin = fx;
        }
    }

    d.xmin = xmin;
    d.xmax = xmax;
    d.ymin = fmin - 0.1*( fmax - fmin );
    d.ymax = fmax + 0.1*( fmax - fmin );
    m.reply(d);
};

worker.setGraph = function(m) {
    var msg = m.msg;
    var f = u[ tree.getRoot( msg.tree ).name ];
    var xmin = msg.xmin;
    var xmax = msg.xmax;
    var ymin = msg.ymin;
    var ymax = msg.ymax;
    var sub = 5;
    var px, py, dx;
    var i, x, a, fx;
    var d = {}, points;
/*    
                x1 = points[l-2],
                fx1 = points[l-1],
                vx1 = x1 - points[l-4],
                vfx1 = fx1 - points[l-3],
                Math.abs(
                    vx1 * ( fx - fx1) - vfx1 * ( x - x1 ) ) > a ||
                    isNaN(fx) !== isNaN(fx1)
  */  
    px = ( xmax - xmin ) / msg.w;
    py = ( ymax - ymin ) / msg.h;
    a = 10 * px * py;
    dx = px * 10;
    x = xmin;
    points = [];
    while ( x < xmax ) {
        fx = f(x);
        if ( isNaN(fx) ) {
            if( ! isNaN( points[points.length-1] ) ) {
                points.push(x);
                points.push(NaN);
            }
        }
        else {
            points.push(x);
            points.push(fx);
        }
        x += dx;
    }
    d.strips = new Float32Array(points);
    m.reply(d);
    console.log( "#points = " + points.length );
};

worker.isInt = function(v) {
    v = v>=0 ? v : -v;
    if ( Math.floor(v) === v ) {
        return true;
    }
    return false;
};

worker.euclide = function( a, b ) {
    
    function div( x, y ) {
        return Math.floor( x / y );
    }
    
    var r = a;
    var rp = b;
    var u = 1;
    var up = 0;
    var v = 0;
    var vp = 1;
    var q, us, vs, rs;
    
    a = Math.abs(a);
    b = Math.abs(b);
    if ( Math.floor(a) != a || Math.floor(b) != b ) {
        return { pgcd:1, u:0, v:0 };
    }
    while( rp !== 0 ) {
        q = div( r, rp );
        rs = r;
        us = u;
        vs = v;
        r = rp;
        u = up;
        v = vp;
        rp = rs - q * rp;
        up = us - q * up;
        vp = vs - q * vp;
    }
    return { pgcd:r, u:u, v:v };
};

worker.printTree = function(n) {
    
    function printNode(m) {
        if ( m.isNum ) {
            return 'Num:' + m.get('v');
        }
        if ( m.isSymb ) {
            return "Symb(" +
                (m.isGlobal ? "global" : "local") +
                "):'" + m.id +"'"; 
        }
        if ( m.isPow ) {
            return "Op:'^'"; 
        }
        if ( m.isMult ) {
            return "Op:'*'"; 
        }
        if ( m.isAdd ) {
            return "Op:'+'"; 
        }
        if ( m.isVect ) {
            return "Op:','"; 
        }
        if ( m.isEq ) {
            return "Op:'='"; 
        }
        return 'Node of unknown type';
    }
    
    var s = '\n';
    var i = 0;
    var t, j, l, m, P, q, p;
    
    n.get('H');
    n.get('v');
    n.get('cst');
    t = [];
    n.scanUp(
        function() {
            t.push(this);
            return this.a;
        } );
    l = t.length;
    for ( i=0; i<l; i++ ){
        s += '[' + i + ']' + '={ ';
        s += printNode( t[i] );
        s += ' r:';
        if ( t[i].r !== null ) {
            s += t.indexOf( t[i].r );
        }
        else {
            s += 'null';
        }
        s += ' a:[ ';
        for ( j=0; j<t[i].a.length; j++ ) {
            s += t.indexOf( t[i].a[j] );
            s += ' ';
        }
        s += ']';
        if ( t[i].get('v') !== null ) {
            s += ' v:' + t[i].get('v');
        }
        s += ' H:' + t[i].get('H');
        if ( t[i].isMult || t[i].isAdd ) {
            s += ' kH:' + t[i].get('kH');
        }
        if ( t[i].isMult ) {
            s += ' nH:' + t[i].get('nH');
        }
        
        P = Object.keys( t[i] );
        for ( j=P.length-1; j>=0; j-- ) {
            p = P[j];
            if ( p !== 'prototype' &&
                 p !== 'a' &&
                 p !== 'r' &&
                 p !== 'attributes') {
                s += ' ' + p + ':';
                q = t[i][p];
                switch ( typeof(q) ) {
                case 'string':
                case 'number':
                case 'boolean':
                    s += q;
                    break;
                default:
                    if ( q === null ) {
                        s += 'null';
                    }
                    else {
                        s += typeof(q);
                    }
                    break;
                }
            }
        }
        s += ' }\n';
    }
    return s;
};

self.addEventListener( 'message', worker.receive );
