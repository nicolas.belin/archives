var u = {};

worker.interpret = function(code) {
    try{
        eval(code);
    }
    catch(err){
        worker.log( err.message + ' in code :' +
                    '\n' + code );
        return false;
    }
    return true;
};

Num.prototype.getJs = function() {
    return '(' + this.get('v').toString() + ')';
};

Symb.prototype.getJs = function() {
    var i, s;
    if ( this.js ) {
        s = this.js;
    }
    else if ( this.name ) {
        s = 'u.' + this.name;
    }
    else if ( this.isBound ) {
        s = '_' + this.id;
    }
    else {
        s = 'u.' + this.id;
    }
    if ( this.nArg > 0 ) {
        s += '(' + this.a[0].getJs();
        for ( i=1; i<this.nArg; i++ ) {
            s += ',' + this.a[i].getJs();
        }
        s += ')';
    }
    return s;
};

Symb.prototype.specials['exp1'].js = "Math.E";
Symb.prototype.specials['piNum'].js = "Math.PI";
Symb.prototype.specials['ln'].js = 'Math.log';
Symb.prototype.specials['log'].js = 'calc.log';
Symb.prototype.specials['cos'].js = 'Math.cos';
Symb.prototype.specials['sin'].js = 'Math.sin';
Symb.prototype.specials['tan'].js = 'Math.tan';
Symb.prototype.specials['abs'].js = 'Math.abs';
Symb.prototype.specials['heaviside'].js = 'calc.H';
Symb.prototype.specials['dirac'].js = 'calc.δ'; 

Symb.prototype.specials['int'].getJs = function() {

    if ( this.get('cst') ) {
        return '(' + this.get('v').toString() + ')';
    }
    return 'calc.int(' + this.a[0].getJs() + ',' +
        this.a[1].getJs() + ',' + this.a[2].getJs() +
        ',0.00001)';
};

Symb.prototype.specials['mapsTo'].getJs = function() {
    var i, vars, l, c;
    if ( this.name ) {
        return 'u.' + this.name;
    }
    vars = this.a[0].isVect ? this.a[0].a : [ this.a[0] ];
    l = vars.length;
    c = "function(";
    for ( i=0; i<l; i++ ){
        if ( i > 0 ){
            c += ",";
        }
        c += vars[i].getJs();
    }
    c += "){return ";
    c += this.a[1].getJs();
    c += ";}";
    return c;
};

Pow.prototype.getJs = function() {
    var s = "Math.pow(";
 
    s += this.a[0].getJs() + ",";
    s += this.a[1].getJs() + ")";
    return s;
};

Mult.prototype.getJs = function() {
    var l = this.a.length;
    var s = "(" + this.a[0].getJs();
    var i;
    
    for ( i=1; i<l; i++ ) {
        s += "*" + this.a[i].getJs();
    }
    s += ")";
    return s;
};

Add.prototype.getJs = function() {
    var l = this.a.length;
    var s = "(" + this.a[0].getJs();
    var i;

    for ( i=1; i<l; i++ ) {
        s += "+" + this.a[i].getJs();
    }
    s += ")";
    return s;
};
/*
Vect.prototype.getJs = function(){
    var t = this.t;
    var l = this.args.length;
    var s = "[" + t[this.args[0]].getJs();
    var i;

    for ( i=1; i<l; i++ ) {
        s += "," + t[this.args[i]].getJs()
    }
    s += "]";
    return s;
}
*/
 
