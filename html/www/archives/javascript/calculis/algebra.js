/*    alg.* functions create and link nodes, using low level constructors from 'nodes.js'
 *    alg.* return node may have its 'r' field not null
 */

var alg = {};

// * Numbers *

alg.numConst = function( v, isApprox ) {
    var aN = new Num( v, null );
    if ( isApprox ) {
        aN.isApprox = true;
    }
    return aN;
};

// * Symbs *

alg.localSymb = function( id, aN ) {
    var d;
    
    d = new Symb( id, false );
    if (aN) {
        d.a.push( aN.ret(d) );
        return d.act();
    }
    return d;
};

alg.symb = function( id, aN, dontAct, notReady ) {
    var d, i, g;

    if ( Symb.prototype.list.hasOwnProperty(id) ) {
        g = Symb.prototype.list[id];
    }
    else {
        if (Symb.prototype.specials.hasOwnProperty(id) ) {
            g = Symb.prototype.specials[id].set();
        }
        else {
            g = new Symb( id, true );
        }
        if ( ! notReady ) {
            worker.sendTag(g);
        }
    }
    if ( g.isGlobal ) {
        d = g.dup(null);
    }
    else { // Symb.prototype.specials[id] is local
        d = g;
    }
    if (aN) {
        d.a.push( aN.ret(d) );
    }
    if ( dontAct ) {
        return d;
    }
    return d.act();
};

// * Atypical symbs *

alg.forAll = function( vN, sN ) {
    return alg.symb( 'forAll', alg.vect( [ vN, sN ], true ) );
};

Symb.prototype.specials['forAll'].over = function() {
    var rN = this.r;
    var first = rN;
    var r = rN.r;
    var i, fN;

    for ( i=rN.a.length-1; i>=0; i-- ) {
        if ( rN.a[i].isSymb && rN.a[i].id === '∀' ) {
            fN = rN.a[i].ret(r);
            rN.a[i] = fN.a[1].ret(rN);
            fN.a[1] = first.ret(fN);
            first = fN;
        }
    }
    if ( r !== null ) {
        r.a[ r.a.indexOf(rN) ] = first;
    }
    rN.altered().act();
    return first.altered().act();
};

alg.mapsTo = function( vN, eN ) {
    return alg.symb(
        'mapsTo', alg.vect(
            [ vN, eN ] ) );
};

Symb.prototype.specials['mapsTo'].over = function() {
    return this.r;
};

Symb.prototype.specials['mapsTo'].apply = function(aN) {
    var expr, vars , A , l;
    var bN, tv, ta, i, list, dup;

    if ( aN.isAtypical ) {
        if ( aN.isSymb && aN.id === '↦' ) {
            aN = aN.dup(aN.r);
            bN = this.apply( aN.a[1] );
            if ( bN === null ) {
                return null;
            }
            aN.a[1] = bN;
            bN.r = aN;
            bN.isNew = true;
            aN.name = null;
            return aN.act();
        }
    }
    vars = this.a[0].isVect ? this.a[0].a : [ this.a[0] ];
    A = aN.isVect ? aN.a : [ aN ];
    l = vars.length;
    if ( A.length != l ) {
        worker.displayText('badDim');
        return null;
    }
    expr = this.a[1].dup(null);
    list = [];
    dup = [];
    for ( i=0; i<l; i++ ) {
        list.push(
            alg.findId(
                vars[i].id, expr ) ); 
    }
    for ( i=0; i<l; i++ ) {
        dup = dup.concat(
            alg.replace(
                A[i], list[i] ) );
    }
    if ( dup.length > 0 ) {
        expr = alg.simplify(dup);
    }
    if (expr) {
        expr.ret(aN.r);
    }
    return expr;
};


// ** Operations **

alg.getOpFunc = function( constructor, isOp ) {
    // Ok for any associative operation
    return function( aN, bN, dontAct ) {
        var r, l, i;
        
        if ( aN[isOp] && bN[isOp] ) {
            l = bN.a.length;
            r = aN;
            for ( i=0; i<l; i++ ) {
                bN.a[i].r = r;
                bN.a[i].isNew = true;
                r.a.push( bN.a[i] );
            }
        }
        else if ( ! aN[isOp] && bN[isOp] ) {
            aN.r = bN;
            aN.isNew = true;
            r = bN;
            r.a.unshift(aN);
        }
        else if ( aN[isOp] && ! bN[isOp] ) {
            bN.r = aN;
            bN.isNew = true;
            r = aN;
            r.a.push(bN);
        }
        else {
            r = new constructor( [ aN, bN ], null );
            aN.r = r;
            aN.isNew = true;
            bN.r = r;
            bN.isNew = true;
        }
        r.altered();
        if (dontAct) {
            return r;
        }
        return r.act();
    };
};

alg.add = alg.getOpFunc( Add, 'isAdd' );

alg.sub = function( aN, bN ) {
    var t = bN.isAdd;

    bN = this.mult( this.numConst(-1), bN );
    if (t) {
	  bN = alg.expand(bN);
    }
    return this.add( aN, bN );
};

alg.mult = alg.getOpFunc( Mult, 'isMult' );

alg.div = function( aN, bN ) {
    return this.mult( aN, this.pow( bN, this.numConst(-1) ) );
};

alg.pow = function( aN, bN, dontAct ) {
    var r;

    if ( aN === null || bN === null ) {
        return null;
    }
    r = new Pow( [ aN, bN ], null );
    aN.r = r;
    aN.isNew = true;
    bN.r = r;
    bN.isNew = true;
    if ( dontAct ) {
        return r;
    }
    return r.act();
};

// * vect *

alg.vect = function( AN, dontAct ) {
    var aN, i;
    var l = AN.length;
    
    for ( i=0; i<l; i++ ) {
        if ( AN[i] === null ) {
            return null;
        }
    }
    aN = new Vect( AN, null );
    for ( i=0; i<l; i++ ) {
        aN.a[i].r = aN;
        aN.a[i].isNew = true;
    }
    if ( dontAct ) {
        return aN;
    }
    return aN.act();
};

// * = *

alg.eq = function( aN, bN, dontAct ) {
    var rN = new Eq( [ aN, bN ] );
    aN.ret(rN);
    bN.ret(rN);
    if( dontAct ) {
        return rN;
    }
    return rN.act();
};

Eq.prototype.over = function() {
    var rN = this.r;
    var r = rN.r;
    var sN = rN.dup(null);
    var i;

    for ( i=rN.a.length-1; i>=0; i-- ) {
        if ( rN.a[i].isEq ) {
            rN.a[i] = rN.a[i].a[0].ret(rN);
            sN.a[i] = sN.a[i].a[1].ret(sN);
        }
    }
    this.a[0] = rN.ret(this).altered().act();
    this.a[1] = sN.ret(this).altered().act();
    if ( r !== null ) {
        r.a[ r.a.indexOf(rN) ] = this;
    }
    return this.ret(r).altered().act();
};

// * misc... *

alg.findId = function( symbId, aN ) {
    // return list of symbols of id 'symbId' in aN
    var list = [];
    aN.scanUp(
        function() {
            if ( this.isSymb && this.id === symbId ) {
                list.push(this);
            }
            return this.a;
        } );
    return list;
};

alg.replace = function( aN, list ) {
    // Replace nodes in list by duplicates of aN
    // Return a list of the duplicates
    var i, cN, dN, dups = [];
    
    for ( i=list.length-1; i>=0; i-- ) {
        cN = list[i];
        dN = aN.dup(cN.r);
        dups.push(dN);
        if ( cN.r !== null ) {
            cN.r.a[ cN.r.a.indexOf(cN) ] = dN;
        }
    }
    return dups;
};

alg.simplify = function( leaves, f ) {
    var i, root = null;

    if ( leaves.length === 1 && leaves[0].r === null ) {
        return leaves[0];
    }
    if ( leaves.length > 1 ) {
        for ( i=leaves.length-1; i>=0; i-- ) {
            leaves[i].scanThrough(
                function() {
                    this.isDirty = true;
                    return this;
                }
            );
        }
    }
    for ( i=leaves.length-1; i>=0; i-- ) {
        if ( leaves[i].r !== null ) {
            leaves[i].r.scanThrough(
                function(arg) {
                    var k, that, r;
                    arg.isNew = true;
                    delete arg.isDirty;
                    for ( k=this.a.length-1; k>=0; k-- ) {
                        if ( this.a[k].isDirty ) {
                            return null;
                        }
                    }
                    if (f) {
                        r = this.r;
                        that = f(this).ret(r);
                    }
                    else {
                        that = this;
                    }
                    that.altered();
                    that = that.act();
                    if ( that === null ) {
                        return null;
                    }
                    if ( that.r === null ) {
                        root = that;
                        return null;
                    }
                    if ( that !== this ) {
                        that.r.a[ that.r.a.indexOf(this) ] = that;
                    }
                    return that;
                }, leaves[i] );
        }
    }
    if (root) {
        delete root.isDirty;
    }
    return root;
};

alg.findAndReplace = function( id, aN, bN ) {
    // Find and replace in aN symbols 'id' by
    // duplicates of bN. Then simplify.
    var list; 

    if ( aN.isSymb && aN.id === id ) {
        return bN.dup(aN.r);
    }
    list = alg.findId( id, aN );
    list = alg.replace( bN, list );
    return alg.simplify(list);
};

alg.replaceLacuna = function( lN, bN ) {
    // replace lacuna lN by bN
    // then simplify
    var i, p, c = null;
    if ( lN.a.length > 0 ) {
        if ( bN.isSymb && bN.a.length === 0 ) {
            bN.a = lN.a;
            for ( i=bN.a.length-1; i>=0; i-- ) {
                bN.a[i].r = bN;
            }
        }
        else {
            worker.log( "replaceLacuna() : can't replace this..." );
            return null;
        }
    }
    if ( lN.r !== null ) {
        i = lN.r.a.indexOf(lN);
        lN.r.a[i] = bN.ret( lN.r );
        c = alg.simplify( [bN] );
        if ( c === null ) {
            return null;
        }
    }
    else {
        c = bN;
    }
    return c;
};

alg.expand = function(rN) {
    var i, j, l, k, n, E, loop, p1;
    var p0 = this.numConst(1);
    var aN = this.numConst(0);
    var A = [], B = [], C = [], D = [];

	if ( ! rN.isPow && ! rN.isMult ) {
		return rN;
	}
    if ( rN.isPow ) {
        rN = new Mult( [ alg.numConst(1), rN ], null );
    }
    l = rN.a.length;
    for ( i=0; i<l; i++ ) {
        if ( rN.a[i].isAdd ) {
            A.push( rN.a[i].a.length );
            B.push( rN.a[i] );
        }
        else if ( rN.a[i].isPow &&
                  rN.a[i].a[1].isNum &&
                  rN.a[i].a[0].isAdd &&
                  worker.isInt( rN.a[i].a[1].get('v') ) &&
                  rN.a[i].a[1].get('v') > 0 ) {
            n = rN.a[i].a[1].get('v');
            k = rN.a[i].a[0].a.length;
            for ( j=0; j<n; j++ ) {
                A.push(k);
                B.push( rN.a[i].a[0] );
            }
        }
        else {
            p0 = this.mult( p0, rN.a[i].dup(null) );
        }
    }
    l = A.length;
    for ( i=0; i<l; i++ ) {
        C.push(0);
    }
    loop = true;
    do {
        E = [];
        for ( i=0; i<l; i++ ) {
            E.push( C[i] );
        }
        D.push(E);

        j=0;
        while ( j < l && C[j] === A[j]-1 ) {
            j++;
        }
        if ( j < l ) {
            for ( i=0; i<j; i++ ) {
                C[i] = 0;
            }
            C[j]++;
        }
        else {
            loop = false;
        }
    }
    while(loop);
    
    n = D.length;
    for ( j=0; j<n; j++ ) {
        A = D[j];
        p1 = p0.dup(null);
        for ( i=0; i<l; i++ ) {
            p1 = this.mult( p1, B[i].a[ A[i] ].dup(null) );
        }
        aN = this.add( aN, p1 ); 
    }
    return aN;
};

Add.prototype.oneFraction = function() {
    var p, l, i, j, aN, b, c, k, dos = false;

    p = alg.numConst(1);
    l = this.a.length;
    for ( i=0; i<l; i++ ) {
	  if ( this.a[i].isMult ) {
		b = this.a[i].a;
	  }
	  else {
		b = [ this.a[i] ];
	  }
	  for ( j=b.length-1; j>=0; j-- ) {
		if ( b[j].isPow ) {
		    if ( b[j].a[1].isAdd ) {
			  c = b[j].a[1].a;
		    }
		    else {
			  c = [ b[j].a[1] ];
		    }
		    for ( k=c.length-1; k>=0; k-- ) {
			  if ( calc.testSign( c[k] ) < 0 ) {
                        dos = true;
				p = alg.mult( p, alg.pow(
                            b[j].a[0].dup(null), c[k].dup(null) ) );
			  }
		    }
		}
	  }
    }
    if (dos) {
        aN = alg.expand( alg.div( this.a[0], p.dup(null) ) );
        for ( i=1; i<l; i++ ) {
            aN = alg.add( aN, alg.expand( alg.div(
                this.a[i], p.dup(null) ) ) );
        }
        return alg.mult( p, aN );
    }
    return this;
};

alg.oneFraction = function(rN) {
    var i, aN;
    var leaves = [];

    rN.scanUp( function() {
        if ( this.a.length === 0 ) {
            leaves.push(this);
        }
        return this.a;
    } );
    return alg.simplify(
        leaves, function(rN) {
            if ( rN.isAdd ) {
                return rN.oneFraction();
            }
            return rN;
        } );
};
    
alg.factorize = function(rN) {

    function prepareSum(sN) {
        var i, j, cN, tN;

        for ( i=sN.a.length-1; i>=0; i-- ) {
            cN = sN.a[i];
            if ( cN.isMult ) {
                for ( j=cN.a.length-1; j>=0; j-- ) {
                    if ( cN.a[j].isNum ) {
                        tN = cN.a[0];
                        cN.a[0] = cN.a[j];
                        cN.a[j] = tN;
                        break;
                    }
                }
                if ( j === -1 ) {
                    cN.a.unshift( alg.numConst(1).ret(cN) );
                }
                for ( j=cN.a.length-1; j>0; j-- ) {
                    if ( ! cN.a[j].isPow ) {
                        cN.a[j] = alg.pow(
                            cN.a[j], alg.numConst(1), true ).ret(cN);
                    }
                }
                cN.altered();
            }
            else if ( cN.isPow ) {
                sN.a[i] = alg.mult(
                    alg.numConst(1), cN, true ).ret(sN);
            }
            else {
                cN = alg.pow( cN, alg.numConst(1), true );
                sN.a[i] = alg.mult(
                    alg.numConst(1), cN, true ).ret(sN);
            }
        }
        return sN.altered();
    }

    function getCommonFactor(rN) {
        // rN must be a sum, and must have been prepared
        var cf = [];
        var i, j, pN, k, min, t = false;

        pN = rN.a[0];
        for ( i=pN.a.length-1; i>0; i-- ) {
            cf.push(
                { base:pN.a[i].a[0], exp:[ pN.a[i].a[1] ] } ); 
        }
        for ( j=rN.a.length-1; j>0; j-- ) {
            pN = rN.a[j];
            for ( k=cf.length-1; k>=0; k-- ) {
                for ( i=pN.a.length-1; i>0; i-- ) {
                    if ( cf[k].base.get('kH') ===
                         pN.a[i].a[0].get('kH') ) {
                        cf[k].exp.push( pN.a[i].a[1] );
                        break;
                    }
                }
                if ( i === 0 ) {
		        cf.splice( k, 1 );
                }
            }
        }
        pN = alg.numConst(1);
        for ( k=cf.length-1; k>=0; k-- ) {
	      min = alg.min.apply( alg, cf[k].exp );
	      if (min) {
                t = true;
		    pN = alg.mult(
                    pN, alg.pow(
                        cf[k].base.dup(null), min.dup(null) ) );
	      }
        }
        if (t) {
            return pN;
        }
        return null;
    }
    
    function listSumFactors(sN) {
        var i, j, iN, A = [];
        
        for ( i=sN.a.length-1; i>=0; i-- ) {
            iN = sN.a[i];
            if ( iN.isPow && iN.a[0].isAdd ) {
    		    A.push( iN.a[0].a.length );            
		    A.push(i);
            }
            else if ( iN.isMult ) {
                for ( j=iN.a.length-1; j>=0; j-- ) {
                    if ( iN.a[j].isAdd ) {
                        A.push( iN.a[j].a.length );
                        A.push(i);
                    }
                    else if ( iN.a[j].isPow && iN.a[j].a[0].isAdd ) {
                        A.push( iN.a[j].a[0].a.length );
                    	A.push(i);
                    }
                }
            }
        }
        return A;
    }

    function listVariations(sN) {
        var m, n, i, j, S, A = [];
        A.push( prepareSum( sN.dup(null) ) );
        S = listSumFactors(sN);
        m = sN.a.length;
        for ( j=S.length-1; j>=0; j-=2 ) {
            n = S[j];
            i = S[j-1];
            if ( m-n-1 < 0 ) {
                continue;
            }
            
        }
        return A;
    }
    
    function factorizeR(aN) {
        var cN, i, A;
        
        if ( aN.isAdd ) {
            A = listVariations(aN);
            for ( i=A.length-1; i>=0; i-- ) {
                cN = getCommonFactor( A[i] );
                if ( cN !== null ) {
                    break;
                }
            }
            if ( i >= 0 ) {
                aN = A[i];
                for ( i=aN.a.length-1; i>=0; i-- ) {
                    aN.a[i] = alg.div( aN.a[i], cN.dup(null) ).ret(aN);
                }
                return factorizeR( alg.mult(
                    cN, aN.altered().act() ).ret(aN.r) );
            }
        }
        else if ( aN.isMult ) {
            for ( i=aN.a.length-1; i>=0; i-- ) {
                aN.a[i] = factorizeR( aN.a[i] ).ret(aN);
            }
            return aN.altered().act();
        }
        else if ( aN.isPow ) {
            aN.a[0] = factorizeR( aN.a[0] ).ret(aN);
            return aN.altered().act();
        }
        return aN;
    }
    
    return factorizeR( alg.oneFraction(rN) );
};

alg.min = function() {
	var e = arguments;
	var l = e.length;
	var min, i, s;

	if ( l === 0 ) {
		return null;
	}
	min = e[0];
	for ( i=1; i<l; i++ ) {
		s = calc.testSign( alg.sub( e[i].dup(null), min.dup(null) ) );
		if ( s < 0 ) {
			min = e[i];
		}
		else if ( s === 0 ) {
			return null;
		}
	}
	return min.dup(null);
};
