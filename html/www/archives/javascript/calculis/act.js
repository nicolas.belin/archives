/*    *.act() activate mechanisms, optionally only on arguments marked as New (as in 'new argument').
 *    After a mechanism has been activated, arguments must be marked Not New. 
 *    Code setting the return node also set (or not) the isNew field. 
 */

Symb.prototype.checkArgs = function() {
    var i, l, left, right, dup;

    l = this.a.length;
    if ( l > 0 && this.nArg === null ) {
        if ( l === 1 && this.a[0].isVect ) {
            l = this.a[0].a.length;
            this.a = this.a[0].a;
            for ( i=0; i<l; i++ ) {
                this.a[i].r = this;
                this.a[i].isNew = true;
            }
        }
        this.setNArg(l);
    }
    else if ( this.nArg > 0 ) {
        if ( l === 0 ) {
            for ( i=0; i<this.nArg; i++ ) {
                this.a.unshift( alg.symb('?') );
                this.a[0].r = this;
                this.a[0].isNew = true;
            }
        }
        else if ( l === 1 && this.nArg > 1 ) {
            if ( this.a[0].isVect &&
                 this.a[0].a.length === this.nArg ) {
                this.a = this.a[0].a;
                for ( i=this.a.length-1; i>=0; i-- ) {
                    this.a[i].r = this;
                    this.a[i].isNew = true;
                }
            }
            else {
                worker.displayText( 'badArg' );
                return null;
            }
        }
    }
    return this;
};

Symb.prototype.act = function() {
    var i, that;

    that = this.checkArgs();
    for ( i=that.a.length-1; i>=0; i-- ) {
        if ( that.a[i].isAtypical ) {
            return that.a[i].over();
        }
    }
    if ( that.mapsTo && that.findLacunae().length === 0 ) {
        return that.g.mapsTo.apply(
            alg.vect( that.a ) ).ret(that.r);
    }
    for ( i=that.a.length-1; i>=0; i-- ) {
        that.a[i].isNew = false;
    }
    return that;
};

Symb.prototype.specials['forAll'].act = function() {
    var x, xi, i, j, e, l, varsId, varsSymb, test, f, m;
    var A, that;

    that = this.checkArgs();
    x = that.a[0];
    e = that.a[1];
    if ( e.isNew ) {
        if ( ! e.isStatement ) {
            worker.displayText('notStatement');
            return null;
        }
        if ( e.isSymb && e.id === '↦' ) {
            return null;
        }
        if ( e.isSymb && e.id === '∀' ) {
            if ( that.a[0].isVect ) {
                A = that.a[0].a;
            }
            else {
                A = [ that.a[0] ];
            }
            if ( e.a[0].isVect ) {
                A = A.concat( e.a[0].a );
            }
            else {
                A.push( e.a[0] );
            }
            that.a[0] = alg.vect(A);
            that.a[1] = e.a[1];
            that.a[0].r = that;
            that.a[1].r = that;
            that.a[0].isNew = true;
            that.a[1].isNew = true;
            return that.act();
        }
    }
    if ( x.isNew ) {
        if ( x.isVect ) {
            for ( i=0; i<x.a.length; i++ ) {
                xi = x.a[i];
                if ( ! xi.isSymb || xi.nArg || xi.isDefined ) {
                    worker.displayText('badArg');
                    return null;
                }
                for ( j=i+1; j<x.a.length; j++ ) {
                    if ( xi.get('H') === x.a[j].get('H') ) {
                        x.a.splice( j, 1 );
                        j--;
                    }
                }
            }
            if ( x.a.length === 1 ) {
                that.a[0] = x.a[0];
                that.a[0].r = that;
                that.a[0].isNew = true;
            }
        }
        else if ( ! x.isSymb || x.nArg || x.isDefined ) {
            worker.displayText('badArg');
            return null;
        }
    }

    if ( ( e.isNew || x.isNew ) &&
         e.isEq && e.a[0].isSymb &&
         ( l = e.a[0].nArg ) > 0 ) {
        varsId = [];
        varsSymb = [];
        test = true;
        for ( i=0; i<l; i++ ) {
            if ( e.a[0].a[i].isSymb && ! e.a[0].a[i].nArg ) {
                varsId.push( e.a[0].a[i].id );
                varsSymb.push( e.a[0].a[i] );
            }
            else {
                test = false;
                break;
            }
        }
        if (test) {
            if ( l === 1 ) {
                if ( ! x.isSymb || x.id !== varsId[0] ) {
                    test = false;
                }
            }
            else if ( x.a.length === l ) {
                for ( i=0; i<l; i++ ) {
                    if ( varsId.indexOf( x.a[i].id ) === -1 ) {
                        test = false;
                        break;
                    }
                }
            }
            else {
                test = false;
            }
        }
        if (test) {
            f = e.a[0];
            f.a.length = 0;
            x = alg.vect(varsSymb);
            m = alg.mapsTo( x, e.a[1] );
            return alg.eq( f, m );
        }
    }
    e.isNew = false;
    x.isNew = false;
    return that;
};

Symb.prototype.specials['mapsTo'].act = function() {
    var that;
    var undefs, vars, i, n, s, c, b;

    that = this.checkArgs();
    b = that.a[0];
    if ( b.isVect ) {
        n = b.a.length;
        for ( i=0; i<n; i++ ) {
            c = that.a[0].a[i];
            if ( ! ( c.isSymb && ! c.nArg && ! c.isDefined ) ) {
                return null;
            }
        }
    }
    else if ( ! b.isSymb || b.nArg || b.isDefined ) {
        return null;
    }
    if ( that.findLacunae().length === 0 ) {
        vars = new Set();
        if ( that.a[0].isVect ) {
            for ( i=that.a[0].a.length-1; i>=0; i-- ) {
                vars.add( that.a[0].a[i].id );
            }
            if ( vars.size !== that.a[0].a.length ) {
                return null;
            }
        }
        else {
            vars.add( that.a[0].id );
        }
        that.scanUp( function() {
            if ( this.isSymb &&
                 ! this.isDefined &&
                 vars.has( this.id ) ) {
                this.isBound = true;
            }
            return this.a;
        } );
        
        s = that.getJs();
        that.name = 'mapsTo' + ( Symb.prototype.specials['mapsTo'].index++ );
        s = 'u.' + that.name + '=' + s + ';';
        worker.log( s );
        worker.interpret(s);
        // hereafter, this mapsTo node must not be changed
    }
    that.a[0].isNew = false;
    that.a[1].isNew = false;
    return that;
};

Symb.prototype.specials['ln'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];

    if ( arg.isNum ) {
        if ( arg.isApprox ) {
            return alg.numConst(
                this.get('v'), true ).ret(r);
        }
        if ( arg.get('v') === 1 ) { // ln(1) -> 0
            return alg.numConst( 0, arg.isApprox ).ret(r);
        }
    }
    if ( arg.isSymb &&
         arg.name === 'exp1' ) { // ln(e) -> 1
            return alg.numConst(1).ret(r);
    }
    if ( arg.isPow &&
         arg.a[0].isSymb &&
         arg.a[0].name === 'exp1' ) { //  ln(e^x) -> x
            return arg.a[1].ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['log'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
    
    if ( arg.isNum ) {
        if ( arg.isApprox ) {
            return alg.numConst(
                that.get('v'), true ).ret(r); 
        }
        if ( arg.get('v') === 1 ) { // log(1) -> 0
            return alg.numConst( 0, arg.isApprox ).ret(r);
        }
        if ( arg.get('v') === 10 ) { // log(10) -> 1
            return alg.numConst( 1, arg.isApprox ).ret(r);
        }
    }
    if ( arg.isPow &&
         arg.a[0].isNum &&
         arg.a[0].get('v') === 10 ) { //  log(10^x) -> x
            return arg.a[1].ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['cos'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
        
    if ( arg.isNum && arg.isApprox ) {
        return alg.numConst( that.get('v'), true ).ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['sin'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
    
    if ( arg.isNum && arg.isApprox ) {
        alg.numConst( that.get('v'), true ).ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['tan'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];

    if ( arg.isNum && arg.isApprox ) {
        return alg.numConst( that.get('v'), true ).ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['abs'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
    var v;
    
    if ( arg.isNum && arg.isApprox ) {
        return alg.numConst( that.get('v'), true ).ret(r); 
    }
    if ( arg.get('cst') && ( v = arg.get('v') ) !== null  ) {
        if ( v >= 0 ) {
            return arg.ret(r);
        }
        else {
            return alg.mult( alg.numConst(-1), arg ).ret(r);
        }
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['heaviside'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
    var v;
    
    if ( arg.get('cst') && ( v = arg.get('v') ) !== null  ) {
        return alg.numConst( v, false ).ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['dirac'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var arg = that.a[0];
    var v;

    if ( arg.get('cst') && ( v = arg.get('v') ) !== null  ) {
        return alg.numConst( v, false ).ret(r);
    }
    arg.isNew = false;
    return that;
};

Symb.prototype.specials['int'].act = function() {
    var r = this.r;
    var that = this.checkArgs();
    var args = that.a;
    var mapsTo, i, list;

    if ( args[2].isSymb && args[2].id === '?' ) {
        args[2] = alg.mapsTo(
            alg.symb('?'), alg.symb('?') ).ret(that);
    }
    if ( args[0].isVect || args[1].isVect ||
         ! ( args[2].isSymb && args[2].id === '↦' ) ||
         args[2].a[0].isVect ) {
        worker.displayText( 'badArg' );
        return null;
    }
    args[0].isNew = false;
    args[1].isNew = false;
    args[2].isNew = false;
    return that;
};

alg.getOpAct = function(isOp) {
    return function() {
        var a = this.a;
        var changed = false;
        var i, j, cN, l, k, r;
                
        l = this.a.length;
        for ( i=0; i<l; i++ ) {
            if ( a[i].isNew ) {
                if ( a[i].isAtypical ) {
                    return a[i].over();
                }
                else if ( a[i][isOp] ) {
                    k = a[i].a.length;
                    for ( j=0; j<k; j++ ) {
                        a[i].a[j].r = this;
                        a[i].a[j].isNew = true;
                    }
                    a[i].a.unshift(1);
                    a[i].a.unshift(i);
                    a.splice.apply( a, a[i].a );
                    l += k-1;
                    i--;
                    changed = true;
                    continue;
                }
                for ( j=l-1; j>=0; j-- ) {
                    if ( j !== i ) {
                        cN = this.combine( a[i], a[j] );
                        if ( cN === undefined ) {
                            return null;
                        }
                        if ( cN !== null ) {
                            break;
                        }
                    }
                }
                if ( j === -1 ) {
                    a[i].isNew = false;
                }
                else {
                    changed = true;
                    cN.r = this;
                    cN.isNew = true;
                    a[i] = cN;
                    a.splice( j, 1 );
                    if ( j < i ) {
                    	i--;
                    }
                    i--;
                    l--;
                }
            }
        }
        if ( changed ) {
            if ( l === 1 ) {
                a[0].r = this.r;
                a[0].isNew = true;
                return a[0];
            }
            this.altered();
        }
        return this;
    };
};

alg.getRatioFactor = function(aN) {
    var p = 1;
    var q = 1;
    var cN = null;
    var i, args;
    
    if ( aN.isNum ) {
        p = aN.v;
    }
    else if ( aN.isPow && aN.get('nH') === 1 ) {
        q = aN.a[0].get('v');
    }
    else if ( aN.isMult ) {
        args = [];
        for ( i=aN.a.length-1; i>=0; i-- ) {
            if ( aN.a[i].get('nH') === 1 ) {
                if ( aN.a[i].isNum ) {
                    p *= aN.a[i].get('v');
                }
                else if ( aN.a[i].isPow ) {
                    q *= aN.a[i].a[0].get('v');
                }
            }
            else {
                args.push( aN.a[i] );
            }
        }
        if ( args.length === 1 ) {
            cN = args[0];
        }
        else {
            cN = aN;
            cN.a = args;
        }
    }
    else {
        cN = aN; 
    }
    if ( cN === null ) {
        cN = this.numConst(1);
    }
    cN.r = aN.r;
    return { p:p, q:q, rest:cN };
};

Add.prototype.combine = function( aN, bN ) {
    var fa, fb, p, q, pgcd, i;

    if ( aN.isVect || bN.isVect ) {
        if ( aN.isVect && ! bN.isVect ||
             ! aN.isVect && bN.isVect ||
             aN.a.length !== bN.a.length ) {
            worker.displayText('badDim');
            return undefined;
        }
        for ( i=aN.a.length-1; i>=0; i-- ) {
            aN.a[i] = alg.add( bN.a[i], aN.a[i] ).ret(aN);
        }
        return aN.altered().act();
    }
    
    if ( aN.isNum && aN.get('v') === 0 && ! aN.isApprox ) {
        return bN;
    }

    if ( bN.isNum && bN.get('v') === 0 && ! bN.isApprox ) {
        return aN;
    }

    if ( ( aN.isNum && aN.isApprox && bN.get('v') !== null ) ||
         ( bN.isNum && bN.isApprox && aN.get('v') !== null ) ) {
        return alg.numConst( aN.get('v') + bN.get('v'), true );
    }

    if ( aN.isNum && bN.isNum ) {
        aN.setValue( aN.get('v') + bN.get('v') );
        return aN;
    }
    
    fa = aN.get('kH');
    if ( fa !== 1 && fa === bN.get('kH') ) {
	  // factorize by constants
	  p = alg.numConst(1);
	  if ( aN.isMult ) {
		for ( i=aN.a.length-1; i>=0; i-- ) {
		    if ( aN.a[i].get('cst') ) {
			  p = alg.mult( aN.a[i].dup(null), p );
		    }
		}
	  }
	  aN = alg.div( aN, p.dup(null) );
	  q = alg.div( bN, aN.dup(null) );
	  return alg.mult( alg.add( p, q ), aN );
    }
    if ( aN.get('nH') === bN.get('nH') ) {
	  fa = alg.getRatioFactor(aN);
	  fb = alg.getRatioFactor(bN);
	  p = fa.p*fb.q + fb.p*fa.q;
	  q = fa.q * fb.q;
	  pgcd = worker.euclide( p, q ).pgcd;
	  p /= pgcd;
	  q /= pgcd;
	  aN = alg.mult( fa.rest, alg.numConst(p) );
	  aN = alg.div( aN, alg.numConst(q) );
	  return aN;
    }
    return null;
};

Add.prototype.act = alg.getOpAct('isAdd');

Mult.prototype.combine = function( aN, bN ) {
    var cN, dN, p, q, pgcd, ah, bh;

    if ( aN.isVect || bN.isVect ) {
        if ( ! aN.isVect && bN.isVect ) {
            for ( p=bN.a.length-1; p>=0; p-- ) {
                bN.a[p] = alg.mult( aN.dup(), bN.a[p] ).ret(bN);
            }
            return bN.altered().act();
        }
        if ( aN.isVect && ! bN.isVect ) {
            return alg.mult( bN, aN ); 
        }
        if ( aN.isVect && bN.isVect ) {
            if ( aN.a.length === 2 && bN.a.length === 2 ) {
                return alg.sub( alg.mult( aN.a[0], bN.a[1] ),
                                alg.mult( aN.a[1], bN.a[0] ) );
            }
            else if ( aN.a.length === 3 && bN.a.length === 3 ) {
                return alg.vect( [
                    alg.sub( alg.mult( aN.a[1].dup(), bN.a[2].dup() ),
                             alg.mult( aN.a[2].dup(), bN.a[1].dup() ) ),
                    alg.sub( alg.mult( aN.a[2], bN.a[0].dup() ),
                             alg.mult( aN.a[0].dup(), bN.a[2] ) ),
                    alg.sub( alg.mult( aN.a[0], bN.a[1] ),
                             alg.mult( aN.a[1], bN.a[0] ) ) ] );
            }
            worker.displayText('badMult');
            return undefined;
        }
    }
    
    if ( aN.isNum ) {
        if ( aN.get('v') == 1 && ! aN.isApprox ) { // 1*b -> b
            return bN;
        }
        if ( aN.get('v') === 0 ) { // 0*b -> 0
            return aN;
        }
        if ( aN.isApprox && bN.get('v') !== null ) {
            return alg.numConst( aN.get('v') * bN.get('v'), true );
        }
        if ( bN.isNum ) {
            aN.setValue( aN.get('v') * bN.get('v') );
            return aN;
        }
        if ( bN.isPow && bN.a[0].isNum &&
		 bN.a[1].isNum && bN.a[1].get('v') === -1 ) {
     	      pgcd = worker.euclide( aN.get('v'), bN.a[0].get('v') ).pgcd;
            if ( pgcd === 1 ) {
                return null;
            }
            aN.setValue( aN.get('v') / pgcd );
            bN.a[0].setValue( bN.a[0].get('v') / pgcd );
            return alg.div( aN, bN.a[0] );
        }
    }
    if ( bN.isNum ) {
        return this.combine( bN, aN );
    }
    if ( bN.isPow && aN.isPow &&
	   bN.a[1].get('H') === aN.a[1].get('H') ) {
        // a^x * b^x -> (a*b)^x
	  cN = this.combine( bN.a[0], aN.a[0] );
	  if ( cN !== null ) {
		return alg.pow( cN, aN.a[1] );
        }
    }
    if ( aN.isPow ) {
	  ah = aN.a[0].get('kH');
    }
    else {
	  ah = aN.get('kH');
    }
    if ( bN.isPow ) {
	  bh = bN.a[0].get('kH');
    }
    else {
	  bh = bN.get('kH');
    }
    if ( ah === bh ) {
	  if ( ! aN.isPow ) {
		aN = alg.pow( aN, alg.numConst(1), true );
	  }
	  if ( ! bN.isPow ) {
		bN = alg.pow( bN, alg.numConst(1), true );
	  }
	  // x^a * x^b -> x^(a+b)
	  cN = alg.add( aN.a[1], bN.a[1] );
	  if ( aN.a[0].get('H') === bN.a[0].get('H') ) {
		return alg.pow( aN.a[0], cN );
	  }
	  p = hash.getNormSumIndex(aN.a[0].a);
	  q = hash.getNormSumIndex(bN.a[0].a);
	  dN = alg.div( bN.a[0].a[q].dup(null),
			    aN.a[0].a[p].dup(null) );
	  dN = alg.pow( dN, bN.a[1].dup(null) );
	  return alg.mult( dN, alg.pow( aN.a[0], cN ) );
    }
    return null;
};

Mult.prototype.act = alg.getOpAct('isMult');

Pow.prototype.act = function() {
    var aN = this.a[0];
    var bN = this.a[1];
    var rN, r, v, l, i, c, av, bv;

    if ( aN === null || bN === null ) {
        return null;
    }
    if ( ! aN.isNew && ! bN.isNew ) {
        return this;
    }
    if ( aN.isAtypical ) {
        return aN.over();
    }
    else if ( bN.isAtypical ) {
        return alg.opAtypical(bN);
    }
    if ( aN.get('cst') && bN.get('cst') ) {
        v = this.get('v');
        if ( v === undefined ) {
            return null;
        }
        if ( ( aN.isNum && aN.isApprox ) || 
             ( bN.isNum && bN.isApprox ) ) {
            rN = alg.numConst( v, true );
            rN.r = this.r;
            return rN;
        }
        if ( aN.isNum && bN.isNum ) {
            r = worker.isInt( aN.get('v') ) && ! aN.isApprox;
            if ( ( !r || ( r && worker.isInt(v) ) ) &&
                 v.toString().length < 5 ) {
                rN = alg.numConst(v);
                rN.r = this.r;
                return rN;
            }
        }
    }
    if ( bN.isNum ) {
        if ( bN.get('v') === 0 ) { // a^0 -> 1
            rN = alg.numConst( 1, bN.isApprox );
            rN.r = this.r;
            return rN;
        }
        if ( bN.get('v') === 1 && ! bN.isApprox ) { // a^1 -> a
            aN.r = this.r;
            return aN;
        }
    }
    if ( aN.isNum ) {
        if ( aN.get('v') === 0 ) { // 0^b -> 0
            rN = alg.numConst( 0, aN.isApprox );
            rN.r = this.r;
            return rN;
        }
        if ( aN.get('v') === 1 ) { // 1^b -> 1
            rN = alg.numConst( 1, aN.isApprox );
            rN.r = this.r
            return rN;
        }
    }
    if ( aN.isPow ) {
        // (a^x)^b -> a^(x*b) 
        aN.a[1] = alg.mult( aN.a[1], bN );
        r = alg.pow( aN.a[0], aN.a[1] );
        r.r = this.r
        return r;
    }
    if ( aN.isMult ) {
        // (x*y)^b -> x^a * y^b
        l = aN.a.length;
        rN = alg.pow( aN.a[0], bN );
        for ( i=1; i<l; i++ ) {
            rN = alg.mult( rN, alg.pow( aN.a[i], bN.dup(rN) ) );
        }
        rN.r = this.r;
        return rN;
    }
    if ( aN.isSymb && aN.name === 'exp1' &&
         bN.isSymb && bN.id === 'ln' ) {
        bN.a[0].r = this.r;
        return bN.a[0];
    }
    aN.isNew = false;
    bN.isNew = false;
    return this;
};

Vect.prototype.act = function() {
    var A = this.a;
    var l = A.length;
    var i, j, B, C;

    for ( i=0; i<l; i++ ) {
        if ( A[i] === null ) {
            return null;
        }
    }
    if ( l === 1 ) {
        return this.a[0].ret( this.r );
    }
    if ( l === 2 && this.r !== null && this.r.isSymb &&
         this.r.id === '∀' && this.a[1].isEq ) {
        return this;
    }
    for ( i=0; i<l; i++ ) {
        if ( A[i].isAtypical ) {
            return A[i].over();
        }
    }
    return this;
};

Eq.prototype.act = function() {
    var aN = this.a[0], bN = this.a[1];

    if ( ! aN.isNew && ! bN.isNew ) {
        return this;
    }
    if ( aN.isAtypical || bN.isAtypical ) {
        if ( bN.isSymb && aN.isSymb &&
             aN.id === '↦' && bN.id !== '?' &&
             bN.a.length === 0 ) {
            this.a[0] = bN;
            this.a[1] = aN;
            aN = this.a[0];
            bN = this.a[1];
        }
        else if ( aN.isSymb && bN.isSymb &&
                  bN.id === '↦' && aN.id !== '?' &&
                  aN.a.length === 0 ) {
            aN.defAsFonction(bN);
            this.isDefining = true;
        }
        else if ( ( bN.isSymb && bN.id === '↦' ) ||
                  ( aN.isSymb && aN.id === '↦' ) ) {
            return null;
        }
        else if ( aN.isEq || bN.isEq ) {
            return null;
        }
    }
    aN.isNew = false;
    bN.isNew = false;
    return this;
};

