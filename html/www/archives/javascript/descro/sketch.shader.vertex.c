#version 100

precision mediump float;

attribute vec4 a_position;
attribute vec4 a_color;

uniform vec4 u_box;

varying vec4 v_color;

void main() {

  vec3 q = a_position.xyz;
  q.x *= u_box[2];
  q.y *= u_box[3];
  q.xy -= u_box.xy*a_position.w;

  gl_Position = vec4( q, a_position.w );
  v_color = a_color;
}
