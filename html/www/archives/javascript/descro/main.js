"use strict";

var construct;

function $(e) {
    return document.getElementById(e);
}

function log(s) {
    console.log(s);
}

function main() {

    var canvas3d, canvasSketch, container, windowing;
    var e, f, getDraw;

    canvas3d = document.createElement('canvas');
    canvasSketch = document.createElement('canvas');
    container = $('container');
    container.style.height = ( window.innerHeight - 65 ) + 'px';
    
    windowing = new Windowing(container);
    construct = new Construct( windowing, canvas3d, canvasSketch );
    windowing.tabulate();
    f = function(event) {
        var target = event.target;
        
        construct.ready = false;
        if ( construct.previousButton ) {
            construct.previousButton.innerText = 'Exécuter';
            construct.previousButton.style.backgroundColor = null;
        }
        construct.previousButton = target;
        target.innerText = 'Réactualiser';
        target.style.backgroundColor = 'lightGreen';
        if( construct.init( target.parentNode.
                            getElementsByTagName('code')[0].innerHTML ) ) {
            construct.ready = true;
        }
    };
    for ( e of document.getElementsByClassName('iButton') ) {
        e.addEventListener(
            'mousedown', f, false );
    }
    document.getElementsByClassName('iButton')[7].dispatchEvent(
        new CustomEvent(
            'mousedown', { bubbles: true } ) );
    
    window.requestAnimationFrame =
        window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;

    getDraw = function getDraw(render) {
        return function draw(t) {
            render(t);
            window.requestAnimationFrame(draw);
        };
    };
    
    window.requestAnimationFrame(
        getDraw( construct.render.bind(construct) ) );
    
    $('rotationBox').addEventListener(
        'change',
        function() {
            construct.triDi.rotating = this.checked;
            construct.triDi.time = -1;
        }, false );
    $('projectionBox').addEventListener(
        'change',
        function() {
            construct.ready = false;
            construct.triDi.projecting = this.checked;
            if( construct.init( construct.previousButton.parentNode.
                                getElementsByTagName('code')[0].innerHTML ) ) {
                construct.ready = true;
            }
        }, false );
    /*
    window.addEventListener( 'resize', resize, false );
    */
}

