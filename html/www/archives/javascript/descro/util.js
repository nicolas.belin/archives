"use strict";

var util = {};

util.preloadTextFiles = function(doAfter) {
    this.doAfter = doAfter;
    this.toBeLoaded =
        Array.prototype.slice.call( arguments, 1 );
    this.loaded = {};
    this.q = new XMLHttpRequest();
    this.q.overrideMimeType('text/plain');
    this.q.addEventListener( 'load', this );
    this.loadNext();
};

util.loadNext = function() {
    if ( this.toBeLoaded[0] ) {
        this.q.open( 'get', this.toBeLoaded[0], true );
        this.q.send();
    }
    else {
        this.doAfter();
    }
};

util.handleEvent = function(event) {
    switch(event.type) {
    case 'load':
        this.loaded[ this.toBeLoaded.shift() ] =
            this.q.responseText;
        this.loadNext();
        break;
    }
};

/*
util.routeEvent = function( element, initEvent, newEvent ) {
    var listener = function(event) {
        event.preventDefault();
        event.stopPropagation();
        element.dispatchEvent(
            new CustomEvent(
                newEvent, { bubbles: true } ) );
    };
    
    element.addEventListener( element, initEvent, listener );
};
*/

util.trackPointer = function( startElement, startEvent, startFunc/*( x, y, time, event )*/,
                              stopElement, stopEvent, stopFunc/*( dx, dy, time, event )*/,
                              
                              refreshFunc/*( dx, dy, time )*/) {

    var x0, y0, x1, y1;
    var start, track, animate;
    var tracking = false;
    
    track = function(event) {
        x0 = Math.round( event.clientX );
        y0 = Math.round( event.clientY );
    };
    
    start = function(event) {
        var t;
        tracking = true;
        x0 = x1 = Math.round( event.clientX );
        y0 = y1 = Math.round( event.clientY );
        document.addEventListener( 'mousemove', track, false );
        t = performance.now();
        startFunc( x0, y0, t, event );
        animate(t);
    };

    stop = function(event) {
        if ( tracking ) {
            document.removeEventListener( 'mousemove', track, false );
            tracking = false;
            stopFunc( Math.round( event.clientX ) - x1,
                      Math.round( event.clientY ) - y1,
                      performance.now(), event );
        }
    };

    animate = function(t) {
        refreshFunc( x0 - x1, y0 - y1, t );
        x1 = x0;
        y1 = y0;
        if ( tracking ) {
            window.requestAnimationFrame(animate);
        }
    };
    
    startElement.addEventListener( startEvent, start, false );
    stopElement.addEventListener( stopEvent, stop, false );
};
