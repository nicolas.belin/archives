#version 100

precision mediump float;

attribute vec4 a_position;
attribute vec4 a_color;

uniform mat4 u_projection;
uniform vec4 u_translate;
uniform vec4 u_sincos;

varying vec4 v_color;

void rotate2D( in float cos, in float sin, inout vec2 m ) {
  m = mat2( cos, sin, -sin, cos ) * m; 
}


void main() {

  vec3 q = a_position.xyz - u_translate.xyz * a_position.w;
  q *= u_translate.w;

  rotate2D( u_sincos[0], u_sincos[1], q.xy );
  rotate2D( u_sincos[2], u_sincos[3], q.xz );
  vec4 m = u_projection * vec4( q.yzx, a_position.w );
  gl_Position = m;
  v_color = a_color;
  gl_PointSize = 12.0 + m.z * 2.0;
}
