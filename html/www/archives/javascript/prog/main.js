/* initial rendering window : */
var xmin_init = -1;
var xmax_init = 1.5;
var ymin_init = -1.5;
var ymax_init = 1.5;
/******************************/

/* initial rendering function ***************/
var code_init = "\
/*                              *\n\
 *    THE RENDERING FUNCTION    *\n\
 *                              */\n\
function ( x, y ) {\n\
var u = 0;\n\
var v = 0;\n\
var t, n = 0;\n\
var red, green, blue;\n\
while ( u*u + v*v < 4 ) {\n\
// z_{n+1} = (z_n)^3 - (z_n)^2 + c\n\
t = u*u*u - 3*u*v*v - u*u + v*v + x;\n\
v = -v*v*v + 3*u*u*v - 2*u*v + y;\n\
u = t;\n\
n++;\n\
if ( n == 500 )// convergence :\n\
return [ 0, 0, 0 ];\n\
}\n\
// divergence :\n\
red = n % 256;\n\
green = (5*n) % 256;\n\
blue = (10*n) % 256;\n\
return [ red, green, blue ];\n\
}";
/******************************************/
var winJSON, container, boxLayer, fractLayer;
var boxCtx, fractCtx, worker = null, t0, image, pattern;

var w, h, l, percent, fractRect, rendering;

var xm0, ym0, mouse_x = NaN, mouse_y = NaN, mouse_down = false;

var code_lines, url = null;

function log(s) {
/*
    var e = document.getElementById("log_zone");
    var t = e.innerHTML;
    if ( t.length > 1000 )
        t = ""; 
    e.innerHTML = t + s + "<br />";
*/
}

function worker_log(s) {
    var e = document.getElementById("worker_log");
    var t = e.innerHTML;
    if ( t.length > 1000 )
        t = ""; 
    e.innerHTML = t + s + "<br />";
}

function render_fract() {
    var r = fractRect[ fractRect.length - 1 ];
    h = Math.round( w * (r.ymax-r.ymin) / (r.xmax-r.xmin) );
    container.style.height = h + 'px'; 
    boxLayer.height = h;
    fractLayer.height = h;
    boxLayer.width = w;
    fractLayer.width = w;
    image = fractCtx.createImageData( w, h );
    l = 0;
    percent = -1;
    var d = new Date();
    t0 = d.getTime();
    log( "render_fract: "
         + w + ', ' + h + ', '
         + r.xmin + ', ' + r.xmax + ', ' + r.ymin + ', ' + r.ymax );
    rendering = true;
    worker.postMessage(
        { cmd:"start", w: w, h: h,
          rect: r } );
}

function check_w() {
    var w_str = document.getElementById("w_str").value;
    var w_new = parseInt( w_str );
    if ( isNaN(w_new) ) {
        document.getElementById("NaN_w").innerHTML = w_str;
        document.getElementById("error_w").hidden = null;
        return false; 
    }
    else {
        document.getElementById("fract_second_col").style.width
            = w_new + 'px'; 
        w = w_new;
        document.getElementById("error_w").hidden = "hidden";
    }
    return true;
}

function parse_user_input() {
    if ( ! check_w() )
        return false;
    var rect_str = winJSON.value;
    try {
        var r = JSON.parse( rect_str );
    }
    catch (e) { // parsing problem
        document.getElementById("error_win_syntax").hidden = null;
        document.getElementById("exeption").innerHTML = e.message;
        return false;
    }

    if ( isNaN(r.xmin) || isNaN(r.xmax) || isNaN(r.ymin) || isNaN(r.ymax) || r.xmin>r.xmax || r.ymin>r.ymax ) {
        document.getElementById("error_win_val").hidden = null;
        return false;
    }
    else {
        document.getElementById("error_win_val").hidden = "hidden";
    }
    document.getElementById("error_win_syntax").hidden = "hidden";
    var p = fractRect[ fractRect.length - 1 ];
    if ( p == undefined || p.xmin != r.xmin || p.xmax != r.xmax ||p.ymin != r.ymin ||p.ymax != r.ymax )
        fractRect.push( { xmin: r.xmin, xmax: r.xmax, ymin: r.ymin, ymax: r.ymax } ); 
    return true;
}

function reset_win() {
    winJSON.value = JSON.stringify(
        { xmin: xmin_init,
          xmax: xmax_init,
          ymin: ymin_init,
          ymax: ymax_init } );
}

function load_func() {
    if ( worker != null ) {
        worker.terminate();
        log( "load_func: worker terminated" );
    }
    if ( url != null )
        URL.revokeObjectURL( url );

    var func_code = document.getElementById("func_code");
    
    // replace '<br />' by '/n' (Firefox workaround) :
    var s = func_code.innerHTML;
    var br = new RegExp( "<br>", "gi" );
    func_code.innerHTML = s.replace( br, "\n" );
    // end of workaround
    
    var code = func_code.textContent;
    try {
        s = "var custom_func = " + code + "\n";
        s += document.getElementById( "worker_code" ).textContent;
        var blob = new Blob( [ s ], { type: "text/javascript" } );
        url = URL.createObjectURL( blob );
        worker = new Worker( url );
        worker.addEventListener('message', receive_data, false );
        //URL.revokeObjectURL( url );
    }
    catch (e) { // parsing problem
        worker_log(
            "<span class='error'>Error : "
                + e.message
                + "</span>" );
    }
    func_code.innerHTML = code_makeup( code ); 
}

function code_makeup(code) {
    // 1. Take to pieces :
    var boundaries = new RegExp( "/\\*|//|\n" );
    var comment_token = "@_";
    var comments = new Array();
    var last_chars = new Array();
    var lines = new Array();
    var has_comment = new Array();
    var b, found, l, line, i, j, k, num = 0;
    var r, t, s, c = code.valueOf() + "\n";
    while ( found = c.match( boundaries ) ) {
        switch( found[0] ) {
        case "\n" :
            i = c.indexOf( "\n" );
            line = c.substr( 0, i ).trim();
            l = line.length;
            if ( l > 0 )
                last_chars.push( line[ l-1 ] );
            else
                last_chars.push( null );
            lines.push(line);
            if ( has_comment[num] == undefined )
                has_comment[num] = false;
            num++;
            c = c.substring( i + 1 );
            break;
        case "//" :
            i = c.indexOf( "//" );
            line = c.substr( 0, i ).trim();
            if ( (l = line.length) > 0 ) {
                last_chars.push( line[ l - 1 ] );
                lines.push( line + " " + comment_token ); 
            }
            else {
                last_chars.push( null );
                lines.push( comment_token ); 
            }
            has_comment[num] = true; 
            num++;
            c = c.substring(i);
            i = c.indexOf( "\n" );
            comments.push( c.substring( 0, i ) );
            c = c.substring( i + 1 );
            break;
        case "/*" :
            i = c.indexOf( "/*" );
            j = c.indexOf( "*/" );
            comments.push( c.substring( i, j+2 ) );
            r = c.substring( j+2 );
            k = r.indexOf( "\n" );
            if ( r.substr( 0, k ).trim() == "" ) {
                line = c.substr( 0, i );
                b = line.split( comment_token );
                l = b.length - 1;
                do {
                    t = b[l].trim();
                    l--;
                }
                while ( l>=0 && t == "" );
                if ( t == "" )
                    last_chars.push( null );
                else
                    last_chars.push( t[ t.length - 1 ] );
                lines.push( line + comment_token );
                has_comment[num] = true; 
                num++;
                c = r.substring( k + 1 );
            }
            else
                c = c.substr( 0, i ) + comment_token + r ; 
            break;
        }
    }

    // 2. Remove trailing empty lines
    while ( lines[ lines.length - 1 ] == "" )
        lines.pop();

    // 3. Colorize reserved words
    var reserved_words = "while|for|do|function|if|then|else|switch|case|catch|default|finally|try|with|var|return|switch|break|boolean|continue|delete|false|in|instanceof|new|null|this|throw|true|typeof";
    var word_char = new RegExp( "\\w" );
    var reserved_patt = new RegExp( reserved_words, "ig" );
    var w;
    l = lines.length;
    for ( k=0; k<l; k++ ) {
        s = lines[k];
        found = s.match( reserved_patt );
        if ( found ) {
            t = found.length;
            c = "";
            for ( j=0; j<t; j++ ){
                w = found[j];
                i = s.indexOf(w);
                if ( word_char.test( s.charAt( i-1 ) ) ||
                     word_char.test( s.charAt( i + w.length ) ) ) {
                    continue; // false hit
                }
                else {
                    c += s.substr( 0, i );
                    c += "<span class='reserved'>" + w + "</span>";
                }
                s = s.substring( i + w.length );
            }
            c += s;
            lines[k] = c.valueOf();
        }
    }

    // 4. Indentation
    var indents = new Array();
    var stack = new Array();
    var current = 0;
    var max = 0;
    l = lines.length;
    for ( i=0; i<l; i++ )
        indents[i] = 0;
    stack[0] = current;
    for ( i=0; i<l; i++ ) {
        switch ( last_chars[i] ) {
        case ";" :
            indents[i] += current;
            break;
        case "{" :
            indents[i] += current;
            stack.push( current );
            stack.push( indents[i] );
            current = indents[i] + 1;
            break;
        case "}" :
            indents[i] = stack.pop();
            current = stack.pop();
            break;
        case null :
            indents[i+1] += indents[i];
            indents[i] += current;
            break;
        case ":" :
            indents[i] += current - 1;
            break;
        default :
            indents[i] += current;
            indents[i+1]++;
            break;
        }
        if ( indents[i] > max )
            max = indents[i];
    }
    max++;
    var tab_length = 3;
    var tabs = [ "" ];
    var tab_unit = "";
    for ( i=0; i<tab_length; i++ )
        tab_unit += " ";
    for ( i=1; i<max; i++ )
        tabs.push( tabs[i-1] + tab_unit );

    // 5. Reconstitute :
    s = "";
    k = 0;
    for ( i=0; i<l; i++ ) {
        s += tabs[ indents[i] ];
        if ( has_comment[i] ) {
            c = lines[i];
            while ( ( j = c.indexOf( comment_token ) ) != -1 ) {
                s += c.substr( 0, j );
                s += "<span class='comments'>" + comments[k] + "</span>";
                k++;
                if ( c.length == j + comment_token.length ) {
                    c = "";
                    break;
                }
                else
                    c = c.substring( j + comment_token.length );
            }
            s += c;
        }
        else
         s += lines[i];
        s += "\n";
    }
    return s;
}

function reset_func() {
    var t = document.getElementById("func_code");
    t.innerHTML = code_makeup( code_init );
}

function main(){
    if( typeof(Worker) == "undefined" ) {
        document.getElementById("no_worker").hidden = null;
        return;
    }
    container = document.getElementById("container");
    boxLayer = document.getElementById("boxLayer");
    boxCtx = boxLayer.getContext("2d");
    makePattern();
    boxLayer.addEventListener( 'mousedown', startSelectArea, false );
    boxLayer.addEventListener( 'mouseup', stopSelectArea, false );
    boxLayer.addEventListener( 'mouseout', stopSelectArea, false );
    fractLayer = document.getElementById("fractLayer");
    fractLayer.addEventListener( 'mousedown', clickOnImage, false );
    fractCtx = fractLayer.getContext("2d");
    fractRect = new Array();
    winJSON = document.getElementById("winJSON");
    reset_win();
    reset_func();
    load_func();
    parse_user_input();
    render_fract();
}

function fill_table() {
    var d = new Date();
    var t = d.getTime() - t0;
    document.getElementById("timer").innerHTML = t;
    document.getElementById("height").innerHTML = h;
    var r = fractRect[ fractRect.length - 1 ];
    document.getElementById("xmin").innerHTML = r.xmin;
    document.getElementById("xmax").innerHTML = r.xmax;
    document.getElementById("ymin").innerHTML = r.ymin;
    document.getElementById("ymax").innerHTML = r.ymax;
}

function render_previous() {
    var l = fractRect.length;
    if ( l > 1 ) {
        if ( l == 2 )
            back_button.style.display = "none";
        fractRect.pop();
        winJSON.value = JSON.stringify( fractRect[ fractRect.length - 1 ] );
        render_fract();
    }
}

function receive_data(event) {
    var s = event.data;
    var i, length = s.length;
    if ( s.cmd != undefined ) { // special cases
        switch (s.cmd) {
        case "over" : // rendering is over
            fractCtx.putImageData( image, 0, 0 );
            document.getElementById("imageURL").href
                = fractLayer.toDataURL();
            fill_table();
            if ( fractRect.length > 1 )
                back_button.style.display = "inline";
            rendering = false;
            break;
        }
    }
    else { // s is a string
        for ( i=0; i<length; i+=6 ) {
            image.data[l++] = parseInt( s.substr( i,2 ), 16 );
            image.data[l++] = parseInt( s.substr( i+2, 2 ), 16 );
            image.data[l++] = parseInt( s.substr( i+4, 2 ), 16 );
            image.data[l++] = 0xff;
            if( Math.floor( 100*l/(w*h*4) ) > percent+1 ){
                percent++;
                var xb = Math.floor( w/10*(percent%10) );
                var yb = Math.floor( h/10*Math.floor(percent/10) );
                var lb = 4 * ( xb + yb*w );
                fractCtx.fillStyle = "rgb(" +
                    image.data[lb] + ","
                    + image.data[lb+1] + ","
                    + image.data[lb+2] + ")";
                fractCtx.fillRect( xb, yb, w/10 + 1, h/10 + 1 );
            }
        }
    }
}

function draw_rect( xr0, yr0, wr0, hr0 ) {
    var xr1, yr1, xr2, yr2;

    var xm = mouse_x;
    var ym = mouse_y;
    if ( !( isNaN(xm0) || isNaN(ym0) || isNaN(xm) || isNaN(ym)) ) {
        if ( ! isNaN(xr0) ) {
            boxCtx.clearRect( xr0-1, yr0-1, wr0+2, hr0+2 );
        }
        xr1 = Math.min( xm0, xm );
        yr1 = Math.min( ym0, ym );
        var wr1 = Math.abs( xm0 - xm );
        var hr1 = Math.abs( ym0 - ym );
        boxCtx.strokeStyle = pattern;
        boxCtx.strokeRect( xr1, yr1, wr1, hr1 );
    }
    if ( mouse_down ){
        setTimeout( 'draw_rect(' + xr1 + ',' + yr1 + ',' + wr1 + ',' + hr1 + ')', 50 );
    }
    else if ( ! ( isNaN(xm0) || isNaN(ym0) || isNaN(xm) || isNaN(ym) ) ) {
        boxLayer.removeEventListener( 'mousemove', mouseTracking, false );
        boxLayer.style.display = "none";
        boxCtx.clearRect( 0, 0, w, h );
        mouse_x = NaN;
        mouse_y = NaN;
        xr1 = Math.min( xm0, xm );
        yr1 = Math.min( ym0, ym );
        xr2 = Math.max( xm0, xm );
        yr2 = Math.max( ym0, ym );
        var r = fractRect[ fractRect.length - 1 ];
        var xmin = r.xmin + xr1 * ( r.xmax - r.xmin ) / w;
        var xmax = r.xmin + xr2 * ( r.xmax - r.xmin ) / w;
        var ymin = r.ymin + yr1 * ( r.ymax - r.ymin ) / h;
        var ymax = r.ymin + yr2 * ( r.ymax - r.ymin ) / h;
        var i = -Math.floor( Math.log( xmax - xmin ) / Math.LN10 );
        xmin = parseFloat( xmin.toFixed(i+4) );           
        xmax = parseFloat( xmax.toFixed(i+4) );           
        ymin = parseFloat( ymin.toFixed(i+4) );           
        ymax = parseFloat( ymax.toFixed(i+4) );           
        var r = { xmin: xmin, xmax: xmax, ymin: ymin, ymax: ymax };
        winJSON.value = JSON.stringify(r);
        if ( check_w() ) {
            fractRect.push(r);
            render_fract();
        }
    }
}

function makePattern() {
    var canvas = document.createElement("canvas");
    canvas.width = 2;
    canvas.height = 2;
    var ctx = canvas.getContext("2d");
    var img = ctx.createImageData(2,2);
    var data = [ 0xff,0xff,0xff,0xff, 0x00,0x00,0x00,0xff,
                 0x00,0x00,0x00,0xff, 0xff,0xff,0xff,0xff ];
    for ( var i=0; i<16; i++ )
        img.data[i] = data[i];
    ctx.putImageData( img, 0, 0 );
    pattern = boxCtx.createPattern( canvas, "repeat" );
}

function mouseTracking(event) {
    var rect = boxLayer.getBoundingClientRect();
    mouse_x = Math.round( event.clientX - rect.left );
    mouse_y = Math.round( event.clientY - rect.top );
    if ( isNaN(xm0) || isNaN(ym0) ) {
        xm0 = mouse_x;
        ym0 = mouse_y;
    }
}

function startSelectArea(event) {
    log( "start !" );
    switch ( event.button ) {
    case 0 : // left button
        mouse_down = true;
        boxLayer.style.display = "inline";
        boxLayer.addEventListener( 'mousemove', mouseTracking, false );
        xm0 = NaN;
        ym0 = NaN;
        draw_rect( NaN, NaN, NaN, NaN );
        break;
    case 2 : // right button
        // so we can 'Save As' images :
        boxLayer.style.display = "none";
        break;
    } 
}

function clickOnImage(event) {
    switch ( event.button ) {
    case 0 : // left button
        boxLayer.style.display = "inline";
        startSelectArea(event);
        break;
    } 
}

function stopSelectArea() {
    log( "stop : up !" );
    mouse_down = false;
}

function on_click_back() {
    if( ! rendering )
        render_previous();
}

function on_click_render_again() {
    if ( parse_user_input() )
        render_fract();
}
