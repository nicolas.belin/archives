var w, h, xmin, xmax, ymin, ymax;
var custom_function, nmax; // set by a message from the main thread
var nmax_set = false;
var func_set = false;

function color_component(c) {
    var s = "";
    if ( c<16 )
        s += "0";
    s += c.toString(16);
    return s;
}

function test( a, b ) {
    var X1, X = 0;
    var Y1, Y = 0;
    var n = 0;
    while ( X*X + Y*Y < 4 ) {
        X1 = X*X - Y*Y + a;
        Y1 = 2*X*Y + b;
        X = X1;
        Y = Y1;
        n++;
        if ( n == nmax ) // convergence
            return [0,0,0];
    }
    return [ (n) % 0x100, (5*n) % 0x100, (10*n) % 0x100 ];
}

function get_color( ax, ay ) {
    var a = xmin + ax * ( xmax-xmin ) / w;
    var b = ymin + ay * ( ymax-ymin ) / h;
    var c = custom_function( a, b );
    var s = "";
    s += color_component( c[0] );
    s += color_component( c[1] );
    s += color_component( c[2] );
    return s;
}

function render(){
    var x = 0;
    var y = 0;
    var str = "";
    var i = 0;
    do {
        str +=  get_color( x, y );
        i++;
        if ( i >= 300 ){
            postMessage( str );
            str = "";
            i = 0;
        }
        x++;
        if ( x >= w ){
            y++;
            x = 0;
        }
    }
    while ( y < h );
    if ( str.length > 0 ) {
        postMessage( str );
    }
    postMessage( { cmd: "over" } );
}

function entry(event) {
    var data = event.data;
    switch( data.cmd ) {
    case "start" :
        xmin = data.rect.xmin;
        xmax = data.rect.xmax;
        ymin = data.rect.ymin;
        ymax = data.rect.ymax;
        w = data.w;
        h = data.h;
        if ( nmax_set && func_set )
            render();
        break;
    case "set_nmax" :
        nmax = data.nmax;
        postMessage( { cmd: "log", string: "'nmax' set to " + nmax } );
        nmax_set = true;
        break;
    case "set_func" :
        func_set = true;
        try {

           //  var blob = new Blob( [ data.code ], { type: "text/javascript" } );
         //   var url = URL.createObjectURL( blob );
            custom_function = test;
         //   importScripts( url );
            // test2;
            // eval( s );
        }
        catch (e) { // parsing problem
            postMessage( { cmd: "log", string: "<span class='error'>Error : " + e.message + "</span>", func_ok: false } );
            func_set = false;
        }
        if ( func_set )
            postMessage( { cmd: "log", string: "Function loaded", func_ok: true } );
        break;
    }
}

self.addEventListener('message', entry );
