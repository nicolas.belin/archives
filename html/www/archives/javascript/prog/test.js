function test2( a, b ) {
    var X1, X = 0;
    var Y1, Y = 0;
    var n = 0;
    while ( X*X + Y*Y < 4 ) {
        X1 = X*X - Y*Y + a;
        Y1 = 2*X*Y + b;
        X = X1;
        Y = Y1;
        n++;
        if ( n == nmax ) // convergence
            return [0,0,0];
    }
    return [ (n) % 0x100, (5*n) % 0x100, (10*n) % 0x100 ];
}
