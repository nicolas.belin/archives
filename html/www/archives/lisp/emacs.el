;---------------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(case-fold-search t)
 '(current-language-environment "English")
 '(global-font-lock-mode t nil (font-lock))
 '(kill-whole-line t)
 '(python-shell-interpreter "python3")
 '(tab-width 6))
 '(mwheel-install)
;----------------------------------------------------------------------------
(setq-default TeX-master nil)
(setq TeX-parse-self t)
(setq TeX-auto-save t)
;----------------------------------------------------------------------------
(setq-default indent-tabs-mode nil) ; ne pas utiliser le caractere TAB
(global-set-key (kbd "<f11>") (quote bury-buffer)) ; recule dans la liste des buffers
(global-set-key (kbd "<f12>") (quote unbury-buffer)) ; avance dans la liste des buffers
(menu-bar-mode -1)              ; supprime la barre de menu
(show-paren-mode)               ; indique le parentheses correspondantes
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(load-file "~/lisp/myLaTeXmode.el")
(load-file "~/lisp/myCmode.el")
(load-file "~/lisp/myHTMLmode.el")
(load-file "~/lisp/myJSmode.el")
(load-file "~/lisp/myCmode.el")
;----------------------------------------------------------------------------
(defun kill-entire-line ()
  "Suppress the current line"
  (interactive)
  (beginning-of-line)
  (kill-line))
(substitute-key-definition 'kill-line 'kill-entire-line (current-global-map))
;----------------------------------------------------------------------------
(defun my-word-completion ()
  "Complete the current word"
  (interactive)
  (let (the-prefix start-word-left start-word-right found-word-left)
    (setq start-word-right (point))
    (re-search-backward "\\_<")
    (if (eq last-command 'my-word-completion)
        (setq the-prefix (car my-word-completion-data))
      (setq the-prefix (buffer-substring (point) start-word-right)))
    (setq start-word-left (point))
    (save-excursion
      (if (eq last-command 'my-word-completion)
          (goto-char (nth 1 my-word-completion-data)))
      (if (setq found-word-left (re-search-backward (concat "\\_<" the-prefix) nil t))
          (progn
            (copy-region-as-kill found-word-left (re-search-forward "\\_>"))
            (goto-char found-word-left))
        (goto-char start-word-left)
        (kill-new the-prefix))
      (setq my-word-completion-data (list the-prefix (point))))
    (delete-region start-word-left start-word-right)
    (yank))
  (setq this-command 'my-word-completion))

(global-set-key [print] 'my-word-completion)
;-------------------------------------------------------------
(defun my-delete-suffix (s suffix)
  "If current buffer filename has suffix SUFFIX, give this filename without it" 
  (let (l l-suffix)
    (setq l-suffix (length suffix))
    (setq l (length s))
;    (if (not (string= suffix (substring buffer-file-name (- l l-suffix))))
    (if (not (string= suffix (substring s (- l l-suffix))))
        (error "Suffix doesn't match %s" suffix)
      (substring s 0 (- l l-suffix))
      )))
;-------------------------------------------------------------
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
