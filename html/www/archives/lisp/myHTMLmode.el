(defun my-end-env ()
  "Ferme le dernier <élément> qui a été ouvert."
  (interactive)
  (condition-case nil
	(let ((n 1))
	  (save-excursion
	    (while (> n 0)
		(re-search-backward "\\\\end\\|\\\\begin")
		(if (char-equal ?b (char-after (+ 1 (point))))
		    (setq n (- n 1))
		  (setq n (+ n 1))))
	    (search-forward "{")
	    (copy-region-as-kill (- (point) 1) (progn (search-forward "}") (point))))
	  (princ "\\end" (current-buffer))
	  (yank))
    (error (message "Pas de d'élément à compléter"))))

;--------------------------------------------------------

(defun my-add-html-keys ()
  (define-key html-mode-map "\C-ce" 'my-end-env))

(add-hook 'html-mode-hook 'my-add-html-keys)
