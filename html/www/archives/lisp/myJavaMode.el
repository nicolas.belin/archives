(defun my-java-build ()
  "Run javac on the current buffer"
  (interactive)
  (save-buffer)
  (let (command output)
    (setq command "make && java ")
    (setq command (concat command (my-delete-suffix (buffer-name) ".java")))
    (setq output (shell-command-to-string command))
    (split-window-vertically)
    (other-window 1)
    (switch-to-buffer "my-buffer")
    (erase-buffer)
    (insert output)
))

(global-set-key "\C-cj" 'my-java-build)
;--------------------------------------------------------

