/* initial rendering window : */
var xmin_init = -2.1;
var xmax_init = 0.9;
var ymin_init = -1.5;
var ymax_init = 1.5;
/******************************/

var winJSON, container, boxLayer, fractLayer, boxCtx, fractCtx, worker, t0, image, pattern;

var w, h, l, percent, fractRect, rendering;

var xm0, ym0, mouse_x = NaN, mouse_y = NaN, mouse_down = false;

function log(s) {
/*
    var e = document.getElementById("log_zone");
    var t = e.innerHTML;
    if ( t.length > 1000 )
        t = ""; 
    e.innerHTML = t + s + "<br />";
*/
}

function render_fract() {
    var r = fractRect[ fractRect.length - 1 ];
    h = Math.round( w * (r.ymax-r.ymin) / (r.xmax-r.xmin) );
    container.style.height = h + 'px'; 
    boxLayer.height = h;
    fractLayer.height = h;
    boxLayer.width = w;
    fractLayer.width = w;
    image = fractCtx.createImageData( w, h );
    l = 0;
    percent = -1;
    var d = new Date();
    t0 = d.getTime();
    log( "render_fract: "
         + w + ', ' + h + ', '
         + r.xmin + ', ' + r.xmax + ', ' + r.ymin + ', ' + r.ymax );
    rendering = true;
    worker.postMessage(
        { cmd:"start", w: w, h: h,
          rect: r } );
}

function parse_user_input() {
    var nmax_str = document.getElementById("nmax_str").value;
    var nmax_new = parseInt( nmax_str );
    if ( isNaN(nmax_new) ) {
        document.getElementById("NaN_nmax").innerHTML = nmax_str;
        document.getElementById("error_nmax").hidden = null;
        return false;
    }
    else {
        worker.postMessage( { cmd:"change_nmax", nmax: nmax_new } );
        document.getElementById("error_nmax").hidden = "hidden";
    }
    var w_str = document.getElementById("w_str").value;
    var w_new = parseInt( w_str );
    if ( isNaN(w_new) ) {
        document.getElementById("NaN_w").innerHTML = w_str;
        document.getElementById("error_w").hidden = null;
        return false; 
    }
    else {
        document.getElementById("fract_second_col").style.width
            = w_new + 'px'; 
        w = w_new;
        document.getElementById("error_w").hidden = "hidden";
    }
    var rect_str = winJSON.value;
    try {
        var r = JSON.parse( rect_str );
    }
    catch (e) { // parsing problem
        document.getElementById("error_win_syntax").hidden = null;
        document.getElementById("exeption").innerHTML = e.message;
        return false;

    }

    if ( isNaN(r.xmin) || isNaN(r.xmax) || isNaN(r.ymin) || isNaN(r.ymax) || r.xmin>r.xmax || r.ymin>r.ymax ) {
        document.getElementById("error_win_val").hidden = null;
        return false;
    }
    else {
        document.getElementById("error_win_val").hidden = "hidden";
    }
    document.getElementById("error_win_syntax").hidden = "hidden";
    fractRect.push( { xmin: r.xmin, xmax: r.xmax, ymin: r.ymin, ymax: r.ymax } ); 
    return true;
}

function reset_win() {
    winJSON.value = JSON.stringify(
        { xmin: xmin_init,
          xmax: xmax_init,
          ymin: ymin_init,
          ymax: ymax_init } );
}

function main(){
    if( typeof(Worker) == "undefined" ) {
        document.getElementById("no_worker").hidden = null;
        return;
    }
    container = document.getElementById("container");
    boxLayer = document.getElementById("boxLayer");
    boxCtx = boxLayer.getContext("2d");
    makePattern();
    boxLayer.addEventListener( 'mousedown', startSelectArea, false );
    boxLayer.addEventListener( 'mouseup', stopSelectArea, false );
    boxLayer.addEventListener( 'mouseout', stopSelectArea, false );
    fractLayer = document.getElementById("fractLayer");
    fractLayer.addEventListener( 'mousedown', clickOnImage, false );
    fractCtx = fractLayer.getContext("2d");
    worker = new Worker( "fractale_worker.js" );
    worker.addEventListener('message', receive_data, false );
    fractRect = new Array();
    winJSON = document.getElementById("winJSON");
    reset_win();
    parse_user_input();
    render_fract();
}

function fill_table() {
    var d = new Date();
    var t = d.getTime() - t0;
    document.getElementById("timer").innerHTML = t;
    document.getElementById("height").innerHTML = h;
    var r = fractRect[ fractRect.length - 1 ];
    document.getElementById("xmin").innerHTML = r.xmin;
    document.getElementById("xmax").innerHTML = r.xmax;
    document.getElementById("ymin").innerHTML = r.ymin;
    document.getElementById("ymax").innerHTML = r.ymax;
}

function render_previous() {
    var l = fractRect.length;
    if ( l > 1 ) {
        if ( l == 2 )
            back_button.style.display = "none";
        fractRect.pop();
        winJSON.value = JSON.stringify( fractRect[ fractRect.length - 1 ] );
        render_fract();
    }
}

function receive_data(event) {
    var s = event.data;
    var i, length = s.length;
    if ( length == 1 ) { // special cases
        switch (s) {
        case "1" : // rendering is over
            fractCtx.putImageData( image, 0, 0 );
            document.getElementById("imageURL").href
                = fractLayer.toDataURL();
            fill_table();
            if ( fractRect.length > 1 )
                back_button.style.display = "inline";
            rendering = false;
            break;
        }       
    }
    else {
        for ( i=0; i<length; i+=6 ) {
            image.data[l++] = parseInt( s.substr( i,2 ), 16 );
            image.data[l++] = parseInt( s.substr( i+2, 2 ), 16 );
            image.data[l++] = parseInt( s.substr( i+4, 2 ), 16 );
            image.data[l++] = 0xff;
            if( Math.floor( 100*l/(w*h*4) ) > percent+1 ){
                percent++;
                var xb = Math.floor( w/10*(percent%10) );
                var yb = Math.floor( h/10*Math.floor(percent/10) );
                var lb = 4 * ( xb + yb*w );
                fractCtx.fillStyle = "rgb(" +
                    image.data[lb] + ","
                    + image.data[lb+1] + ","
                    + image.data[lb+2] + ")";
                fractCtx.fillRect( xb, yb, w/10 + 1, h/10 + 1 );
            }
        }
    }
}

function draw_rect( xr0, yr0, wr0, hr0 ) {
    var xr1, yr1, xr2, yr2;

    var xm = mouse_x;
    var ym = mouse_y;
    if ( !( isNaN(xm0) || isNaN(ym0) || isNaN(xm) || isNaN(ym)) ) {
        if ( ! isNaN(xr0) ) {
            boxCtx.clearRect( xr0-1, yr0-1, wr0+2, hr0+2 );
        }
        xr1 = Math.min( xm0, xm );
        yr1 = Math.min( ym0, ym );
        var wr1 = Math.abs( xm0 - xm );
        var hr1 = Math.abs( ym0 - ym );
        boxCtx.strokeStyle = pattern;
        boxCtx.strokeRect( xr1, yr1, wr1, hr1 );
    }
    if ( mouse_down ){
        setTimeout( 'draw_rect(' + xr1 + ',' + yr1 + ',' + wr1 + ',' + hr1 + ')', 50 );
    }
    else if ( ! ( isNaN(xm0) || isNaN(ym0) || isNaN(xm) || isNaN(ym) ) ) {
        boxLayer.removeEventListener( 'mousemove', mouseTracking, false );
        boxLayer.style.display = "none";
        boxCtx.clearRect( 0, 0, w, h );
        mouse_x = NaN;
        mouse_y = NaN;
        xr1 = Math.min( xm0, xm );
        yr1 = Math.min( ym0, ym );
        xr2 = Math.max( xm0, xm );
        yr2 = Math.max( ym0, ym );
        var r = fractRect[ fractRect.length - 1 ];
        var xmin = r.xmin + xr1 * ( r.xmax - r.xmin ) / w;
        var xmax = r.xmin + xr2 * ( r.xmax - r.xmin ) / w;
        var ymin = r.ymin + yr1 * ( r.ymax - r.ymin ) / h;
        var ymax = r.ymin + yr2 * ( r.ymax - r.ymin ) / h;
        var i = -Math.floor( Math.log( xmax - xmin ) / Math.LN10 );
        xmin = parseFloat( xmin.toFixed(i+4) );           
        xmax = parseFloat( xmax.toFixed(i+4) );           
        ymin = parseFloat( ymin.toFixed(i+4) );           
        ymax = parseFloat( ymax.toFixed(i+4) );           
        var r = { xmin: xmin, xmax: xmax, ymin: ymin, ymax: ymax };
        winJSON.value = JSON.stringify(r);
        fractRect.push(r);
        render_fract();
    }
}

function makePattern() {
    var canvas = document.createElement("canvas");
    canvas.width = 2;
    canvas.height = 2;
    var ctx = canvas.getContext("2d");
    var img = ctx.createImageData(2,2);
    var data = [ 0xff,0xff,0xff,0xff, 0x00,0x00,0x00,0xff,
                 0x00,0x00,0x00,0xff, 0xff,0xff,0xff,0xff ];
    for ( var i=0; i<16; i++ )
        img.data[i] = data[i];
    ctx.putImageData( img, 0, 0 );
    pattern = boxCtx.createPattern( canvas, "repeat" );
}

function mouseTracking(event) {
    var rect = boxLayer.getBoundingClientRect();
    mouse_x = Math.round( event.clientX - rect.left );
    mouse_y = Math.round( event.clientY - rect.top );
    if ( isNaN(xm0) || isNaN(ym0) ) {
        xm0 = mouse_x;
        ym0 = mouse_y;
    }
}

function startSelectArea(event) {
    log( "start !" );
    switch ( event.button ) {
    case 0 : // left button
        mouse_down = true;
        boxLayer.style.display = "inline";
        boxLayer.addEventListener( 'mousemove', mouseTracking, false );
        xm0 = NaN;
        ym0 = NaN;
        draw_rect( NaN, NaN, NaN, NaN );
        break;
    case 2 : // right button
        // so we can 'Save As' images :
        boxLayer.style.display = "none";
        break;
    } 
}

function clickOnImage(event) {
    log( 'clickOnImage' );
    switch ( event.button ) {
    case 0 : // left button
        boxLayer.style.display = "inline";
        startSelectArea(event);
        break;
    } 
}

function stopSelectArea() {
    log( "stop : up !" );
    mouse_down = false;
}

function on_click_back() {
    if( ! rendering )
        render_previous();
}

function on_click_render_again() {
    if ( parse_user_input() )
        render_fract();
}

function on_click_reset() {
    reset_win();
}