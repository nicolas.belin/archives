function factorielle(n) {
    var i, f;
    for ( i=2, f=1; i<=n; i++ )
        f *= i;
    return f;
}

function on_click_poisson(){
    document.getElementById("erreur_lambda").hidden = "hidden";
   // document.getElementById("poisson_tab").hidden = "hidden";
    var l_str = document.getElementById("lambda_str").value;
    var l = parseFloat( l_str );
    if ( isNaN(l) ){
        document.getElementById("NaN_lambda").innerHTML = l_str;
        document.getElementById("erreur_lambda").hidden = null;
    }
    else {
        document.getElementById("lambda").childNodes[0].nodeValue = l;
        var poisson_tab = document.getElementById("poisson_tab");
        var rows = document.getElementsByClassName("poisson_row");
        var rl = rows.length; 
        for ( ; rl>0; rl-- ) 
            poisson_tab.removeChild( rows[0] );
        var k = 0;
        var e_l = Math.exp( -l );
        var prob = "";
        while ( k<l || prob != "0.000" ){
            left_cell = document.createElement("td");
            left_cell.innerHTML = k;
            right_cell = document.createElement("td");
            prob = ((Math.pow(l,k)*e_l)/factorielle(k)).toFixed(3);
            right_cell.innerHTML = prob;
            row_k = document.createElement("tr");
            row_k.className = "poisson_row";
            row_k.appendChild( left_cell )
            row_k.appendChild( right_cell )
            poisson_tab.appendChild( row_k );
            k++;
        }
        poisson_tab.hidden = null;
    }
}
