var tree = {};

tree.roots = [];

tree.getRoot = function(i) {
    return tree.roots[i];
};

tree.addRoot = function(root) {
    root.r = null;
    tree.roots.push(root);
    return this.roots.length - 1;
};

tree.del = function(i) {
    tree.roots[i].scanUp(
        function() {
            if ( this.del ) {
                this.del();
            }
            return this.a;
        } );
    delete( tree.roots[i] );
};

tree.str2tree = function(string) {

    function splitStr( s, i ){
        var j, A, l;
        
        if ( i === -1 ) {
            return s;
        }
        if ( s === "" ) {
            return "?";
        }
        A = s.split( ops[i] );
        l = A.length;
        if ( l === 1 ) {
            A = splitStr( s, i-1 );
        }
        else {
            for ( j=0; j<l; j++ ) { 
                if ( A[j] === "" ) {
                    A[j] = "?";
                }
            }
            A.unshift( ops[i] );
            for ( j=1; j<=l; j++ ) { 
                    A[j] = splitStr( A[j], i-1 ); 
            }
        }
        return A;
    }
    
    function findPars(s) {
        var k, l, p, c = 1;
        
        p = s.indexOf( '(' );
        if ( p === -1 ) {
            if ( s.indexOf( ')' ) >= 0 ) {
                return 0;
            }
            return null;
        }
        if ( s.indexOf( ')' ) < p ) {
            return 0;
        }
        
        l = s.length;
        k = p;
        while ( k < l-1 && c > 0 ) {
            k++;
            if ( s.charAt(k) === '(' ) {
                c++;
            }
            else if ( s.charAt(k) === ')' ) {
                c--;
            }
        }
        if ( k === l-1 && c>0 ) {
            return 0;
        }
        return { opening:p, closing:k };
    }
    
    function stackPars(index) {
        var left, between, right;
        var s = fifo[index];
        var p;
        
        while ( ( p = findPars(s) ) !== null ) {
            if ( p === 0 ) {
                return null;
            }
            left = s.substring(0, p.opening );
            between = s.substring( p.opening + 1, p.closing );
            right = s.substring( p.closing + 1 ); 
            if ( left.length > 0 &&
                 ops.indexOf( left.charAt( left.length-1 ) ) === -1
               ) {
                s = left + comp + '#' + right;
            }
            else {
                    s = left + '#' + right;
            }
            fifo.push(between);
            if ( stackPars(fifo.length-1) === null ) {
                return null;
            }
        }
        fifo[index] = s;
    }
    
    function connectArrays(A) {
        var i, r, l;
        
        if ( A[0] === comp ) {
            A.shift();
        }
        l = A.length;
        for ( i=0; i<l; i++ ) {
            if ( A[i] === '#' ) {
                r = fifo.shift();
                A[i] = connectArrays(r);
            }
            else if ( typeof( A[i] ) !== 'string' ) {
                A[i] = connectArrays( A[i] );
            }
        }
        return A;
    }
    
    function build(B) {
        var v, aN, bN, l, j;
        
        if ( typeof(B) === 'string' ) {
            v = parseFloat(B);
            if ( isNaN(v) ){
                aN = alg.symb( B, null );
            }
            else {
                aN = alg.numConst(v);
            }
            leaves.push(aN);
            return aN;
        }
        if ( B.length === 2 ) {
            return alg.symb( B[0], build( B[1] ), true );
        }
        l = B.length;
        aN = build( B[1] );
        switch( B[0] ) {
        case "^" :
            for ( j=2; j<l; j++ ) {
                aN = alg.pow( aN, build( B[j] ), true );
            }
            return aN;
        case "/" :
            for ( j=2; j<l; j++ ) {
                aN = alg.div( aN, build( B[j] ), true );
            }
            return aN;
        case "+" :
            for ( j=2; j<l; j++ ) {
                aN = alg.add( aN, build( B[j] ), true );
            }
            return aN;
        case "*" :
            for ( j=2; j<l; j++ ) {
                aN = alg.mult( aN, build( B[j] ), true );
            }
            return aN;
        case ",":
            B.shift();
            B[0] = aN;
            for ( j=1; j<l-1; j++ ) {
                B[j] = build( B[j] );
            }
            return alg.vect( B, true );
        case '=':
            return alg.eq( aN, build( B[2] ), true ); 
        default:
            console.error( "Error : undefined operation '" + B[0] + "'" );
            return null;
        }            
    }
        
    var b = true;
    var s = "";
    var comp = '&';
    // operations by order of precedence :
    var ops = [ comp, '^', '/', '*', '+', ',', '=' ];
    var fifo = [];
    var leaves = [];
    var B, j, i, str;
        
    if ( string.length === 0 ) {
        return null;
    }
    str = string.trim();
    if ( str === "" ) {
        return null;
    }
    str = '(' + str + ')';
    while ( ( i = str.indexOf("|") ) != -1 ){
        s += str.substring( 0, i );
        if (b){
            s += "abs(";
            b = false;
        }
        else{
            s += ")";
            b = true;
        }
        str = str.substring( i + 1 );
    }
    s += str;
    s = s.replace( / /g, "" );
    s = s.replace( /²/g, "^2" );
    s = s.replace( /\u002d/g, "+(-1)*" ); // -
    s = s.replace( /\u0028\u002b/g, "(0+" ); // (+
    s = s.replace( /\u002c\u002b/g, ",0+" ); // ,+
    s = s.replace( /\u005b/g, "(" ); // [
    s = s.replace( /\u005d/g, ")" ); // ]

    fifo.push( s );
    if ( stackPars(0) === null ) {
        worker.log( "ERROR : mismatching parenthesis" );
        return null;
    }
    for ( j=fifo.length-1; j>= 0; j-- ) {
        if ( fifo[j] === "#" ) {
            fifo.splice( j, 1 );
        }
        else {
            fifo[j] = splitStr( fifo[j], ops.length-1 );
        }
    }
    B = connectArrays( [ '#' ] );
    if ( B === null ) {
        return null;
    }
    // rebuild the whole function :
    build( B[0] );

    // and then simplify
    return alg.simplify(leaves);
};

tree.split = function(root) {
    var AN = [];
    var i, l = root.a.length;
    if ( l > 0 ) {
        root = root.dup(null);
        for( i=0; i<l; i++ ) {
            if ( ! ( root.a[i].isSymb && root.a[i].id === '?' ) ) {
                root.a[i].r = null;
                AN.push( root.a[i] );
                root.a[i] = alg.symb('?');
                root.a[i].r = root;
            }
        }
        if ( AN.length > 0 ) {
            AN.unshift(root);
        }
    }
    return AN;
};
