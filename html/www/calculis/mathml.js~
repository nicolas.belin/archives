var mathml = { digitsNr:3 };

mathml.get = function(root) {
    var s = root.getMathml();
    if ( root.get('v') !== null && ! root.isNum && ! root.isStatement ) {
        s += "<mstyle class='approx'><mo>≈</mo>" +
            this.printApproxNum( root.get('v') ) + "</mstyle>";
    }
    return s;
};

mathml.addPar = function( aN, bN, forceOnMinus ) {
    var s = bN.getMathml();
    
    if ( aN.precedence > bN.precedence ||
			( forceOnMinus && s.charAt(4) === '-' ) ) {
        s = "<mo>(</mo><mrow>" + s + "</mrow><mo>)</mo>";
    }
    return s;
};

mathml.printApproxNum = function(v) {
    var z, e, vs, d = this.digitsNr;
    var s = '';
    if ( v === 0 ) {
        z = true;
        v = 1;
    }
    if ( v < 0 ) {
        s += "<mo>-</mo>" 
        v = Math.abs(v);
    }
    s += "<mn>";
    e = Math.floor( Math.log(v) / Math.LN10 );
    v *= Math.pow( 10, d - 1 - e );
    v = Math.round(v);
    vs = v.toString();
    if (z) {
        vs = '0' + vs.substr(1);
    }

    if ( e === -1 ) {
        s += '0';
        s += this.comma;
        s += vs;
    }
    else if ( e < -1 || e > d ) {
        s += vs.charAt(0);
        s += this.comma;
        s += vs.slice(1);
    }
    else if ( e === d-1 ) {
        s += vs;
    }
    else if ( e === d ) {
        s += vs + '0';
    }
    else {
        s += vs.substring( 0, e + 1 );
        s += this.comma;
        s += vs.slice( e + 1 );
    }
    s += "</mn>";
    if ( e < -1 || e > d ) {
        s += "<mo>×</mo><msup><mn>10</mn><mn>";
        s += e + "</mn></msup>";
    }
    return s;
};

Num.prototype.getMathml = function() {
    var prev = this.r;
    var v = this.get('v');
    var isNeg = v < 0; 
    var noMinus = false;
    var s = '';
    
    v = Math.abs(v);
    if ( this.isApprox ) {
        s += mathml.printApproxNum(v);
    }
    else {
        s += "<mn>" + v.toString() + "</mn>";
    }

    if ( prev !== null ) {
        if ( prev.isMult ) {
            if ( v === 1 ) {
                return '';
            }
            noMinus = true;
        }
        else if ( prev.isPow && isNeg ) {
			if ( prev.a[1] === this ) {
				noMinus = true;
			}
			else {
				
			}        
        }
    }
    if ( isNeg && ! noMinus ){
        s =  "<mo>-</mo>" + s;
    }
    if ( this.isApprox ) {
        s = '<mstyle class="approx">' + s + '</mstyle>';
    }
    return s;
};

Symb.prototype.getMathml = function(noArg) {
    var s = "";
    var l, i, n;

    if ( this.dd && this.dd.length === 1 ) {
        s += "<msup>";
        s += "<mi>" + this.first.id + "</mi>"; 
        switch( this.dd[0] ) {
        case 1 :
            s += "<mo>&prime;</mo>";
            break;
        case 2 :
            s += "<mo>&Prime;</mo>";
            break;
        case 3 :
            s += "<mo>&tprime;</mo>";
            break;
        default :
            s += "<mrow><mo>(</mo><mn>";
            s += dd[0];
            s += "</mn><mo>)</mo></mrow>";
            break;
        }
        s += "</msup>";
    }
    else if ( this.dd && ( l = this.dd.length ) > 1 ) {
        n = 0;
        for ( i=0; i<l; i++ ) {
            n += this.dd[i];
        }
        if ( n === 0 ) {
            s += "<mi>" + this.first.id + "</mi>"; 
        }
        else {
            s += "<mfrac><mrow>";
            if ( n === 1 ) {
                s += "<mo>&part;</mo>";
            }
            else {
                s += "<msup><mo>&part;</mo><mn>";
                s += n + "</mn></msup>";
            }
            s += "<mi>" + this.first.id + "</mi>";
            s += "</mrow><mrow>";
            for ( i=0; i<l; i++ ) {
                if ( this.dd[i] > 0 ) {
                    s += "<mo>&part;</mo>";
                    if ( this.dd[i] === 1 ) {
                        if ( l <= 3 ) {
                            switch(i) {
                                case 0:
                                s += "<mi>x</mi>";
                                break;
                                case 1:
                                s += "<mi>y</mi>";
                                break;
                                case 2:
                                s += "<mi>z</mi>";
                                break;
                            }
                        }
                        else {
                            s += "<msub><mi>x</mi><mn>";
                            s += (i+1) + "</mn></msub>";
                        }
                    }
                    else {
                        if ( l <= 3 ) {
                            s += "<msup>";
                            switch(i) {
                                case 0:
                                s += "<mi>x</mi>";
                                break;
                                case 1:
                                s += "<mi>y</mi>";
                                break;
                                case 2:
                                s += "<mi>z</mi>";
                                break;
                            }
                            s += "<mn>" + this.dd[i] + "</mn>";
                            s += "</msup>";
                        }
                        else {
                            s += "<msubsup>";
                            s += "<mi>x</mi>";
                            s += "<mn>" + (i+1) + "</mn>";
                            s += "<mn>" + this.dd[i] + "</mn>";
                            s += "</msubsup>";
                        }
                    }
                }
            }
            s += "</mrow></mfrac>";
        }
    }
    else {
        s += "<mi";
        if ( this.isInternal ) {
            s += " class='internal'";
            delete this.isInternal;
        }
        s += ">" + this.id + "</mi>";
    }
    if ( ( l = this.a.length ) > 0 && ! noArg ) {
        s += "<mrow><mo>(</mo><mrow>" + this.a[0].getMathml();
        for ( i=1; i<l; i++ ) {
            s += '<mo>,</mo>' + this.a[i].getMathml();
        }
        s += "</mrow><mo>)</mo></mrow>";
    }
    return s;
};

Pow.prototype.getMathml = function() {
    var value = this.a[1].get('v');
    var sign = 1;
    var s, n;

    if ( value !== null &&
         ( value === 0.5 || value === -0.5 ) ) {
        s = "<msqrt><mrow>";
        s += this.a[0].getMathml();
        s += "</mrow></msqrt>";
    }
    else if ( this.a[1].isNum ) {
        if ( value < 0 ) {
            sign = -1;
            value *= -1;
        }
        if ( value === 1 ) {
            s = this.a[0].getMathml();
        }
        else {
            s = "<msup><mrow>";
            s += mathml.addPar( this, this.a[0], value !== 1 );
            s += "</mrow><mrow>";
            s += this.a[1].getMathml();
            s += "</mrow></msup>";
        }
        if ( sign < 0 &&
             ( this.r === null || ! this.r.isMult ) ) {
            s = "<mfrac><mn>1</mn><mrow>" +
                s +
                "</mrow></mfrac>";
        }
        return s;
    }
    else {
        s = "<msup><mrow>";
        s += mathml.addPar( this, this.a[0], true );
        s += "</mrow><mrow>";
        s += this.a[1].getMathml() + "</mrow></msup>";
    }
    return s;
};

Mult.prototype.getMathml = function() {
    var l = this.a.length;
    var sign = 1;
    var N = [];
    var D = [];
    var su, sd, a, i, c;

    for ( i=0; i<l; i++ ) {
        a = this.a[i];
        if ( a.isNum && a.get('v') < 0 ) {
            sign *= -1;
        }
        if ( a.isPow &&
             a.a[1].isNum &&
             a.a[1].get('v') < 0 ) {
            D.push(a);
        }
        else if ( ! ( a.isNum && a.get('v') === 1 )  ) {
            N.push(a);
        }
    }

    l = N.length;
    if ( l === 0 ) {
        su = "<mn>1</mn>";
    }
    else if ( l === 1 ) {
        if ( N[0].isNum && Math.abs( N[0].get('v') ) === 1 ) {
            su = "<mn>1</mn>";
        }
        else {
            su = N[0].getMathml();
        }
    }
    else if ( l > 1 ) {

        for ( i=0; i<l; i++ ) {
            if ( N[i].isNum ) {
                c = N.splice( i, 1 );
                N.unshift( c[0] );
            }
        }

        su = mathml.addPar( this, N[0] );
        if ( N[1].isPow &&
             N[1].a[0].isNum ) {
            su += "<mo>&sdot;</mo>";
        }
        for ( i=1; i<l; i++ ) {
            su += "<mo>&InvisibleTimes;</mo>" +
                mathml.addPar( this, N[i] );
        }
    }
    
    l = D.length;
    if ( l === 1 ) {
        sd = D[0].getMathml();
    }
    else if ( l > 1 ) {
        for ( i=0; i<l; i++ ) {
            if ( D[i].a[0].isNum &&
                 D[i].a[1].get('v') === -1 ) {
                c = D.splice( i, 1 );
                D.unshift( c[0] );
            }
        }
        if ( D[0].a[1].get('v') === -1 ) {
            sd = mathml.addPar( this, D[0].a[0] );
        }
        else {
            sd = mathml.addPar( this, D[0] );
        }
        if ( D[1].isPow &&
             D[1].a[0].isNum ) {
            sd += "<mo>&sdot;</mo>";
        }
        for ( i=1; i<l; i++ ) {
            sd += "<mo>&InvisibleTimes;</mo>";
            if ( D[i].a[1].get('v') === -1 ) {
                sd += mathml.addPar( this, D[i].a[0] );
            }
            else {
                sd += mathml.addPar( this, D[i] );
            }
        }
    }
    if ( l > 0 ) {
        su = "<mfrac><mrow>" + su + "</mrow><mrow>" +
            sd + "</mrow></mfrac>";
    }
    if ( sign === -1 ) {
        su = "<mo>-</mo>" + su;
    }
    else if ( this.r !== null &&
              this.r.isAdd &&
              this.r.a.indexOf(this) > 0 ) {
        su = "<mo>+</mo>" + su;
    }
    return su;
};

Add.prototype.getMathml = function() {
    var l = this.a.length;
    var s = this.a[0].getMathml();
    var sign, i, c, b, p;

    for ( i=1; i<l; i++ ) {
        b = this.a[i];
        sign = "<mo>+</mo>";
        if ( b.isMult ) {
            sign = "";
        }
        else if( b.isNum && b.get('v') < 0 ) {
            sign = "";
        }
        s += sign + b.getMathml();
    }
    return s;
};

Vect.prototype.getMathml = function() {
    var l = this.a.length;
    var s = "<mo>(</mo><mrow>";
    var i;

    s += this.a[0].getMathml();
    for ( i=1; i<l; i++ ) {
        s += '<mo>' + mathml.sep + '</mo>' + this.a[i].getMathml();
    }
    s += "</mrow><mo>)</mo>";
    return s;
};

Eq.prototype.getMathml = function() {
    var aN = this.a[0], bN = this.a[1];
    if ( this.isDefining ) {
        return '<mrow>' + aN.getMathml(true) + '</mrow>' +
            "<mspace width='0.1em'/><mo>:</mo><mspace width='0.1em'/>" +
            bN.getMathml();
    }
    else {
        return aN.getMathml() + "<mo>=</mo>" + bN.getMathml();
    }
};

Symb.prototype.specials['mapsTo'].getMathml = function() {
    var s = '';
    if ( this.r !== null && this.r.isSymb && this.r.id === '∫' ) {
        if ( this.a[1].precedence < Mult.prototype.precedence ) {
            s += '<mo>(</mo><mrow>';
            s += this.a[1].getMathml();
            s += '</mrow><mo>)</mo>';
        }
        else {
            s += this.a[1].getMathml();
        }
        s += '<mspace width="0.3em" /><mi>d</mi>';
        s += this.a[0].getMathml();
        return s;
    }
    s += '<mrow>' + this.a[0].getMathml() + '</mrow>' +
        "<mspace width='0.1em'/><mo>↦</mo><mspace width='0.1em'/><mrow>" +
        this.a[1].getMathml() + "</mrow>";
    return s;
};

Symb.prototype.specials['forAll'].getMathml = function() {
    return "<mo>∀</mo><mrow>" +
        this.a[0].getMathml() +
        "</mrow><mo>,</mo><mspace width='0.2em'/>" +
        this.a[1].getMathml();    
};

Symb.prototype.specials['?'].getMathml = function() {
    var i, l = this.a.length;
    var s = "<mi";
    if ( this.index >= 0 ) {
        s += " class='color" + this.index + "'";
    }
    s += ">●</mi>";
    if ( l > 0 ) {
        s += "<mrow><mo>(</mo><mrow>" + this.a[0].getMathml();
        for ( i=1; i<l; i++ ) {
            s += '<mo>,</mo>' + this.a[i].getMathml();
        }
        s += "</mrow><mo>)</mo></mrow>";
    }
    return s;
};

Symb.prototype.specials['abs'].getMathml = function(noArg){
    var s = "<mo>|</mo><mrow>";
    s += "<mspace width='0.2em'/>";
    if (noArg) {
        s += '<mi>●</mi>';
    }
    else {
        s += this.a[0].getMathml();
    }
    s += "<mspace width='0.2em'/>";
    s += "</mrow><mo>|</mo>";
    return s;
};

Symb.prototype.specials['int'].getMathml = function() {
    var s, i, list;

    if ( this.a[2].a[0] && this.a[2].a[0].id !== '?' &&
         this.a[2].a[1] && this.a[2].a[1].id !== '?' ) {
        list = alg.findId( this.a[2].a[0].id, this.a[2] );
        for ( i=list.length-1; i>=0; i-- ) {
            list[i].isInternal = true;
        }
    }

    s = '<msubsup><mo>&int;</mo><mrow>';
    s += this.a[0].getMathml();
    s += '</mrow><mrow>';
    s += this.a[1].getMathml();
    s += '</mrow></msubsup><mrow>';
    s += this.a[2].getMathml();
    s += '</mrow>';
    return s;
};
