#version 100

precision mediump float;

attribute vec2 a_position;

uniform vec4 u_frame;

varying vec4 v_color;

struct comb {
  float pos;
  float color;
};

comb get( float l, float  u, float pos ) {
  float step1 = 1.0;
  float step2 = 5.0;

  pos -= 20.0;
  float w = u - l;
  float nv = floor( log(w) * 0.4342945 );

  float sv = exp( nv * 2.3025851 );
  float xmin = l / sv;
  float dx = 1.0 - fract(xmin);
  float s = 10.0  * sv / w;

  float t1 = step( step1, s );
  float tc1 = 1.0 - smoothstep( step1, 10.0, s );
  float t2 = step( step2, s );
  float tc2 = smoothstep( step2, 10.0, s );
  tc2 = 1.0 - tc2*tc2;
  
  float div =  1.0 + t1 + 8.0 * t2;
  float x = pos / div;

  float color = 0.0;
  float fx = step( 0.001, fract(x) );
  color += t1 * ( 1.0 - t2 ) * tc1 * fx;

  float g = step( 0.001, fract(pos/5.0) );
  
  color += t2 * fx * ( g * tc2 + ( 1.0 - g ) * tc1 );

  x += dx;
  x *= s;

  x = x / 5.0 - 1.0;
  comb c;
  c.pos = x;
  c.color = color;
  return c;
}
  
void main() {
  vec2 m = a_position;
  comb c;
  
  if ( m[1] < 2.0 ) {
    c = get( u_frame[0], u_frame[1], m[0] );
    gl_Position = vec4( c.pos, m[1], c.color, 1.0 );
  }
  else {
    c = get( u_frame[2], u_frame[3], m[1] );
    gl_Position = vec4( m[0], c.pos, c.color, 1.0 );
  }
  
  v_color = vec4( vec3(c.color), 1.0 );
}
