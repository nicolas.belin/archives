function Node() {
    this.a = [];
    this.r = null;
    this.attributes = null; 
    this.precedence = 1000;
    this.isNum = false;
    this.isSymb = false;
    this.isMult = false;
    this.isAdd = false;
    this.isPow = false;
    this.isVect = false;
    this.isEq = false;
    this.isAtypical = false;
    this.isStatement = false;
    this.isNew = true;
}

Node.prototype.replacedBy = function(rN) {
    if ( this.r !== null ) {
        this.r.a[ this.r.a.indexOf(this) ] = rN;
        this.r.altered();
    }
    return rN.ret(this.r);
};

Node.prototype.ret = function(r) {
    this.r = r;
    this.isNew = true;
    return this;
};

Node.prototype.altered = function() {
    if ( this.hasOwnProperty('attributes') ) {
        delete this.attributes;
    }
    this.isNew = true;
    return this;
};

Node.prototype.get = function(id) {
    // getting recursive attributes
    var att = this.attributes;
    var s;

    if ( att !== null &&
         ( s = att[id] ) !== undefined ) {
        return s;
    }
    if ( att === null ) {
        this.attributes = att = {};
    }
    switch(id) {
    case 'v' :
        s = this.value();
        break;
    case 'cst' :
        s = this.isCst();
        break;
    case 'H' :
        s = this.hash();
        break;
    case 'nH' :
        s = this.hash(hash.num2one);
        break;
    case 'cH' :
        s = this.hash(hash.cst2one);
        break;
    case 'kH' :
        s = this.hash(hash.cstFact2one);
        break;
    default:
        console.log( "'" + id + "'" +
                     ' is not a supported attribute' );
        return null;
    }
    att[id] = s;
    return s;
};

Node.prototype.scanUp = function(f) {
    var i;
    var a = f.call(this);

    for ( i=a.length-1; i>=0; i-- ) {
        a[i].scanUp(f);
    }
};

Node.prototype.scanThrough = function( f, prev ) {
    var that = f.call( this, prev );
    if ( that && that.r ) {
        that.r.scanThrough( f, that );
    }
};

Node.prototype.findLacunae = function() {
    var A = [];
    this.scanUp(
        function() {
            if ( this.isSymb && this.id === '?' ) {
                A.push(this);
            }
            return this.a;
        } );
    return A;
};

Node.prototype.checkSymbs = function() {
    var vars = {};
	vars.bound = new Set();
	vars.defined = new Set();
	vars.free = new Set();
    this.scanUp(
        function() {
            if ( this.isSymb ) {
		    if ( this.isBound ) {
			  vars.bound.add(this.id);
		    }
		    else if ( this.isDefined ) {
			  vars.defined.add(this.id);
		    }
		    else {
			  vars.free.add(this.id);
		    }
            }
            return this.a;
        } );
    return vars;
};

//********  Scalars  ***********************************
function Num( value, rN ) {
    this.ownValue = value;
    this.isApprox = false;
    this.r = rN;
    this.isNum = true;
}

Num.prototype = new Node();

Num.prototype.dup = function(rN) {
    var r = new Num( this.ownValue, rN );
    r.isApprox = this.isApprox;
    r.isNew = this.isNew;
    if ( this.hasOwnProperty('attributes') ){
        r.attributes = this.attributes;
    }
    return r;
};

function Symb( id, isGlobal ) {
    if ( isGlobal ) {
        this.list[id] = this;
        this.isGlobal = true;
    }
    else {
        this.isGlobal = false;
        this.r = null;
        this.a = [];
        this.getTag = null;
    }
    this.id = id;
    this.g = this;
    this.isSymb = true;
    this.nArg = null;
    this.isDefined = false;
}

Symb.prototype = new Node();

Symb.prototype.list = {};

Symb.prototype.dup = function(rN) {
    var i, d, g, l = this.a.length;

    if ( this.isGlobal ) {
        //g = this.g;
        d = Object.create( this.g );
        //d.prototype = g;
    }
    else {
        d = new Symb( this.id, false );
        d.nArg = this.nArg;
    }
    if ( this.hasOwnProperty('attributes') ){
        d.attributes = this.attributes;
    }
    d.r = rN;
    d.isNew = this.isNew;
    if ( this.isBound ) {
        d.isBound = true;
    }
    if ( this.isAtypical ) {
        d.isAtypical = true;
        if ( this.isStatement ) {
            d.isStatement = true;
        }
    }
    d.a = [];
    for ( i=0; i<l; i++ ) {
        d.a.push( this.a[i].dup(d) );
    }
    return d;
};

Symb.prototype.setNArg = function(nArg) {
    var i, dd, lk;

    this.g.nArg = nArg;
    if ( nArg > 0 ) {
        this.g.precedence = 120;
    }
};

Symb.prototype.defAsFonction = function(mapsToN) {
    var s;
    
    this.g.isDefined = true;
    this.g.mapsTo = mapsToN;
    u[this.id] = u[mapsToN.name];

    if ( this.nArg === null ) {
        this.setNArg(
            mapsToN.a[0].isVect ? mapsToN.a[0].a.length : 1 );
    }
};

Symb.prototype.defAsNumber = function(v) {
    this.g.isDefined = true;
    this.setNArg(0);
    this.makeConst(v);
};

Symb.prototype.getTag = function() {
    var s;
/*
    if ( this.di !== null ) {
        s = '_d(' + this.id + ',' + this.di + ')';
    }
    else {
*/
        s = this.id;
//    }
    return s;
};

Symb.prototype.specials = {};

Symb.prototype.specials['exp1'] = {
    set: function() {
        var e = new Symb( 'e', true );
        e.defAsNumber( Math.exp(1) );
        e.name = 'exp1';
        e.js = this.js;
        return e;
    }
};

Symb.prototype.specials['piNum'] = {
    set: function() {
        var pi = new Symb( 'π', true );
        pi.defAsNumber( Math.PI );
        pi.name = 'piNum';
        pi.js = this.js;
        return pi;
    }
};

Symb.prototype.specials['?'] = {
    set: function() {
        var n = alg.localSymb( '?', null );
        n.isStatement = true;
        n.dup = this.dup;
        n.getMathml = this.getMathml;
        n.hash = this.hash;
        n.index = -1;
        return n;
    },
    dup: function(rN) {
        var n = Symb.prototype.dup.call( this, rN );
        n.isStatement = true;
        n.getMathml = this.getMathml;
        n.hash = this.hash;
        n.dup = this.dup;
        n.index = -1;
        return n.altered();
    }
};

Symb.prototype.specials['mapsTo'] = {
    // after being defined, this node is immutable
    set: function() {
        var n = new Symb( '↦', true );
        n.isAtypical = true;
        n.dup = this.dup;
        n.act = this.act;
        n.apply = this.apply;
        n.over = this.over;
        n.getMathml = this.getMathml;
        n.getJs = this.getJs;
        n.del = this.del;
        n.d = this.d;
        n.getTag = null;
        n.isDefined = true;
        n.setNArg(2);
        return n;
    },
    dup: function(rN) {
        var n = Symb.prototype.dup.call( this, rN );
        if ( this.name ) {
            n.name = 'mapsTo' +
                ( Symb.prototype.specials['mapsTo'].index++ );
            u[n.name] = u[this.name];
        }
        return n;
    },
    del: function() {
        if ( u[this.name] ) {
            delete u[this.name];
        }
    },
    index: 0
};

Symb.prototype.specials['forAll'] = {
    set: function() {
        var n = new Symb( '∀', true );
        n.isStatement = true;
        n.isAtypical = true;
        n.act = this.act;
        n.over = this.over;
        n.getMathml = this.getMathml;
        n.setNArg(2);
        n.isDefined = true;
        n.getTag = null;
        return n;
    }
};

Symb.prototype.specials['_d'] = {
    set: function() {
        var n = alg.localSymb( '_d', null );
        n.act = this.act;
        return n;
    },
    act: function() {
        var that = this.checkArgs();
        var f = that.a[0];
        f.di = that.a[1].get('v');
        return f;
    }
};

Symb.prototype.specials['ln'] = {
    set: function() {
        var f = new Symb( 'ln', true );
        f.value = this.value;
        f.act = this.act;
        f.d = this.d;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['log'] = {
    set: function() {
        var f = new Symb( 'log', true );
        f.value = this.value;
        f.act = this.act;
        f.d = this.d;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['cos'] = {
    set: function() {
        var f = new Symb( 'cos', true );
        f.d = this.d;
        f.value = this.value;
        f.act = this.act;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['sin'] = {
    set: function() {
        var f = new Symb( 'sin', true );
        f.d = this.d;
        f.value = this.value;
        f.act = this.act;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['tan'] = {
    set: function() {
        var f = new Symb( 'tan', true );
        f.d = this.d;
        f.value = this.value;
        f.act = this.act;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['abs'] = {
    set: function() {
        var f = new Symb( 'abs', true );
        f.d = this.d;
        f.value = this.value;
        f.act = this.act;
        f.getMathml = this.getMathml;
        f.isDefined = true;
        f.js = this.js;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['heaviside'] = {
    set: function() {
        var f = new Symb( 'H', true );
        f.d = this.d;
        f.value = this.value;
        f.act = this.act;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['dirac'] = {
    set: function() {
        var f = new Symb( 'δ', true );
        f.act = this.act;
        f.value = this.value;
        f.js = this.js;
        f.isDefined = true;
        f.setNArg(1);
        return f;
    }
};

Symb.prototype.specials['int'] = {
    set: function() {
        var f = new Symb( '∫', true );
        f.d = this.d;
        f.js = this.js;
        f.value = this.value;
        f.isCst = this.isCst;
        f.act = this.act;
        f.getJs = this.getJs;
        f.getMathml = this.getMathml;
        f.getTag = null;
        f.isDefined = true;
        f.setNArg(3);
        return f;
    }
};

//********  Operations  ************************
function Pow( argumentNodes, returnNode ) {
    this.a = argumentNodes;
    this.r = returnNode;
    this.precedence = 130;
    this.isPow = true;
}

Pow.prototype = new Node();

Pow.prototype.dup = function(returnNode) {
    var r = new Pow( [], returnNode );
    r.isNew = this.isNew;
    r.a[0] = this.a[0].dup(r);
    r.a[1] = this.a[1].dup(r);
    if ( this.hasOwnProperty('attributes') ){
        r.attributes = this.attributes;
    }
    return r;
};

function Mult( aN, rN ) {
    this.a = aN;
    this.r = rN;
    this.isMult = true;
}

Mult.prototype = new Node();

Mult.prototype.precedence = 110;

Mult.prototype.dup = function(returnNode) {
    var l = this.a.length;
    var r = new Mult( [], returnNode );
    var i;

    r.isNew = this.isNew;
    for ( i=0; i<l; i++ ) {
        r.a.push( this.a[i].dup(r) );
    }
    if ( this.hasOwnProperty('attributes') ){
        r.attributes = this.attributes;
    }
    return r;
};

function Add( argumentNodes, returnNode ) {
    this.a = argumentNodes;
    this.r = returnNode;
    this.precedence = 100;
    this.isAdd = true;
}

Add.prototype = new Node();

Add.prototype.dup = function(returnNode) {
    var l = this.a.length;
    var r = new Add( [], returnNode );
    var i;

    r.isNew = this.isNew;
    for ( i=0; i<l; i++ ) {
        r.a.push( this.a[i].dup(r) );
    }
    if ( this.hasOwnProperty('attributes') ){
        r.attributes = this.attributes;
    }
    return r;
};

function Vect( argumentNodes, returnNode ) {
    this.a = argumentNodes;
    this.r = returnNode;
    this.precedence = 90;
    this.isVect = true;
}

Vect.prototype = new Node();

Vect.prototype.dup = function(returnNode) {
    var l = this.a.length;
    var r = new Vect( [], returnNode );
    var i;

    r.isNew = this.isNew;
    for ( i=0; i<l; i++ ) {
        r.a.push( this.a[i].dup(r) );
    }
    if ( this.hasOwnProperty('attributes') ){
        r.attributes = this.attributes;
    }
    return r;
};

function Eq(aN) {
    this.a = aN;
    this.precedence = 60;
    this.isEq = true;
    this.isStatement = true;
    this.isAtypical = true;
}

Eq.prototype = new Node();

Eq.prototype.dup = function(rN) {
    var eq;

    eq = new Eq( [] );
    eq.a.push( this.a[0].dup(eq) );
    eq.a.push( this.a[1].dup(eq) );
    eq.r = rN;
    eq.isNew = this.isNew;
    return eq;
};
