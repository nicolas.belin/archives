"use strict";

Interface.prototype.text = {
    error:
    { eng: "Error",
      fr: "Erreur" },
    comma:
    { eng: ".",
      fr: "," },
    separator:
    { eng: ",",
      fr: ";" },
    functions:
    { type: 'PANEL',
      eng: "Functions",
      fr: "Fonctions" },
    numbers:
    { type: 'PANEL',
      eng: "Numbers",
      fr: "Nombres" },
    expressions:
    { type: 'PANEL',
      eng: "Expressions",
      fr: "Expressions" },
    symbols:
    { type: 'PANEL',
      eng: "Symbols",
      fr: "Symboles" },
    equal:
    { type: 'PANEL',
      eng: "Equal",
      fr: "Égale" },
    home:
    { type: 'TITLE',
      eng: "Home",
      fr: "Page principale" },
    startFS:
    { type: 'TITLE',
      eng: "Fullscreen",
      fr: "Plein écran" },
    endFS:
    { type: 'TITLE',
      eng: "Windowed",
      fr: "Fenêtré" },
    fontPlus:
    { type: 'TITLE',
      eng: "Increase font size",
      fr: "Augmenter la taille des caractères" },
    fontMinus:
    { type: 'TITLE',
      eng: "Decrease font size",
      fr: "Baisser la taille des caractères" },
    undo:
    { type: 'TITLE',
      eng: "Undo",
      fr: "Défaire" },
    jewelAdd:
    { type: 'TITLE',
      eng: "Addition",
      fr: "Addition" },
    jewelMult:
    { type: 'TITLE',
      eng: "Multiplication",
      fr: "Multiplication" },
    jewelDiv:
    { type: 'TITLE',
      eng: "Division",
      fr: "Division" },
    jewelSub:
    { type: 'TITLE',
      eng: "Subtraction",
      fr: "Soustraction" },
    jewelPow:
    { type: 'TITLE',
      eng: "Power",
      fr: "Puissance" },
    jewelDump:
    { type: 'TITLE',
      eng: "Dump",
      fr: "Effacer" },
    jewelDup:
    { type: 'TITLE',
      eng: "Duplicate",
      fr: "Dupliquer" },
    jewelDeriv:
    { type: 'TITLE',
      eng: "Differentiate",
      fr: "Dériver" },
    jewelExpand:
    { type: 'TITLE',
      eng: "Expand",
      fr: "Developper" },
    jewelInfo:
    { type: 'TITLE',
      eng: "Info",
      fr: "Info" },
    jewelEmpty:
    { type: 'TITLE',
      eng: "Choose a color",
      fr: "Choisir une couleur" },
    devmode:
    { type: 'INFO',
      eng: "Dev mode enabled",
      fr: "Mode développement activé" },
    notStatement:
    { type:'ERROR',
      eng: [ "Not a statement !" ],
      fr: [ "Une affirmation est nécéssaire" ] },
    notEqual:
    { type:'ERROR',
      eng: [ "Not equal !" ],
      fr: [ "Égalité fausse !" ] },
    divBy0:
    { type:'ERROR',
      eng: [ "divide by 0" ],
      fr: [ "division par 0" ] },
    negSqRoot:
    { type:'ERROR',
      eng: [ "square root of a negative number" ],
      fr: [ "racine carrée d'un nombre négatif" ] },
    negRealPow:
    { type:'ERROR',
      eng: [ "real power of a negative number" ],
      fr: [ "puissance réelle d'un nombre négatif" ] },
    negLn:
    { type:'ERROR',
      eng: [ "logarithm of a negative number" ],
      fr: [ "logarithme d'un nombre négatif" ] },
    badDim:
    { type:'ERROR',
      eng: [ "mismatching dimensions" ],
      fr: [ "dimensions incompatibles" ] },
    badMult:
    { type:'ERROR',
      eng: [ "can't multiply" ],
      fr: [ "impossible à multiplier" ] },
    badOp:
    { type:'ERROR',
      eng: [ "operation not permitted" ],
      fr: [ "opération interdite" ] },
    badArg:
    { type:'ERROR',
      eng: [ "bad argument type" ],
      fr: [ "mauvais type d'argument" ] },
}
