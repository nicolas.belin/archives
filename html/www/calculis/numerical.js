
Num.prototype.setValue = function(v) {
    this.ownValue = v;
    this.altered();
};

Num.prototype.value = function() {
    return this.ownValue;
};

Symb.prototype.makeConst = function(v) {
    this.g.attributes = { v:v, cst:true };
};

Symb.prototype.value = function() {
    return null;
};

Symb.prototype.specials['ln'].value = function() {
    var v = this.a[0].get('v');

    if ( v !== null ) {
        if ( v > 0 ) {
            return Math.log(v);
        }
        else {
            worker.displayText('negLn');
            return null;
        }
    }
    return null;
};

Symb.prototype.specials['log'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        if ( v > 0 ) {
            return Math.log(v)/Math.LN10;
        }
        else {
            worker.displayText('negLn');
            return null;
        }
    }
    return null;
};

Symb.prototype.specials['cos'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return Math.cos(v);
    }
    return null;
};

Symb.prototype.specials['sin'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return Math.sin(v);
    }
    return null;
};

Symb.prototype.specials['tan'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return Math.tan(v);
    }
    return null;
};

Symb.prototype.specials['abs'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return Math.abs(v);
    }
    return null;
};

Symb.prototype.specials['abs'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return Math.abs(v);
    }
    return null;
};

Symb.prototype.specials['heaviside'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return v >= 0 ? 1 : 0;
    }
    return null;
};

Symb.prototype.specials['dirac'].value = function() {
    var v = this.a[0].get('v');
    if ( v !== null ) {
        return v === 0 ? Infinity : 0;
    }
    return null;
};

Symb.prototype.specials['int'].value = function() {
    if ( this.get('cst') ) {
        return calc['int'](
            this.a[0].get('v'), this.a[1].get('v'),
            u[ this.a[2].name ], 0.0000001 );
    }
    return null;
};

Add.prototype.value = function() {
    var l = this.a.length;
    var i, w, v = 0;

    for ( i=0; i<l && ( w = this.a[i].get('v') ) !== null; i++ ) {
        v += w;
    }
    if ( i === l ) {
        return v;
    }
    else {
        return null;
    }
};

Mult.prototype.value = function() {
    var l = this.a.length;
    var i, w, v = 1;

    for ( i=0; i<l && ( w = this.a[i].get('v') ) !== null; i++ ) {
        v *= w;
    }
    if ( i === l ) {
        return v;
    }
    else {
        return null;
    }
};

Pow.prototype.value = function() {
    var av = this.a[0].get('v');
    var bv = this.a[1].get('v');

    if ( av !== null && bv !== null ) {
        if ( av === 0 && bv < 0 ) {
            worker.displayText( 'divBy0' );
            return undefined;
        }
        if ( av < 0 &&
             ( ! worker.isInt(bv) || this.a[1].isApprox ) ) {
            if ( bv === 0.5 ) {
                worker.displayText('negSqRoot');
            }
            else {
                worker.displayText('negRealPow');
            }
            return undefined;
        }
        return Math.pow( av, bv );
    }
    return null;
};

Vect.prototype.value = function() {
    return null;
};

Eq.prototype.value = function() {
    var aN = this.a[0];
    var bN = this.a[1];
    
    if ( aN.get('v') !== null ) {
        if ( bN.get('v') !== null && aN.get('v') !== bN.get('v')  ) {
            worker.displayText( 'notEqual' );
            return undefined;
        }
        return aN.get('v');
    }
    return bN.get('v');
}

// ** Constants ***

Num.prototype.isCst = function() {
    return true;
};

Symb.prototype.isCst = function() {
    var i, l = this.a.length;
    
    if ( l > 0 ) {
        for ( i=0; i<l; i++ ) {
            if ( ! this.a[i].get('cst') ) {
                return false;
            }
        }
        return true;
    }
    return false;
};

Symb.prototype.specials['int'].isCst = function() {
    var list = this.checkSymbs();
    if ( list.free.size === 0 ) {
        return true;
    }
    return false;
};

Add.prototype.isCst = function() {
    var i, l = this.a.length;
    
    for ( i=0; i<l; i++ ) {
        if ( ! this.a[i].get('cst') ) {
            return false;
        }
    }
    return true;
};

Vect.prototype.isCst = Mult.prototype.isCst =
    Pow.prototype.isCst = Add.prototype.isCst;

Eq.prototype.isCst = function() {
    if ( this.a[0].get('cst') || this.a[1].get('cst') ) {
        return true;
    }
    return false;
};
