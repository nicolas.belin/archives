"use strict";

function Graph( pebble, plot ) {
    var verticesMax = 1000;
    var c, j;

    this.pebble = pebble;
    this.plot = plot;
    this.wgl = plot.wgl;
    this.gl = plot.gl;
    this.isDirty = true;
    for ( c=0; c<inter.colors.length; c++ ) {
        j = plot.pebbles.length-2;
        while ( j>=0 ) {
            if ( plot.pebbles[j].graph.colorIndex === c ) {
                break;
            }
            j--;
        }
        if ( j === -1 ) {
            break;
        }
    }
    this.color = inter.colors[c];
    this.colorIndex = c;
    this.buffer = this.wgl.buffer(
        verticesMax, 'attributes', 2, this.gl.STREAM_DRAW );
    this.curve = new Chain( plot.axes.shader );
    this.curve.segment = [];
    this.curve.addAttribute( 'a_position', this.buffer );
    this.curve.addUniform( 'u_frame', this.plot.frame );
    this.curve.addUniform(
        'u_color', [ this.color[0]/255,
                     this.color[1]/255,
                     this.color[2]/255, 1.0 ] );
    this.curve.setPrimitiveType('line_strip');
    this.make();
}

Graph.prototype.getFrame = function() {
    var msg = { cmd:'frame', tree:this.pebble.t.index }
    core.send( msg, this.frameReply.bind(this) );
};

Graph.prototype.frameReply = function(d) {
    this.plot.setFrame(
        d.xmin, d.xmax, d.ymin, d.ymax );
    this.make();
};

Graph.prototype.make = function() {
    var msg = { cmd:'graph', tree:this.pebble.t.index,
                w:this.plot.w, h:this.plot.h };
    msg.xmin = this.plot.frame[0];
    msg.xmax = this.plot.frame[1];
    msg.ymin = this.plot.frame[2];
    msg.ymax = this.plot.frame[3];
    core.send( msg, this.makeReply.bind(this) );
};

Graph.prototype.makeReply = function(d) {
    this.wgl.updateBuffer( this.buffer, 0, d.strips );
    this.curve.segment[0] = 0;
    this.curve.segment[1] = d.strips.length/this.buffer.dim;
    this.plot.refresh();
};

function Plot( x, y ) {
    var wgl, gl, r, shader;
    var e = $('plotTemplate').cloneNode(true);

    Plot.prototype.list.push(this);
    this.pebbles = [];
    this.element = e;
    e.id = 'plot' + (Plot.prototype.index++);
    e.title = e.id;
    e.style.left = x + 'px';
    e.style.top = y + 'px';
    e.style.zIndex = inter.getZIndex();
    $('board').appendChild(e);
    this.canvas = e.getElementsByTagName('canvas')[0];
    this.canvas.addEventListener( 'pointerstart', this, true );
    this.canvas.addEventListener( 'pointerend', this, true );
    this.canvas.addEventListener( 'wheel', this, true );
    this.animating = 0;
    this.zoom = { count:0, div:10, zooming:false };
    this.canvas.style.width = 20 + 'em';
    this.canvas.style.height = 11 + 'em';
    r = this.canvas.getBoundingClientRect();
    this.w = this.canvas.width = r.width;
    this.h = this.canvas.height = r.height;
    this.frame = [];
    this.wgl = new WebGL( this.canvas );
    if ( ! this.wgl.gl ) {
        $('board').removeChild( this.element );
        return null;
    }
    wgl = this.wgl;
    this.gl = wgl.gl;
    gl = this.gl;
    wgl.resize();
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );
    gl.enable(gl.DEPTH_TEST);
    gl.clearDepth(1.0);
    gl.depthFunc(gl.LESS);
    
    this.setFrame( -2, 2, -1.5, 1.5 );
    if ( ! this.makeAxes() ) {
        return null;
    }
    if ( ! this.makeGrid() ) {
        return null;
    }
    //    delete core.loaded['scale.vertex.c'];
    //    delete core.loaded['basic.fragment.c'];
    this.refresh();
} 

Plot.prototype.index = 0;
Plot.prototype.list = [];

Plot.prototype.handleEvent = function(event) {
    event.preventDefault();
    event.stopPropagation();
    switch( event.type ) {
    case 'pointerstart':
        this.grab();
        break;
    case 'pointerend':
        this.ungrab();
        break;
    case 'wheel':
        if ( event.deltaY !== 0 ) {
            event.preventDefault();
            event.stopPropagation();
            this.zoom.count += this.zoom.div *
                event.deltaY / Math.abs( event.deltaY );
            if ( ! this.zoom.zooming ) {
                this.zoom.zooming = true;
                this.startAnimating();
            }
        }
        break;
    }
};

Plot.prototype.grab = function() {
    this.isGrabbed = true;
    this.justGrabbed = true;
    inter.dragging = true;
    this.startAnimating();
    window.setTimeout( this.notJustGrabbed.bind(this), 200 );
    inter.nextEventForwardedTo(
        document, 'pointerend', true,
        this.canvas, 'pointerend' );
};

Plot.prototype.ungrab = function() {
    this.isGrabbed = false;
    inter.dragging = false;
    this.animating--;
    this.canvas.style.cursor = null;
};

Plot.prototype.notJustGrabbed = function() {
    this.justGrabbed = false;
    if ( this.isGrabbed ) {
        this.canvas.style.cursor = 'move';
    }
};

Plot.prototype.startAnimating = function() {
    var self = this;
    var px = inter.pointerX + window.pageXOffset;
    var py = inter.pointerY + window.pageYOffset;
    var animate;

    this.animating++;
    if ( this.animating === 1 ) {
        animate = function() {
            var f = self.frame;
            var n, tzf, zf = 0.97;
            var x, y, dx, dy, w, h;
            
            if ( self.isGrabbed ) {
                x = inter.pointerX + window.pageXOffset;
                y = inter.pointerY + window.pageYOffset;
                dx = ( px - x ) * ( f[1] - f[0] ) / self.w;
                dy = ( y - py ) * ( f[3] - f[2] ) / self.h;
                px = x;
                py = y;
                self.setFrame( f[0] + dx, f[1] + dx,
                               f[2] + dy, f[3] + dy );
            }
            if ( self.zoom.count !== 0 ) {
                x = ( f[0] + f[1] ) / 2;
                y = ( f[2] + f[3] ) / 2;
                w = ( f[1] - f[0] ) / 2;
                h = ( f[3] - f[2] ) / 2;

                n = Math.abs(self.zoom.count) + self.zoom.div;
                n = Math.floor( n / self.zoom.div );
                tzf = Math.pow( zf, n );
                
                if ( self.zoom.count > 0 ) {
                    self.setFrame( x - tzf*w, x + tzf*w,
                                   y - tzf*h, y + tzf*h );
                    self.zoom.count -= n;
                        
                }
                else {
                    self.setFrame( x - w/tzf, x + w/tzf,
                                   y - h/tzf, y + h/tzf );
                    self.zoom.count += n;
                        
                }
                if ( self.zoom.count === 0 ) {
                    self.animating--;
                    self.zoom.zooming = false;
                }
            }
            if ( self.animating > 0 ) {
                self.draw();
                window.requestAnimationFrame(animate);
            }
        };
        animate();
    }
};

Plot.prototype.draw = function() {
    var gl = this.gl;
    var p, i, n, c;
    
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
    this.grid.render();
    this.axes.render();
    for ( p of this.pebbles ) {
        c = p.graph.curve;
        n = c.segment.length;
        for ( i=0; i<n; i+=2 ) {
            c.render( c.segment[i], c.segment[i+1] - c.segment[i] );
        }
    }
};

Plot.prototype.refresh = function() {
    if ( ! this.animating ) {
        this.draw();
    }
};

Plot.prototype.setFrame = function( xmin, xmax, ymin, ymax ) {
    this.frame[0] = xmin;
    this.frame[1] = xmax;
    this.frame[2] = ymin;
    this.frame[3] = ymax;
};

Plot.prototype.add = function(pebble) {
    var e = pebble.element;
    var c, j;

    $('board').removeChild(e);
    e.classList.add('pebbleInPlot');
    this.pebbles.push( pebble );
    this.element.insertBefore( e, this.canvas );
    pebble.x = e.offsetLeft + this.element.offsetLeft;
    pebble.y = e.offsetTop + this.element.offsetTop;
    pebble.graph = new Graph( pebble, this );
    e.style.borderColor =
        inter.getColorString( pebble.graph.colorIndex );
    /*
    if ( this.xmin === undefined ) {
        pebble.graph.getFrame();
    }
    else {
        pebble.graph.update();
    }
*/
};

Plot.prototype.remove = function(pebble) {
    var e = pebble.element;
    var i = this.pebbles.indexOf(pebble);
    var j, p;
    
    pebble.graph = null;
    this.element.removeChild(e);
    e.classList.remove('pebbleInPlot');
    e.style.borderColor = null;
    $('board').appendChild(e);
    for ( j=this.pebbles.length-1; j>i; j-- ) {
        p = this.pebbles[j];
        p.x = p.element.offsetLeft + this.element.offsetLeft;
        p.y = p.element.offsetTop + this.element.offsetTop;
    }
    this.pebbles.splice( i, 1 );
    if ( this.pebbles.length === 0 ) {
        $('board').removeChild( this.element );
        Plot.prototype.list.splice(
            Plot.prototype.list.indexOf(this), 1 );
    }
    else {
        this.refresh();
    }
};

Plot.prototype.makeGrid = function() {

    var wgl = this.wgl;
    var gl = wgl.gl;
    var shader = new Shader( 'Grid', wgl );
    var buffer, i, A = [];
    
    if ( ! shader.make(
        core.loaded['grid.vertex.c'],
        core.loaded['basic.fragment.c'] ) ) {
        return false;
    }
    // vertical lines
    for ( i=20-10; i<=20+15; i++ ) {
        A.push(i);
        A.push(-1);
        A.push(i);
        A.push(1);
    }
    //  horizontal lines
    for ( i=20-10; i<=20+15; i++ ) {
        A.push(-1);
        A.push(i);
        A.push(1);
        A.push(i);
    }

    this.grid = new Chain(shader);
    buffer = wgl.buffer( new Float32Array(A), 'attributes',
                         2, gl.STATIC_DRAW );
    this.grid.addAttribute( 'a_position', buffer );
    this.grid.count = A.length / 2;
    this.grid.addUniform( 'u_frame', this.frame );
    this.grid.setPrimitiveType('lines');
    return true;
};

Plot.prototype.makeAxes = function() {
    var wgl = this.wgl;
    var gl = wgl.gl;
    var shader = new Shader( 'Axes', wgl );

    if ( ! shader.make(
        core.loaded['scale.vertex.c'],
        core.loaded['basic.fragment.c'] ) ) {
        return false;
    }
    this.axes = new Chain(shader);
    this.axes.buffer = wgl.buffer(
        new Float32Array( [ 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0 ] ), 'attributes', 2, gl.STATIC_DRAW );
    this.axes.addAttribute(
        'a_position', this.axes.buffer );
    this.axes.count = 4;
    this.axes.addUniform( 'u_frame', this.frame );
    this.axes.addUniform( 'u_color', [ 0.0, 0.0, 0.0, 1.0 ] );
    this.axes.setPrimitiveType('lines');
    return true;
};

/*

Plot.prototype.toWinCoords = function(frameCoords) {
    var i, l = frameCoords.length;
    var A = [];
    for ( i=0; i+1<l; i+=2 ) {
        A[i] = ( frameCoords[i] - this.xmin ) /
            ( this.xmax - this.xmin ) * this.w;
        A[i+1] = this.h -
            ( frameCoords[i+1] - this.ymin ) /
            ( this.ymax - this.ymin ) * this.h;
    }
    return A;
};

Plot.prototype.makeAxes = function() {
    var O = this.toWinCoords( [ 0, 0 ] );
    var x = O[0], y = O[1];
    var lines = [], tri = [];
    var arrowSize = 20;
    var tickSize = 6;
    var tmp = [], ticks = [];
    var d, t, i;
    if ( x >= 0 && x <= this.w ) {
        lines = lines.concat(
            [ x, arrowSize/2,
              x, this.h ] );
        d = this.xmax - this.xmin;
        d = Math.floor( Math.log(d/3) / Math.LN10 );
        d = Math.pow( 10, d );
        for ( t=Math.ceil( this.xmin / d );
              t<=Math.floor( this.xmax / d );
              t++ ) {
            tmp.push(t*d);
            tmp.push(0);
        }
        tmp = this.toWinCoords(tmp);
        for ( i=tmp.length-2; i>=0; i-=2 ) {
            ticks.push( tmp[i] );
            ticks.push( tmp[i+1] - tickSize );
            ticks.push( tmp[i] );
            ticks.push( tmp[i+1] + tickSize );
        }
        tri = tri.concat(
            [ x, 0,
              x - arrowSize/2, arrowSize,
              x, arrowSize/2,
              x, 0,
              x + arrowSize/2, arrowSize,
              x, arrowSize/2 ] );
    }
    if ( y >= 0 && y <= this.h ) {
        lines = lines.concat(
            [ 0, y,
              this.w - arrowSize/2, y ] );
        d = this.ymax - this.ymin;
        d = Math.floor( Math.log(d/3) / Math.LN10 );
        d = Math.pow( 10, d );
        tmp.length = 0;
        for ( t = Math.ceil( this.ymin / d );
              t <= Math.floor( this.ymax / d );
              t++ ) {
            tmp.push(0);
            tmp.push(t*d);
        }
        tmp = this.toWinCoords(tmp);
        i = tmp.length-2;
        if ( tmp[i+1] < 1.5 * arrowSize ) {
            i -= 2;
        }
        for ( ; i>=0; i-=2 ) {
            ticks.push( tmp[i]-tickSize );
            ticks.push( tmp[i+1] );
            ticks.push( tmp[i]+tickSize );
            ticks.push( tmp[i+1] );
        }
        tri = tri.concat(
            [ this.w, y,
              this.w - arrowSize, y - arrowSize/2,
              this.w - arrowSize/2, y,
              this.w, y,
              this.w - arrowSize, y + arrowSize/2,
              this.w - arrowSize/2, y ] );
    }
    if ( lines.length > 0 ) {
        this.axes =
            { type:this.g.LINES, array:lines, width:3, color:'#000000' };
    }
    if ( ticks.length > 0 ) {
        this.ticks =
            { type:this.g.LINES, array:ticks, width:1 };
    }
    if ( tri.length > 0 ) {
        this.arrows =
            { type:this.g.TRIANGLES, array:tri };
    }
};

function Bitmap(plot) {
    this.plot = plot;
    try {
        this.ctx = plot.canvas.getContext("2d");
    }
    catch(err) {
        console.error( "Can't init Canvas : " + err.message );
        this.ctx = null;
        return;
    }
    this.primitives = [];
}

Bitmap.prototype.LINES = 0;
Bitmap.prototype.TRIANGLES = 1;
Bitmap.prototype.LINE_STRIPS = 2;

Bitmap.prototype.setFrame = function( left, bottom, right, top ) {
//    var hs = this.plot.w / ( right - left );
//    var vs = this.plot.h / ( top - bottom )
    this.xmin = left;
    this.xmax = right;
    this.ymin = bottom;
    this.ymax = top;
//    this.ctx.setTransform( hs, 0, 0, -vs, -left*hs, bottom*vs + this.plot.h );
};

Bitmap.prototype.store = function(p) {
    var A = p.vertices;
    var l, j;
    var b = new Path2D();
    var xmin, ymax, xs, ys;
    if ( p.frameCoords ) {
        xmin = this.plot.xmin;
        ymax = this.plot.ymax;
        xs = this.plot.w / ( this.plot.xmax - xmin );
        ys = this.plot.h / ( ymax - this.plot.ymin );
    }
    else {
        xmin = 0;
        ymax = this.plot.h;
        xs = 1;
        ys = 1;
    }
    
    switch( p.type ) {
    case this.LINES:
        l = A.length;
            for ( j=0; j+3<l; j+=4 ) {
                b.moveTo( ( A[j] - xmin ) * xs, ( ymax - A[j+1] ) * ys );
                b.lineTo( ( A[j+2] - xmin ) * xs, (ymax - A[j+3] ) * ys );
            }
        break;
    case this.TRIANGLES:
        l = A.length;
        for ( j=0; j+5<l; j+=6 ) {
            b.moveTo( ( A[j] - xmin ) * xs, ( ymax - A[j+1] ) * ys );
            b.lineTo( ( A[j+2] - xmin ) * xs, ( ymax - A[j+3] ) * ys );
            b.lineTo( ( A[j+4] - xmin ) * xs, ( ymax - A[j+5] ) * ys );
        }
        break;
    case this.LINE_STRIPS:
        l = A.length;
        for ( j=0; j+1<l; j+=2 ) {
            if ( isNaN( A[j+1] ) ) {
                j += 2;
                b.moveTo( ( A[j] - xmin ) * xs, ( ymax - A[j+1] ) * ys );
            }
            else {
                b.lineTo( ( A[j] - xmin ) * xs, ( ymax - A[j+1] ) * ys );
            }
        }
    }
    p.path = b;
    this.primitives.push(p);
    return this.primitives.length - 1;
};

Bitmap.prototype.remove = function(n) {
    this.primitives.splice( n, 1 );
};

Bitmap.prototype.refresh = function() {
    var i, l = this.primitives.length;
    this.ctx.clearRect( 0, 0, this.plot.canvas.width,
                        this.plot.canvas.height ); 
    this.ctx.save();
    for ( i=0; i<l; i++ ) {
        this.draw(i);
    }
    this.ctx.restore();
};

Bitmap.prototype.draw = function(n) {
    var p = this.primitives[n];
    
    if ( p.width ) {
        this.ctx.lineWidth = p.width;
    }
    if ( p.color ) {
        this.ctx.strokeStyle = 'rgb(' + p.color[0] + ',' +
            p.color[1] + ',' + p.color[2] + ')';
    }
    this.ctx.stroke( p.path );
};
*/
