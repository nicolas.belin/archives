"use strict";

function WebGL( canvas ) {
    var gl;

    this.canvas = canvas;
    this.verbose = true;

    try {
        this.gl = canvas.getContext('webgl') ||
            canvas.getContext('experimental-webgl');
    }
    catch(error) {
        if (error) {
            console.log(error);
        }
    }
    if ( ! this.gl ) {
        console.log("Can't get a WebGL context...");
        return;
    }
    gl = this.gl;
    
    if ( this.verbose ) {
        console.log( '* Vendor : ' +
                     gl.getParameter( gl.VENDOR ) );
        console.log( '* Renderer : ' +
                     gl.getParameter( gl.RENDERER ) );
        console.log( '* Version : ' +
                     gl.getParameter( gl.VERSION ) );
        console.log( '* Shading language version : ' +
                     gl.getParameter( gl.SHADING_LANGUAGE_VERSION ));
    }
    //gl.enable(gl.DEPTH_TEST);         
    //gl.depthFunc(gl.LEQUAL);
    //gl.cullFace( gl.BACK );
    //gl.enable( gl.CULL_FACE );
    //    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
}

WebGL.prototype.ortho = function( xmin, xmax, ymin, ymax,
                                  zmin, zmax, M ) {
    // orthogonal projection of the cube :
    // [xmin;xmax] x [ymin;ymax] x [zmin;zmax]
    // on the z=zmax plane
    if ( ! M ) {
        M = [];
    }
    // Matrix by columns :
    M[0] = 2.0 / ( xmax - xmin );
    M[1] = M[2] = M[3] = M[4] = 0.0;
    M[5] = 2.0 / ( ymax - ymin );
    M[6] = M[7] = M[8] = M[9] = 0.0;
    M[10] = 2.0 / ( zmax - zmin );
    M[11] = 0.0;
    M[12] = - ( xmax + xmin ) / ( xmax - xmin );
    M[13] = - ( ymax + ymin ) / ( ymax - ymin );
    M[14] = - ( zmax + zmin ) / ( zmax - zmin );
    M[15] = 1.0;
    return M;
};

WebGL.prototype.fustrum = function( a, p, e, n, f, M ) {
    /* a : vertical opening angle (deg)
     * p : w/h ratio
     * e : eye's z
     * n : near clipping plane (z)
     * f : far clipping plane (z)
     * M : matrix container
     * Looking along the Oz axis
     */
    var t, r;
    t = ( e-n ) * Math.sin( Math.PI/180*(a/2.0) );
    r = t * p;
    n -= e;
    f -= e;
    if ( ! M ) {
        M = [];
    }
    // Matrix by columns :
    M[0] = 1.0 / r;
    M[1] = M[2] = M[3] = M[4] = 0.0;
    M[5] = 1.0 / t;
    M[6] = M[7] = M[8] = M[9] = 0.0;
    M[10] = -( f + n ) / ( n * (f-n) );
    M[11] = 1.0 / n;
    M[12] = M[13] = 0;
    M[14] = ( f*(n+e) + n*(f+e) ) / ( n * (f-n ) );
    M[15] = -e / n;
    return M;
};

WebGL.prototype.camera = function( r, theta, phi, M ) {
    // R( Oy, phi ) -> R( Ox, theta ) -> T( 0, 0, r )
    theta *= Math.PI / 180;
    phi *= Math.PI / 180;
    var cos_phi = Math.cos( phi );
    var cos_theta = Math.cos( theta );
    var sin_phi = Math.sin( phi );
    var sin_theta = Math.sin( theta );
    if ( ! M ) {
        M = [];
    }
    // Matrix by columns :
    M[0] = cos_phi;
    M[1] = cos_theta * sin_phi;
    M[2] = sin_theta * sin_phi;
    M[3] = 0.0;
    M[4] = -sin_phi;
    M[5] = cos_theta * cos_phi;
    M[6] = sin_theta * cos_phi;
    M[7] = M[8] = 0.0;
    M[9] = -sin_theta;
    M[10] = cos_theta;
    M[11] = M[12] = M[13] = 0.0;
    M[14] = r;
    M[15] = 1.0;
    return M;
}

WebGL.prototype.buffer = function( data, type, dim, usage ) {
    // data : array or Float32Array for attributes, or a number of vertices
    // usage : gl.STATIC_DRAW || gl.STREAM_DRAW || gl.DYNAMIC_DRAW
    
    var gl = this.gl;
    var r = {};
    
    r.vbo = gl.createBuffer();
    r.type = type;
    switch(type) {
    case 'indexes':
        gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, r.vbo );
        gl.bufferData( gl.ELEMENT_ARRAY_BUFFER,
                       new Uint16Array(data), usage );
        dim = 1;
        break;
    case 'attributes':
        if ( typeof data === 'number' ) {
            r.max = data;
            r.count = 0;
            data *= dim * Float32Array.BYTES_PER_ELEMENT;
        }
        else {
            r.max = data.length/dim;
            r.count  = r.max;
        }
        gl.bindBuffer( gl.ARRAY_BUFFER, r.vbo );
        gl.bufferData( gl.ARRAY_BUFFER, data, usage );
        break;
    default:
        console.log( 'buffer() : unknown type' );
    }
    r.dim = dim;
    return r;
};

WebGL.prototype.updateBuffer = function( buffer, offset, data ) {
    // offset : a number of vertices
    var gl = this.gl;

    if ( offset + data.length/buffer.dim > buffer.max ) {
        console.log( 'updateBuffer() : buffer overflow' ); 
        return;
    }
    switch( buffer.type ) {
    case 'indexes':
        offset *= buffer.dim *
            Float32Array.BYTES_PER_ELEMENT;
        gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, buffer.vbo );
        gl.bufferSubData(
            gl.ELEMENT_ARRAY_BUFFER, offset, data );
        break;
    case 'attributes':
        offset *= buffer.dim *
            Uint16Array.BYTES_PER_ELEMENT;
        gl.bindBuffer( gl.ARRAY_BUFFER, buffer.vbo );
        gl.bufferSubData(
            gl.ARRAY_BUFFER, offset, data );
        break;
    default:    
        console.log( 'updateBuffer() : unknown buffer type' );
    }
};

WebGL.prototype.resize = function() {
    var gl = this.gl;
    var canvas = this.canvas;
    
    var w = canvas.clientWidth;
    var h = canvas.clientHeight;
    
    //console.log( 'Resize ' + w + ' x ' + h );
    if ( this.w !== w || this.h !== h ) {
        canvas.width = this.w = w;
        canvas.height = this.h = h;
        gl.viewport(0, 0, w, h );
        if ( this.verbose ) {
            console.log(
                'Viewport : ' + this.w + " x " + this.h );
        }
    }
};

function Shader( name, wgl ) {
    this.name = name;
    this.wgl = wgl;
    this.gl = wgl.gl; 
    this.program = null;
}

Shader.prototype.make = function( vertCode, fragCode ) {
    var program, vShader, fShader, status;
    var gl = this.gl;
    
    vShader = this.create( vertCode, gl.VERTEX_SHADER );
    fShader = this.create( fragCode, gl.FRAGMENT_SHADER );
    if ( vShader === null || fShader === null ) {
        return false;
    }
    program = gl.createProgram();
    gl.attachShader( program, vShader );
    gl.attachShader( program, fShader );
    gl.linkProgram(program);
    status = gl.getProgramParameter( program, gl.LINK_STATUS );
    if ( ! status ) {
        if ( wgl.verbose ) {
            console.log( "Can't link shader " + name + ' :' );
            console.log( gl.getProgramInfoLog(program) );
        }
        return false;
    }
    this.program = program;
    return true;
};

Shader.prototype.create = function( code, type ) {
    var gl = this.gl;
    var shader, status, s
    
    switch (type) {
    case gl.VERTEX_SHADER :
        s = 'vertex';
        break;
    case gl.FRAGMENT_SHADER :
        s = 'fragment';
        break;
    default :
        console.log( "Error : unknown shader type." );
        return null;
    }
    shader = gl.createShader( type );
    gl.shaderSource( shader, code );
    gl.compileShader(shader);
    status = gl.getShaderParameter( shader, gl.COMPILE_STATUS );
    if ( ! status ){
        console.log( "Can't compile " + this.name + ' ' + s +
                     ' shader :');
        console.log( gl.getShaderInfoLog(shader) );
        return null;
    }
    return shader;
};

Shader.prototype.use = function() {
    if ( this.wgl.shaderInUse !== this ) {
        this.gl.useProgram( this.program );
    }
    this.wgl.shaderInUse = this;
};

function Chain(shader) {
    this.shader = shader;
    this.wgl = shader.wgl;
    this.gl = shader.wgl.gl;
    this.attributes = [];
    this.uniforms = [];
    this.type = null;
    this.parts = null;
};

Chain.prototype.addUniform = function( name, target ) {
    var gl = this.gl;
    var loc = gl.getUniformLocation(
        this.shader.program, name );

    switch( target.length ) {
    case 16:
        this.uniforms.push(
            function() {
                gl.uniformMatrix4fv( loc, gl.FALSE, target );
            } );
        break;
    case 4:
        this.uniforms.push(
            function() {
                gl.uniform4fv( loc, target );
            } );
        break;
    default :
        console.log( 'addUniform() : uniform type not supported' );
    }
};

Chain.prototype.addAttribute = function( name, buffer ) {
    var gl = this.gl;
    var p = gl.getAttribLocation(
        this.shader.program, name );
    this.count  = buffer.count;
    this.attributes.push( function() {
        gl.enableVertexAttribArray(p);
        gl.bindBuffer( gl.ARRAY_BUFFER, buffer.vbo );
        gl.vertexAttribPointer(
            p, buffer.dim, gl.FLOAT, gl.FALSE, 0, 0 );
    } );
};

Chain.prototype.addIndexes = function(buffer) {
    this.indexes = buffer;
};

Chain.prototype.setPrimitiveType = function(type) {
    var gl = this.gl;

    switch(type) {
    case 'triangles':
        this.mode = gl.TRIANGLES;
        break;
    case 'lines':
        this.mode = gl.LINES;
        break;
    case 'line_strip':
        this.mode = gl.LINE_STRIP;
        break;
    case 'line_loop':
        this.mode = gl.LINE_LOOP;
        break;
    default:
        this.mode = null;
        console.log(
            'getRender() : primitive type not supported' );
    }
};

Chain.prototype.render = function( offset, count ) {
    var gl = this.gl;
    var a;

    if ( offset === undefined ) {
        offset = 0;
    }
    if ( count === undefined ) {
        count = this.count;
    }
    
    this.shader.use();
    for ( a of this.uniforms ) {
        a();
    }
    for ( a of this.attributes ) {
        a();
    }
    if ( this.indexes ) {
        gl.bindBuffer(
            gl.ELEMENT_ARRAY_BUFFER, this.indexes.vbo );
        gl.drawElements(
            this.mode, count, gl.UNSIGNED_SHORT, offset );
    }
    else {
        gl.drawArrays( this.mode, offset, count );
    }
};
