function Hash() {
    var i, m;

    this.cst2one = 1;
    this.cstFact2one = 2;
    this.num2one = 3;

    // a large Carol prime
    m = Math.pow( 2, 12 ) - 1;
    m = m*m - 2;
    this.p = m;
}

Hash.prototype.mod = function(n) {
    var p = this.p;
    var m = n % p;

    if ( m < 0 ) {
        m += p;
    }
    return m;
};

Hash.prototype.div = function( a, b ) {
    var e = worker.euclide( b, this.p );
    
    return this.mult( a, this.mod(e.u) );
};

Hash.prototype.mult = function( a, b ) {
    return this.mod( a * b );
};

Hash.prototype.add = function( a, b ) {
    return this.mod( a + b );
};

Hash.prototype.pow = function( a, b ) {
    var s = 1;
    var q = a;
    var bBin, i;
    
    bBin = b.toString(2).split("");
    for ( i=bBin.length-1; i>=0; i-- ) {
        if ( bBin[i] === "1" ) {
            s = this.mult( s, q );
        }
        q = this.mult( q, q );
    } 
    return s;
};

Hash.prototype.getStringHash = function(s) {
    var l = s.length;
    var i, k;
    
    k = 0;
    for ( i=0; i<l; i++ ) {
        k = this.add( k, this.mod( ( s.charCodeAt(i) + 56 ) * (i+1) * 38295357 ) );
    }
    return k;
};

Hash.prototype.num = function(x) {
    return this.mod(x);
};

Hash.prototype.getRandomHash = function() {
    return Math.round( Math.random() * this.p );
};

Hash.prototype.normSum = function(s) {
    var i, j, max = -Infinity;
    var t, h, l = s.length;
    
    for ( i=0; i<l; i++ ) {
	  h = s[i].get('kH');
	  if ( h > max ) {
		j = i;
		max = h;
	  }
    }
    h = s[j].get('H');
    t = 0;
    for ( i=0; i<l; i++ ) {
	  t = hash.add( t, s[i].get('H') );
    }
    if ( h === max ) {
	  return t;
    }
    return hash.mult( t, hash.div( max, h ) );
};

Hash.prototype.getNormSumIndex = function(s) {
    var i,j, h, max = -Infinity;
    
    for ( i=s.length-1; i>=0; i-- ) {
	  h = s[i].get('kH');
	  if ( h > max ) {
		max = h;
		j = i;
	  }
    }
    return j;
};

Num.prototype.hash = function(i) {
	
    if ( i === hash.cst2one ||
	   i === hash.num2one ) {
        return 1;
    }
    return hash.num( this.get('v') );
};

Symb.prototype.hash = function(k) {
    var a = 125648;
    var b = 9875634;
    var i, l, n;
    
    if ( k === hash.cst2one && this.get('cst') ) {
	  return 1;
    }
    l = this.a.length;
    n = hash.getStringHash( this.id );
    for ( i=0; i<l; i++ ) {
        n = hash.add(
            hash.mult( n, hash.pow( this.a[i].get('H'), a ) ), b );
        a = hash.mult( a, a );
        b = hash.add( b, a );
    }
    return n;
};

Symb.prototype.specials['?'].hash = function() {
    return hash.getRandomHash(); 
};

Add.prototype.hash = function(k) {
    var i, s, l;

    if ( this.get('cst') && k === hash.cst2one ) {
	  return 1;
    }
    if ( k === hash.cstFact2one ) {
	  return hash.normSum(this.a);
    }
    l = this.a.length;
    s = this.a[0].get('H');
    for ( i=1; i<l; i++ ) {
        s = hash.add( s, this.a[i].get('H') );
    }
    return s;
};

Mult.prototype.hash = function(n) {
    var i, s, l;
    if ( n === hash.cst2one && this.get('cst') ) {
	  return 1;
    }
    if ( n === hash.cstFact2one ) {
        l = this.a.length;
        s = this.a[0].get('cH');
        for ( i=1; i<l; i++ ) {
            s = hash.mult( s, this.a[i].get('cH') );
        }
        return s;
    }
    if ( n === hash.num2one ) {
        l = this.a.length;
        s = this.a[0].get('nH');
        for ( i=1; i<l; i++ ) {
            s = hash.mult( s, this.a[i].get('nH') );
        }
        return s;
    }
    l = this.a.length;
    s = this.a[0].get('H');
    for ( i=1; i<l; i++ ) {
        s = hash.mult( s, this.a[i].get('H') );
    }
    return s;
};

Pow.prototype.hash = function(i) {
    var eN, r;
    if ( ( i === hash.cst2one || i === hash.cstFact2one ) &&
         this.get('cst') ) {
	  return 1;
    }
    if ( i === hash.num2one && this.a[0].isNum &&
	   this.a[1].isNum && this.a[1].get('v') === -1 ) {
	  return 1;
    }
    eN = this.a[1];
    if ( eN.isNum && eN.get('v') < 0 ) {
	  r = eN.get('H');
	  r = hash.mult( r, hash.num(-1) );
	  r = hash.pow( this.a[0].get('H'), r );
	  return hash.div( hash.num(1), r );
    }
    return hash.pow( this.a[0].get('H'), eN.get('H') );
};

Vect.prototype.hash = function() {
    var a = 845236, b = 569841;
    var i, s, l, r;

    l = this.a.length;
    s = hash.mult( this.a[0].get('H'), a );
    s = hash.add( s, b );
    for ( i=1; i<l; i++ ) {
        a = hash.mult( a, a );
        b = hash.add( b, a );
        r = hash.mult( this.a[i].get('H'), a );
        r = hash.add( r, b );
        s = hash.add( s, r );
    }
    return s;
};

Eq.prototype.hash = function() {
    this.a[0].get('H');
    this.a[1].get('H');
    return 1;
};
