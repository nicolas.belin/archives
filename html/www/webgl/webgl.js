"use strict";

function WebGL( canvas ) {
    this.canvas = canvas;
    this.verbose = true;
    this.init();
}

WebGL.prototype.init = function() {
    var gl;

    try {
        this.gl = this.canvas.getContext("webgl") ||
            this.canvas.getContext("experimental-webgl");
    }
    catch(error) {
        if (error) {
            console.log(error);
        }
    }
    gl = this.gl;
    if ( ! gl ) {
        return;
    }
    if ( this.verbose ) {
        console.log( '* Vendor : ' +
                     gl.getParameter( gl.VENDOR ) );
        console.log( '* Renderer : ' +
                     gl.getParameter( gl.RENDERER ) );
        console.log( '* Version : ' +
                     gl.getParameter( gl.VERSION ) );
        console.log( '* Shading language version : ' +
                     gl.getParameter( gl.SHADING_LANGUAGE_VERSION ));
    }
    //gl.enable(gl.DEPTH_TEST);         
    //gl.depthFunc(gl.LEQUAL);
    gl.cullFace( gl.BACK );
    gl.enable( gl.CULL_FACE );
    this.resize();
    //    gl.viewport(0, 0, gl.drawingBufferWidth,
//                gl.drawingBufferHeight );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    //    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
    gl.clear( gl.COLOR_BUFFER_BIT );
//    this.w = gl.drawingBufferWidth;
//    this.h = gl.drawingBufferHeight;
    if ( this.verbose ) {
        console.log( 'Viewport : ' + this.w + " x " + this.h );
    }
};

WebGL.prototype.fustrum = function( a, p, e, n, f, M ) {
    /* a : angle d'ouverture vertical
     * p : proportion largeur/hauteur
     * e : côte de l'oeil
     * n : near clipping plane (côte)
     * f : far clipping plane (côte)
     * M : matrix container
     * Looking along the Oz axis, toward negative numbers
     */
    var t, r;
    t = ( e-n ) * Math.sin( Math.PI/180*(a/2.0) );
    r = t * p;
    n -= e;
    f -= e;
    if ( ! M ) {
        M = [];
    }
    // Matrix by columns :
    M[0] = 1.0 / r;
    M[1] = M[2] = M[3] = M[4] = 0.0;
    M[5] = 1.0 / t;
    M[6] = M[7] = M[8] = M[9] = 0.0;
    M[10] = -( f + n ) / ( n * (f-n) );
    M[11] = 1.0 / n;
    M[12] = M[13] = 0;
    M[14] = ( f*(n+e) + n*(f+e) ) / ( n * (f-n ) );
    M[15] = -e / n;
    return M;
};

WebGL.prototype.camera = function( r, theta, phi, M ) {
    // R( Oy, phi ) -> R( Ox, theta ) -> T( 0, 0, r )
    theta *= Math.PI / 180;
    phi *= Math.PI / 180;
    var cos_phi = Math.cos( phi );
    var cos_theta = Math.cos( theta );
    var sin_phi = Math.sin( phi );
    var sin_theta = Math.sin( theta );
    if ( ! M ) {
        M = [];
    }
    // Matrix by columns :
    M[0] = cos_phi;
    M[1] = cos_theta * sin_phi;
    M[2] = sin_theta * sin_phi;
    M[3] = 0.0;
    M[4] = -sin_phi;
    M[5] = cos_theta * cos_phi;
    M[6] = sin_theta * cos_phi;
    M[7] = M[8] = 0.0;
    M[9] = -sin_theta;
    M[10] = cos_theta;
    M[11] = M[12] = M[13] = 0.0;
    M[14] = r;
    M[15] = 1.0;
    return M;
}

WebGL.prototype.buffer = function( data, type, dim ) {
    var gl = this.gl;
    var vbo = gl.createBuffer();

    switch(type) {
    case 'indexes':
        gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, vbo );
        gl.bufferData( gl.ELEMENT_ARRAY_BUFFER,
                       new Uint16Array(data), gl.STATIC_DRAW );
        dim = 1;
        break;
    case 'attributes':
        gl.bindBuffer( gl.ARRAY_BUFFER, vbo );
        gl.bufferData( gl.ARRAY_BUFFER, new Float32Array(data),
                       gl.STATIC_DRAW );
        break;
    default:
        console.log( 'buffer() : unknown type' );
    }
    return { vbo:vbo, nr:data.length/dim, type:type, dim:dim };
};

WebGL.prototype.addShader = function( name, vertCode,
                                      fragCode ) {
    var s = new Shader( name, this, vertCode, fragCode );

    if ( s.program !== null ) {
        return s;
    }
    return null;
};

WebGL.prototype.resize = function() {
    var gl = this.gl;
    var canvas = this.canvas;
    
    var w = canvas.clientWidth;
    var h = canvas.clientHeight;

    //console.log( 'Resize ' + w + ' x ' + h );
    if ( this.w !== w || this.h !== h ) {
        canvas.width = this.w = w;
        canvas.height = this.h = h;
        gl.viewport(0, 0, w, h );
    }
};

function Shader( name, wgl, vertCode, fragCode ) {

    var program, vShader, fShader, status, gl;
    
    this.name = name;
    this.wgl = wgl;
    this.gl = wgl.gl; 
    this.program = null;
    this.attributes = {};
    this.uniforms = {};
    gl = this.gl;
    
    vShader = this.create( vertCode, gl.VERTEX_SHADER );
    fShader = this.create( fragCode, gl.FRAGMENT_SHADER );
    if ( vShader === null || fShader === null ) {
        return;
    }
    program = gl.createProgram();
    gl.attachShader( program, vShader );
    gl.attachShader( program, fShader );
    gl.linkProgram(program);
    status = gl.getProgramParameter( program, gl.LINK_STATUS );
    if ( ! status ) {
        if ( wgl.verbose ) {
            console.log( "Can't link shader " + name + ' :' );
            console.log( gl.getProgramInfoLog(program) );
        }
        return;
    }
    else {
        this.program = program;
    }
}

Shader.prototype.create = function( code, type ) {
    var gl = this.gl;
    var shader, status, s
    
    switch (type) {
    case gl.VERTEX_SHADER :
        s = 'vertex';
        break;
    case gl.FRAGMENT_SHADER :
        s = 'fragment';
        break;
    default :
        console.log( "Error : unknown shader type." );
        return null;
    }
    shader = gl.createShader( type );
    gl.shaderSource( shader, code );
    gl.compileShader(shader);
    status = gl.getShaderParameter( shader, gl.COMPILE_STATUS );
    if ( ! status ){
        console.log( "Can't compile " + name + ' ' + s +
                     ' shader :');
        console.log( gl.getShaderInfoLog(shader) );
        return null;
    }
    return shader;
};

Shader.prototype.addUniform = function( name, target ) {
    var gl = this.gl;
    var loc = gl.getUniformLocation( this.program, name );

    switch( target.length ) {
    case 16:
        this.uniforms[name] = function() {
            gl.uniformMatrix4fv( loc, gl.FALSE, target );
        };
        break;
    default :
        console.log( 'addUniform() : uniform type not supported' );
    }
};

Shader.prototype.addAttribute = function( name, buffer ) {
    var gl = this.gl;
    var p = gl.getAttribLocation( this.program, name );
    this.attributes[name] = function() {
        gl.enableVertexAttribArray(p);
        gl.bindBuffer( gl.ARRAY_BUFFER, buffer.vbo );
        gl.vertexAttribPointer(
            p, buffer.dim, gl.FLOAT, gl.FALSE, 0, 0 );
    };
};

Shader.prototype.use = function() {
    if ( this.wgl.shaderInUse !== this ) {
        this.gl.useProgram( this.program );
    }
    this.wgl.shaderInUse = this;
};

Shader.prototype.getRender =
    function( type, ib ) {
        var self = this;
        var gl = this.gl;
        var primitive;

        switch(type) {
        case 'triangles':
            primitive = gl.TRIANGLES;
            break;
        default:
            console.log(
                'getRender() : primitive type not supported' );
            return;
        }
        return function() {
            var name;

            self.use();
            for ( name in self.uniforms ) {
                if ( self.uniforms.hasOwnProperty(name) ) {
                    self.uniforms[name]();
                }
            }
            for ( name in self.attributes ) {
                if ( self.attributes.hasOwnProperty(name) ) {
                    self.attributes[name]();
                }
            }
            if ( ib ) {
                gl.bindBuffer(
                    gl.ELEMENT_ARRAY_BUFFER, ib.vbo );
                gl.drawElements(
                    primitive, ib.nr, gl.UNSIGNED_SHORT, 0 );
            }
            else {
                gl.drawArrays( primitive, 0, a.buffer.nr );
            }
        };
    };

