"use strict";

function Vertex( x, y, z ) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.lk = [];
}

var sphere = {};

sphere.makeHemi = function( layerNr ) {

    function mod5i( m, i ) {
        if ( m < 0 )
            m += 5*i;
        return m % ( 5 * i );
    }

    // construit une demi-sphere à layerNr couche
    var i, j, k, m;
    var vtxu = [];
    
    // point au sommet : centre du pentagone
    vtxu[0] = [];
    vtxu[0][0] = new Vertex( 0.0, 0.0, 1.0 );
  
    // les autres points : hexagones

    // coordonnées
    for ( i=1; i<layerNr; i++ ){
        vtxu[i] = [];
        for ( j=0; j<5; j++ ) {
            for ( k=0; k<i; k++ ){
                m = i*j + k;
                vtxu[i][m] = new Vertex(
                    Math.cos( Math.PI/2 * ( 1 - i/layerNr ) ) *
                        Math.cos( 2 * Math.PI * m / (5*i) ),
                    Math.cos( Math.PI/2 * ( 1 - i/layerNr ) ) *
                        Math.sin( 2 * Math.PI * m / (5*i) ),
                    Math.sin( Math.PI/2 * ( 1 - i/layerNr ) ) );
            }
        }
    }
    
    // arrêtes
    for ( j=0; j<5; j++ ) {
        vtxu[0][0].lk[j] = vtxu[1][j];
    }

    for ( i=1; i<layerNr; i++ ){
        for ( j=0; j<5; j++ ){
            for ( k=0; k<i; k++ ){
                m = i*j + k;
                vtxu[i][m].lk[0] = vtxu[i][ mod5i( m+1, i ) ];
                if ( k === 0 ) {
                    vtxu[i][m].lk[1] = vtxu[ i-1 ][ (i-1)*j ];
                    vtxu[i][m].lk[2] = vtxu[i][ mod5i( m-1, i ) ];
                    if ( i !== layerNr-1 ){ 
                        vtxu[i][m].lk[3] = vtxu[i+1][ mod5i( (i+1)*j - 1, i+1 ) ];
                        vtxu[i][m].lk[4] = vtxu[i+1][ mod5i( (i+1)*j, i+1 ) ];
                        vtxu[i][m].lk[5] = vtxu[i+1][ mod5i( (i+1)*j + 1, i+1 ) ];
                    }
                }
                else{
                    vtxu[i][m].lk[1] = vtxu[i-1][ mod5i( (i-1)*j + k, i-1 ) ];
                    vtxu[i][m].lk[2] = vtxu[i-1][ mod5i( (i-1)*j + k-1, i-1 ) ];
                    vtxu[i][m].lk[3] = vtxu[i][ mod5i( m-1, i ) ];
                    if ( i !== layerNr-1 ){ 
                        vtxu[i][m].lk[4] = vtxu[i+1][ mod5i( (i+1)*j + k, i+1 ) ];
                        vtxu[i][m].lk[5] = vtxu[i+1][ mod5i( (i+1)*j + k+1, i+1 ) ];
                    }
                }
            }
        }
    }
    return vtxu;
};

sphere.make = function( layerNr ) {
    
    function mod5i( m, i ) {
        if ( m < 0 )
            m += 5*i;
        return m % ( 5 * i );
    }

    var up, down, equa;
    var vtces = [];
    var i, j, k, t, l;

    // **** demi-sphère supérieure
    up = sphere.makeHemi(layerNr);

    // **** demi-sphere inférieure
    down = sphere.makeHemi(layerNr);
    // symétrie par rapport au plan équatorial
    for ( i=down.length-1; i>=0; i-- ) {
        for ( j=down[i].length-1; j>=0; j-- ) {
            down[i][j].z *= -1; 
        }
    }
    // la symétrie précédente à inverser l'orientation des polygônes (sens direct)
    for( i=down.length-1; i>=0; i-- ){
        for( k=down[i].length-1; k>=0; k-- ){
            l = down[i][k].lk.length;
            for( j=0; j<Math.floor(l/2); j++ ) {
                t = down[i][k].lk[j];
                down[i][k].lk[j] = down[i][k].lk[l-j-1];
                down[i][k].lk[l-j-1] = t;
            }
        }
    }

    // **** équateur
    equa = [];
    l = layerNr;
    for ( j=0; j<5; j++ ) {
        for ( k=0; k<l; k++ ){
            i = l*j + k;
            equa[i] = new Vertex(
                Math.cos( 2 * Math.PI * i / (5*l) ),
                Math.sin( 2 * Math.PI * i / (5*l) ),
                0 );
        }
    }
    
    // collage des deux demi-sphères 
    for ( j=0; j<5; j++ ) {
        for ( k=0; k<l; k++ ) {
            i = l*j + k;
            equa[i].lk[0] = equa[ mod5i( i+1, l ) ];
            if ( k === 0 ) {
                equa[i].lk[1] = up[l-1][ (l-1)*j ];
                equa[i].lk[2] = equa[ mod5i( i-1, l ) ];
                equa[i].lk[3] = down[l-1][ (l-1)*j ];
            }
            else{
                equa[i].lk[1] = up[l-1][ mod5i( (l-1)*j + k, l-1 ) ];
                equa[i].lk[2] = up[l-1][ mod5i( (l-1)*j + k-1, l-1 ) ];
                equa[i].lk[3] = equa[ mod5i( i-1, l ) ];
                equa[i].lk[4] = down[l-1][ mod5i( (l-1)*j + k-1, l-1 ) ];
                equa[i].lk[5] = down[l-1][ mod5i( (l-1)*j + k, l-1 ) ];
            }
        }
    }
    
    l = layerNr-1;
    for ( j=0; j<5; j++ ) {
        for ( k=0; k<l; k++ ){
            i = l*j + k;
            if ( k === 0 ) {
                up[l][i].lk[3] = equa[ mod5i( (l+1)*j - 1, l+1 ) ];
                down[l][i].lk[3] = equa[ mod5i( (l+1)*j - 1, l+1 ) ];
                up[l][i].lk[4] = equa[ mod5i( (l+1)*j, l+1 ) ];
                down[l][i].lk[4] = equa[ mod5i( (l+1)*j, l+1 ) ];
                up[l][i].lk[5] = equa[ mod5i( (l+1)*j + 1, l+1 ) ];
                down[l][i].lk[5] = equa[ mod5i( (l+1)*j + 1, l+1 ) ];
            }
            else{
                up[l][i].lk[4] = equa[ mod5i( (l+1)*j + k, l+1 ) ];
                down[l][i].lk[4] = equa[ mod5i( (l+1)*j + k, l+1 ) ];
                up[l][i].lk[5] = equa[ mod5i( (l+1)*j + k+1, l+1 ) ];
                down[l][i].lk[5] = equa[ mod5i( (l+1)*j + k+1, l+1 ) ];
            }
        }
    }
    
    l = up.length;
    for ( i=0; i<l; i++ ) {
        k = up[i].length;
        for ( j=0; j<k; j++ ) {
            vtces.push( up[i][j] );
        }
    }
    l = equa.length;
    for ( i=0; i<l; i++ ) {
        vtces.push( equa[i] );
    }
    l = down.length;
    for ( i=0; i<l; i++ ) {
        k = down[i].length;
        for ( j=0; j<k; j++ ) {
            vtces.push( down[i][j] );
        }
    }
    
    l = vtces.length;
    for ( i=0; i<l; i++ ) {
        vtces[i].index = i;
    }

    return vtces;
};

sphere.glide = function( M, f ) {
    
    var x, y, z, fxyz, dfx, dfy, dfz, sq, l, dx, dy, dz;
    var dl = 0.001;
    
    x = M.x;
    y = M.y;
    z = M.z;
    do {
        fxyz = f.p( x, y, z );
        dfx = f.x( x, y, z );
        dfy = f.y( x, y, z );
        dfz = f.z( x, y, z );
        sq = dfx*dfx + dfy*dfy + dfz*dfz;
        l = Math.sqrt(sq);
        dx = -fxyz * dfx * dl / sq;
        dy = -fxyz * dfy * dl / sq;
        dz = -fxyz * dfz * dl / sq;
        x += dx;
        y += dy;
        z += dz;
    }
    while ( Math.abs(fxyz) / l > 0.05 );
    M.nx = dfx/l;
    M.ny = dfy/l;
    M.nz = dfz/l;
    M.x = x+dx;
    M.y = y+dy;
    M.z = z+dz;
};
