from liste_7776_mots import LISTE_MOTS

print("Type de la liste :",type(LISTE_MOTS))

print("Longueur de la liste :", len(LISTE_MOTS))

print("Tous des chaines de caractere :", all(isinstance(s, str) for s in LISTE_MOTS))

print("Nombre de mots differents :", len(set(LISTE_MOTS)))

def test_mot(mot):
    return all(ord('a') <= ord(c) <= ord('z') or ord('A') <= ord(c) <= ord('Z') for c in mot)
        
print("Les mots de contiennent que des lettres :", all(test_mot(s) for s in LISTE_MOTS))

print_mot = lambda i: print(i, ':', LISTE_MOTS[i])

print_mot(0)
print_mot(len(LISTE_MOTS)-1)
print_mot(2094)