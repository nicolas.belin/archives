
def insert(word, char, pos):
    return word[:pos] + char + word[pos:]

def permutations(s):
    if len(s) > 1:
        c = s[len(s)-1]
        sp = s[:-1]
        permprev = permutations(sp)
        liste = []
        n = len(sp) + 1
        for w in permprev:
            for i in range(n):
                liste.append(insert(w, c, i))
        return liste
    elif len(s) == 1:
        return [s]
            
l = permutations('nico')

print(l)




